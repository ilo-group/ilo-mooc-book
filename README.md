## ILO Book

This is the static HTML producing machine for the Book of the MOOC of the Information Literacy Online project.

### About

This work-in-progress ports [ILO-content](https://gitlab.educs-hosting.net/ilo-team/ilo-content/)
and [ILO-videos](https://gitlab.educs-hosting.net/ilo-team/ilo-videos/) to a static web site.

### Credits

Eleventy skeleton based on Pietro Passerlli's [11ty-book-with-navigation](https://github.com/pietrop/11ty-auto-navigation-book-template).
Runs the [11ty](https://www.11ty.dev/) static site generator.

### Running

- `npm run import`: copy from the directory of ilo-content
  - `npm run dev`: Observe debug tr
- `npm run build`: builds into `_site`

Deploy with `rsync` to your pleasure.



### TO-DO

- navigation properly ordered and filtering by language (11ty code, Paul)
- multilingual declinations of chapters' home pages (Paul)
- quizes should show empty first and auto-correct or at least hide hints (Paul)
- make 11ty-like the content of each of the content pages including pictures (see [example](/1_Orienting/1.1_Intro_cat/)) (Paul)
- content of home page (Stefan?)
- responsiveness (Eliott)
- layout of home page (Eliott)
- language switcher (Paul)
- content of each chapter's home (Stefan?)
- convert content files with boilerplate (mostly markdown, Paul)