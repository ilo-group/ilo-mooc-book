---
title: Information use, the right and fair way
permalink: /5_Information_use/
eleventyNavigation:
    order: 5
---

## MOOC Module: Information use: the right and fair way

 
In this module you will gain an understanding of ethical issues both in decision making and gathering of information, as well applying ethical principles in the process of creating and sharing of information.

