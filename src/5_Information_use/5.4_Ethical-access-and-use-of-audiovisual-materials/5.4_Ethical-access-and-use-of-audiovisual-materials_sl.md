             @page { size: 21cm 29.7cm; margin-left: 3cm; margin-right: 3cm; margin-top: 2.5cm; margin-bottom: 2.5cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } a:link { color: #0563c1; text-decoration: underline } a:visited { color: #800000; so-language: zxx; text-decoration: underline } 

**Lekcija 5.4 – Etični pristop in uporaba avdiovizualnega gradiva**

**Uvod**

V tej lekciji se bomo ukvarjali z uporabo slik in vizualnih gradiv ter z etičnimi, pravnimi, družbenimi in gospodarskimi vprašanji, ki so povezana z njo.

Učni cilji

*   Spoznavanje konceptov, ki se nanašajo na intelektualno lastnino in avtorske pravice ter pošteno rabo v povezavi z uporabo slik in vizualnih gradiv.
    
*   Spoznavanje osnovnih licenčnih omejitev, ki določajo uporabo slik.
    
*   Spoznavanje lastnih pravic glede intelektualne lastnine, kadar smo ustvarjalci slik.
    
*   Prepoznavanje vidikov zasebnosti, etike in varnosti, ki se pojavijo pri uporabi, posredovanju in ustvarjanju slik.
    

  
  

**Etični dostop in uporaba avdiovizualnega gradiva**

  
  

(video)

**Lekcija 5.4.1 – Predstavitev organizacije Creative Commons**

V znanstvena dela je dobro vključiti vizualna gradiva, kot so slike, fotografije in celo avdiovizualne posnetke.

Vendar morate vedeti, da ne smete uporabljati vsega, kar je dosegljivo na spletu. Vizualno gradivo je avtorsko zaščiteno, če ni označeno z licenco Creative Commons ali če ni jasno razvidno, da je uporaba dovoljena.

Creative Commons je neprofitna organizacija, ki si prizadeva povečati število kreativnih del z legalnim dostopom do njih. Organizacija ponuja nekaj licenc za avtorske pravice. Veliko ljudi uporablja licence Creative Commons, saj jim to omogoča lažje izmenjevanje in preoblikovanje stvaritev, ne da bi jih skrbelo kršenje avtorskih pravic.

Ali so spletni viri (slike, videe, besedila itd.) prosto dostopni, lahko preverite tako, da pogledate, ali je kje licenca Creative Commons.

_\[VIDEO\_5\_4\_1\]_

  
  

_Navodila za oblikovanje. OPOMBE NA SLIDE 2_

_V videoposnetku__\[VIDEO\_5\_4\_1\]_ _boste videli statične slike različnih vrst vizualnih gradiv._ _Primer za iskanje slik s spletnim iskalnikom Google. Slike z licenco Creative Commons. Link k videu o predstavitvi CC:_ [_https://vimeo.com/25684782_](https://vimeo.com/25684782)_. Voice-over (glas pripovedovalca)_

  
  

  
  

**Lekcija 5.4.2 – Iskanje slik z licenco Creative Commons**

Znan vir za iskanje slik je Flickr (spletna stran za izmenjavo fotografij). Na žalost niso vse slike na Flickru dostopne brez omejitev. Slike, ki so označene z “vse pravice pridržane” so avtorsko zaščitene. To pomeni, da potrebujete dovoljenje osebe, ki je fotografijo objavila. Slike z “nekatere pravice pridržane” zaščitijo le nekatere avtorske pravice. V tem primeru vemo, da uporablja uporabnik Flickra licenco Creative Commons – držati se moramo pogojev licence.

Traja lahko zelo dolgo, preden eno za drugo pregledamo vse slike, ki smo jih našli. Pri iskanju vam lahko pomagajo spletni iskalniki za slike, kot sta: Google Slike ali Photos for Class (http://www.photosforclass.com). Slednji je študentom prijazen iskalnik za iskanje slik na Flickru z licenco Creative Commons.

Naložena slika vsebuje pripis avtorstva (oz. fotografa) in opis pogojev licence. Pogoj vseh Creative Commons licenc pa je navedba avtorja. To pomeni, da nikoli ne moremo uporabiti slike, ne da bi bil ustvarjalec vizualnega gradiva naveden. Ko uporabite digitalno sliko, morate v vsakem primeru dodati ime avtorja, ustvarjalca ali proizvajalca. Prav tako morate dodati povezavo na spletno stran, na kateri se slika nahaja.

Včasih preoblikujemo in prilagodimo slike s spleta ali jim dodamo kratko besedilo. To je dovoljeno, če ste navedli avtorja izvirnega dela, če licenca Creative Commons to dovoljuje in če sliko delite z uporabo enake Creative Commons licence.

_\[VIDEO\_5\_4\_2\]_

  
  

_Navodila za oblikovanje. OPMOBE SLIDE 3_

_V videoposnetku \[VIDEO\_5\_4\_2\] vam bomo predstavili iskanje slik na Flickru (in na drugih spletnih straneh) in različne licence. Videli boste, kako navajamo avtorja in kdaj je dovoljeno prilagoditi oz. preoblikovati slike._ _Voice-over (glas pripovedovalca)_

  
  

**Lekcija 5.4.3 – Brezplačne in javno dostopne slike**

Obstajajo spletne strani, ki ponujajo uporabo brezplačnih in javno dostopnih slik. Vizualno gradivo je v tem primeru javno dostopno, ker a) ni zaščiteno z avtorskimi pravicami in je zaradi tega javno dostopno, b) je avtorska pravica potekla ali c) je ustvarjalec dovolil javnosti uporabo svojega dela. Uporaba javno dostopnih slik je brezplačna in neomejena.

Wikimedijina zbirka je brezplačno in javno dostopno spletno skladišče datotek, ki nam omogoča uporabo slik, avdio-datotek in videoposnetkov. Skoraj vse slike in drugi mediji, ki jih najdemo na Wikimedijini zbirki, imajo prosto licenco (večinoma javno dostopne, CC-BY, CC-BY-SA ali GFDL (dovoljenje GNU za rabo proste dokumentacije) in lahko zahtevajo pripis avtorstva.

_\[VIDEO\_5\_4\_3\]_

  
  

_Navodila za oblikovanje. OPOMBE SLIDE_ _4_

_V videoposnetek \[VIDEO\_5\_4\_3\] smo dodali primere za iskanje javno dosegljivih slik._ _Voice-over (glas pripovedovalca)_

**Lekcija 5.4.4 – Tudi vi imate pravice**

Licence Creative Commons dovolijo drugim uporabo vašega dela. Vseeno pa je uporaba vezana na pogoje izbrane licence Creative Commons.

Če želite objaviti datoteke pod licenco Creative Commons, se lahko pozanimate na naslednji spletni strani: _Creative Commons Choose A License page_ (https://creativecommons.org/choose/). Določite pogoje licence s klikom na gumbe.

_\[VIDEO\_5\_4\_4\]_

  
  

_Navodila za oblikovanje. OPOMBE SLIDE 5_

_V videoposnetku \[VIDEO\_5\_4\_3\] boste videli, kako določiti pogoje licenc__._ _Voice-over (glas pripovedovalca)_