             @page { margin-left: 3cm; margin-right: 3cm; margin-top: 2.5cm; margin-bottom: 2.5cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } a:link { color: #0563c1 } 

# **5.2 ¿Qué es el plagio y cómo evitarlo?**

**Introducción**

En esta lección aprenderás acerca de los aspectos legales y éticos del uso de la información.

**Resultados de aprendizaje:**

*  articular y aplicar el comportamiento legal y ético en contextos en línea
*  definir y evitar el plagio
*  ser consciente de tu propia responsabilidad y ser honesto en todos los aspectos del manejo y difusión de la información (por ejemplo, derechos de autor, plagio y cuestiones de propiedad intelectual)
*  aprender sobre las consecuencias y los efectos a largo plazo de un comportamiento poco ético
    



**5.2.1 ¿Qué es el plagio?**

Hay muchas definiciones de plagio, pero aquí tienes una simple pero clara:

\[object_ 5-2-1_img\]
_*«El plagio es la "apropiación ilícita" y el "robo y publicación" del "lenguaje, los pensamientos, las ideas o las expresiones" de otro autor y la representación de los mismos como una obra original propia.»_

>*Fuente: „Plagiarism“. Wikipedia. Last edited on 4 February 2018. [https://en.wikipedia.org/wiki/Plagiarism](https://en.wikipedia.org/wiki/Plagiarism) [Retrieved: Feb. 8th 2018]


Seamos claros sobre lo que es plagio con esta definición aún más diáfana:

\[object_ 5-2-2_img\]
_*«...el plagio es un acto de fraude. Implica tanto robar el trabajo de otra persona como mentir sobre ello después.»_

>*Fuente: “What is plagiarism?” _Plagiaris.org._ Published May 18, 2017 [ttps://www.plagiarism.org/article/what-is-plagiarism](https://www.plagiarism.org/article/what-is-plagiarism) [Retrieved: Feb. 8th 2018]


eberías saber que "Si has usado el trabajo de otra persona sin reconocer tu fuente, has plagiado." (McGarry, 2017).

Es tan fácil copiar y pegar el texto o las imágenes de cualquier otra persona en nuestro trabajo, que la creciente aparición de plagios está causando gran preocupación en la comunidad académica: profesores, estudiantes, investigadores, editores, etc. Para hacer frente a la extensión del plagio, numerosas compañías de software han desarrollado herramientas de detección de plagio. Un ejemplo de este tipo de herramienta es [Turnitin](http://turnitin.com/), que también financia el sitio web [Plagiarism.org](http://www.plagiarism.org/), una buena fuente de información sobre los problemas y soluciones para hacer frente a este fenómeno.

Por favor, ahora mira el siguiente vídeo para darte cuenta de lo que está en juego y cómo encontrar una salida:

*   El plagio es un fenómeno que se considera muy grave en las universidades de todo el mundo, pero es un fenómeno que en muchos casos se produce por ignorancia.
*   Puede tener consecuencias más o menos graves, pero en la mayoría de los casos no se puede invocar la ignorancia para evitar estas consecuencias.
*   Las bibliotecas publican materiales de formación y pueden ayudarte junto con tus tutores académicos. Pregunta siempre que tengas dudas.
    

\[object_ 5-2-3_youtube-vid\] : USU Libraries. _What is plagiarism?_ Published on 6 Mar 2017. 1,22 min. Available from Youtube: <https://youtu.be/OFLFaSjNr2I> \[Retrieved: Feb. 8th 2018\].

\[object\_5-2-4\_img\]: source of video

Ahora, una vez que hayas visto el vídeo, piensa en las siguientes preguntas, escribe en un papel tus reflexiones y continúa pensando en ellas durante un tiempo:

1. ¿Crees que alguna vez has cometido plagio?
2. ¿En qué ocasión? ¿Puede dar un ejemplo concreto?
3. Si la respuesta es sí, ¿sabías que lo estabas haciendo?

  
  

_Guidelines for design. COMMENTS ON SLIDE 2_

_Along this lesson 5.2, we plan to use mainly text, but with some images or text in images, to highlight ideas or examples, for instance in the case of quotations of plagiarism definitions._

_This will be also the case when we need to show examples of wrong doing and good practices to avoid plagiarism. This brings us the question of some kind of style guide to the look and feel of the pages of text at the MOOC._

_Also we propose to use a video. It will appear after the texts with the quotes for the definitions of plagiarism, we propose the student to watch a USU video from YouTube ([https://youtu.be/OFLFaSjNr2I](https://youtu.be/OFLFaSjNr2I)) \[with a detailed attribution, as it has a_ _S__tandard YouTube License\]. If it is possible we could adapt it to a new video without specific mentions to USU._

_After watching the video we request the student to do some reflection and try to remember if in the past he/she has committed some kind of plagiarism._

  
  
**5.2.2 ¿Por qué evitar el plagio?**

Aquí tienes algunas buenas razones para evitar el plagio:

*   En las instituciones de educación superior el plagio se considera **deshonestidad académica** y puede ser castigado con penas, suspensión o expulsión.
Más allá de las penas o castigos, se trata de una **cuestión de ética**, honestidad personal e integridad académica: se trata de valorar y respetar el trabajo de los demás como te gustaría que respetaran el tuyo.
Usar la información publicada, usar el conocimiento y los descubrimientos hechos por otros - siempre y cuando cites las fuentes apropiadamente- añade *valor y calidad a tu trabajo**. Proporciona una base firme para lo que estás escribiendo; confiere validez y objetividad a tus ideas; muestra que has leído, analizado y reflexionado antes de escribir; y, por último, pero no por ello menos importante, es una prueba de que utilizas datos fiables y pruebas de investigaciones anteriores que pueden ser rastreadas y validadas.
Desde una perspectiva más general y normativa, cada país tiene su propia legislación sobre plagio. En muchos casos, puede representar una **infracción de los derechos de autor y/o de las leyes de propiedad intelectual** (según la regulación) y puede acarrear algún tipo de sanción.

Además, debido al gran impacto de la viralidad en el contexto actual de los medios de comunicación social, el plagio puede causar graves daños a la reputación personal. Todos conocemos algunos casos de plagio que han tenido consecuencias desagradables, incluso a largo plazo, independientemente de que las acusaciones fueran verdaderas o no. Para evitar estos problemas en tu vida profesional, es importante adquirir valores y habilidades sobre cómo evitar el plagio durante tu tiempo como estudiante. Para comprobarlo, puedes repasar algunos ejemplos que alguna vez han tenido repercusión en los medios de comunicación: 

>* Werkhäuser, Nina. “A chronology of the Schavan plagiarism affairs” _DW,_ 10.02.2013 [https://www.dw.com/en/a-chronology-of-the-schavan-plagiarism-affair/a-16589171](https://www.dw.com/en/a-chronology-of-the-schavan-plagiarism-affair/a-16589171) [Retrieved: Feb. 8th 2018]
>* Dionne, E.J. Jr. “Biden admits plagiarism in school but says it was not ‘malevolent’.” _The New York Times,_ September 18th, 1987. Available at the NYT Archive [https://www.nytimes.com/1987/09/18/us/biden-admits-plagiarism-in-school-but-says-it-was-not-malevolent.html?smid=pl-share](https://www.nytimes.com/1987/09/18/us/biden-admits-plagiarism-in-school-but-says-it-was-not-malevolent.html?smid=pl-share) [Retrieved: Feb. 8th 2018]
>* “US election: Melania Trump ‘plagiarised’ Michelle Obama.” _BBC News,_ 19 July 2016 [https://www.bbc.com/news/election-us-2016-36832095](https://www.bbc.com/news/election-us-2016-36832095) [Retrieved: Feb. 8th 2018]

Una vez que haya visto estos ejemplos, reflexiona sobre los efectos que la acusación o sospecha de plagio puede haber tenido en estas personas.


**5.2.3 ¿Cómo evitar el plagio?  Conocer los límites de lo que es y lo que no es plagio**

La mayoría de las veces, los estudiantes no saben que están plagiando. Para evitarlo, lo primero que necesitan es tener una idea clara y detallada de lo que se considera plagio y lo que no.

**NO, no es plagio...**

Si explicas hechos o reflejas conocimientos antiguos ya conocidos por todos, o usa ideas de uso común o palabras que no se pueden decir de otra manera, no estás cometiendo plagio si no citas las fuentes. Es decir, cuando utilizas lo que se denomina "conocimiento común".

\[object\_5-2-6\_img\]

Puedes profundizar en el concepto de “conocimiento común” en este sitio web [https://integrity.mit.edu/handbook/citing-your-sources/what-common-knowledge](https://integrity.mit.edu/handbook/citing-your-sources/what-common-knowledge)
  
  

**SÍ, es plagio...**

It’s considered plagiarism if you copy ideas, literal phrases or paragraphs, images, etc., without correctly referencing where it came from. It is plagiarism if you extract content from another persons work and do not credit the original author. 

\[object\_5-2-7\_img\]

>*Fuente: Purrington, Collin “Examples of plagiarism using Donald McCabe quotation”. _Preventing plagiarism,_ 2016. [https://colinpurrington.com/tips/plagiarism](https://colinpurrington.com/tips/plagiarism) [Retrieved: Jul. 26th 2018]  
  

**Ejercicio opcional**
Si crees que necesitas más ejemplos de lo que se considera plagio y los tipos de mala práctica que conducen al plagio, te recomendamos la siguiente actividad opcional:

1. Obtén tus respuestas a las preguntas de la sección 5.2.1:

    1. ¿Crees que alguna vez has cometido plagio?
    2. ¿En qué ocasión? ¿Puede dar un ejemplo concreto?
    3. Si la respuesta es sí, ¿sabías que lo estabas haciendo?
        
2.  Trata de clasificar los casos en los que crees que has cometido plagio en el pasado en uno de los 10 tipos que se muestran en esta imagen:
    
\[object\_5-2-8\_img\] 
>*Fuente: _The Plagiarism Spectrum: tagging 10 types of unoriginal word._ Turnitin, iParagigms, 2012 [https://www.turnitin.com/static/plagiarism-spectrum/](https://www.turnitin.com/static/plagiarism-spectrum/) [Retrieved: Feb. 8th 2018]


3.   ¿Has descubierto alguna forma de plagio que no conocías? ¿Cuál de ellas?
    

  
  

_Guidelines for design. COMMENTS ON SLIDE_ _4_

_We present the material with a basically textual focus (but with graphical differentiation in the case of the examples), although, if there are resources, the text that is proposed as reading in a script for a video could be transformed. At this moment we believe that the textual version will work quite well._

_This screen presents the first step to avoid plagiarism, reinforcing and expanding the ideas already seen in the initial video: knowing what is considered plagiarism and what does not. It is necessary for the student to know that he can use words, ideas and documents of other people, provided that they cite sources of origin and do so correctly as explained in the next screen and even more in detail the next lesson (5.3)._

_Finally, the student is asked to examine a classification of the most widespread forms of plagiarism, to expand their knowledge and reflect again. In this way, it is intended to deepen the awareness of the behaviour of plagiarism, which had been introduced at the beginning of the lesson._

  
  


**5.2.4 ¿Cómo evitar el plagio? Citar y hacer referencia a la fuente utilizada**

En nuestro trabajo tenemos que indicar qué información es el producto de nuestra creación y cuál no lo es. Además, tenemos que hacerlo de forma que la información de otros esté bien integrada en nuestro discurso, a la vez que la diferenciamos e identificamos claramente.

Para evitar el plagio cuando usamos información de cualquier otro autor o fuente, podemos proceder de dos maneras: citando o parafraseando. 
  

**a) Citar.** Al poner fragmentos en nuestro trabajo con las palabras exactas del el texto original, debemos citar al autor del texto original entre comillas:

**a.1.** Si el fragmento que citamos es corto, podemos insertarlo dentro de nuestro texto pero tiene que aparecer entre comillas (precedidas por la puntuación de los dos puntos, o enlazadas sintácticamente con nuestro texto). Al final de la frase citada añadimos la identificación de la fuente con un sistema codificado en el texto (por ejemplo, el nombre del autor y el año del documento citado) o con una nota bibliográfica a pie de página [ver ejemplos de cómo hacerlo en la lección 5.3].

\[object\_5-2-9\_img\]
>*Fuente: Peters, Timothy. _An introduction to plagiarism,_ 2011 [https://www.slideshare.net/peter1t/an-introduction-to-plagiarism](https://www.slideshare.net/peter1t/an-introduction-to-plagiarism) [Retrieved: Jul. 26th 2018]


**a.2.** Si se trata de un texto más largo (más de cuatro líneas) o de un párrafo entero, lo copiamos cambiando de línea, ampliando el margen a una pulgada y utilizando un tamaño de letra más pequeño para el texto citado, añadiendo la identificación de la fuente entre paréntesis (nombre del autor, año) o mediante una nota al pie.

\[object\_5-2-10\_img\]
>*Fuente: “Sample APA Block Quote”. _How to format a block quote._ [https://www.wikihow.com/Format-a-Block-Quote](https://www.wikihow.com/Format-a-Block-Quote) [Retrieved: Jul. 26th 2018]

**b) Parafrasear.** Cuando usamos nuestras palabras pero como reelaboración de las palabras de otros o de datos:

**b.1.**  Breve reexpresión de las ideas de un autor, centrándonos solo en las ideas y puntos principales. Puedes usar algunas de las palabras del autor. En este caso, se requiere una fórmula introductoria (por ejemplo: De acuerdo con Nombre del Autor, Como dice Nombre del Autor, Contrariamente a lo que dice  Nombre del Autor, Nombre del Autor sugiere...) y añadimos la fecha entre paréntesis o con una nota a pie de página.

\[object\_5-2-11\_img\]
>*Fuente: _University of Newcastle Library._ “Example of a summary”. _Paraphrasing, quoting and summarizing_
>[https://libguides.newcastle.edu.au/paraphrasing-summarising/example-of-summarising](https://libguides.newcastle.edu.au/paraphrasing-summarising/example-of-summarising) [Retrieved: Jul. 26th 2018]


**b.2.** Reelaboración de lo escrito, explicación de la idea con nuestras propias palabras. Es una opción mejor que la anterior cuando la idea es más compleja y no se puede explicar con una sola frase. La explicamos con nuestras propias palabras, pero la idea sigue sin ser nuestra y, por lo tanto, indicamos la fuente añadiendo su identificación con un paréntesis (autor, año) o mediante una nota a pie de página:

\[object\_5-2-12\_img\]
>*Fuente: _University of Newcastle Library._ “Example of a paraphrasing”. _Paraphrasing, quoting and summarizing_ [https://libguides.newcastle.edu.au/paraphrasing-summarising/example-of-paraphrasing](https://libguides.newcastle.edu.au/paraphrasing-summarising/example-of-paraphrasing) [Retrieved: Jul. 26th 2018]

  
  

_Guidelines for design. COMMENTS ON SLIDE 5_

_This screen explains briefly the techniques that allow the use of the data, ideas or documents of other authors without incurring in plagiarism: quoting and paraphrasing. This is a simple introduction to the basic techniques, which will be developed in a more comprehensive and practical way at the next lesson. At the same time, mention is made of the examples of application of the techniques that the student has been able to see throughout the text of the lesson, which can now be reviewed._

  


**5.2.5 Recursos para profundizar en el tema**

**Referencias**

En la bibliografía final de tu trabajo habrá referencias bibliográficas completas, bien sea ordenadas alfabéticamente por el apellido de los autores, bien numéricamente según el orden de aparción de las citas en el texto, para que el lector pueda localizar e identificar claramente las fuentes que han sido citadas de forma más o menos abreviada a lo largo del texto.

En la próxima lección (5.3) aprenderás más sobre cómo citar la información de otros en tu trabajo y preparar correctamente los diversos tipos de bibliografía correspondiente.

Echa un vistazo a los recursos para profundizar en lo aprendido en esta lección. La bibliografía final de cualquiera de tus futuros trabajos académicos debe ser similar:
* Massachusetts Institute of Technology (MIT). "What is Common Knowledge?" In *Academic Integrity at MIT; a handbook for students.* https://integrity.mit.edu/handbook/citing-your-sources/what-common-knowledge [Retrieved Feb 8th 2018]
* McGarry, Janet [et al.] *Bibliography: Bibliography and Reference List - Introduction* Last Update Sep 21, 2017 https://libguides.mhs.vic.edu.au/c.php?g=174589&p=1149960 [Retrieved Feb 8th 2018]
* Peters, Timothy, *An introduction to plagiarism*, 2011 https://www.slideshare.net/peter1t/an-introduction-to-plagiarism [Retrieved Jul 26th 2018]
* "Pagiarism", *Wikipedia*. Last edited on Feb 4, 2018 https://en.wikipedia.org/wiki/Plagiarism [Retrieved Feb 8th 2018]
* *The Plagiarism Spectrum: Taggin 10 types of unoriginal work*. Turnitin, iParadigms, 2012 https://www.turnitin.com/static/plagiarism-spectrum/ [Retrieved Feb 8th 2018]
* Purrington, Colin. *Preventing plagiarism*. 2016 https://colinpurrington.com/tips/plagiarism [Retrieved Jul 26th 2018]
* "Sample APA Block Quote". *How to format a block quote* https://www.wikihow.com/Format-a-Block-Quote [Retrieved Jul 26th 2018]
* University of Newcastle Library. *Paraphrasing, quoting and summarizing*. Last updated: Jan 15, 2018. https://libguides.newcastle.edu.au/paraphrasing-summarising [Retrieved Jul 26th 2018]
* USU Libraries. *What is plagiarism?*. Published on 6 March 2017, 1,22 min avaialble from YouTube: https://www.youtube.com/watch?v=OFLFaSjNr2I  [Retrieved Feb 8th 2018]
* "What is plagiarism?". *Plagiarism.org* Published May 18, 2017 https://www.plagiarism.org/article/what-is-plagiarism [Retrieved Feb 8th 2018]
