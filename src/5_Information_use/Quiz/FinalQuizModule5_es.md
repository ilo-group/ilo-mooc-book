              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } a:link { color: #0563c1 } 

# **Test final**

**Test Módulo 5**

  
1. Si recuperas imágenes con licencias Creative Commons, ¿tienes derecho a transformarlas como quieras para usarlas en un nuevo trabajo que estés haciendo?
( ) En absoluto, siempre debemos respetar la forma original de la creación del autor.
( ) Sí, pero solo si utilizas la imagen con fines comerciales.
( ) Sí, si las imágenes no están etiquetadas con el código ND (No Derivative) junto al logo CC, eso es todo lo que necesitas.
(x) Sí, si las imágenes no están etiquetadas con el código ND (No Derivative) junto al logo CC, y haces la atribución correcta requerida por el código BY que siempre está presente en cualquier licencia CC.


2. ¿Quién tiene razón, Marco o Stefany?

( ) Marco: "Copié y adapté para mis necesidades algunos párrafos de Wikipedia, pero estoy muy tranquilo porque es una fuente pública y abierta de Internet, que podemos usar libremente. Los artículos no están firmados y la enciclopedia no tiene autores, por lo tanto, no hay problema en copiar el texto que se desea y no es necesario citar ya que no hay ningún autor a quien acreditar su trabajo".

(x) Stefany: "La Wikipedia es una fuente de Internet que podemos usar gratuitamente, y como cualquier otra fuente podemos copiar el texto de ella. Pero creo que tenemos que indicar explícitamente que esos párrafos no han sido creados por nosotros. Además, normalmente es mucho mejor utilizar y citar las fuentes que aparecen en las referencias del artículo de Wikipedia porque es de donde proviene la información original".

[explanation]

.Stefany tiene razón, porque si no decimos que lo que hemos puesto no es nuestro, estamos usando el trabajo de otra persona (aunque no sabemos quién lo ha hecho) y estamos mintiendo sobre nuestro propio trabajo diciendo que es nuestra creación. Se puede utilizar y se puede copiar el texto literalmente, pero es necesario indicar claramente que no es nuestro y dar los datos de la fuente de donde lo hemos extraído.

•Marco está equivocado, porque si no decimos que lo que hemos copiado proviene de una fuente externa estamos utilizando el trabajo de otra persona (aunque no sabemos quién lo ha hecho) y estamos mintiendo sobre nuestro propio trabajo, presentándolo como si fuera nuestro trabajo. Se puede utilizar y se puede copiar el texto literalmente, pero es necesario indicar claramente que no es nuestro y dar los datos de la fuente de donde lo hemos extraído.

[explanation]

3. ¿Qué sistema de citación se utiliza más en las disciplinas de las artes y las humanidades?

( ) Estilo APA.

( ) Sistema de datos de autor.

(x) Sistema de notas bibliográficas.

( ) Estilo IEEE.

4. ¿Qué herramienta se recomienda para archivar y anotar de forma ordenada los documentos bibliográficos que recopilas y lees?

( ) Google Drive.

( ) Microsoft OneDrive.

( ) Amazon Cloud.

(x) Un software de gestión de referencias como Zotero.

5. Estoy escribiendo un trabajo universitario sobre ética y negocios, y quiero introducir un razonamiento metafórico en mi trabajo usando la frase "La tierra es redonda y gira alrededor del sol". Para evitar el plagio, ¿qué debo hacer?

( ) Tengo que citar a Galileo Galilei, quien lo descubrió, para probar que yo no lo inventé y fundamentar mi trabajo.

(x) No tengo que citar nada, puedo utilizarlo ya que no es plagio porque se considera "conocimiento común".

( ) Tengo que citar de qué fuente lo he extraído porque alguien lo escribió y lo he leído en alguna parte, no puedo copiarlo sin decir de dónde lo he copiado.

[explanation]
Como se comentó en la sección 5.2.3 de este Módulo 5, los hechos conocidos y descubiertos hace mucho tiempo y que la comunidad académica utiliza como "conocimiento común" no necesitan ser referenciados y pueden ser utilizados sin incurrir en plagio. De todos modos, en caso de duda y si realmente encontraste el texto en un documento real, cítalo.
[explanation]

6. Marca todas las respuestas correctas
¿Qué herramienta te ayudará a ser más productivo?

(x) OneNote.

(x) Overnote.

(x) Google Keep. 

7. Lee el texto de este enlace (https://www.psychologytoday.com/us/blog/ulterior-motives/201711/mindfulness-is-not-always-positive). Escoge la solución más correcta para usar el texto de esa fuente publicada en un trabajo sobre el mindfulness que estás escribiendo para evitar el plagio:

( ) Como algunos expertos recuerdan, "el análisis cuidadoso de los datos sugiere que la relación entre el mindfulness y el pensamiento criminogénico es compleja".

( ) El aspecto del mindfulness asociado con la reserva de juicio sobre el yo en realidad aumentó significativamente el pensamiento criminogénico. Así que, en general, los efectos positivos y negativos del mindfulness casi se anulan mutuamente (Markman).

(x) Al evaluar los costes y beneficios del mindfulness, es importante saber que tiene efectos diferentes en el pensamiento (Markman, 2017).

[explanation]

•La opción c) es la correcta: En esta opción, se hace un breve resumen utilizando las palabras del autor y se añaden entre paréntesis el nombre y el año de publicación de la fuente. Estos datos entre paréntesis enlazan con la lista de referencias con los datos completos de los documentos y que están ordenados alfabéticamente por autor y por fecha bajo el mismo autor.

•En la opción a) se reproduce una frase completa, entre comillas, y eso es correcto, pero no se indica la fuente de la que se extrajo la frase.

•En la opción b) el texto se reproduce literalmente, pero no hay citas que lo indiquen. Sería incorrecto porque no se declara que se trata de un texto literalmente copiado, se indica el autor pero sin mencionar el año de publicación.

[explanation]

8. ¿Dónde deberías guardar tu información?

( ) En un lápiz de memoria.

(x) En lugares seguros o herramientas de software con copias de seguridad.

( ) En el correo electrónico.

( ) En un disco duro externo.