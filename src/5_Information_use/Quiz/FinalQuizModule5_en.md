              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } a:link { color: #0563c1 } 

**5.6 – Final Quiz for Module 5**

  
  

**1\. If you retrieve images with Creative Commons licences, do you have the right to transform them as you like for use in a new work you are doing?**

1.  Not at all, always should be respected the orginal shape of the author’s creation.
    
2.  Yes, but only if you use the image for commercial purposes.
    
3.  Yes, if the images are not labeled with the code ND (No Derivative) code next to logo CC, that’s all you need.
    
4.  Yes, if the images are not labeled with the code ND (No Derivative) code next to logo CC, and you do the correct attribution required by the code BY that always are present in any CC licence.
    

_**Correct answer: d)**_

  
  

2**. Who is right, Marco or Stefany?**

**a) Marco**: “I copy-pasted and adapted for my needs a handful of paragraphs from Wikipedia, but I am very calm because it is a public source and open Internet, we can use freely. The articles are not signed and the encyclopaedia has no named authors, therefore, there is no problem copying the text that you want and it is not necessary to cite as there is no longer any author to give credit for his work.”

**b) Stefany**: “The Wikipedia is an Internet source we can use it for free, and like any other source we can copy the text from it. But I think that we have to indicate explicitly that those paragraphs have not been created by us. Furthermore, it is usually much better to use and cite the sources that appear in the Wikipedia article’s references because it is where the original information comes from.”

_**Correct answer: b).**_ _If possible, in this question it would be nice to give feedback to the student with this explanation:_

*   **Stefany is right**, because if we do not say that what we have put is not ours we are using someone else's work (although we do not know who has done it) and we are lying about our own work saying that it is our creation. It can be used and you can copy text literally, but it is necessary to clearly indicate that it is not ours and give the source data from where we have extracted it.
    
*   **Marco is wrong**, because if we do not say that what we have copied comes from an external source we are using someone else's work (although we do not know who has done it) and we are lying about our own work, presenting it like it were our work. It can be used and you can copy text literally, but it is necessary to indicate clearly that it is not ours and give the source data from where we have extracted it.
    

  
  

**3\. Which system of citation is most used in arts and humanities disciplines?**

1.  APA Style.
    
2.  Author-data system.
    
3.  Bibliographic notes system.
    
4.  IEEE Style.
    

_**Correct answer: c)**_

  
  

**4\.** **Which tool is recommended to archive and annotate in an ordered way the bibliographical documents do you gather and read?**

5.  Google Drive.
    
6.  Microsoft OneDrive.
    
7.  Amazon Cloud.
    
8.  Reference manager software like Zotero.
    

_**Correct answer: d)**_

  
  

**5\. I’m** **writing a university term paper about ethics and business, and I want to introduce a metaphorical reasoning into my work using the phrase "The earth is round and it revolves around the sun." To avoid plagiarism, what should I do?**

a) I have to cite Galileo Galilei who discovered it to prove that I did not invent it and base my work.

b) I do not have to cite anything, I can use it as it is not plagiarism since it is considered "common knowledge".

c) I have to cite what source I have extracted because someone wrote it and I have read it somewhere, I cannot copy it without telling where I copied it.

_**Correct answer: b).**_ _If possible, in this question it would be nice to give feedback to the student with this explanation:_

*   _As discussed in section 5.2.3 of this Module 5, the facts known and discovered long time ago and that the academic community uses as "common knowledge" do not need to be mentioned and can be used without incurring in plagiarism. Anyway, in case of doubt and if you actually found the text in a real document, cite it._
    

  
  

**6\.** **Which tool will help you to be more productive?**

1.  OneNote.
    
2.  Evernote.
    
3.  Google Keep.
    
4.  All of them.
    

_Correct answer: d)_

  
  

**7\.** **Read the text behind this link ([https://www.psychologytoday.com/us/blog/ulterior-motives/201711/mindfulness-is-not-always-positive](https://www.psychologytoday.com/us/blog/ulterior-motives/201711/mindfulness-is-not-always-positive)). Choose the most correct solution of using the text from that published source in a work on mindfulness your are writing in order to avoid plagiarism:**

1.  As some experts recall, “careful analyses of the data suggested that the relationship between mindfulness and criminogenic thinking is complex.”
    
2.  The aspect of mindfulness associated with reserving judgment about the self actually increased criminogenic thinking significantly. So, overall the positive and negative effects of mindfulness nearly cancelled each other out (Markman).
    
3.  In assessing the costs and benefits of mindfulness, it's important to know that it has different effects on thinking (Markman, 2017).
    

_Correct answer: c). If possible, in this question it would be nice to give feedback to the student with this explanation:_

*   _Option c) is the correct one: In this option, a short summary is made using the words of the author and the name and year of publication of the source are added in brackets. This data in brackets links with the list of references with the complete data of the documents and which is arranged alphabetically by author and by date under the same author._
    
*   _In option a) a full sentence is played, in quotation marks and that is correct, but the source from which the phrase was extracted is not indicated._
    
*   _In option b) the text is reproduced literally but there are no quotes that indicate it. It would be incorrect because it is not declared that it is a literally copied text, the author is indicated but without mention of the year of publication._
    

  
  

**8\.** **Where should your information  be saved?**

1.  Into a memory stick.
    
2.  Into the email.
    
3.  Into an external hard disk.
    
4.  Into safe places or software tools with backup copies.
    

_Correct answer: d)_