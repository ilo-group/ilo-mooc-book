              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } a:link { color: #0563c1 } 

# **Test final**

**Test Mòdul 5**
  
1. Si recuperes imatges amb llicència Creative Commons, tens dret a transformar-les com vulguis per a usar-les en un nou treball que estiguis fent?

( ) En absolut, sempre hem de respectar la forma original de la creació de l'autor.

( ) Sí, però només si utilitzes la imatge amb fins comercials.

( ) Sí, si les imatges no estan etiquetades amb el codi ND (No Derivative) al costat del logo CC, això és tot el que necessites.

(x) Sí, si les imatges no estan etiquetades amb el codi ND (No Derivative) al costat del logo CC, i fas l'atribució correcta requerida pel codi BY que sempre està present en qualsevol llicència CC.

2. Qui té raó, en Marco o la Stefany? 

( ) Marco: "Vaig copiar i vaig adaptar per a les meves necessitats alguns paràgrafs de la Wikipedia, però estic molt tranquil perquè és una font pública i oberta d'Internet, que podem usar lliurement. Els articles no estan signats i l'enciclopèdia no té autors, per tant, no hi ha cap problema en copiar el text que es desitja i no cal citar ja que no hi ha cap autor a qui acreditar la seva feina".

(x) Stefany: "La Wikipedia és una font d'Internet que podem fer servir gratuïtament, i com qualsevol altra font podem copiar el text d'ella. Però crec que hem d'indicar explícitament que aquests paràgrafs no han estat creats per nosaltres. A més, normalment és molt millor utilitzar i citar les fonts que apareixen en les referències de l'article de Wikipedia perquè és d'on prové la informació original".

[explanation]
.La Stefany té raó, perquè si no diem que el que hem posat no és el nostre, estem usant el treball d'una altra persona (encara que no sabem qui ho ha fet) i estem mentint sobre el nostre propi treball dient que és la nostra creació. Es pot utilitzar i es pot copiar el text literalment, però cal indicar clarament que no és nostre i donar les dades de la font d'on ho hem extret.

• En Marco està equivocat, perquè si no diem que el que hem copiat prové d'una font externa estem utilitzant el treball d'una altra persona (encara que no sabem qui ho ha fet) i estem mentint sobre el nostre propi treball, presentant-lo com si fos el nostre treball. Es pot utilitzar i es pot copiar el text literalment, però cal indicar clarament que no és nostre i donar les dades de la font d'on ho hem extret.
[explanation]

3. Quin sistema de citació s'utilitza més en les disciplines de les arts i les humanitats?

( ) Estil APA.

( ) Sistema de dades d'autor.

(x) Sistema de notes bibliogràfiques.

( ) Estil IEEE.


4. Quina eina es recomana per arxivar i anotar de forma ordenada els documents bibliogràfics que recopiles i llegeixes?

( ) Google Drive.

( ) Microsoft onedrive.

( ) Amazon Cloud.

(x) Un programari de gestió de referències com Zotero.

5. Estic escrivint un treball universitari sobre ètica i negocis, i vull introduir un raonament metafòric en el meu treball fent servir la frase "La terra és rodona i gira al voltant del sol". Per evitar el plagi, què he de fer?

( ) He de citar Galileu Galilei, que va ser qui ho va descobrir, per provar que jo no ho vaig inventar i fonamentar el meu treball.

(x) No he de citar res, puc utilitzar-la ja que no és plagi perquè es considera "coneixement comú".

( ) He de citar de quina font ho he extret perquè algú ho va escriure i ho he llegit en algun lloc, no puc copiar-ho sense dir d'on ho he copiat.

[explanation]
Com es va comentar en la secció 5.2.3 d'aquest Mòdul 5, els fets coneguts i descoberts fa molt temps i que la comunitat acadèmica utilitza com "coneixement comú" no necessiten ser referenciats i poden ser utilitzats sense incórrer en plagi. De tota manera, en cas de dubte i si realment vas trobar el text en un document real, cíta'l.
[explanation]

6. Marca totes les respostes correctes
Quina eina t'ajudarà a ser més productiu?

(x) OneNote.

(x) Overnote.

(x) Google Keep

7. Llegeix el text d'aquest enllaç (https://www.psychologytoday.com/us/blog/ulterior-motives/201711/mindfulness-is-not-always-positive). Tria la solució més correcta per utilitzar el text d'aquesta font publicada en un treball sobre el mindfulness que estàs escrivint per evitar el plagi:

( ) Com alguns experts recorden, "l'anàlisi acurada de les dades suggereix que la relació entre el mindfulness i el pensament criminogènic és complexa".

( ) L'aspecte del mindfulness associat amb la reserva de judici sobre el jo en realitat va augmentar significativament el pensament criminogènic. Així que, en general, els efectes positius i negatius del mindfulness gairebé s'anul·len mútuament (Markman).

(x) A l'hora d'avaluar els costos i beneficis del mindfulness, és important saber que té efectes diferents en el pensament (Markman, 2017).

[explanation]
• L'opció c) és la correcta: En aquesta opció, es fa un breu resum utilitzant les paraules de l'autor i s'afegeixen entre parèntesis el nom i l'any.
• A l'opció a) es reprodueix una frase completa, entre cometes, i això és correcte, però no s'indica la font de la qual es va extreure la frase.
• A l'opció b) el text es reprodueix literalment, però no hi ha cites que ho indiquin. Seria incorrecte perquè no es declara que es tracta d'un text literalment copiat, s'hi indica l'autor però sense esmentar-ne l'any de publicació.
[explanation]

8. On hauries de desar la teva informació?

( ) En un llapis de memòria.

(x) En llocs segurs o eines de programari amb còpies de seguretat.

( ) Al correu electrònic.

( ) En un disc dur extern.