
**Kviz modula 5**

1.  Želim uporabiti sliko z licenco CC (Creative Commons). Ali jo lahko spremenim in prilagodim osebnim potrebam?
    

( ) Ne, nikoli. Spoštovati in ohraniti moram izvirno sliko.

( ) Da, ampak samo, če želim razširiti slike v komercialne namene.

( ) Da, če je slika označena z logotipom CC in ne s kodo ND (No Derivative).

(X) Da, če je slika označena z logotipom CC in ne s kodo ND (No Derivative) ter jo ustrezno citiram.

  
  

2.  Kdo ima prav? Marko ali Špela?
    

( ) **Marko**: Prevzel sem nekaj podatkov iz Wikipedije in jih vključil v svoje znanstveno delo. Ker vem, da spada Wikipedija med javno dosegljive vire na spletu, sem prepričan, da jih lahko uporabim. Avtorji člankov na Wikipediji niso razvidni. Prav tako ni omenjen izdajatelj ali urednik enciklopedije. Brez skrbi torej lahko uporabim podatke in jih vključim v seminarsko nalogo. Poleg tega mi vira ni treba citirati, saj ni omenjen noben avtor. Zato mi ni treba spoštovati avtorskih pravic.

(X) **Špela:** Wikipedija je prosta spletna enciklopedija in spada k informacijskim virom. Uporabljamo jo tako kot tudi vse druge vire. To pomeni, da lahko prevzamemo besedila.

Mislim pa, da moramo natančno označiti, da prepisano besedilo ni naša intelektualna lastnina. Poleg tega je uporaba in citiranje bibliografskih referenc, ki jih najdemo tudi na Wikipediji, priporočljivejša, saj nam ti omogočajo dostop do izvirnih informacij.

  
  

_\_Povratne informacije k temu vprašanju\__

_**Špela ima prav,**_ _saj smo uporabili informacije drugih avtorjev. Če navajamo besedila drugih oseb brez navajanja citatov smo uporabili intelektualno lastnino drugih avtorjev (tudi, če ni razvidno, kdo je avtor). V tem primeru se lažemo, saj bralcu ne more biti jasno, da to niso naše osebne zamisli in besede. Seveda lahko prevzamete besedila drugih avtorjev, celo dobesedno, a treba je razjasniti, da to niso vaše osebne besede in dodati morate v vsakem primeru informacijski vir._

_**Marko nima prav,**_ _saj smo uporabili informacije drugih avtorjev s tem, da smo jih vključili v svoje delo in ne da bi navedli citatov (tudi, če ni razvidno, kdo je avtor). V tem primeru se lažemo, saj ustvarimo vtis, da smo napisali besedilo ali določen odlomek sami._

_Seveda lahko prevzamete besedila, tudi dobesedno, a treba je razjasniti, da to niso vaše osebne besede in dodati morate v vsakem primeru informacijski vir._

  
  

3.  Na humanističnih fakultetah uporabljajo večinoma eno vrsto citiranja. Katero?
    

( ) standard APA

( ) sistem avtor–datum

(X) bibliografski sistem z opombami

( ) Standard IEEE

  
  

4.  Katero orodje smo vam priporočali za shranjenje in zbiranje bibliografskih podatkov?
    

( ) Google Drive

( ) Microsoft OneDrive

( ) Amazon Cloud

(x) Programske opreme za upravljanje z referencami kot je Zotero

  
  

5.  Trenutno pišem seminarsko nalogo o etiki in gospodarstvu. Želim vključiti naslednjo izjavo: „Zemlja se vrti okoli Sonca“. Kako lahko preprečim plagiat?
    

( ) V tem primeru moram citirati Galileja, ki je to odkril. Samo tako lahko dokažem, da si te izjave nisem izmislil/a.

(x) Ni treba mi citirati ničesar. Izjavo lahko vključim v znanstveno delo, saj spada k splošno znanemu znanju.

( ) V tem primeru moram citirati uporabljen vir. Ne morem vključiti izjave, če ne dodam vira.

  
  

_V poglavju 5.2.3 smo omenili, da ni treba navajati virov, ki se nanašajo na informacije, ki so nam vsem dobro znane („splošno znanje“). Uporabljamo jih lahko, ne da bi jih posebno označili – vseeno pa to ni plagiat. Če vam ni jasno, ali naj citirate vir ali ne, vam priporočamo, da ga dodate._

  
  

6.  Katera orodja vam lahko pomagajo pri povečanju produktivnosti?
    

  
  

(x) OneNote

(X) Evernote

(X) GoogleKeep

(X) Vsi

  
  

7.  Preberite naslednji pogovor ([https://www.delo.si/novice/slovenija/prestavi-sume-o-plagiatorstvu-naj-presojajo-fakultete.html](https://www.delo.si/novice/slovenija/prestavi-sume-o-plagiatorstvu-naj-presojajo-fakultete.html) ).
    

Želite vključiti izjave tega pogovora v svoje znanstveno delo. Kako preprečite plagiat?

  
  

( ) „Vse več univerzitetnih nalog zlasti javnih osebnosti je pod sumom plagiatorstva.“

( ) Vse več univerzitetnih nalog zlasti javnih osebnosti je pod sumom plagiatorstva (Grgič).

(X) Vedno več znanstvenih nalog znanih oseb obtožujemo plagiatorstva (Grgič, 2013).

  
  

_\_Povratna informacija\__

_Odgovor c) je pravilen: Stavek zapišemo z lastnimi besedami. V oklepaja dodamo ime in leto objave. Podrobnejšo informacijo vira najdemo v seznamu literature._

_Odgovor a): Prevzeli smo stavek, ne da bi dodali narekovaje. To je sicer v redu, a dodali nismo vira._

_Odgovor b): Prevzeli smo odlomek, ne da bi dodali narekovaje. To ni pravilno, saj ne smemo dodati nespremenjenega besedila, ne da bi ga označili kot takega. Sicer smo omenili avtorja, a nismo dodali leta objave._

  
  

8.  Kje lahko shranimo podatke?
    

( ) USB-ključ

( ) e-sporočilo

( ) zunanji pomnilnik

(x) na varnem mestu, izdelava varnostnih kopij