              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } a:link { color: #0563c1; text-decoration: underline } 

**Quiz zu Modul 5**

1.  Wenn ihr Bilder mit Creative Commons Lizenz abruft, dürft ihr sie dann für eure Arbeit verändern, wie ihr wollt?
    

( ) Nein, auf keinen Fall, die ursprüngliche Form des Erstellers muss immer erhalten und respektiert werden.

( ) Ja, aber nur, wenn die Bilder für kommerzielle Zwecke verwendet werden

( ) Ja, wenn die Bilder neben dem CC Logo nicht mit den Code ND (No Derivative) gekennzeichnet sind, dann ist das in Ordnung

(X) Ja, wenn die Bilder neben dem CC Logo nicht mit den Code ND (No Derivative) gekennzeichnet sind und ihr eine korrekte Quellenangabe macht.

  
  

2.  Wer hat recht Marco oder Stephanie?
    

( ) **Marco**: Ich habe ein paar Paragraphen aus Wikipedia kopiert und in meine Arbeit eingefügt, aber ich bin nicht beunruhigt, da es eine öffentliche Quelle im Internet ist. Die Artikel sind nicht signiert und die Enzyklopädie hat keine genannten Autoren. Deshalb ist es kein Problem, wenn ich den Text, den ich brauche, kopiere und es ist auch nicht notwendig, dass ich es zitiere, da kein Autor angegeben ist, dessen Arbeit ich anerkennen muss.

(X) **Stephanie:** Wikipedia ist eine Internetquelle, die wir kostenlos nützen können und aus der wir, wie bei jeder anderen Quelle, den Text kopieren können. Aber ich denke, wir müssen explizit darauf hinweisen, dass diese Paragraphen nicht von uns verfasst wurden. Des Weiteren ist es normalerweise viel besser die Quellen aus den Referenzen im Wikipedia- Artikel zu nutzen und zu zitieren, weil daraus die ursprünglichen Informationen kommen.

  
  

_\_Feedback zu dieser Frage\__

_**Stephanie hat Recht,**_ _da wir die Arbeit von jemand anders verwenden, wenn wir es in unsere Arbeit inkludieren und diesen nicht nennen (auch wenn wir nicht wissen, wer der Verfasser ist). Dabei lügen wir über unsere Arbeit, da wir es aussehen lassen, als wäre es von uns selbst verfasst. Der Text kann kopiert werden, auch wortwörtlich, aber es muss klargestellt werden, dass es sich nicht um eure Arbeit handelt und die Originalquelle angegeben werden._

_**Marco liegt falsch,**_ _da wir die Arbeit von jemand anders verwenden, wenn wir es in unsere Arbeit inkludieren und diesen nicht nennen (auch wenn wir nicht wissen, wer der Verfasser ist). Dabei lügen wir über unsere Arbeit, da wir es aussehen lassen, als wäre es von uns selbst verfasst. Der Text kann kopiert werden, auch wortwörtlich, aber es muss klargestellt werden, dass es sich nicht um eure Arbeit handelt und die Originalquelle angegeben werden._

  
  

3.  Welche Form der Zitation wird am meisten in Geistes- und Humanwissenschaften verwendet?
    

( ) APA Stil

( ) Autor-Jahr Systeme

(X) Bibliographic notes system

( ) IEEE Stil

  
  

4.  Welches Hilfsmittel wurde empfohlen, um bibliographische Angaben, in einer klaren Art und Weise zu archivieren und abzuspeichern?
    

( ) Google Drive

( ) Microsoft OneDrive

( ) Amazon Cloud

(x) Literaturverwaltungsprogramme wie Zotero

  
  

5.  Ich schreibe eine Seminararbeit über Ethik und Wirtschaft und ich will eine metaphorische Beweisführung mit der Phrase „Die Erde ist rund und sie dreht sich um die Sonne“ in meine Arbeit einfließen lassen. Um ein Plagiat zu vermeiden, was sollte ich tun?
    

( ) Ich muss Galileo Galilei, der das entdeckt hat, zitieren, um zu beweisen, dass ich diese Schlussfolgerung nicht erfunden habe.

(x) Ich muss nichts zitieren. Ich kann die Phrase verwenden und es wird kein Plagiat sein, da es sich um Allgemeinwissen handelt.

( ) Ich muss die Quelle zitieren, von der ich es entnommen habe, weil jemand es geschrieben hat und ich habe es irgendwo gelesen. Ich kann es nicht verwenden, ohne zu sagen, woher ich es kopiert habe.

  
  

_Wie in Teil 5.2.3 besprochen, müssen Fakten, die bereits vor langer Zeit entdeckt wurden, bekannt sind und welche die wissenschaftliche Gemeinschaft als „Allgemeinwissen“ nutzt, nicht extra gekennzeichnet werden und können verwendet werden, ohne sie zu plagiieren. Im Zweifelsfall, wenn ihr das ursprüngliche Dokument gefunden habt, zitiert es._

  
  

6.  Welches Werkzeug kann euch helfen produktiver zu werden?
    

(x) OneNote

(X) Evernote

(X) GoogleKeep

(X) Alle

  
  

7.  Werft einen Blick auf folgenden Artikel ([https://www.zeit.de/2011/09/N-Plagiat-Hochschulen](https://www.zeit.de/2011/09/N-Plagiat-Hochschulen)). Entscheidet euch für die korrekteste Lösung zur Verwendung des Texts in eurer eigenen Arbeit, um ein Plagiat zu vermeiden:
    

( ) „Nimmt man alle Universitäten zusammen, kommt man hochgerechnet im Jahr auf vielleicht zwei Dutzend Annullierungen.“

( ) Nimmt man alle Universitäten zusammen, kommt man hochgerechnet im Jahr auf vielleicht zwei Dutzend Annullierungen (Spiewak).

(X)  Auch wenn statistisch valide Daten fehlen, lassen die bekannten Fälle in denen die Doktorwürde aberkannt wurde darauf schließen, dass an allen deutschen Universitäten zusammen im Jahr etwa zwei Dutzend Annullierungen eingeleitet werden (Spiewak, 2011).

  
  

_\_Feedback\__

_Antwort c) ist die richtige Antwort: Bei dieser Alternative wird eine kurze Zusammenfassung mit den Worten des Autors gegeben. Name und Jahr der Publikation werden in Klammern genannt. Diese Information in Klammer knüpft an die Liste der Referenzen im Literaturverzeichnis an._

_Antwort a) Ein ganzer Satz wird übernommen und unter Anführungszeichen gesetzt. Das ist richtig, aber die Quelle, von der dieser Satz übernommen wurde, ist nicht angeführt_

_Antwort b) Der ganze Absatz wird unverändert übernommen, aber es gibt keine Anführungszeichen, die darauf hindeuten würden. Das wäre falsch, da nicht gekennzeichnet ist, dass es sich um einen unverändert übernommenen Text handelt. Der Autor wird zwar genannt, aber ohne die Anmerkung des Jahrs der Veröffentlichung._

  
  

8.  Wo sollten eure Informationen gespeichert werden?
    

( ) Auf einem Memory Stick

( ) In einer Email

( ) Auf einer externen Festplatte

(x) An sicheren Plätzen oder in Softwaretools mit Backup Kopien