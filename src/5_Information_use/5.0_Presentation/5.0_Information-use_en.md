**Lesson 5.0 – Presentation Module 5. Information use: the right and fair way**

_**SLIDE 1**_

  

In this module you will gain an understanding of ethical issues both in decision making and gathering of information, as well applying ethical principles in the process of creating and sharing of information.

**Learning outcomes:**
At the end of this lesson, learners will be able to:

* recognize the need for personal information management
    
* understand ethical issues of information
    
* share digital content in an ethical manner
    
* articulate and apply legal and ethical behaviour in online contexts (B&R)
    
* know how to properly cite and use referencing tools
    

  

Try to recognize those learning outcomes in this video with the presentation of the Module 5:

_\[VIDEO\_5\_0_1\]: it’s in preparation the final version of the provisional video presented at the Zadar’s Meeting:_ [_https://informationliteracy.eu/encoded/5\_Information\_use/Module-5-Presentation/Module-5-Presentation/Module-5-Presentation-lq.webm_](https://informationliteracy.eu/encoded/5_Information_use/Module-5-Presentation/Module-5-Presentation/Module-5-Presentation-lq.webm)