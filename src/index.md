---
title: 'ILO-Book'
image: '/img/icons/ilo-logo.png'
emoji: 🏡
order: 0
permalink: /
---

## Welcome to the ILO Book

This work-in-progress ports [ILO-content](https://gitlab.educs-hosting.net/ilo-team/ilo-content/)
and [ILO-videos](https://gitlab.educs-hosting.net/ilo-team/ilo-videos/) to a static web site.

Please surf based on the table of contents to the left.

If lost, [start here](/1_Orienting/1.1_Intro_en/).
Or see the somewhat better laid out page [here](/2_Inquiries/2_Research_Questions_cat/).

_About the project? See [repository](https://gitlab.com/ilo-group/ilo-mooc-book/)._