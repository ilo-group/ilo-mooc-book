const environment = process.env.ELEVENTY_ENV;
const PROD_ENV = 'prod';
const pathPrefix = ``;
const prodUrl = `https://ilo-mooc-book.hoplahup.net${pathPrefix}`;
const devUrl = `${pathPrefix}`;
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl;
const isProd = environment === PROD_ENV;
const packageJson = require('../../package.json');
const {name,version,description, homepage} = packageJson;

module.exports = {
    environment,
    isProd,
    tracking: {
      // gtag: ''
    },
    title: name.replace(/-/g, ' '),// optional 
    version, // optional 
    description,// optional 
    baseUrl,
    pathPrefix,
    repo: "https://gitlab.com/ilo-group/ilo-mooc-book/",
    author: {
      name: "ILO Team",
      email: "team@informationliteracy.eu"
    },
    owner: {
      //name: "Pietro",
      //last_name: "Passarelli",
      //email: "pietro.passarelli@gmail.com", // optional
      //twitter: "pietropassarell", // optional
      //facebook: "pietro.passarelli", // optional
      //github: "pietrop/11ty-auto-navigation-book-template", // optional
      //linkedin: "pietropassarelli", // optional
      //instagram: "pietro.ps", // optional
      image: "/img/icons/ilo-logo.png"
    },
    //og_locale: "en_US"
};