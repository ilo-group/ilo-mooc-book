              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } a:link { color: #0000ff; text-decoration: underline } em { font-style: italic } 

**FSE.4 Bewerten und Verwendung von Informationen**

**Einleitung**

In dieser Einheit werdet ihr euer Wissen über die Bewertung und Nutzung der gesammelten Informationen vertiefen.

**Lernziele**

Am Ende dieser Einheit werdet ihr in der Lage sein:

*   Zu verstehen, wie Zeitschriften bewertet werden
    
*   Zwischen verschiedenen Rankings zu unterscheiden und auch ihre Nachteile zu kennen
    
*   Zu verstehen, wieso Informationen in einer ethisch vertretbaren Art benutzt werden sollen
    

**FSE.4.1 Rankings**

**Rankings von Zeitschriften**

Rankings sind eine Möglichkeit, die Relevanz eurer Inhalte zu prüfen. Aber Rankings werden nicht in jeder Disziplin als gleich wichtig erachtet. Zum Beispiel werden in der deutschen Forschungskultur im Bereich der Soziologie Bücher von einem einzelnen Autor (Monographien) höherwertiger eingestuft als Zeitschriftenartikel und Rankings auch generell kritisch diskutiert. Im Gegensatz dazu wird in den Wirtschaftswissenschaften Publikationen in Zeitschriften eine hohe Bedeutung zugeschrieben, wobei Zeitschriftenrankings als das Hauptkriterium zur Beurteilung der Relevanz gelten. Es gibt verschiedene fachspezifische Zeitschriftenrankings, die euch helfen können, die Qualität eurer Recherche abzuschätzen:

*   SSCI
    
*   CitEc (RePEc)
    
*   VHB JOURQUAL - [https://vhbonline.org/en/service/jourqual/vhb-jourqual-3/](https://vhbonline.org/en/service/jourqual/vhb-jourqual-3/)
    
*   RePEc Journal Ranking
    
*   Eigenfactor.org
    
*   _Handelsblatt Ranking BWL/VWL_
    

  

Rankings verwenden verschiedene quantitative und/oder qualitative Kriterien zur Einstufung der Zeitschriften. Das wichtigste Maß ist der sogenannte Impact-Faktor. Der Impact-Faktor wird dadurch berechnet, dass die Zitationen aller Publikationen der Zeitschrift in den letzten zwei Jahren durch die Anzahl der Artikel der Zeitschrift in den letzten zwei Jahren dividiert wird. Inzwischen werden auch fünf Jahres Impact-Faktoren berechnet. Umso höher der resultierende Impact-Faktor, desto höher die Relevanz der jeweiligen Zeitschrift. Aber man muss sich dessen bewusst sein, dass der Impact-Faktor nur die Relevanz der Zeitschrift darzustellen vermag und nichts über die Qualität einzelner Artikel in der Zeitschrift aussagt. Obwohl es viel Kritik an der Berechnung und Verwendung von Impact-Faktoren gibt, werden diese in vielen Disziplinen verwendet.

Welches Ranking relevant ist, ist nicht nur abhängig von der Disziplin, sondern auch vom Land. Zum Beispiel ist in deutschsprachigen Ländern das VHB JOURQUAL Ranking des Verbands der Hochschullehrer für Betriebswirtschaftslehre (VHB) sehr wichtig in der Disziplin der Wirtschaftswissenschaften. Dieses Ranking stuft Zeitschriften in die Ränge A+ bis D ein. A+ steht hierbei für herausragende und weltweit führende wissenschaftliche Zeitschriften auf dem Gebiet der Betriebswirtschaftslehre oder ihrer Teildisziplinen.

**Übung**

Denkt darüber nach, was für Auswirkungen es hätte, wenn ihr nur Zeitschriftenrankings zur Bewertung der Qualität eines Artikels verwendet. Wieso könnte das irreführend sein?

\[Text Feld\]

  
  

**Forscher Rankings**

Ähnlich wie das Zeitschriftenranking gibt es auch Rankings von Forschern. Solche Rankings haben aber nicht die gleiche Relevanz wie Zeitschriftenrankings und haben bereits viele kritische Diskussionen verursacht. Beispiele für Rankings von Forschern sind:

*   RePEc Researcher Ranking
    
*   _Forscher-Ranking des Handelsblatts_
    

  
  

**FSE.4.2 Bewertung von Nachrichten und „Fakten“**

In den letzten Jahren ist der Begriff „Fake News“ aufgekommen und hat sich schnell etabliert. Auch im wirtschaftlichen Kontext ist es wichtig, sich dessen bewusst zu sein, dass weitverbreitete Nachrichten und Fakten nicht notwendigerweise wahr sind. Wie ihr bereits in Modul 4 gesehen habt, gibt es viele Teile einer Information, die ihr verwenden könnt, um die Glaubwürdigkeit der Information zu prüfen. Fragt euch daher immer:

*   Wer ist der Autor?
    
*   Woher kommt die Information?
    
*   Ist sie glaubwürdig?
    
*   Wer hat sie veröffentlicht?
    

**Denkt darüber nach:**

Was könnten die möglichen Intentionen einer Organisation/Institution sein, die einen Artikel publiziert?

Welche Probleme können Fake News für Unternehmen verursachen?

\[Textfeld\]

**FSE.4.3 Die ethisch korrekte Nutzung von Informationen**

In Modul 5 haben wir bereits den ethischen Umgang mit Informationen besprochen. Wie ihr bereits in Einheit 5.3 gehört habt, gibt es viele mögliche Varianten, wie man zitieren kann. Ein paar Zitierweisen sind häufiger in einer spezifischen Disziplin als in einer anderen. In Wirtschaftswissenschaften werden vor allem die APA und Harvard Methode verwendet. Welche Zitierweise ihr schlussendlich verwenden müsst, hängt von den Richtlinien eurer Lektoren, Institution oder Verleger ab.

Hier findet ihr mehr Informationen zur APA und Harvard Methode:

APA Methode:  [http://www.apastyle.org/](http://www.apastyle.org/)

Harvard Methode:  [https://www.mendeley.com/guides/harvard-citation-guide](https://www.mendeley.com/guides/harvard-citation-guide)

In der Wirtschaftspraxis werdet ihr oft mit Aspekten des geistigen Eigentums und des Rechtes auf Privatsphäre und Datenschutz konfrontiert.

Geistiges Eigentum ist ein immaterielles Gut. Man kennt es hauptsächlich durch Begriffe wie Copyright, Patente oder Trademarks. Es beinhaltet Geschäftsgeheimnisse, Veröffentlichungsrechte, Entdeckungen und Erfindungen, aber auch künstlerische Arbeiten wie Wörter, Phrasen, Symbole und Designs können durch das Urheberrecht geschützt werden. Geistiges Eigentum soll als ein Anreiz zur Erschaffung von geistigen Gütern und Informationen dienen. Es erlaubt Menschen, Gewinne durch die Erschaffung immaterieller Schöpfungen zu erzielen, da sie das Risiko der billigen Nachahmung von geistigem Eigentum durch andere verringert. Da es sich aber um immaterielles Gut handelt, stellt geistiges Eigentum auch immer eine gewisse Schwierigkeit dar. Besonders, da auch die Vagheit des Begriffs _Geistiges Eigentum_ selbst oft kritisiert wird.

Das Recht auf Privatsphäre zielt darauf ab, die Privatsphäre jedes Individuums zu schützen. Dieses Recht wird durch die wachsende Anzahl von Daten, die ein Mensch tagtäglich durch die Verwendung von digitalen Medien erzeugt, immer relevanter. Es gibt verschiedene lokale Vorschriften, die das Recht auf die Einsicht der Daten, die Unternehmen von ihren Kunden speichern, die Aufforderung zur Löschung dieser Daten oder das Recht zur vollkommenen Untersagung der Verarbeitung individueller Daten regeln. In der Europäische Union ist seit 2018 die Datenschutz-Grundverordnung (DSGVO) in Kraft.

Mehr Informationen zur Datenschutz-Grundverordnung findet ihr hier:

[https://de.wikipedia.org/wiki/Datenschutz-Grundverordnung](https://de.wikipedia.org/wiki/Datenschutz-Grundverordnung)

**FSE Fallstudie**

In diesem Abschnitt könnt ihr nun ein paar der zuvor erwähnten Informationsquellen anwenden und sie für eure Recherche zum Thema Corporate Social Responsibility (CSR) bzw. unternehmerische Gesellschaftsverantwortung und Nachhaltigkeit in Bezug auf ein spezifisches Unternehmen benützen. Zusätzlich werdet ihr ein paar kostenlose online Tools zur Analyse von sozialen Medien kennenlernen und diese vergleichen.

Ihr arbeitet als Praktikant in einem Unternehmen in der Automobilindustrie und bekommt die Aufgabe, Informationen zu ein paar größeren Akteuren in diesem Bereich zu beschaffen. Ihr fangt mit der Volkswagen AG an.

Zuerst müsst ihr euch auf die Suche nach ein paar allgemeinen Informationen machen. Ihr könnt ein paar der Informationsquellen verwenden, die in der vorigen Einheit aufgelistet wurden.

*   Wie viele Mitarbeiter hat das Unternehmen zurzeit?
    
*   Wie groß war der Gewinn/Umsatz des Unternehmens im letzten Jahr?
    
*   Wie sieht die jetzige mediale Berichterstattung über das Unternehmen aus?
    
*   Wie sehen derzeitige Prognosen zum Börsenwert aus?
    

\[Textfeld\]

Danach solltet ihr nach den CSR Bestimmungen suchen.

*   Könnt ihr dazu etwas auf der Webseite des Unternehmens finden?
    
*   Könnt ihr dazu auf den Profilen des Unternehmens in sozialen Medien etwas finden?
    
*   Findet ihre Blogs oder Foren, die sich mit diesem Thema beschäftigen?
    

\[Textfeld\]

Zuletzt solltet ihr analysieren, was in sozialen Medien und bei Internetnutzern zurzeit in Bezug auf Volkswagen zu sehen ist. Ihr habt vielleicht schon von den Begriffen O_pinion Mining, Sentimentanalyse oder Emotion AI_ gehört. Es sind Prozesse, die versuchen, subjektive Informationen zu identifizieren und zu quantifizieren. Sie werden überwiegend verwendet, um schriftliche Texte von Kunden (wie beispielsweise Bewertungen oder Beiträge in Sozialen Medien) zu analysieren und deren Stimmungsbild oder Meinung darzustellen. Unten findet ihr eine Liste kostenloser Software zur Sentimentanalyse (bzw. mit kostenloser Probephase):

*   [http://www.socialmention.com/](http://www.socialmention.com/)
    
*   [http://www.opiniontracker.net/en/](http://www.opiniontracker.net/en/)
    
*   [https://www.csc2.ncsu.edu/faculty/healey/tweet\_viz/tweet\_app/](https://www.csc2.ncsu.edu/faculty/healey/tweet_viz/tweet_app/)
    

Ihr solltet nun diese drei Softwaretools verwenden, um folgende Frage zu beantworten:

*   Spielt der Abgasskandal von 2015 immer noch eine Rolle in der öffentlichen Meinung?
    
*   In welchem Kontext wird Volkswagen hauptsächlich erwähnt?
    
*   In welche Sentimente lassen sich die derzeitigen Tweets über Volkswagen gliedern?
    

\[Textfeld\]