              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } em { font-style: italic } 

**Fachspezifische Erweiterung – Wirtschaftswissenschaften**

**Einleitung**

In dieser Erweiterung werdet ihr lernen, wie ihr das Wissen aus den vorigen Einheiten in einem fachspezifischen Kontext anwenden könnt und verschiedene neue fachspezifische Informationsquellen kennenlernen.

**Lernziele**

Am Ende dieser Lektion werdet ihr in der Lage sein:

*   Unterschiede zwischen allgemeinen und fachspezifischen Informationsquellen zu erkennen
    
*   Verschiedene fachspezifische Informationsquellen zu verwenden
    
*   Informationsprobleme in den Wirtschaftswissenschaften zu lösen, im Speziellen die Analyse von Daten aus sozialen Medien
    
*   Zu verstehen, dass fachspezifische Informationen Unterschiede zwischen Ländern und Regionen aufweisen
    

  
  

**FSE.1 Was ist Informationskompetenz und wieso sollte es euch als Studierende der Wirtschaftswissenschaften interessieren?**
    

**Einleitung**

In dieser Einheit werdet ihr lernen, wieso Informationskompetenzen wichtig für euch sind. Insbesondere werden wir uns ansehen, was Informationskompetenzen sind und welche speziellen Kompetenzen in den Wirtschaftswissenschaften gefordert sind. In den vorigen Einheiten habt ihr euch verschiedene Fertigkeiten zum Umgang mit Informationen angeeignet. In dieser Erweiterung werdet ihr nun zusätzliches Wissen für den Fachbereich der Wirtschaftswissenschaften erwerben.

**Lernziele**

Am Ende dieser Einheit werdet ihr in der Lage sein:

*   Zu verstehen, was Informationskompetenz ist und wieso sie für Wissensarbeiter notwendig ist
    
*   Verschiedene Teilbereiche der Informationskompetenz kennen
    

  
  

**FSE.1.1  Einführung in die Informationskompetenz für die Wirtschaftswissenschaften**
    

Für die Recherche von Informationen gibt es keine allgemeingültige Vorgehensweise. Diese ist stark von der jeweiligen Disziplin beeinflusst. Wenn ihr eine Recherche im Bereich der Betriebswirtschaft macht, wollt ihr vielleicht Social-Media Daten der weltweiten Kunden eines spezifischen Produkts analysieren. Im Gegensatz dazu wird man sich bei einer Recherche im Fachgebiet Geschichte beispielsweise mit dem Leben einer bestimmten Person beschäftigen und dabei auf regionale Archive zugreifen. Dieses Beispiel zeigt, dass sich nicht nur die relevanten Informationsquellen zwischen Disziplinen unterscheiden, sondern auch die grundsätzlichen Forschungsprobleme, die gelöst werden sollen.

Um Informationen für wirtschaftswissenschaftliche Problemstellen finden zu können, ist es vor allem notwendig, die wichtigsten fachspezifischen Informationsquellen zu kennen. Deshalb legt dieses Modul einen starken Fokus auf diese fachspezifischen Quellen.

Man beachte, dass es auch immer regionale und lokale Informationsquellen in verschiedenen Ländern gibt. Wir fokussieren uns in diesem Modul auf englische und deutsche Informationsquellen.

**FSE.1.2  Was ist Informationskompetenz?**
    

Fangen wir damit an, dass wir Informationen und Informationskompetenz auf einer theoretischen Ebene betrachten. Da das Konzept der Informationskompetenz verschiedene Aspekte und Entwicklungen aus vielen Disziplinen und Fachbereichen einbindet, hat sich die Bedeutung des Begriffs „Informationskompetenz“ seit der ersten Verwendung 1970 mehrmals geändert. Zum heutigen Zeitpunkt ist eine der am häufigsten verwendeten Definitionen jene der _American Association of College and Research Libraries_ (ACRL). Diese besagt:

“Information literacy is the set of integrated abilities encompassing the reflective discovery of information, the understanding of how information is produced and valued, and the use of information in creating new knowledge and participating ethically in communities of learning”

Laut UNESCO gibt es fünf Elemente der Informationskompetenz:

*   Erkennung des Informationsbedarfs
    
*   Auffinden und Bewerten der Qualität von Informationen
    
*   Speichern und Abrufen von Informationen
    
*   Effektive und ethisch vertretbare Nutzung von Informationen
    
*   Verwendung von Informationen zur Erstellung und Kommunikation von Wissen
    

Informationskompetenz ist nicht dasselbe wie Medienkompetenz, Computerkompetenz, Bibliothekskompetenz oder digitale Kompetenz, überschneidet sich aber in Teilbereichen mit all diesen Kompetenzen. Kognitive Aktivitäten wie kritisches Denken und Kommunikation sind Kernbestandteile der Informationskompetenz.

**FSE.1.3 Wieso Informationskompetenzen auch in der Wirtschaftspraxis wichtig sind**
    

Da Informationen ein immer wichtigeres Wirtschaftsgut für viele Unternehmen werden, ist es auch genauso wichtig, dass Beschäftigte die notwendigen Fähigkeiten haben, mit diesen Informationen effizient umzugehen. Mangelnde Informationskompetenz von Mitarbeitern kann zu operationalen Ineffizienzen oder dem Übersehen von Geschäftsfähigkeiten führen. Betriebe brauchen verschiedene Arten von Informationen, um Risiken und/oder Chancen in ihrem Umfeld erkennen zu können. Dazu braucht man aber auch Mitarbeiter, die diesen Informationsbedarf erkennen und solche Informationen auffinden und auf ihre Qualität bewerten, sie speichern und abrufen, sowie effektiv und ethisch vertretbar nutzen können.

Eine immer größere Herausforderung ist der Überfluss an Daten, mit denen wir tagtäglich konfrontiert sind. Informationskompetenz ermöglicht, mit diesen Herausforderungen besser umgehen zu können.

  
  

**FSE.1.4 Einordnung des Begriffs der Informationskompetenz im Kontext wirtschaftswissenschaftlicher Fachbegriffe**
    

Nicht viele Führungskräfte in der Wirtschaft sind mit dem Begriff „Informationskompetenz“ vertraut. Auch wenn der Begriff nicht gekannt wird, sind viele Teilbereiche der Informationskompetenz in Verwendung und werden als relevant eingestuft. Um euch einen besseren Eindruck darüber zu geben, wieso Informationskompetenz für euch wichtig ist und welche Konzepte in der Wirtschaftspraxis damit verbunden sind, werden wir euch ein paar Beispiele in diesem Zusammenhang vorstellen.

_“Identifikation von Information durch Kategorisierung, Abschätzung, Erkennung von Differenzen oder Ähnlichkeiten und Entdeckung von Änderungen in Rahmenbedingungen und Ereignissen“_

Wie bereits zuvor erwähnt, eine der Kernkomponenten der Informationskompetenz ist die Fähigkeit, Informationsbedarf zu erkennen. Informationen in einem wirtschaftlichen Kontext zu erkennen, schließt daher das Verständnis über verschiedene Arten von Informationen ein z.B. rechtliche Rahmenbedingungen oder Marktdaten. Zusätzlich beinhaltet Informationskompetenz im Bereich der Wirtschaftswissenschaften das Verständnis darüber, welchen wirtschaftlichen Einfluss Veränderungen von Daten haben.

_„Informationen erhalten – Beobachten, Empfangen und anderwärtiges Erhalten von Informationen aus allen relevanten Quellen“_

Eine der Hauptaufgaben in vielen Kursen in wirtschaftswissenschaftlichen Studiengängen, aber auch im Berufsleben, ist die Durchführung von Recherchen zu verschiedenen Themen. Ob es nun das Auffinden von Informationen zu Lieferanten, das Sammeln statistischer Daten oder die Suche nach rechtlichen oder finanziellen Unterlagen ist, die Beschaffung qualitativ hochwertiger Informationen ist wichtig, da solche auch strategische Entscheidungen stark beeinflussen können._

_„Analyse von Daten oder Informationen, Entscheidungen fällen und Probleme lösen - Analysieren der Informationen und die Ergebnisse bewerten, um die beste Lösung zur Problemlösung zu wählen“_

Im Laufe eures Studiums oder eures Berufslebens werdet ihr vielen Informationen und Daten gegenüberstehen._ _Informationskompetent_ _zu sein_  _bedeutet, in der Lage zu sein, Daten richtig zu interpretieren. In unserem Fall können solche Daten und Informationen beispielsweise statistische Wirtschaftsforschungsdaten, Strategien oder Vorschriften sein. Ihr könnt solche Informationen als Grundlage für Analysen nutzen, wie z.B. Finanz-, Arbeitsfluss- oder Verkaufsanalysen etc. Die Ergebnisse und deren Bedeutung für gegenwärtige, aber auch zukünftige Probleme und Entscheidungen zu verstehen, das ist eine der wesentlichen Fähigkeiten, nicht nur im wirtschaftlichen Kontext. Zusätzlich kann das Hinterfragen nicht nur der Resultate, sondern auch der Informationen, die der Analyse zugrunde liegen, die Entwicklung kritischen Denkens und des Lernens fördern.

_„Entwicklung von Zielen und Strategien“_

Eine Kernkomponente jeglicher betriebswirtschaftlicher Tätigkeit ist die Strategie. Um eine Strategie entwickeln zu können, müssen Personen einen gewissen Grad an Informationskompetenz aufweisen können, da sie in der Lage sein müssen, die wesentlichen Informationen richtig zu interpretieren. Zusätzlich ist es wichtig, in der Lage zu sein, die Stärken und Schwächen alternativer Lösungen, Strategien und Probleme zu identifizieren.

_„Bewertung von Informationen zur Bestimmung der Regelkonformität – Verwendung von relevanten Informationen und persönlichem Urteil zur Bestimmung, ob Ereignisse oder Prozesse Gesetzen, Richtlinien oder Standards_ _entsprechen__.“_

Informationen zu bewerten, kann eine Überprüfung von Daten auf Vollkommenheit und Exaktheit, Prüfung von Bankdaten und Buchhaltung, aber auch die Betrachtung der Herkunft erfordern. Finanz- und Wirtschaftsinformationen auf eine ethisch vertretbare Art abzuwägen wird als_ _kritische Informationskompetenz für die Wirtschaftswissenschaften_ _(Critical Business Information Literacy)_  _bezeichnet.

_„Anderen Beratung und Ratschläge anbieten“_

_Informationen effektiv zu vermitteln ist ein grundlegender Aspekt jeglicher geschäftlichen Tätigkeit. Die Vorbereitung und Kommunikation von Informationen an Kollegen oder Dritte in einer angemessenen Art stellt ein weiteres Element der Informationskompetenz dar. Dies beinhaltet die Fähigkeit zu verstehen, wie man Informationen organisiert, wiedergibt, richtig zitiert und klar präsentiert._