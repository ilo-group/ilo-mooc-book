**2\. Recognizing the need for information**

**Introduction**

In this lesson, you will learn some of the most commonly used types of business information and how to better recognize your need for such information.

Learning objectives

At the end of this lesson, learners will be able to:

*   Identify crucial business information
    
*   Recognize the importance of ones information needs
    

  
  

**2.1. How to start**

Let us start this lesson with a question: What kind of business information do you know?

\[Insert text field to allow answers here\]

As you might see from your answer, there is a wide range and depth of business information and you might need many different types of information. To know at which point you need what kind of information is one of the major challenges. Still, not only it is important to understand what kind of information you need, but also in which context you need it. Ask yourself these questions:

*   Have you ever made a bad choice or decision in the past, because of lack of quality information?
    
*   Was the information that would help you make a better decision available? If so, were you aware of this possibility?
    
*   In hindsight, would you decide differently now?
    

Asking questions is a good way to articulating the need for information. Being able to recognize what kind of information you need in which context, will also help you formulate a more purposeful search query and help access the information you need faster. Furthermore, you will see that at a later point in time re-evaluation might be needed and other kinds of needed information could be identified.

  
  

**2.2 What kind of Information?**

Recognizing the need for information requires having a basic understanding about a specific situation or object. You have to ask yourself a number of questions to identify the information needed:

*   Do I actually understand the issue/situation?
    
*   What kind of information exists, is it relevant and available?
    
*   What information do I need to write a good paper/prepare an insightful presentation etc.?
    
*   What kind of information is connected to the one I need and could it be linked and useful in my situation?
    
*   What are the desired results?
    
*   Who needs this information? Is it for myself/client/colleague?
    

Asking and answering these questions will help you get a clearer picture of what and why you need information.

In a business environment, there are huge sets of data and information. They are used in many different types of situations e.g. search for business partners, vendors or clients. Knowing what kind of information exists can help you recognize your information needs early and more precisely. Some of the most commonly used kinds of information in business environments are:

*   Information about products (technical facts, reviews, etc.)
    
*   Information about a company (dossiers, solvency information, etc.)
    
*   Personal information (employment, CV, etc.)
    
*   Market and financial information (stock information, market trends and developments, etc.)
    
*   Public Information (demographics, statistical data etc.)
    
*   Legal information (regulation, policies, restriction etc.)
    
*   Press information/news
    
*   Science and technological advances
    
*   Social web
    
*   Etc.
    

There are many different types of information and not all of them will always be relevant to you and your situation. However, it is good to know what kind of information exist and how one can influence the results. Just think about the last time you and your classmates had to prepare and give a presentation. Did everybody use the same information and information sources? Was there someone that drew a connection between different kinds of information that was not completely obvious? Where did your classmates gather the needed information? Was the use and synthesis of the information logically sound or dubious? Reflecting and learning from your peers can help you further enhance your capabilities regarding information needs, retrieval, evaluation, use and communication.

**Exercise**

Imagine you are working at a company that produces juice. Your assignment is to research foreign markets and decide where to launch your product next. What kind of information could be useful to make a good decision?

_\[Insert text field here\]_

  
  

Sources

_Spackman, A., & Camacho, L. (2009). Rendering information literacy relevant: A case-based pedagogy._ _The Journal of Academic Librarianship, 35(6), 548-554._