**3\. Locate and retrieve Information**

**Introduction**

In this lesson, you will learn what kind of information resources exist in the area of economics and business administration and how to get relevant data.

**Learning objectives**

At the end of this lesson, learners will be able to:

• use different subject-specific information sources

• solve information problems in the area of economics and business administration

  
  

**3.1 Information seeking and retrieval tactics**

Before we proceed, let us repeat the generic search tools we got to know in module 3:

**BOOLEAN OPERATORS (AND, OR, NOT)**

*   _**AND:** Every term has to appear somewhere in the document. Example: benefit AND marketing  all documents, that contain both terms, are given._
    

  

*   _**OR:** One of the terms has to appear somewhere in the document. Example: facebook OR marketing  all documents, that contain one of the terms, are given._
    

  

*   _**NOT:** Excludes Terms. Example: marketing NOT facebook  all documents, that contain the term “marketing” but not the term “facebook” are given._
    

  
  

**TRUNCATION AND WILDCARDS**

*   Examples Truncation: economic*  economic, economical, economics
    

_*equality   equality, inequality_

*   Example Wildcards: colo?r   color, colour
    

  
  

**PHRASE SEARCH**

*   _Example: "social media marketing“_
    

  
  

**3.2 Subject-specific information sources**

To be able to approach research problems in context of the discipline of economics and business administration, you need to know the most important information sources. Thus, on the next pages we will focus on the most important information sources in the context of this discipline. Please note that there are also local information sources within different country. We will focus here only on resources available in English.

  
  

**3.2.1 Fact databases**

Often you will just need some simple facts and figures for or a report or a presentation. Fact databases allow you to access fact data in various forms of aggregation. This includes for instance statistical data, economic data or corporate registers. The data ranges from statistics provided in image formats, to text reports and raw data containing thousands of datasets.

Research showed that fact databases are the most commonly referred information sources in the discipline of economics and business administration. The pivotal role of data analysis in various business cases might be one of the reasons for their importance.

  
  

Some important examples of fact databases for economics and business administration are:

*   Eurostat
    
*   OECD iLibrary
    
*   World Databank
    
*   Statista
    
*   UNdata
    

  

_// German translation only_

_Statistik Austria_

_GENESIS-Online Datenbank des statistischen Bundesamts der Bundesrepublik Deutschland_

_Statistische Ämter der Länder, z.B. OpenData Portale bestimmter Bundesländer wie Berlin_

_Bisnode/Noppenstedt: Firmendatenbank Deutschland_

_Kompass - https://de.kompass.com/_

_LexisNexis Wirtschaft_

_Perinorm (Datenbank für Normen)_

_www.german-business.de_

_Handelsregister_

_//_

  
  

Aggregated statistical data:

*   Allthatstats
    
*   EUKlems
    
*   International Household Survey Network
    
*   Inter-university consortium for Policial and Social Research (ICPSR)
    
*   quandl.com
    
*   Total Economy Database (TED)
    
*   Urbam Audit
    

  
  

Microdata:

*   Amadeus Datenbank
    
*   Bloomberg
    
*   Bureau van Dijk
    
*   Thomson Reuters Datastream
    
*   Wharton Research Data Services (WRDS)
    

  
  

Survey data – some examples:

*   ALLBUS
    
*   Community Innovation Survey
    
*   ESS
    
*   EU-LFS
    
*   EU-SILC
    
*   GLES
    
*   Gruppenbezogene Menschenfeindlichkeit
    
*   ifo-Geschäftsklimaindes
    
*   Innovationsmonitor des IW
    
*   Mannheimer Innovationsportal
    
*   NEPS
    
*   PAIRFAM
    

  
  

**3.2.2 Journals for Practitioners and Business Newspapers**

Concerning some issues you can also refer to journals for practitioners and business newspapers. For example if you conduct research about cryptocurrencies, you might also want to mention briefly that the topic had a broad coverage in the media recently and refer to some newspaper articles. Increasingly newspapers tend to introduce paywalls and let you access only a small number of articles without charge. If you need recent articles, you can also try to consult your local library. Sometimes you will find the recent issues of the most important newspapers there.

  
  

Some examples of journals for practitioners and business newspapers are:

*   The Financial Times
    
*   The Wall Street Journal
    
*   The Economist
    

  
  

_// German translation only_

_Some examples in the German language:_

_Handelsblatt_

_Wirtschaftswoche_

_BrandEins_

_Capital.de_

_Manager Magazin_

_//_

  
  

**3.2.3 Web portals**

Web portals had their main era in the beginnings of the world wide web when they were starting points to surf the web. The rise of search engines led to a decline in web portals. Nevertheless, there are still several web portals, which collect relevant information for economics and business from various sources. Some examples are:

  
  

*   EconBiz
    
*   RePEc
    
*   SSRN
    
*   RFE
    
*   INOMICS
    

  
  

_// German version only_

_sowiport.de_

_wiwi-online.de_

_www.industrie.de_

_In some regions there are also regional business portals (E.g. in Germany regioweb.de and wirtschaftsportal-niedersachsen.de)_

_//_

  
  

**3.2.4 Press databases**

Sometimes you want to refer to the press coverage of a particular topic for your research. You can use press databases to access historic press coverage. Various press databases exist which cover especially topics out of the field of economics and business administration:

  
  

*   Nexis
    
*   Factiva
    

  
  

_// German version only_

_Genios Presse_

_WISO Presse_

_ZBW - Pressemappe 20. Jahrhundert_

_//_

  
  

**3.2.5 Public and Research Institutions**

Sometimes you also need to gather information from public and research institutions. There are many possibilities, depending on what you are looking for. For example if you conduct research on labour policies by your government you might want to access statements by labour unions, the responsible ministry and your government’s unemployment data. Here is a list of some possible institutions, to give you an idea of some possibilities:

  
  

*   Ministries
    
*   Chambers
    
*   Management associations
    
*   Government authorities
    
*   Labour unions
    
*   International organizations like the World Trade Organization or European Commission
    
*   Research institutes like CERN
    

  
  

**3.2.6 Repositories and Data Archives**

Repositories and data archives allow you to access collections of data, for example from previous studies. There are some examples of particular relevance for business administration:

  
  

*   Social Science Open Access Repository (SSOAR)
    
*   UN Depository Library
    
*   Australian Data Archive
    
*   DataHub
    

  
  

_//German version only_

_datorium - https://datorium.gesis.org/xmlui/_

_DSZ-BO_

_Verbund Vorschungsdaten Bildung_

_//_

  
  

**3.2.7 Encyclopedia, Dictionaries**

Encyclopedia and dictionaries can help you to get a first insight into a new topic and to find additional keywords through obtaining translations.

  
  

*   New Palgraves Dictionary of Economics
    
*   Concise Encylopedia of Economics
    
*   International Encyclopedia of the Social Science
    
*   Encyclopedia of social theory
    
*   International Encyclopedia of the social & behavioral sciences
    
*   The SAGE glossary of the social and behavioral sciences
    
*   Online Dictionary of the Social Sciences
    

  

_// German version only_

_Gabler Online-Lexikon Wirtschaftswissenschaften_

_Social Science Dictionary_

_Brockhaus Wirtschaft_

_BWL Lexikon_

_Vahlens großes Wirtschaftslexikon_

_Lexikon der Betriebswirtschaftslehre_

_Großwörterbuch Wirtschafsenglisch_

_Encylopädie der BWL_

  

_Speziallexika_

_Das Börsenlexikon_

_Gabler Kompakt-Lexikon Bank und Börse_

_Gabler Lexikon Marketing_

_Gabler Lexikon Logistik_

_Lexikon der deutschen Familienunternehmen_

_Personenlexikon der Wirtschaftsgeschichte_

_//_

  
  

**3.2.8 Thesauri**

You already learned in unit 2.2.2 that thesauri can help you to define the keywords for your search. There are special thesauri for the discipline of economics and business administration:

  
  

EBSCO Business Thesaurus - http://web.b.ebscohost.com/ehost/thesaurus?vid=2&sid=19440e7a-2b5a-4e60-a4b2-716f3c02b93b%40pdc-v-sessmgr05

  
  

_// German version only_

_WISO Standard Thesaurus Wirtschaft - https://www.wiso-net.de/_

_//_

  
  

**3.2.9 Literature databases**

Literature databases allow you to search for scientific literature, especially articles in scientific journals. Usually these databases have some kind of quality-control, before they list particular journals and/or publications. This makes literature databases a very important tool to search and access previous research. You have already heard about databases in unit 3. There are literature databases that are specialized on economics and business administration. Important examples are:

  
  

*   ProQuest ABI/INFORM
    
*   EBSCO Business Source Premier
    

  

_//German version only_

_WISO_

_EconBiz_

_Genios_

_//_

  
  

Additionally there are also databases that cover several disciplines, which are relevant to economics and business administration. Some examples are:

*   Web of Science
    
*   Scopus
    
*   Springerlink
    
*   Emerald Insight
    
*   ACM Digital Library
    

  
  

**3.2.10 Scientific Journals**

If you want to access papers from a specific scientific journal, you can use the website of the journal. Usually journals provide an archive on their website. Open Access Articles are free of charge, in other cases it depends if your institution has obtained a license to access the content.

  
  

Some examples of the scientific journals for economics and business administration are:

*   American Economic Review
    
*   Econometrica
    
*   Academy of Management Journal (AMJ)
    
*   Journal of Political Economy
    
*   Administrative Science Quarterly (ASQ)
    
*   Management Science
    

  
  

_// German translation only_

_Some examples of German journals are:_

_Betriebswirtschaftliche Forschung und Praxis_

_Die Betriebswirtschaft_

_Journal für Betriebswirtschaft_

_Zeitschrift für Betriebswirtschaft_

_Zeitschrift für betriebswirtschaftliche Forschung_

_//_

  
  

**3.2.11 Open Access Databases**

Finding resources in databases does not always mean that you can actually access them for free. This is different with Open Access Databases, where you can access all listed resources for free. Especially in economics and business administration it is common that researchers publish working papers that have not gone through a peer-review-process. A similar form are preprints that are finished but not peer-reviewed papers. Both working papers and preprints can often be found in Open Access Databases.

  
  

*   RePec (Working Papers)
    
*   SSRN (Working Papers)
    
*   Open Access Publishing in European Networks (OAPEN)
    

_// German version only_

_EconStore_

_//_

  
  

**3.2.12 Specialized Libraries**

There are libraries that are specialized on economics and business administration. These provide you collections of relevant resources and can help you organize your search for a specialized topic within the subject. Some examples for such libraries are:

  
  

*   British Library of Political & Economic Science
    
*   www.zbw.eu
    

  
  

_//German version only_

_USB Köln (SSG Sozialwissenschaften)_

_SUB Hamburg (SSG Politik- und Verwaltungswissenschaften)_

_Bibliothek des Wissenschaftszentrums Berlin für Sozialforschung_

  

_Some university libraries also offer special websites for this subject:_

_ub.fernuni-hagen.de/wirtschaft_

_ub.uni-bielefeld.de/portals/wiwi_

_//_

  
  

**3.2.13 Publisher**

In some cases, you might want to access the website of the publisher to retrieve information about books, retrieve e-books or to download single chapters of books.

  
  

_//German version only_

_Some publishers are especially relevant for the discipline of economics and social science:_

  

_Gabler Verlag_

_Haufe Verlagsgruppe_

_Hanser_

_Pearson Verlag_

_Schäffer Poeschel_

_Verlag Recht und Wirtschaft_

_Verlag Wahlen_

_//_

  
  

**3.2.14 Classification systems**

  
  

Classification systems have been developed to allow clustering of related research. There are several different classification systems. Per example if you would like to identify all research about behavioural finance, you could look into the classification G4 according to JEL classification system. Examples for classification systems for economics and business administration are:

  
  

*   JEL
    
*   NACE
    
*   ISIC
    
*   SITC
    
*   WZ 2008
    

  
  

**3.2.15 Conference Calendars**

Not only faculty members, also students and practitioners can attend scientific conferences and present e.g. a poster about a recent project. There are many different conferences at many different topics all around the world and all around the year. Conference calendars collect these opportunities. However, be aware that not all conferences must be legitimate. This phenomenon is called predatory conferences.

  
  
Some relevant conference calendars for economics and business administration are:
    
*   INOMICS Conferences
    
*   SSRN Conferences
    
*   The Economist - Events/Conferences
    

  
  

_// German version only_

_EconBiz Veranstaltungskalender_

_//_

  
  

This Wikipedia article gives you more insights into the phenomenon of predatory conferences: https://en.wikipedia.org/wiki/Predatory_conference