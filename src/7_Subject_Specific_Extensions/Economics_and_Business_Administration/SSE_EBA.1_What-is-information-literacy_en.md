**Subject-specific extension - Economics and Business Administration Module**

**Introduction**

In this extension, you will learn how to apply your knowledge out of the previous modules in a subject-specific context and get to know various subject-specific information sources.

**Learning outcomes**

At the end of this lesson, you will be able to:

*   understand the difference between generic and subject-specific information sources
    
*   use different subject-specific information sources
    
*   solve information problems in the area of economics and business administration, in particular the analysis of social media data
    
*   understand that subject-specific information sources differ between countries and regions
    

**1 What is information literacy and why does it matter to you as an economics or business administration student?**

**Introduction**

In this lesson, you will learn why it is important to be information literate. In particular, we will focus on what is information literacy and which skills are part of business information literacy. In the previous units, you learned various skills for dealing with information. This extension will give you additional knowledge for the discipline of economics and business administration.

  
  

**Learning objectives**

At the end of this lesson, you will be able to:

• To recognize why Information literacy is a necessary skill of white-collar workforce

• To recognize different terminology for information literacy elements

**1.1 Introduction to subject-specific Information Literacy for Economics and Business Administration**

An information problem is never completely generic. It is always influenced by the discipline and culture we are used to. If you research in business administration, you might want to analyse social media data of customers of a particular product from all around the world. Instead, if you research in history you might want to analyse the life of a particular person and therefore use local archives in a particular country. This example shows that it is not just the information source that differs between disciplines, but also the research problems we might want to solve.

To be able to approach research problems in context of the discipline of economics and business administration, you need to know the most important information sources. Thus, this extension will focus heavily on the most important information sources. Please note that there are also local information sources within different countries. We will focus here only on resources available in English.

**1.2 What is Information Literacy?**

First, let us review information and information literacy on a more theoretical level. As the concept of Information Literacy reflects the thinking and development of many disciplines and research areas, also the term “Information literacy” has changed its meaning several times since its first use in the 1970. At the present day one of the most common definitions of Information Literacy was published by The American Association of Collage and Research Libraries (ACRL). Which is:

“Information literacy is the set of integrated abilities encompassing the reflective discovery of information, the understanding of how information is produced and valued, and the use of information in creating new knowledge and participating ethically in communities of learning”

According to the UNESCO there are five elements of Information Literacy:

• Recognize information needs

• Locate and evaluate the quality of information

• Store and retrieve information

• Make effective and ethical use of information

• Apply information to create and communicate knowledge

Information literacy is not the same as media literacy, computer literacy, library literacy, internet literacy or digital literacy, but it does has overlaps with all of these concepts. Information literacy may be confused with such concepts. However, information literacy is concerned with content, communication, analysis and information searching and evaluating, while computer literacy just requires the understanding and usage of technology. Thus, cognitive activities such as critical thinking and communication are core aspects of being information literate.

  
  

_Sources_

Bawden, D. (2001). Information and digital literacies: a review of concepts. _Journal of documentation, 57(2),_ 218-259.

ACRL (2016). Framework for Information Literacy for Higher Education. Retreived from: [http://www.ala.org/acrl/standards/ilframework](http://www.ala.org/acrl/standards/ilframework)

Catts, R., & Lau, J. (2008). Towards information literacy indicators.

Conley, T. M., & Gil, E. L. (2011). Information literacy for undergraduate business students: Examining value, relevancy, and implications for the new century. _Journal of Business & Finance Librarianship, 16(3),_ 213-228.

  
  

**1.3. Why Information Literacy matters in a business environment**

As information is becoming a more and more important key asset for businesses, it is just as important for workers to have the necessary skills to manage information efficiently. Lack of such skill can result in noticeable costs to the business as shortage of information literate workers may lead to operational inefficiencies or loss of business opportunities. Businesses need different types of information to be able to recognize environmental threats and/or opportunities and workers have to be able to recognize this need, locate and evaluate the quality of information, store and retrieve information and in result make effective and ethical use of it.

An ever-growing challenge is the overload of data with which we are confronted on a daily basis. Likewise, also business environments are affected by this development. To make better decisions and to deal with information more effectively and appropriately enhancing your information literacy skills is necessary.

_Sources_

Conley, T. M., & Gil, E. L. (2011). Information literacy for undergraduate business students: Examining value, relevancy, and implications for the new century. _Journal of Business & Finance Librarianship, 16(3),_ 213-228.

  
  

**1.4 Connecting phrases from the business environment to specific information literacy elements**

Not many professionals in the business world are familiar with the term “information literacy”. However, even though the term is not recognized, many components of information literacy are in use and valued. To give you a clearer picture why information literacy should matter to you and what concepts are linked to the business community we will now show you some phrases and terms of information literacy and how they align with terms in business schools.

„_Identifying information by categorizing, estimating, recognizing differences or similarities, and detecting changes in circumstances or events.”_

As already mentioned above one of the key aspects of information literacy is the ability to recognize ones need for information. Identifying information in a business context thus could involve the understanding of different types of information such as government regulations or market data. Furthermore, it includes the understanding of how changes of such might affect your business or undertaking. But also identifying and solving complex problems includes the identification of crucial information that often might not be easily recognizable on the surface.

“_Getting Information–Observing, receiving, and otherwise obtaining information from all relevant sources”_

One of the core activities in many classes in business programs but also in a work environment is to carry out research on different topics. Whether it is locating sources of supply for purchasing, collecting statistical data or searching for legal or financial records, obtaining good quality information is important to everybody as it might heavily influence the decision-making in the future.

“_Analyzing Data or Information, making Decisions and Solving Problems–Analyzing information and evaluating results to choose the best solution and solve problems.”_

During your studies or at work you face a lot of data and information. Being information literate means to be able to interpret this data correctly. In our case, such information might be charts or tables on economic research, policies or regulations. You can use this information to conduct different kinds of analysis such as financial, workflow, sales or any other kind of analysis. Understanding the results and the possible implication for current and future problems and decision-making is a key ability not just in a business environment. Furthermore, asking questions and thinking about not only the results but also the information that underlies analysis result can help develop your critical thinking capabilities and facilitate learning.

  
  

“_Developing Objectives and Strategies”_

  
  

A core component of any business related activity is strategy. To develop such strategies a person must be information literate in some way and capacity to be able to use the right kind of information in a sound way. In addition, it is important to be able to identify the strength and weaknesses of alternative solutions, strategies and problems.

  
  

“_Evaluating Information to Determine Compliance with Standards–Using relevant information and individual judgment to determine whether events or processes comply with laws, regulations, or standards.”_

  
  

Evaluating information to determine compliance may entail examination data on completeness or accuracy, verification of bank or financial records, but also the reflection on the derivation of information. Assessing financial and economic information in an ethical sound way is a skill referred to as Critical Business Information Literacy.

  
  

“_Provide Consultation and Advice to Others”_

  
  

Conveying information effectively is a pivotal aspect of any business activity. Preparing and communicating information to colleagues and third parties in a suitable way is another element of information literacy. It includes the ability to understand how to organize content, paraphrase, quote correctly, and present the appropriate information and ideas clearly.

  
  

  
  

_Sources_

Conley, T. M., & Gil, E. L. (2011). Information literacy for undergraduate business students: Examining value, relevancy, and implications for the new century. _Journal of Business & Finance Librarianship, 16(3),_ 213-228.

Klusek, L., & Bornstein, J. (2006). Information literacy skills for business careers: Matching skills to the workplace. _Journal of Business & Finance Librarianship, 11(4),_ 3-21.