             @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } em { font-style: italic } a:link { color: #0000ff; text-decoration: underline } 

**FSE.3\. Orten und Abrufen von Informationen**

**Einleitung**

In dieser Einheit werdet ihr lernen, was für Arten an Informationen es in dem Bereich der Wirtschaftswissenschaften gibt und wie ihr zu relevanten Daten kommt.

**Lernziele**

Am Ende dieser Einheit werdet ihr in der Lage sein:

*   Informationsprobleme in den Wirtschaftswissenschaften zu lösen
    
*   Unterschiedliche fachspezifische Informationsquellen zu verwenden
    

**FSE.3.1 Informationssuche und Suchstrategien**

Bevor wir fortfahren, sollten wir uns noch einmal die allgemeinen Suchfunktionen aus dem Modul 3 in Erinnerung rufen.

**Bool’sche Operatoren (UND, ODER, NICHT)**

**UND:** Jedes Wort muss irgendwo im Dokument vorkommen. Beispiel: Marketing UND Vorteil -> alle Dokumente, die die Kombination der Begriffe beinhalten, werden aufgelistet.

**ODER:** Einer der beiden Begriffe muss im Dokument vorhanden sein. Beispiel Facebook ODER Marketing -> Alle Dokumente, die einen der beiden Begriffe aufweisen, werden aufgelistet.

**NICHT:** Schließt Begriffe aus. Beispiel: Marketing NICHT Facebook -> alle Dokumente, die den Begriff Marketing beinhalten, aber zugleich Facebook nicht, werden aufgelistet.

**TRUNKIERUNG UND PLATZHALTER**

*   Beispiel Trunkierung: ökonom\* -> Ökonomie, ökonomisch, ökonomische
    

\*gleichheit -> Gleichheit, Ungleichheit

*   Beispiel Platzhalter: ?kologisch -> ökologisch, oekologisch
    

**PHRASENSUCHE**

Beispiel: „Marketing in sozialen Medien“

**FSE.3.2 Fachspezifische Informationsquellen**

Um Forschungsprobleme im Bereich der Wirtschaftswissenschaften lösen zu können, müsst ihr die wichtigsten Informationsquellen kennen. Deshalb fokussieren wir uns in den nächsten Schritten auf die wichtigsten Informationsquellen in Bezug auf diese Disziplin. Bitte beachtet auch, dass es lokale Informationsquellen in verschiedenen Ländern gibt. Wir fokussieren uns im folgenden Abschnitt nur auf Quellen in deutscher und englischer Sprache.

**FSE.3.2.1 Faktendatenbanken**

Oft werdet ihr einfach nur ein paar Fakten und einfache Zahlen für einen Bericht oder eine Präsentation brauchen. Faktendatenbanken erlauben euch den Zugriff zu Faktendaten in verschiedenen Formen der Aggregation. Dies sind beispielsweise statistische Daten, Konjunkturdaten oder öffentliche Register. Die Daten erstrecken sich von Statistiken in Bildformaten bis zu schriftlichen Berichten oder Rohdaten mit tausenden Datensätzen.

Faktendatenbanken sind die am häufigsten genannten Ressourcen in Informationskompetenz-Schulungen für die Wirtschaftswissenschaften. Dies unterstreicht deren Bedeutung für die Praxis.

Ein paar wichtige Beispiele für Faktendatenbanken in den Wirtschaftswissenschaften sind:

*   [Eurostat](https://ec.europa.eu/eurostat)
*   [OECD iLibrary](https://www.oecd-ilibrary.org/)
*   [World Databank](http://databank.worldbank.org/data/home.aspx)
*   [Statista](https://www.statista.com/)
*   [UNdata](http://data.un.org/)
*   [Statistik Austria](https://www.statistik.at/)
*   [GENESIS-Online Datenbank des statistischen Bundesamts der Bundesrepublik Deutschland](https://www-genesis.destatis.de/genesis/online)
*   Statistische Ämter der Länder, z.B. OpenData Portale bestimmter Bundesländer wie Berlin
*    Bisnode/Noppenstedt: Firmendatenbank Deutschland
*   [Kompass](https://de.kompass.com/)
*   LexisNexis Wirtschaft
*   Perinorm (Datenbank für Normen)
*   [_www.german-business.de_](http://www.german-business.de/)
*   Handelsregister

  

Aggregierte statistische Daten:

*   [Allthatstats](https://www.allthatstats.com/en/)
*   [EUKlems](http://www.euklems.net/)
*   [International Household Survey Network](http://www.ihsn.org/)
*   [Inter-university consortium for Policial and Social Research (ICPSR)](https://www.icpsr.umich.edu/icpsrweb/)
*   [quandl.com](quandl.com)
*   [Total Economy Database (TED)](https://www.conference-board.org/data/economydatabase/)
*   Urbam Audit
    

  

Mikrodaten:

*   Amadeus Datenbank
    
*   Bloomberg
    
*   Bureau van Dijk
    
*   Thomson Reuters Datastream
    
*   Wharton Research Data Services (WRDS)
    

  

Umfragedaten – ein paar Beispiele:

*   [ALLBUS](https://www.gesis.org/en/allbus/allbus-home/)
*   [Community Innovation Survey](https://www.gesis.org/en/allbus/allbus-home/)
*   [ESS](https://www.gesis.org/en/allbus/allbus-home/)
*   [EU-LFS](https://ec.europa.eu/eurostat/statistics-explained/index.php/EU_labour_force_survey_%E2%80%93_data_and_publication)
*   [EU-SILC](https://ec.europa.eu/eurostat/statistics-explained/index.php/EU_labour_force_survey_%E2%80%93_data_and_publication)
*   [GLES](http://gles.eu/wordpress/english/)
*   Gruppenbezogene Menschenfeindlichkeit
*   ifo-Geschäftsklimaindex
*   Innovationsmonitor des IW
*   Mannheimer Innovationsportal
*   [NEPS](https://www.neps-data.de/de-de/home.aspx)
*   [PAIRFAM](http://www.pairfam.de/)
    

  
  

**FSE.3.2.2 Fachzeitschriften und Wirtschaftszeitungen**

In manchen Fällen könnt ihr auch zu Wirtschaftszeitungen und Fachzeitschriften greifen. Zum Beispiel: Wenn ihr gerade eine Recherche zu Kryptowährungen durchführt, wollt ihr vielleicht auch erwähnen, dass das Thema in den Medien eine breite Reichweite hat und deshalb auf ein paar Artikel verweisen. Online-Ausgaben von Zeitungen führen allerdings zunehmend Bezahlschranken ein und erlauben nur mehr den kostenlosen Zugriff auf eine beschränkte Anzahl an Artikeln. Wenn ihr aktuelle Artikel benötigt, könnt ihr euch auch in eurer (Universitäts-)Bibliothek umsehen. Manchmal findet ihr dort eine Aufstellung der aktuellsten Ausgaben ausgewählter Fachzeitschriften und Wirtschaftszeitungen.

Ein paar Beispiele für Wirtschaftszeitungen und Fachzeitschriften sind:

*   [The Financial Times](https://www.ft.com/)
*   [The Wall Street Journal](https://www.wsj.com/)
*   [The Economist](https://www.economist.com/)

*   [_Handelsblatt_](https://www.handelsblatt.com/)
*   [_Wirtschaftswoche_](https://www.wiwo.de/)
*   [_BrandEins_](https://www.brandeins.de/)
*   [_Capital.de_](https://www.capital.de/)
*   [_Manager Magazin_](http://www.manager-magazin.de/)
    

  
  

**FSE.3.2.3 Webportale**

Webportale hatten ihre Blütezeit in den Anfängen des World Wide Web, als sie ein üblicher Startpunkt zum Surfen im Web waren. Mit dem Aufstieg der Suchmaschinen ging die Anzahl der Webportale immer mehr zurück. Trotzdem gibt es noch einige Webportale, die als relevante Informationsquellen in Bereich der Wirtschaftswissenschaften gelten. Beispiele dafür sind:

*   [EconBiz](https://www.econbiz.de/)
*   [RePEc](http://repec.org/)
*   [SSRN](https://www.ssrn.com/en/)
*   [RFE](http://rfe.org/)
*   [INOMICS](https://inomics.com/)


*   [_sowiport.de_](sowiport.de)
*   [_wiwi-online.de_](wiwi-online.de)
*   [_www.industrie.de_](http://www.industrie.de/)
*   _In gewissen Regionen gibt es auch noch regionale_ _Wirtschaftsportale_ _(Zum Beispiel in Deutschland [regioweb.de](http://www.regioweb.de/) und [wirtschaftsportal-niedersachsen.de](http://www.wirtschaftsportal-niedersachsen.de/))_
    

  

**FSE.3.2.4 Presse-Datenbanken**

Manchmal kann es auch notwendig sein, auf aktuelle Medienberichte zu verweisen. Dafür könnt ihr Presse-Datenbanken verwenden. Es gibt verschiedenen Presse-Datenbanken, die besondere Themen aus dem Bereich der Wirtschaftswissenschaften behandeln:

*   Nexis
    
*   Factiva
    
*   _Genios Presse_
    
*   _WISO Presse_
    
*   _ZBW - Pressemappe 20. Jahrhundert_
    

  
  

**FSE.3.2.5 Öffentliche Einrichtungen und Forschungsinstitutionen**

Manchmal müsst ihr auch auf Informationen von öffentlichen Einrichtungen und Forschungsinstituten zugreifen. Je nach dem, was ihr sucht, gibt es eine große Vielfalt an Möglichkeiten. Beispielsweise wenn ihr eine Arbeit über eine aktuelle Novellierung des Arbeitsrechts schreiben wollt, benötigt ihr eventuell auch Statements von Gewerkschaften, den zuständigen Ministerien und Daten zu den Arbeitslosenzahlen. Beispiele für mögliche relevante Institutionen sind:

*   Ministerien
    
*   Kammern
    
*   Arbeitgeber/-nehmer Verbände etc.
    
*   Behörden
    
*   Gewerkschaften
    
*   Internationale Organisationen wie die Welthandelsorganisation (WTO) oder die Europäische Kommission
    
*   Forschungseinrichtungen wie CERN
    

  

**FSE.3.2.6 Repositorien und Datenarchive**

Repositorien und Datenarchive ermöglichen euch den Zugriff auf Datensammlungen wie beispielsweise Daten aus vorhergegangener Forschung. Einige davon haben besondere Relevanz für den Bereich der Wirtschaftswissenschaften:

*   [Social Science Open Access Repository (SSOAR)](https://www.gesis.org/en/ssoar/home/)
    
*   [UN Depository Library](http://www.mpil.de/en/pub/library/about-the-library/un-depot-library.cfm)
    
*   [Australian Data Archive](https://ada.edu.au/)
    
*   [DataHub](https://datahub.io/)
    
*   [_datorium_](https://datorium.gesis.org/xmlui/)
    
*   [_DSZ-BO_](https://www.bildungsserver.de/institution.html?institutionen_id=13575)
    
*   [_Verbund Forschungsdaten Bildung_](https://www.forschungsdaten-bildung.de/)
    

  
  

**FSE.3.2.7 Enzyklopädien, Wörterbücher**

Enzyklopädien und Wörterbücher können helfen, einen ersten Einblick in ein neues Thema zu erhalten und zusätzliche Schlagwörter für die Informationssuche zu einem bestimmten Thema zu finden:

*   New Palgraves Dictionary of Economics
    
*   Concise Encylopedia of Economics
    
*   International Encyclopedia of the Social Science
    
*   Encyclopedia of social theory
    
*   International Encyclopedia of the social & behavioral sciences
    
*   The SAGE glossary of the social and behavioral sciences
    
*   Online Dictionary of the Social Sciences
    
*   _Gabler Online-Lexikon Wirtschaftswissenschaften_
    
*   _Social Science Dictionary_
    
*   _Brockhaus Wirtschaft_
    
*   _BWL Lexikon_
    
*   _Vahlens großes Wirtschaftslexikon_
    
*   _Lexikon der Betriebswirtschaftslehre_
    
*   _Großwörterbuch Wirtschafsenglisch_
    
*   _Encylopädie der BWL_
    
*   _Speziallexika_
    
*   _Das Börsenlexikon_
    
*   _Gabler Kompakt-Lexikon Bank und Börse_
    
*   _Gabler Lexikon Marketing_
    
*   _Gabler Lexikon Logistik_
    
*   _Lexikon der deutschen Familienunternehmen_
    
*   _Personenlexikon der Wirtschaftsgeschichte_
    

  
  

**FSE.3.2.8 Thesauri**

In Einheit 2.2.2 habt ihr bereits Thesauri kennengelernt und wie sie euch bei der Suche nach Schlagwörtern helfen können. Es gibt spezielle Thesauri für die Wirtschaftswissenschaften:

*   [EBSCO Business Thesaurus](http://web.b.ebscohost.com/ehost/thesaurus?vid=2&sid=19440e7a-2b5a-4e60-a4b2-716f3c02b93b%40pdc-v-sessmgr05)
    
*   [_WISO Standard Thesaurus Wirtschaft_](https://www.wiso-net.de/)
    

**FSE.3.2.9 Literaturdatenbanken**

Literaturdatenbanken (Manchmal im bibliothekarischen Kontext auch nur als Datenbanken bezeichnet) helfen euch bei der Suche nach wissenschaftlicher Literatur, vor allem Artikel in wissenschaftlichen Fachzeitschriften. In der Regel haben solche Datenbanken eine Qualitätskontrolle, bevor sie eine Zeitschrift oder Publikationen in ihre Listen aufnehmen. Dies macht Literaturdatenbanken zu einem wichtigen Werkzeug für die Suche und den Zugriff auf bisherige Forschung. Ihr habt bereits in Modul 3 mehr über Datenbanken erfahren. Es gibt auch Datenbanken, die auf den Bereich der Wirtschaftswissenschaften spezialisiert sind. Wichtige Beispiele sind:

*   [ProQuest ABI/INFORM](https://www.proquest.com/)
    
*   EBSCO Business Source Premier
    
*   [_WISO_](https://www.wiso-net.de/dosearch)
    
*   [_EconBiz_](https://www.econbiz.de/)
    
*   [_Genios_](https://www.genios.de/)
    

  

Zusätzlich dazu gibt es auch interdisziplinäre Datenbanken, die auch für die Wirtschaftswissenschaften bedeutsam sind:

*   [Web of Science](http://www.webofknowledge.com/)
*   [Scopus](https://www.scopus.com/search/form.uri?display=basic)
*   [Springerlink](https://link.springer.com/)
*   [Emerald Insight](https://www.emeraldinsight.com/)
*   [ACM Digital Library](https://dl.acm.org/)
    

  

**FSE.3.2.10 Wissenschaftliche Fachzeitschriften**

Wenn ihr auf einen Artikel in einer spezifischen wissenschaftlichen Fachzeitschrift zugreifen wollt, könnt ihr dies in der Regel über die Webseite der jeweiligen Zeitschrift. Normalerweise bieten Zeitschriften ein Archiv auf ihrer Webseite an. Open Access Artikel sind kostenlos, in anderen Fällen hängt es davon ab, ob eure Institution eine Lizenz für den Zugriff auf den Inhalt erworben hat.

  

Ein paar Beispiele für wirtschaftswissenschaftliche Zeitschriften sind:

*   [American Economic Review](https://www.aeaweb.org/journals/aer)
*   [Econometrica](https://onlinelibrary.wiley.com/journal/14680262)
*   [Academy of Management Journal (AMJ)](http://aom.org/amj/)
*   [Journal of Political Economy](https://www.journals.uchicago.edu/toc/jpe/current)
*   [Administrative Science Quarterly (ASQ)](http://www.johnson.cornell.edu/Administrative-Science-Quarterly)
*   [Management Science](http://www.informs.org/Journal/ManSci)
*   [_Betriebswirtschaftliche Forschung und Praxis_](http://www.bfup.de/)
*   [_Die Betriebswirtschaft_](http://www.dbwnet.de/)
*   _Journal für Betriebswirtschaft_
*   [_Zeitschrift für Betriebswirtschaft_](http://www.zfb-online.de/)
*   [_Zeitschrift für betriebswirtschaftliche Forschung_](https://www.schmalenbach.org/index.php/publikationen/zfbfsbr)
    

  

**FSE.3.2.11 Open Access Datenbanken**

Nicht immer ist es möglich, auf die in Literaturdatenbanken gefundenen Artikel kostenlos zugreifen zu können. Das ist anders bei Open Access Datenbanken, wo ihr auf alle aufgelisteten Ressourcen auch tatsächlich zugreifen könnt. Vor allem im Bereich der Wirtschaftswissenschaften kommt es oft vor, dass Forscher Arbeitspapiere veröffentlichen. Diese Arbeitspapiere haben in der Regel kein peer-review-Verfahren durchlaufen. Eine ähnliche Form sind Pre-Prints, die zwar abgeschlossene Forschungsarbeiten darstellen, aber noch keinen Peer-Review-Prozess durchlaufen haben. Sowohl Arbeitspapiere, wie auch Pre-Prints können in Open Access Datenbanken gefunden werden:

*   [RePec (Working Papers)](https://econpapers.repec.org/paper/)
*   [SSRN (Working Papers)](https://www.ssrn.com/en/)
*   [Open Access Publishing in European Networks (OAPEN)](http://www.oapen.org/home)
*   [_EconStore_](https://www.econstor.eu/?locale=de)
    

  

**FSE.3.2.12 Fachbibliotheken**

Es gibt Bibliotheken, die sich auf den Bereich der Wirtschaftswissenschaften spezialisiert haben. Sie bieten Sammlungen relevanter Ressourcen an und können euch helfen, eure Recherche zu einem spezifischen Thema in diesem Bereich durchzuführen. Ein paar Beispiele für solche Bibliotheken sind:

  

*   British Library of Political & Economic Science
    
*   [www.zbw.eu](http://www.zbw.eu/)
    
*   _USB Köln (SSG Sozialwissenschaften)_
    
*   _SUB Hamburg (SSG Politik- und Verwaltungswissenschaften)_
    
*   _Bibliothek des Wissenschaftszentrums Berlin für Sozialforschung_
    
*   _Some university libraries also offer special websites for this subject:_
    
*   [_ub.fernuni-hagen.de/wirtschaft_](http://www.ub.fernuni-hagen.de/wirtschaftswissenschaft/)
*   [_ub.uni-bielefeld.de/portals/wiwi_](https://www.ub.uni-bielefeld.de/portals/wiwi/)
    

**FSE.3.2.13 Verlage**

In manchen Fällen könnt ihr auch auf die Webseite eines Verlags zugreifen, um Informationen über Bücher, Zugriff auf E-Books oder Downloads von einzelnen Kapiteln zu bekommen. Folgende Verlage sind besonders im Bereich der Wirtschaftswissenschaften relevant_:_

  

*   _Gabler Verlag_
    
*   _Haufe Verlagsgruppe_
    
*   _Hanser_
    
*   _Pearson Verlag_
    
*   _Schäffer Poeschel_
    
*   _Verlag Recht und Wirtschaft_
    
*   _Verlag Wahlen_
    

  

_**3.2.14**_ _**Klassifikationssysteme**_

  

Klassifikationssysteme wurden entwickelt, um verwandte Bereiche von Literatur zu gruppieren. Es gibt verschiedene Klassifikationssysteme. Zum Beispiel wenn ihr die gesamte Forschung zur Verhaltensökonomik identifizieren wollt, könnt ihr die Klassifikation G4 nach JEL Klassifikationssystem suchen. Beispiele für Klassifikationssysteme in den Wirtschaftswissenschaften sind:

*   JEL
    
*   NACE
    
*   ISIC
    
*   SITC
    
*   WZ 2008
    

  
  

**FSE.3.2.15 Konferenzkalender**

Nicht nur Wissenschaftler, sondern auch Studierende und Praktiker können wissenschaftliche Konferenzen besuchen und z.B. ein Poster zu ihrer derzeitigen Forschung einreichen und – sofern dieses im Rahmen des Peer-Review-Prozesses angenommen wird – auch vorstellen. Es gibt weltweit und über das ganze Jahr viele verschiedene wissenschaftliche Konferenzen zu verschiedensten Themen. Konferenzkalender sammeln diese Möglichkeiten. Allerdings ist hier auch Vorsicht geboten: Nicht alle gelisteten Konferenzen müssen legitim sein. Dieses in den letzten Jahren zunehmende Problem von Pseudokonferenzen, welche keine wissenschaftlichen Standards einhalten und versuchen an gutgläubigen und unerfahrenen (Jung-)Wissenschaftlern Geld zu verdienen, nennt sich _Predatory conferences_.

Ein paar relevante Konferenzkalender in den Wirtschaftswissenschaften sind:

*   INOMICS Conferences
    
*   SSRN Conferences
    
*   The Economist - Events/Conferences
    
*   _EconBiz Veranstaltungskalender_
    

Folgender Artikel gibt euch einen Einblick zum Phänomen Predatory conferences: https://www.zeit.de/2017/44/wissenschaftskonferenzen-pseudokonferenzen-waset