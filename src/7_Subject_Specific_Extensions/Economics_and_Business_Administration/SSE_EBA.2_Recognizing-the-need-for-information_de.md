              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } em { font-style: italic } 

**FSE 2. Den Informationsbedarf erkennen**

**Einleitung**

In dieser Einheit werden ihr ein paar der bekanntesten wirtschaftsbezogenen Informationen und Daten kennenlernen und wie ihr euren diesbezüglichen Informationsbedarf besser erkennen könnt.

**Lernziele**

Am Ende dieser Einheit solltet ihr in der Lage sein:

*   Wesentliche Unternehmensinformationen zu identifizieren
    
*   Die Wichtigkeit eurer Informationsbedürfnisse zu erkennen
    

  
  

**FSE.2.1 Wie fängt man an**

Lasst uns diese Einheit mit einer Frage anfangen: Welche Arten von wirtschaftlichen Informationen kennt ihr?

\[Antwortfeld\]

Wie ihr vielleicht an euren Antworten seht, gibt es eine große Breite von wirtschaftsbezogenen Informationen und ihr werdet wahrscheinlich verschiedene dieser Informationen brauchen. Zu wissen, wann man welche Informationen braucht, ist eine der größten Herausforderungen. Es ist nicht nur wichtig zu wissen, was für Informationen ihr braucht, sondern auch in welchem Kontext diese gebraucht werden. Stellt euch diese Fragen:

*   Habt ihr in der Vergangenheit schon schlechte Entscheidungen getroffen, weil ihr keine oder nicht genug gute Informationen hattet?
    
*   Waren die Informationen, die euch zu einer besseren Entscheidung verholfen hätten, vorhanden? Wenn ja, wusstet ihr von dieser Möglichkeit?
    
*   Rückblickend, würdet ihr euch anders entscheiden?
    

Sich Fragen zu stellen ist eine gute Möglichkeit, euren Informationsbedarf zu formulieren. Im Stande zu sein zu erkennen, welche Art von Information man in welcher Situation braucht, kann auch dabei helfen, eine stärker zielgerichtete Suchanfrage zu formulieren und Informationen dadurch schneller abzurufen. Außerdem werdet ihr sehen, dass zu einem späteren Zeitpunkt Re-Evaluierungen notwendig sein könnten und eventuell auch der Bedarf an zusätzlichen Informationen entsteht.

**FSE.2.2 Welche Informationen?**

Um den eigenen Informationsbedarf erkennen zu können, muss man ein grundlegendes Verständnis über eine Situation oder Thematik haben. Ihr solltet euch dabei eine Reihe an Fragen stellen, um die gebrauchten Informationen zu identifizieren:

*   Verstehe ich das Thema/ die Situation?
    
*   Welche Arten von Informationen gibt es, sind sie relevant und verfügbar?
    
*   Welche Informationen brauche ich, um eine gute Arbeit zu schreiben/ eine aufschlussreiche Präsentation vorzubereiten etc.?
    
*   Welche Art von Information steht in Verbindung zu den Informationen, die ich brauche, und gibt es eine Verknüpfung, die in meiner Situation nützlich wäre?
    
*   Was sind meine erhofften Resultate?
    
*   Wer braucht diese Information? Ich, Kunden/Klienten, Kollegen?
    

Sich diese Fragen zu stellen und sie zu beantworten wird euch helfen, einen besseren Eindruck darüber zu gewinnen, welche Informationen ihr wozu braucht.

Im wirtschaftlichen Umfeld gibt es enorme Mengen an Daten und Informationen. Diese werden in verschiedenen Arten und Situationen genützt, z.B. bei der Suche nach Geschäftspartnern, Lieferanten oder Kunden. Zu wissen, welche Arten von Informationen es gibt, kann euch helfen, euren Informationsbedarf besser und genauer zu erkennen. Einige Arten von Informationen im wirtschaftlichen Umfeld, die am häufigsten benutzt werden, sind:

*   Informationen zu Produkten (technische Fakten, Bewertungen etc.)
    
*   Unternehmensinformationen (Dossiers, Bonität etc.)
    
*   Personeninformationen (Anstellung, Lebenslauf etc.)
    
*   Markt- und Finanzinformationen (Aktienkurse, Markttrends und Entwicklungen etc.)
    
*   Rechtliche Informationen (Gesetzgebung, Richtlinien, Vorschriften etc.)
    
*   Öffentliche Informationen (statistische Daten, Ausschreibungen etc.)
    
*   Presseinformationen/Nachrichten
    
*   Wissenschaftliche und technische Fortschritte
    
*   Soziale Netzwerke
    
*   Etc.
    

Es gibt viele verschiedene Arten von Informationen und nicht alle werden immer für euch und eure Situation relevant sein. Trotzdem ist es gut zu wissen, welche Informationen existieren und wie sie Einfluss nehmen können. Denkt nur daran, wie ihr und eure Kolleginnen und Kollegen das letzte Mal eine Präsentation vorbereiten mussten. Hat jeder die gleichen Informationen bzw. die gleichen Informationsquellen verwendet? Hat jemand eine Verbindung zwischen verschiedenen Informationen geknüpft, die nicht gänzlich offensichtlich war? Wo haben eure Kolleginnen und Kollegen die verwendeten Informationen eingeholt? War die verwendete Darstellung  der Informationen schlüssig oder eher fragwürdig? Über die Vorgehensweise eurer Kolleginnen und Kollegen nachzudenken und dadurch zu lernen kann euch helfen, eure Fähigkeiten bezüglich Informationsbedarf, - abfrage, -bewertung, -verwendung und -kommunikation weiter zu verbessern.

**Übung**

Stellt euch vor, ihr arbeitet in einem Unternehmen, das Saft produziert. Eure Aufgabe ist es zu recherchieren, in welchem Auslandsmarkt ihr euer Produkt als nächstes einführen solltet. Welche Art von Informationen könnte in dieser Situation hilfreich sein, um eine gute Entscheidung zu treffen?

\[Textfeld einfüge\]

Quellen

_Spackman, A., & Camacho, L. (2009)._ _Rendering information literacy relevant: A case-based pedagogy.__The Journal of Academic Librarianship, 35(6), 548-554._