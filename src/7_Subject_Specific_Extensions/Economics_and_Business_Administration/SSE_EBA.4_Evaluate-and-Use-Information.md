**4\. Evaluate and Use Information**

**Introduction**

In this lesson, you will deepen your previous knowledge about how to evaluate and use your gathered information.

**Learning objectives**

At the end of this lesson, learners will be able to:

*   Understand how journals are evaluated
    
*   Understand different kind of rankings and their possible flaws
    
*   Understand why information should be used in an ethical sound way
    

  
  

**4.1 Rankings**

**Rankings of Journals**

Rankings are a possibility to assess the relevance of your content. Nevertheless, rankings are not of the same importance within all disciplines. The same applies to scientific journals. Per example in the German research culture of sociology single-authored books tend to be considered as more valuable than journal articles and rankings are discussed critically. Instead, in economics and business administration, publications in journals are of high importance and journal rankings are the main criteria to assess the quality of research and to value the research output of researchers. There are several journal rankings that can help you to assess the quality of research:

  
  

*   SSCI
    
*   CitEc (RePEc)
    
*   VHB JOURQUAL - https://vhbonline.org/en/service/jourqual/vhb-jourqual-3/
    
*   RePEc Jounrnal Ranking
    
*   Eigenfactor.org
    

  
  

_//German version only_

_Handelsblatt Ranking BWL/VWL_

_//_

  
  

Rankings use different quantitative and/or qualitative measurements to rank journals. The most important one is the so-called impact factor. The impact factor is calculated by dividing the citations of all publications from the journal in the previous two years through the number of articles in the journal in the previous two years. In the meantime, also five-year impact factors are calculated. The higher the resulting impact factor, the higher the relevance of the journal. Nevertheless, you have to be aware that the impact factor only shows the relevance of a journal and does not say anything about the quality of a single article out of the journal. Despite there being various criticism about the impact factor, it is widely used in various disciplines.

  
  

Which rankings are important is not only depending on the discipline, but also on the country. Per example, in the German speaking countries the VHB JOURQUAL, published by the German Academic Association for Business Research (VHB), is a very important ranking for the discipline of economics and business administration. This ranking does rate journals from A+ to D, while A+ refers to outstanding and leading international academic business research journals.

  
  

**Exercise**

Think about the implication of only using rankings of journals to evaluate the quality of an article. Why could this be misleading?

_\[text field\]_

  
  

**Rankings of Researchers**

Similar to rankings of journals, there are also rankings of researchers. These rankings do not have the same relevance as journal rankings do and have caused many critical discussions. Examples for researcher rankings are:

  
  

RePEc Researcher Ranking

_//German only_

_Forscher-Ranking des Handelsblatts_

_//_

  
  

**4.2 Evaluating News and “facts”**

In the last years the term Fake News emerged and became rather popular. Also in a business environment it is important to be aware of such possible untrue but widespread news and facts. As you have already seen in Module 4, there are many parts of information that you can examine to evaluate the credibility of an information. Always ask yourself the questions:

*   Who is author?
    
*   Where does it come from?
    
*   Is it credible?
    
*   Who is publishing it?
    

  
  

**Reflect upon:**

What could be possible intentions of an organization/institution publishing an article etc.?

What problems can fake news cause to a business?

_\[text field\]_

  
  

  
  

**4.3 Ethical use of information**

We already discussed in Module 5 the right and fair way of using information. As you have already learned in unit 5.3 there are many different citation styles you can use. Some citation styles are more common for specific disciplines than other ones. In economics and business administration, especially the APA style and Harvard style are widely used. Which style you have to use is finally depended on the regulations of your instructor, institution or publisher.

You can find more information about the APA and Harvard style here:

APA style: [http://www.apastyle.org/](http://www.apastyle.org/)

Harvard style: [https://www.mendeley.com/guides/harvard-citation-guide](https://www.mendeley.com/guides/harvard-citation-guide)

  
  

Often in the business world, you will also have to consider aspects of intellectual property and privacy rights.

  
  

Intellectual property is intangible property that originates from the human intellect. It is mainly known in terms of copyright, patents and trademarks. It includes trade secrets, publicity rights, discoveries and inventions, but also artistic works like words, phrases, symbols, and designs can be protected by intellectual property rights. Intellectual property rights intent to act as incentive for the creation of intellectual goods and information. It allows people to make profit of their intangible creations by reducing the risk of cheap replication of their intellectual property by others. However, the intangible nature of such kind of property presents it difficulties and also the vagueness of the term intellectual property itself is often criticised.

  
  

Privacy rights are intended to protect the privacy of individuals. They gain of growing relevance due to the growing amount of data that individuals are creating when using different information technology. There are different local regulations that might include the right of individuals to review the data saved by corporations, to demand the deletion or gives the right to completely forbid any data processing of individual data beforehand. In the European Union the General Data Protection Regulation is in force since 2018.

You can find more information about the EU General Data Protection Regulation here: https://en.wikipedia.org/wiki/General\_Data\_Protection_Regulation

  
  

  
  

**Case Study**

  
  

In this part, you will be able to apply some of the previously mentioned information sources and use them to research about corporate social responsibility and sustainability in regards to a specific company. Further, you will get to know some free online tools to analyse social media and compare their possibilities.

  
  

You work as intern for a small manufacturer of car supplies. You receive the task to get some information about larger players in this field. You should start first with Volkswagen AG.

  
  

First, you should find some basic information. You can use some of the information sources mentioned in the previous chapters.

*   How many employees has the company at the moment?
    
*   How much profit/turnover did the company have last year?
    
*   How is recent news coverage about the company?
    
*   How is the current outlook for the share price?
    

_\[text field\]_

  
  

Second, you should look more into the Corporate Social Responsibility regulations.

*   Can you find any information on the company’s webpage?
    
*   Can you find anything on their social media accounts or on social media in general?
    
*   Are there any blogs/forums that deal with this issue?
    

_\[text field\]_

  
  

Third, you should analyse what social media and web users are currently concerned with in regard to Volkswagen. You might have already heard about the terms opinion mining, sentiment analysis or emotion AI. It is a process that tries to identify and quantify subjective information. It is widely applied to analyse the wordings of customer texts (reviews, social media etc.) and present the sentiment/opinion of a statement. Further bellow you can find a list of free or free trial sentiment analysis tools:

*   [http://www.socialmention.com/](http://www.socialmention.com/)
    
*   [http://www.opiniontracker.net/en/](http://www.opiniontracker.net/en/)
    
*   [https://www.csc2.ncsu.edu/faculty/healey/tweet\_viz/tweet\_app/](https://www.csc2.ncsu.edu/faculty/healey/tweet_viz/tweet_app/)
    

  
  

You should now use these three tools to answer the following questions:

Are users still concerned about the emission scandal from 2015?

In which connection is Volkswagen currently mentioned?

In which sentiments are current tweets about Volkswagen at the moment?

_\[text field\]_