              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } 

**Quiz zur fachspezifischen Erweiterung – Wirtschaftswissenschaften**

Wahr oder falsch: Ein Informationsproblem ist immer generisch und kann nicht durch eine Disziplin oder Kultur beeinflusst werden

( ) Wahr

(X) Falsch

Informationskompetenz ist..

( ).. gleichzusetzen mit Computerkompetenz

( ) .. gleichzusetzen mit Bibliothekskompetenz

( ) … gleichzusetzen mit digitaler Kompetenz

(X) nicht dasselbe wie Medienkompetenz, Computerkompetenz, Bibliothekskompetenz oder digitale Kompetenz

Welche Literaturdatenbanken sind auf den Bereich der Wirtschaftswissenschaften spezialisiert?

( ) Web of Science

(x) ProQuest ABI/INFORM

(x) EBSCO Business Source Premier

( ) Eurostat

  
  

Welche der folgenden Informationsquellen ist in den Wirtschaftswissenschaften am bedeutsamsten?

(X) Faktendatenbanken

( ) Pressedatenbanken

( ) Repositorien und Datenarchive

  
  

Zeitschriftenrankings wird eine hohe Relevanz in allen wissenschaftlichen Disziplinen zugeschrieben.

( ) Wahr

(X) Falsch

  

Wie wird der Impact-Faktor einer Zeitschrift berechnet?

( ) Zitationen aller Publikationen der Zeitschrift in diesem Jahr / Anzahl der Artikel der Zeitschrift in diesem Jahr

(X) Zitationen aller Publikationen der Zeitschrift in den letzten zwei Jahren / Anzahl der Artikel der Zeitschrift in den letzten zwei Jahren

( ) Anzahl der Artikel in der Zeitschrift in den letzten zwei Jahren / Zitationen aller Publikationen der Zeitschrift in den letzten zwei Jahren

  

Dein Vorgesetzter hat dir den Auftrag gegeben, die Bewerbung von Hotelzimmern in der Herbstsaison vorzubereiten. Du sollst hierbei ein schönes Foto, welches Weinberge mit farbigen Blättern zeigt, beifügen. Darfst du ein beliebiges Foto, das du durch eine Suche bei Google gefunden hast, verwenden?

( ) Ja

(X) Nein

  
  

Welche Art von Gütern können durch geistiges Eigentumsrecht geschützt werden?

(X) Künstlerische Arbeiten

(x) Entdeckungen und Erfindungen

( ) Gebäude und umliegende Landflächen

(X) Patente