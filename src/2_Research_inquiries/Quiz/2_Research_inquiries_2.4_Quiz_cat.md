**Què he après en aquest Mòdul?**

Preguntes d'opció múltiple

1. Quan es realitza una cerca per al tema assignat, cal conèixer:

[ ] L'edat de la persona que ens va assignar el treball

[ ] Bibliografia bàsica sobre l'assignació de tasques

[x] Camps més amplis i més específics

[ ] Com començar a buscar immediatament a la web, tan ràpid com sigui possible


2. Les xarxes socials són una font fiable d'informació per explorar el tema assignat:

[ ] Sí

[x] Només ocasionalment

[ ] No


3. Tria una acció apropiada per al procés d'identificació del tema i la formulació de les preguntes de recerca:

[ ] Triar un editor de text de codi obert

[x] Pensar en el camp més general i específic del tema

[ ] Trobar el teu interior


4. Tria una acció que sigui apropiada per al procés d'identificació del tema i la formulació de les preguntes de recerca:

[ ] Escollir el navegador

[ ] Posar-nos en contacte amb els nostres sentiments

[x] Prendre notes i organitzar-les


5. Tria una acció que sigui apropiada per al procés d'identificació del tema i la formulació de les preguntes de recerca:

[ ] Llegir alguns llibres de ficció

[x] Pensar en el que ja sabem i en el que ens agradaria saber sobre el tema


6. Tria una acció apropiada per al procés d'identificació del tema i la formulació de les preguntes de recerca:

[x] Fer preguntes de recerca precises i concretes

[ ] Les preguntes de recerca no són necessàries


7. Les paraules clau descriuen els principals temes, conceptes i idees del contingut d'un document i generalment s'escriuen en les teves pròpies paraules:

[x] Cert

[ ] Fals


8. Per establir els termes de cerca, és útil:

[ ] copiar exemples de termes de cerca d'altres temes

[x] investigar i llegir definicions sobre el teu tema


9. Per establir els termes de cerca, és útil:

[ ] utilitzar tantes paraules com sigui possible en la consulta

[x] determinar sinònims


10. Per establir els termes de cerca, és útil:

[x] determinar els termes i temes relacionats

[ ] començar a buscar amb qualsevol terme tan aviat com sigui possible


11. La informació acadèmica actual i revisada per parells sobre algun tema és millor buscar-la en:

[ ] Llibres de text

[x] Bases de dades amb articles científics

[ ] Enciclopèdies

[ ] Portals web

[ ] Diaris


12. Bàsicament, el tipus de font que has triat fer servir depèn del propòsit de la teva tasca de recerca, però hi ha excepcions, qualsevol font pot ser útil depenent de la teva necessitat d'informació.

[x] Cert
[ ] Fals


13. Posa en l'ordre correcte



Ordena les següents activitats dins el procés d'investigació:

- Començar a buscar

- Fer una pluja d'idees sobre el que ja sé sobre el tema de la meva recerca

- Acotar el camp

- Buscar una perspectiva més àmplia del tema

- Pensar en els recursos més apropiats per a la cerca

- Clarificar les preguntes de recerca

- Determinar les paraules clau i els termes de cerca

- Determinar l'àrea o disciplina a la que pertany el meu tema

Escull l'activitat per al **Primer pas (1)**.

Escull l'activitat per al **Segon pas (2)**.

Escull l'activitat per al **Tercer pas (3)**.

Escull l'activitat per al **Quart pas (4)**.

Escull l'activitat per al **Cinquè pas (5)**.

Escull l'activitat per al **Sisè pas (6)**.

Escull l'activitat per al **Setè pas (7)**.

Escull l'activitat per al **Vuitè pas (8)**.



