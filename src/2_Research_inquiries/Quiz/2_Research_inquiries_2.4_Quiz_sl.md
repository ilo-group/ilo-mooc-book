             @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } 

Iz seznama izberite pravilen odgovor.

Lahko dosežete največ 12 točk.

  

Ko iščete informacije na temo seminarske naloge, morate:

  

\[ \] vedeti starost osebe, ki vam je dodelila nalogo.

\[ \] poznati osnovno literaturo izbrane naloge.

\[x\] poznati osnovna in specifična področja.

\[ \] vedeti, kako najti v najkrajšem času bistvene informacije na spletu.

  

Spletna družbena omrežja so informacijski viri, ki omogočajo raziskovanje določene teme.

  

\[ \] Da.

\[x\] Samo včasih.

\[ \] Ne.

  

Iz seznama izberite (1) dejavnost, ki je ob postopku identificiranja teme in postavitve raziskovalnega vprašanja najprimernejša:

  

\[ \] Izberite odprtokodni urejevalnik besedila.

\[x\] Razmislite o osnovnem področju ter sorodnih podpodročjih teme.

\[ \] Najdite svoj notranji jaz.

  

Iz seznama izberite (1) dejavnost, ki je ob seznanitvi s temo in postavitvi raziskovalnega vprašanja najprimernejša:

  

\[ \] Izberite brskalnik.

\[ \] Stopite v stik s svojimi občutki.

\[x\] Ustvarite zabeležke in jih uredite.

  

Iz seznama izberite (1) dejavnost, ki je ob seznanitvi s temo in postavitvi raziskovalnega vprašanja najprimernejša:

  

\[ \] Preberite nekaj romanov.

\[x\] Razmislite o informacijah, ki jih poznate in o tistih, o katerih želite izvedeti več.

Iz seznama izberite (1) dejavnost, ki je ob seznanitvi s temo in postavitvi raziskovalnega vprašanja najprimernejša:

  

\[x\] Postavite natančna raziskovalna vprašanja.

\[ \] Postavitev raziskovalnih vprašanj ni nujna.

  

S ključnimi besedami predstavljamo glavno področje, koncepte in zamisli določene datoteke. Po navadi jih zapišemo z lastnimi besedami.

  

\[x\] Da.

\[ \] Ne.

  

Za določitev iskalnih izrazov moramo …

  

\[ \] povzeti primere iskalnih izrazov, ki so namenjene drugim temam.

\[x\] prebrati opredelitve izbrane teme ali izbranega področja.

  

Za določitev iskalnih izrazov …

  

\[ \] morajo vprašanje vsebovati čim več izrazov.

\[x\] moramo določiti sopomenke.

  

Za določitev iskalnih izrazov moramo …

\[x\] določiti sorodna področja in teme.

\[ \] začeti čim prej z iskanjem kakršnihkoli izrazov.

  

Najnovejše in od strokovnjakov preverjene informacije na določeno teme najdete:

  

\[ \] V učbenikih.

\[x\] V podatkovni zbirki z znanstvenimi članki.

\[ \] V enciklopedijah.

\[ \] Na spletih portalih.

\[ \] V časopisu.

  

Informacijski vir, ki ste ga izbrali, je odvisen od namena raziskovalne naloge. A obstajajo izjeme, saj lahko vsak informacijski vir zadovolji vašo informacijsko potrebo.

  

\[x\] Da.

\[ \] Ne.

  

Določite pravilni vrstni red.

Lahko dosežete največ 8 točk.

  

Določite pravilni vrstni red naslednjih dejavnosti.

  

Iščite informacije.

Viharjenje možganov: Razmislite o informacijah, ki so vam že znane.

Zožite raziskovalno področje

Poiščite osnovne informacije na temo

Pomislite na najprimernejše informacijske vire, ki vam bodo pomagali pri iskanju informacij

Postavite raziskovalno vprašanje

Določite ključne besede in iskalne izraze

Določite področje, h kateremu spada izbrana tema

  

Izberite dejavnost prvega postopka (1).

Iz seznama izberite dejavnost.

Izberite dejavnost drugega postopka (2). within the journey of inquiries:

Izberite dejavnost tretjega postopka (3). within the journey of inquiries:

Izberite dejavnost četrtega postopka (4). within the journey of inquiries:

Izberite dejavnost petega postopka (5). within the journey of inquiries:

Izberite dejavnost šestega postopka (6). within the journey of inquiries:

Izberite dejavnost sedmega postopka (7). within the journey of inquiries:

Izberite dejavnost osmega in zadnjega postopka (8.) within the journey of inquiries: