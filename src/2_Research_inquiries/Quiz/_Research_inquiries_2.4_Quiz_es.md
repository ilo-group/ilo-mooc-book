**¿Qué he aprendido en este Módulo?**

1. Cuando se realiza una búsqueda para el tema asignado, es necesario conocer:

( ) la edad de la persona que nos asignó el trabajo

( ) bibliografía básica sobre la asignación de tareas 

(x) campos más amplios y más específicos 

( ) cómo empezar a buscar inmediatamente en la web, tan rápido como sea posible

2. Las redes sociales son una fuente fiable de información para explorar el tema asignado.

( )Sí

(x)Solo ocasionalmente

( )No

3. Elige una acción apropiada para el proceso de identificación del tema y la formulación de las preguntas de investigación.

( ) Elegir un editor de texto de código abierto

(X) Pensar en el campo más general y específico del tema.

( ) Encontrar tu interior

4. Elige <b>una</b> acción que sea apropiada para el proceso de identificación del tema y la formulación de las preguntas de investigación.

( ) Elegir el navegador

( ) Ponerse en contacto con nuestros sentimientos

(X) Tomar notas y organizarlas

5. Elige una acción que sea apropiada para el proceso de identificación del tema y la formulación de las preguntas de investigación

( ) Leer algunos libros de ficción

(X) Pensar en lo que ya sabemos y en lo que nos gustaría saber sobre el tema

6. Elige **una** acción apropiada para el proceso de identificación del tema y la formulación de las preguntas de investigación.

(X) Hacer preguntas de investigación precisas y concretas 

( ) Las preguntas de investigación no son necesarias

7. Las palabras clave describen los principales temas, conceptos e ideas del contenido de un documento y generalmente se escriben en tus propias palabras.

(X) Verdadero

( ) Falso

8. Para establecer los términos de búsqueda, es útil:

( ) copiar ejemplos de términos de búsqueda de otros temas 

(X) investigar y leer definiciones sobre tu tema

9. Para establecer los términos de búsqueda, es útil:

( ) utilizar tantas palabras como sea posible en la consulta 

(X) determinar sinónimos

10. Para establecer los términos de búsqueda, es útil:

(X) determinar los términos y temas relacionados 

( ) empezar a buscar con cualquier término tan pronto como sea posible

11. La información académica actual y revisada por pares sobre algún tema es mejor buscarla en:

( ) Libros de texto

(X) Bases de datos con artículos científicos

( )Enciclopedias

( ) Portales web

( )Periódicos

12. Básicamente, el tipo de fuente que has elegido usar depende del propósito de tu tarea de investigación, pero hay excepciones, cualquier fuente puede ser útil dependiendo de tu necesidad de información.

(X) Verdadero

( ) Falso

13. Pon en el orden correcto

8 points possible (graded)
Ordena las siguientes actividades dentro del proceso de investigación:
- Comenzar a buscar 
- Hacer una lluvia de ideas sobre lo que ya sé sobre el tema de mi investigación 
- Acotar el campo 
- Buscar una perspectiva más amplia del tema 
- Pensar en los recursos más apropiados para la búsqueda 
- Clarificar las preguntas de investigación 
- Determinar las palabras clave y los términos de búsqueda 
- Determinar el área o disciplina a la que pertenece mi tema