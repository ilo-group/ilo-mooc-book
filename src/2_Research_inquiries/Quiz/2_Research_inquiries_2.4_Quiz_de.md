1) Wenn du eine Recherche zu dem zugewiesenen Thema durchführst, ist es notwendig Folgendes zu wissen:

[ ] Wie alt ist die Person, welche dir die Aufgabe zugewiesen hat?

[ ] Basisliteratur zur Aufgabenverteilung

[x] Allgemeines und spezifischeres Feld

[ ] Wie man so schnell wie möglich mit der Suche im Web beginnt


2) Soziale Netzwerke sind eine zuverlässige Informationsquelle für die Erforschung des zugewiesenen Themas.

[ ] Ja

[x] Nur manchmal

[ ] Nein




3) Wähle eine (1) Aktion, die für den Prozess der Identifizierung des Themas und der Formulierung von Forschungsfragen geeignet ist:

[ ] Einen Open-Source-Texteditor auswählen


[x] Denke an das allgemeine und spezifischere/engere Feld des Themas.


[ ] Finde dein inneres Selbst.


4) Wähle eine (1) Aktion, die für den Prozess der Identifizierung des Themas und der Formulierung von Forschungsfragen geeignet ist:


[ ] Wähle den Browser aus

[ ] Konzentriere dich auf deine Gefühle

[x] Mache dir Notizen und organisiere sie


5) Wähle eine (1) Aktion, die für den Prozess der Identifizierung des Themas und der Formulierung von Forschungsfragen geeignet ist:

[ ] Lies ein paar Romane.

[x] Denk darüber nach, was du bereits weißt und was du über das Thema erfahren willst. 


6) Wähle eine (1) Aktion, die für den Prozess der Identifizierung des Themas und der Formulierung von Forschungsfragen geeignet ist:

[x] Stelle präzise und konkrete Forschungsfragen.

[ ] Forschungsfragen sind nicht notwendig.



7) Keywords beschreiben Hauptthemen, Konzepte und Ideen im Inhalt eines Dokuments und werden in der Regel in eigenen Worten geschrieben.

[x] Wahr

[ ] Falsch


8) Um Suchbegriffe zu bestimmen, ist es sinnvoll:

[ ] Suchbegriffsbeispiele aus anderen Themenbereichen zu kopieren

[x] Definitionen zu deinem Thema zu recherchieren und zu lesen


9) Um Suchbegriffe zu bestimmen, ist es sinnvoll:

[ ] so viele Wörter wie möglich in der Anfrage zu verwenden

[x] Synonyme zu bestimmen


10) Um Suchbegriffe zu bestimmen, ist es sinnvoll:

[x] verwandte Begriffe und Themen festzulegen

[ ] mit der Suche nach beliebigen Begriffen so schnell wie möglich zu beginnen


11) Aktuelle und von Experten geprüfte wissenschaftliche Informationen zu einem bestimmten Thema sind am besten zu finden in:

[ ] Lehrbüchern

[x] Datenbanken mit wissenschaftlichen Artikeln

[ ] Enzyklopädien

[ ] Webportale

[ ] Zeitungen



12) Grundsätzlich hängt die Art der von dir gewählten Quelle vom Ziel deiner Forschungsaufgabe ab. Es gibt aber Ausnahmen, denn jede Quelle kann, je nach Informationsbedarf, nützlich sein.

[x] Wahr

[ ] Falsch


13.1) Bringe die folgende Auflistung in die  richtige Reihenfolge, wähle den 1. Schritt. 

Stelle die folgenden Aktivitäten im Rahmen der Anfragebearbeitung in die richtige Reihenfolge:

- Suche starten 
- Brainstorming, was ich bereits über das Thema meiner Forschung weiß. 
- Einengung des Feldes 
- Suche nach einen allgemeineren Einblick in das Thema.
- Überlege, welche Ressourcen für die Suche am besten geeignet sind
- Formulierung der Forschungsfragen
- Bestimmung die Keywords und Suchbegriffe.
- Bestimmung des Gebiets oder der Disziplin, zu der mein Thema gehört


Wähle die Aktivität für den ersten Schritt (1).
Wähle eine Option



13.2) Zweiter Schritt

Wähle die Aktivität für den zweiten Schritt (2.) innerhalb der Bearbeitung der Anfragen:


13.3) Dritter Schritt
Wähle die Aktivität für den dritten Schritt (3.) innerhalb der Bearbeitung der Anfragen:


13.4) Vierter Schritt

Wähle die Aktivität für den vierten Schritt (4.) innerhalb der Bearbeitung der Anfragen:


13.5) Fünfter Schritt

Wähle die Aktivität für den fünften Schritt (5.) innerhalb der Bearbeitung der Anfragen:


13.6) Sechster Schritt

Wähle die Aktivität für den sechsten Schritt (6.) innerhalb der Bearbeitung der Anfragen:


13.7) Siebter Schritt

Wähle die Aktivität für den siebten Schritt (7.) innerhalb der Bearbeitung der Anfragen:


13.8) Acht Schritte

Wähle die Aktivität für den letzten, achten Schritt (8.) innerhalb der Bearbeitung der Anfragen:
