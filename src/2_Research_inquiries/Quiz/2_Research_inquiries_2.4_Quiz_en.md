Choose one answer per question

12 points possible

1) When doing a research for the assigned topic, it is necessary to know:

[ ] how old is the person who assigned the task
[ ] basic literature about assigning the tasks
[x] broader and narrower field
[ ] how to immediately start searching the web, as fast as you can


2) Social networks are a reliable source of information for exploring the assigned topic

[ ] Yes
[x] Only sometimes
[ ] No


3) Choose one (1) action that is appropriate for the process of identification of the topic and formulation of research questions:

[ ] Choose an open source text editor
[x] Think about the narrower and wider field of the topic
[ ] Find your inner self


4) Choose one (1) action that is appropriate for the process of identification of the topic and formulation of research questions:

[ ] Choose the browser
[ ] Get in touch with your feelings
[x] Make notes and organize them


5) Choose one (1) action that is appropriate for the process of identification of the topic and formulation of research questions:

[ ] Read a few fiction books
[x] Think what you already know and would you like to know about the topic


6) Chose one (1) action that is appropriate for the process of identification of the topic and formulation of research questions:

[x] Ask precise and concrete research questions
[ ] Research questions are not necessary


7) Keywords describe main topic, concepts and ideas in the content of a document and usually are written in your own words.

[x] True
[ ] False


8) To determine search terms, it is useful to:

[ ] copy search term examples from other topics
[x] read the definitions of your topic or theme


9) To determine search terms, it is useful to:

[ ] use as many words as possible in your query
[x] determine synonyms


10) To determine search terms, it is useful to:

[x] determine related terms and themes
[ ] start searching with any terms as soon as possible


11) Current and peer-reviewed scholarly information on some subject is best to look in:

[ ] Textbooks
[x] Databases with scientific articles
[ ] Encyclopedia
[ ] Web portals
[ ] Newspapers


12) Basically, the kind of source you have chosen to use depends on the purpose of your research task, but there are exceptions, any source can be useful depending on your information need.

[x] True
[ ] False



Put in right order

8 points possible

Put in right order the following activities within the journey of inquiries:
- Start searching 
- Brainstorming what I already know about the topic of my research 
- Narrow down the field 
- Look for a wider picture of the topic 
- Think about the most appropriate resources for searching 
- Clarify the research questions 
- Determine the keywords and search terms 
- Determine the area or discipline that my topic belongs to

Choose the activity for the First step (1)

Choose an option

Choose the activity for the second step (2.) within the journey of inquiries:

Choose the activity for the third step (3.) within the journey of inquiries:

Choose the activity for the fourth step (4.) within the journey of inquiries:

Choose the activity for the fifth step (5.) within the journey of inquiries:

Choose the activity for the sixth step (6.) within the journey of inquiries:

Choose the activity for the seventh step (7.) within the journey of inquiries:

Choose the activity for the final, eight step (8.) within the journey of inquiries:



