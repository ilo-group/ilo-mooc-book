---
title: Raziskovanje je potovanje skozi spraševanje
permalink: /2_Inquiries/2_Intro_sl/
eleventyNavigation:
    order: 1
    parent: 2_Inquiries
---


**Modul 2: Raziskovanje je potovanje skozi spraševanje**

  

Kadar dobite zadolžitev, ki vključuje postopek iskanja informacij ali kadar nekaj raziskujete in iščete informacije, morate, preden dejansko začnete z iskanjem, določiti širše ter ožje področje in čim natančnejša raziskovalna vprašanja — to vam bo v korist pri iskanju ustreznih informacijskih virov.

Ko boste torej morali napisati seminarsko nalogo ali potrebujete neke informacije, vam svetujemo, da ne začnete takoj z iskanjem informacij. Brez skrbi, našli boste ustrezne vire — obstaja jih veliko, mogoče celo preveč. Najprej **RAZMISLITE** o svojih informacijskih potrebah — kakšne informacije so vam potrebne, za kakšen namen, h kateremu področju pripadajo, kje bi se lahko nahajale in kako jih dobiti?

V tem modulu se boste naučili, **kako formulirati ustrezna vprašanja**, ki vodijo k zadovoljitvi vaše informacijske potrebe. To lahko primerjamo s preiskovalnim potovanjem, ki zahteva postopno preoblikovanje iskalnih nizov, področij in prepoznavanje vrzeli v znanju, ki se jih bomo naučili dopolniti.

  

**Učni cilji:**

*   spoznavanje informacijskih potreb in ravnanja glede njih
    
*   sposobnost (pre-)oblikovanja raziskovalnih vprašanj, ki temeljijo na določeni temi
    
*   razvijanje sposobnosti samostojnega in vseživljenjskega učenja
    
*   razvijanje veščin akademske pismenosti in učenja učenja
    
*   spoznavanje proaktivnega pristopa do raziskovanja
    

  

Če vam ni jasno, h kateremu področju pripada izbrana tema in kje najti ustreznih informacij, vam svetujemo, da se še ne lotite z iskanjem informacij - kaj storiti s tolikšnimi zadetki? _(Opomba za oblikovanje: zadnji odstavek naj bo v obliki oblaka, v katerem vidite na levi strani osebo, ki govori. Pod oblakom je dodan posnetek zaslona, kjer vidite iskalno polje brskalnika Google.)_