---
title: La investigación es un viaje de preguntas
permalink: /2_Inquiries/2_Intro_es/
eleventyNavigation:
    order: 1
    parent: 2_Inquiries
---

**Módulo 2: La investigación es un viaje de preguntas**

**Introducción**

Cuando tienes que hacer un trabajo que requiere buscar información o deseas realizar alguna investigación que implique la búsqueda de información, antes de comenzar a buscar información es importante determinar el campo más amplio, el tema y las preguntas precisas que podrían conducirte a las fuentes de información relevantes. Por lo tanto, cuando tengas que hacer un trabajo o necesites alguna información, no comiences inmediatamente con el proceso de búsqueda. No te preocupes por encontrar fuentes, hay muchas, ¡quizás demasiadas! Por lo tanto, PIENSA de antemano en qué tipo de información necesitas, para qué propósito, a qué ámbito pertenece, dónde se encuentra y cómo podría recuperarse.

En este módulo aprenderás a formular las preguntas correctas para abordar una necesidad de información específica. Es un viaje de investigación de reformulación gradual de las preguntas, definiendo tus temas de interés, reconociendo tus lagunas de conocimiento y aprendiendo cómo abordarlas.

**Resultados del aprendizaje:**

*   reconocer las necesidades de información y cómo abordarlas
*  ser capaz de formular y reformular preguntas de investigación basadas en la exploración de tu tema
*  potenciar las capacidades para el aprendizaje independiente y el aprendizaje a lo largo de toda la vida
*  desarrollar habilidades académicas de lectura y aprender habilidades de aprendizaje
*  mostrar un enfoque proactivo de la investigación

Si no sabes a qué área pertenece tu tema y dónde se puede encontrar la información, es mejor no comenzar con la búsqueda, porque podrías no saber qué hacer con la gran cantidad de resultados que posiblemente recuperarías.