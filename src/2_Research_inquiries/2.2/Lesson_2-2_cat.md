**2.2 Idees clau de recerca i paraules clau derivades de les preguntes de recerca**

**Introducció a la Lliçó 2.2**

Al final d'aquesta lliçó, seràs capaç de:

*  posicionar les preguntes de recerca en una àrea o disciplina, permetent així establir els conceptes bàsics
*  identificar les idees clau de la recerca
*  formular les paraules clau de la recerca

**2.2.1**

En aquesta lliçó seguiràs desenvolupant les preguntes de recerca i seràs capaç d'extreure de forma clara les idees clau que guiaran les teves activitats de recerca d'informació.

Imagina-t'ho com si fossin punts de partida i passos…

Conec el tema ⟾ Conec l'àrea i les disciplines que s'ocupen d'ell ⟾ Conec les preguntes específiques de recerca ⟾ Escric les **paraules clau i termes de cerca** derivats del tema i de les preguntes de recerca.

_[VIDEO, here]_

**2.2.2**

Les paraules clau i els termes de cerca són conceptes diferents.

Les **paraules clau** són les paraules o frases més significatives per al tema en què estàs treballant.

El **terme de cerca** és el que es escrius en el quadre de cerca del cercador o base de dades.

De vegades un terme de recerca és igual a una paraula clau.

Anem a conèixer amb més detall quina és la diferència entre paraules clau i termes de cerca i què cal saber abans de començar a buscar informació:

Les **paraules clau** descriuen el tema principal, els conceptes i idees en el contingut d'un document i generalment s'escriuen amb les teves pròpies paraules. Les paraules clau poden ser una sola paraula o una frase i poden trobar-se dins d'un títol d'un document, en el text sota d'una imatge o en qualsevol lloc on es trobi informació important.

Quan utilitzes paraules clau dins d'una base de dades, de vegades pots trobar paraules clau adients en vocabularis o tesaures controlats que poden ajudar-te a triar i formular paraules o frases de cerca apropiades.

En introduir una paraula clau en el quadre de cerca d'un catàleg de biblioteca o una base de dades, els resultats han d'incloure tots els registres que continguin aquest terme en el títol, autor, tema (si estàs buscant en un catàleg en línia) o dins del resum, cos del text, peus de foto en gràfics, imatges, taules, notes al peu de pàgina, etc. (Per exemple, si estàs buscant en una base de dades a text complet).

Per obtenir resultats més precisos, hauries transformar les teves paraules clau o frases en termes de cerca. Pensa en quines paraules clau faria servir un autor en escriure sobre el seu tema. Per crear termes de cerca de bona qualitat, has d'utilitzar tècniques de recerca avançades que podràs trobar al Mòdul 3.

El **terme de cerca** t'ajuda a refinar la cerca per obtenir resultats més precisos i adequats que cercar només per paraules clau. 

De vegades les paraules clau i els termes de cerca són els mateixos, però de vegades cal crear nous termes per trobar els resultats que necessites.

**2.2.3**

Fixa't en el tema i les preguntes de recerca de l'escenari anterior ([Lliçó 2.1](https://informationliteracy.eu/courses/course-v1:ILO+ILOEng+2018_1/courseware/37d9415583ff4c88ba79659e8afbf788/6b8457fdedec4fb1820442c4615a37f6/1?activate_block_id=block-v1%3AILO%2BILOEng%2B2018_1%2Btype%40vertical%2Bblock%40dbabf3a171914fccaedf15b6b634db7b)). Fes clic a Lliçó 2.1 per refrescar la teva memòria sobre quin era el tema.

Els suggeriments de <span style="color: #339966;"><span style="color: #99cc00;">**paraules clau** estan en verd</span>:</span>

Tema: Aplicació de l'energia solar a les llars

➢ Quines <span style="color: #99cc00;">**stecnologies solars*</span> hhan estat desenvolupades en els últims cinc anys per a edificis d'habitatges?

➢ WQuins requisits hi ha en el disseny i la <span style="color: #99cc00;">**cconstrucció d'edificis d'habitatges*</span> wpel que fa a l'ús de l'energia solar?

➢  Quins són els possibles avantatges i desavantatges comparatius dels diferents <span style="color: #99cc00;">**ssistemes i tecnologies solars*</span> iquant a la seva eficàcia?

<span style="color: #3366ff;">**Els termes de cerca**</span> podrien ser els següents:

<span style="color: #000000;">✓ Energia solar a la llar</span>

<span style="color: #000000;">✓ EEficàcia dels sistemes solars</span>

<span style="color: #000000;">✓ Avantatges de l'ús de l'energia solar</span>

<span style="color: #000000;">✓ Desavantatges de l'ús de l'energia solar</span>

<span style="color: #000000;">✓ Construcció d'edificis d'habitatges</span>

**2.2.4**

Després de definir les paraules clau i els termes de recerca, fes una llista de sinònims, ja que són necessaris per dirigir la teva cerca d'informació.

Els sinònims són paraules del mateix idioma que tenen el mateix o gairebé el mateix significat. Per exemple, les paraules bell, atractiu i bonic podrien ser sinònims.

Observa'n alguns exemples!

➢ <span style="color: #0000ff;">huge </span>and <span style="color: #ff9900;">big </span>

➢ <span style="color: #3366ff;">start</span> and <span style="color: #ff9900;">begining </span>

➢ <span style="color: #0000ff;">garbage</span> and <span style="color: #ff9900;">vaste</span> and <span style="color: #ff00ff;">trash</span> ...

**2.2.5**

Més tard, una vegada hagis d'iniciar-te en l'execució de les cerques (en el Mòdul 3), tingues en compte aquests criteris:

Si obtens massa resultats de cerca en alguna consulta, intenta limitar la cerca. Per exemple: si utilitzes el terme de cerca energia, és possible que obtinguis massa resultats, cosa que exigeix molt de temps per navegar per ells. Aquest terme de cerca es pot acotar, per exemple: energia renovable, energia solar, energia potencial, energia de camp, etc. Un exemple més simple és: la paraula clau animals pot ser acotada: animals salvatges o animals domèstics.

Si obtens menys resultats dels que esperaves, la consulta pot ampliar-se. Per exemple, si vas usar el terme de cerca roses i vas obtenir molt pocs resultats, prova amb el terme flors. La cerca és un procés iteratiu: al mateix temps que reformulem els termes de cerca, de vegades també reformem les nostres paraules clau, o fins i tot les nostres preguntes de recerca.

En el proper Mòdul 3 aprendràs més sobre com combinar termes de cerca o consultes de cerca mentre practiques la cerca d'informació.

**2.2.6**

Passos bàsics per estar preparat per fer cerques:

Genera paraules clau i termes de cerca a partir de les teves preguntes de recerca. Mentre ho fas:

*  Utilitza una combinació de paraules clau que representi conceptes clau
*  Utilitza els sinònims de les paraules clau
*  Amplia el terme amb termes més generals per obtenir més informació a la consulta de cerca.
*  Acota els termes si vols obtenir menys informació

**2.2.7**

**Test**

**Si haguessis d'escriure un treball sobre els hàbits alimentaris dels gats salvatges europeus…**

1. Escull tres paraules clau adients:

( ) carnívor, Europa, menjar _¡Ups, prova de nou!_

( ) animals, tigre, muntanyes _¡Ups, prova de nou!_

(x) gat salvatge, hàbits alimentaris, Europa _Sí, és correcte!_

( ) riu, bosc, gat salvatge _¡Ups, prova de nou!_

2. Escull dos sinònims per al terme hàbits alimentaris:

Has de seleccionar totes les respostes correctes. 

[X] hàbits d'alimentació _selected: Sí, A és correcta, però n'hi ha una més!_

[ ] menjar

[X] nutrició _selected: Sí, C és correcta, però n'hi ha una més!_

[ ] dinar


_((A C)) Sí, és correcte! _

3. Per ampliar la cerca, selecciona un terme de cerca adient per al terme gat salvatge:

( ) Wildcat Ted Grant _¡Ups, prova de nou!_

( ) Felis silvestris _¡Ups, prova de nou!_

(x) gat _Sí, és correcte!_

( ) gat com a mascota _¡Ups, prova de nou!_