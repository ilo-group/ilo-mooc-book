2.2.0

**2.2 Wichtige Forschungsideen und Schlüsselwörter aus Forschungsfragen**

Am Ende dieser Lektion könnt ihr Folgendes:

* Forschungsfragen in einem Bereich oder einer Disziplin einordnen, was die Bestimmung von Grundkonzepten ermöglicht
* Die wichtigsten Forschungsideen identifizieren
* Wissen, wie man Schlüsselwörter der Forschung formuliert

**2.2.1**

In dieser Lektion werdet ihr weitere Punkte über Forschungsfragen lernen und in der Lage sein, klare Schlüsselideen abzuleiten, die eure Informationssuche leiten.

Stellt euch folgende Ausgangspunkte und Schritte vor....

* Ich kenne das **Thema** 
* Ich kenne den **Bereich** und die **Disziplinen**, die sich damit befassen
* Ich kenne die konkreten **Forschungsfragen**
* Ich erstelle **Keywords** und **Suchbegriffe**, die aus dem Thema und Forschungsfragen abgeleitet werden.


**2.2.2**

Es gibt einen Unterschied zwischen Schlagwörtern/Keywords und Suchbegriffen.

**Schlagwörter** sind die wichtigsten Wörter oder Phrasen für das Thema, an dem ihr arbeitet.

Ein **Suchbegriff** ist das, was ihr in das Suchfeld der Suchmaschine oder Datenbank eingibt. 

Manchmal gleicht ein Suchbegriff dem Schlüsselwort.

Erfahrt mehr über den Unterschied zwischen Keywords und Suchbegriffen und was ihr wissen müsst, bevor ihr mit der Suche nach Informationen beginnt!

**Schlagwörter** beschreiben Hauptthemen, Konzepte und Ideen im Inhalt eines Dokuments und werden in der Regel in eigenen Worten beschrieben. Schlüsselwörter können ein einzelnes Wort oder eine Phrase sein und können innerhalb eines Titels eines Dokuments, eines Textes unterhalb eines Bildes oder an jedem Ort gefunden werden, an dem sich wichtige Informationen befinden.

Wenn ihr Schlagwörter innerhalb einer Datenbank verwendet, könnt ihr manchmal geeignete Schlüsselwörter in kontrollierten Vokabularen oder Thesauri finden, die euch bei der Auswahl und Formulierung geeigneter Suchbegriffe oder Phrasen helfen.

Wenn ihr ein Schlagwort in das Suchfeld des Bibliothekskatalogs oder der Datenbank eingebt, enthalten die Ergebnisse alle Einträge, in welchen das Schlagwort sich im Titel, Autor, Betreff (in Online-Katalogen) oder im Abstract, Textkörper, Bildunterschriften unter Grafiken, Bildern, Tabellen, Fußnoten usw. befindet. (z.B. in  Volltextdatenbanken).

Wenn ihr genauere Ergebnisse erzielen möchtet, solltet ihr eure Keywords oder Phrasen in Suchbegriffe umwandeln. Stellt euch vor, welche Keywords ein Autor verwenden würde, wenn er über sein Thema schreibt.

Um qualitativ hochwertige Suchbegriffe zu erstellen, solltet ihr erweiterte Suchtechniken verwenden, die im  Modul 3 vorgestellt werden.

**Der Suchbegriff** hilft euch eure Suche zu verfeinern, um genauere und adäquatere Ergebnisse zu erzielen, als nur die Suche nach Stichworten.

Manchmal sind Schlagwörter und Suchbegriffe identisch, aber manchmal müssen neue Begriffe angelegt werden, um die gewünschten Ergebnisse zu finden.


**2.2.3**


Betrachtet das Thema und die Forschungsfragen aus dem vorherigen Szenario ([Lektion 2.1](https://informationliteracy.eu/courses/course-v1:ILO+ILOEng+2018_1/Kursware/37d9415583ff4c88ba79659e8afbf788/6b8457fdedec4fb1820442c4615a37f6/1?).activate_block_id=block-v1%3AILO%2BILOEng%2B2018_1%2Btype%40vertical%2Block%40dbabf3a171914fccaedf15b6b634db7b))). Klickt auf Lektion 2.1, um euer Gedächtnis bezüglich des Themas aufzufrischen.

Vorschläge für die <span style="color: #339966;"><span style="color: #99cc00;">**Schlagwörter** sind grün</span>:</span>:</span>>

Thema: Nutzung der Sonnenenergie in Haushalten

➢ Welche <span style="color: #99cc00;">**solare Technologien**</span> wurden in den letzten fünf Jahren für Wohngebäude entwickelt?

➢ Welche Anforderungen gibt es an das Design und den Bau von Wohngebäuden bezüglich der Solarenergienutzung?

➢ Was sind die möglichen Vor- und Nachteile verschiedener <span style="color: #99cc00;">**Solarsysteme und -Technologien**</span> in Bezug auf ihre Wirksamkeit?

<span style="color: #3366ff;">**Suchbegriffe**</span> könnten wie folgt lauten:

<span style="color: #00000000;">✓ Solarenergie im Haushalt</span>

<span style="color: #00000000;">✓ Effektivität von Sonnensystemen</span>

<span style="color: #00000000;">✓ Vorteile der Solarenergienutzung</span>

<span style="color: #00000000;">✓ Nachteile der Solarenergienutzung</span>

<span style="color: #00000000;">✓ Wohngebäudebau</span>


**2.2.4**

Nachdem ihr eure Schlagwörter und Suchbegriffe definiert habt, schreibt eine Liste mit  **Synonymen**, welche eure Informationssuche beeinflussen und für diese notwendig sind.
Synonyme sind Wörter der gleichen Sprache, welche die gleiche oder fast die gleiche Bedeutung haben. Zum Beispiel können „schöne“, „attraktive“ und „hübsche“ Synonyme sein.
Schaut euch einige **Beispiele** an!

➢ <span style="color: #0000ff;">groß </span>und <span style="color: #ff9900;">riesig </span>

➢ <span style="color: #3366ff;">Start</span> und <span style="color: #ff9900;">Beginn </span>

➢ <span style="color: #0000ff;">Müll</span> und <span style="color: #ff9900;">Abfall</span>...

**2.2.5**

Wenn ihr die Suche später beginnt (Modul 3), denkt an diese Ansätze:

Wenn ihr bei einer Suchanfrage **zu viele Suchergebnisse** erhaltet, versucht eure Anfrage einzugrenzen.

Zum Beispiel: Wenn ihr den Suchbegriff „Energie“ verwendet, ist es möglich, dass ihr zu viele Ergebnisse erhaltet deren Bearbeitung viel Zeit in Anspruch nimmt. Dieser Suchbegriff kann eingegrenzt werden, z.B.: erneuerbare Energien, Solarenergie, potenzielle Energien, Feldenergie, etc. Ein einfacheres Beispiel ist das Stichwort Tiere, welches eingegrenzt werden kann: Wildtiere oder Haustiere.

Wenn ihr **weniger Ergebnisse** als erwartet erhalten habt, kann die Abfrage erweitert werden.

Wenn ihr beispielsweise den Suchbegriff „Rosen“ verwendet und zu wenig Ergebnisse erhalten habt, versucht es mit dem Begriff Blumen. (Re)search ist ein iterativer Prozess - bei der Überarbeitung der Suchbegriffe optimieren wir manchmal ebenfalls unsere Keywords oder wählen sogar eine neue Forschungsfrage.

Im nächsten Modul 3 erfahrt ihr mehr darüber, wie ihr Suchbegriffe oder Suchanfragen kombinieren könnt, während ihr die Suche nach Informationen übt.

**2.2.6**

Grundlegende Schritte **zur Vorbereitung der Suche**:

Generiert Schlagwörter und Suchbegriffe aus euren Forschungsfragen. Während ihr das tut:

* Verwendet eine Kombination von Schlagwörtern, welche die Schlüsselkonzepte darstellen
* Verwendet Synonyme dieser Schlagwörter
* Erweitert den Begriff um weitere Begriffe, um mehr Informationen über die Suchanfrage zu erhalten
* Schränkt die Begriffe ein, wenn ihr weniger Informationen erhalten möchtet

**2.2.7 Quiz**

Wenn du eine Arbeit über die Ernährungsgewohnheiten europäischer Wildkatzen schreiben würdest......

1. Wähle drei geeignete Keywords aus:
() Fleischfresser, Europa, essen __Ups, noch einmal versuchen!}} 
() Tiere, Tiger, Berge __Ups, versuche es noch einmal!}} 
(x) Wildkatze, Ernährungsgewohnheiten, Europa __Ja, das ist richtig!}}
() Fluss, Wald, Luchs __Ups, versuche es noch einmal!}}

2. Wähle zwei Synonyme für den Begriff Ernährungsgewohnheiten:
Du musst alle richtigen Antwortmöglichkeiten auswählen.
[x] Essgewohnheiten __ Ja, A ist richtig, aber es gibt noch einen mehr!}}}} 
[ ] Mahlzeit__Ups, versuche es noch einmal!}}
[x] Ernährung __Ja, C ist richtig, aber es gibt noch einen mehr!}}}} 
[ ] Mittagessen __Ups, versuche es noch einmal!}}

3. Um deine Suche zu erweitern, wähle einen geeigneten Suchbegriff für den Begriff Wildkatze:
() Wildkatze Ted Grant __Ups, versuche es noch einmal!}}
( ) Felis silvestris __Ups, versuche es noch einmal!}} 
(x) Katze __Ja, das ist richtig!}} 
() Katze wie ein Haustier __Ups, versuche es noch einmal!}}