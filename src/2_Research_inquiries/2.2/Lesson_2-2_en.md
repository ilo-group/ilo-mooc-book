2.2.0

**2.2 Key research ideas and keywords derived from research questions**

At the end of this lesson, you will be able to:

*   position research questions in an area or a discipline, thus enabling determination of basic concepts
*   identify key research ideas
*   know how to formulate keywords of the research

2.2.1

In this lesson you will think further into research questions and be able to **clearly derive key ideas** that will lead your information searching activities.

Imagine this as starting points and steps …

I know the **topic** ⟾ I know the **area** and the **disciplines** that deal with it ⟾ I know the concrete **research questions** ⟾ I create **keywords** and **search terms** derived from the topic and research questions

[VIDEO, here]

2.2.2

There is a difference between keywords and search terms.

**Keywords** are most significant words or phrases for the topic you are working on.

**Search term** is what you type in the search box of the search engine or database.

Sometimes a search term is equal to a keyword.

Find out in more detail what is the difference between keywords and search terms and what do you need to know before starting to search for information!

**Keywords** describe main topic, concepts and ideas in the content of a document and usually are written in your own words. Keywords can be a single word or a phrase and can be found within a title of a document, text under image or in any place where significant information lies.

When you use keywords within a database, sometimes you can find proper keywords in controlled vocabularies or thesauri that may help you choose and formulate appropriate search words or phrases.

When you enter a keyword into the search box of library catalogue or database, the results will contain all records with that term found within the title, author, subject (if you are searching an online catalog) or within abstract, body of text, captions under graphs, images, tables, footnotes etc. (e.g. if you are searching in a full text database).

If you want to get more accurate results, you should transform your keywords or phrases into search terms. Think what keywords would an author use when writing on their topic.

In order to create good quality search terms, you should use advanced search techniques that you can find in Module 3.

**Search term** helps you refine your search in order to get more accurate and adequate results than searching only by keywords.

Sometimes keywords and search terms are the same but sometimes you have to create new terms in order to find results you need.

2.2.3

Look at the topic and research questions from the previous scenario ([Lesson 2.1](https://informationliteracy.eu/courses/course-v1:ILO+ILOEng+2018_1/courseware/37d9415583ff4c88ba79659e8afbf788/6b8457fdedec4fb1820442c4615a37f6/1?activate_block_id=block-v1%3AILO%2BILOEng%2B2018_1%2Btype%40vertical%2Bblock%40dbabf3a171914fccaedf15b6b634db7b)). Click on Lesson 2.1 to refresh your memory on what was the topic.

Suggestions for the <span style="color: #339966;"><span style="color: #99cc00;">**keywords** are in green</span>:</span>

Topic: Application of solar energy in households

➢ What <span style="color: #99cc00;">**solar technologies**</span> have been developed for residential buildings over the past five years?

➢ What requirements are there in the design and <span style="color: #99cc00;">**construction of residential buildings**</span> with regard to solar energy use?

➢ What are the possible comparative advantages and disadvantages of different <span style="color: #99cc00;">**solar systems and technologies**</span> in regards to their effectiveness?

<span style="color: #3366ff;">**Search terms**</span> could be as follows:

<span style="color: #000000;">✓ Solar energy in household</span>

<span style="color: #000000;">✓ Effectiveness of solar systems</span>

<span style="color: #000000;">✓ Advantages of solar energy usage</span>

<span style="color: #000000;">✓ Disadvantages of solar energy usage</span>

<span style="color: #000000;">✓ Residential buildings construction</span>

2.2.4

After you defined your keywords and search terms, mark down the list of **synonyms**, as they are necessary for directing your information search.

Synonyms are words of the same language that have the same or nearly the same meaning. For example, words beautiful, attractive and pretty could be synonyms.

Look at some **examples**!

➢ <span style="color: #0000ff;">huge </span>and <span style="color: #ff9900;">big </span>

➢ <span style="color: #3366ff;">start</span> and <span style="color: #ff9900;">begining </span>

➢ <span style="color: #0000ff;">garbage</span> and <span style="color: #ff9900;">vaste</span> and <span style="color: #ff00ff;">trash</span> ...

2.2.5

Once you start the search later on (Module 3) have in mind these approaches:

If you get **too many search results** on some search query, try to narrow down your query.

For example: if you use the search term energy, it is possible that you will get too many results, which demands a lot of time to browse it. This search term can be narrowed, for example: renewable energy, solar energy, potential energy, field energy, etc. A simpler example is: keyword animals can be narrowed: wild animals or domestic animals.

If you get **fewer results** than you expected, query can be broadened.

For example, if you used search terms roses and get too few results try with the term flowers. (Re)search is an iterative process - while reshaping the search terms we sometimes reshape our keywords too, or even our research questions.

In the next Module 3 you will learn more how to combine search terms or search queries while practicing search for information

2.2.6

Basic steps **to be prepared for searching**:

Generate keywords and search terms from your research questions. While doing that:

*   Use a combination of keywords that represent key concepts
*   Use the synonyms for these keywords
*   Expand the term with broader terms in order to get more information on your search query
*   Narrow the terms if you want to get less information

2.2.7

Quiz

If you were to write an assignment about feeding habits of European wild cats...

1. Choose three appropriate keywords:
( ) carnivore , Europe, eating __Ups, try again!}} 
( ) animals, tiger, mountains __Ups, try again!}} 
(x) wildcat, feeding habits, Europe __Yes, this is correct!}} 
( ) river, wood, bobcat __Ups, try again!}}

2. Choose two synonyms for the term feeding habits:
>>You must choose all answer choices correctly.<< 
[x] eating habits __ selected: Yes, A is correct, but there is one more!}} 
[ ] meal 
[x] nutrition __ selected: Yes, C is correct, but there is one more!}} 
[ ] lunch 
__ ((A C)) Yes, this is correct! }}

3. In order to broaden your search choose an appropriate search terms for the term wild cat:
( ) wild cat Ted Grant __Ups, try again!}}
( ) Felis silvestris __Ups, try again!}} 
(x) cat __Yes, this is correct!}} 
( ) cat like a pet __Ups, try again!}}