

**2.2 Ideas clave de investigación y palabras clave derivadas de las preguntas de investigación**

**Introducción a la Lección 2.2**

Al final de esta lección, serás capaz de:

* posicionar las preguntas de investigación en un área o disciplina, permitiendo así la determinación de conceptos básicos
* identificar las ideas clave de la investigación
* formular las palabras clave de la investigación

**2.2.1**

En esta lección seguirás desarrollando las preguntas de investigación y serás capaz de extraer de forma clara las ideas clave que guiarán tus actividades de búsqueda de información.

Imagínatelo como si fueran puntos de partida y pasos...

Conozco el tema ⟾ Conozco el área y las disciplinas que se ocupan de él ⟾ Conozco las preguntas específicas de investigación ⟾ Escribo las palabras clave y términos de búsqueda derivados del tema y de las preguntas de investigación.
_[VIDEO, here]_

**2.2.2**

TLas palabras clave y los términos de búsqueda son conceptos distintos.

Las **palabras clave** son las palabras o frases más significativas para el tema en el que estás trabajando.

El **término de búsqueda** es lo que se escribes en el cuadro de búsqueda del buscador o base de datos.

A veces un término de búsqueda es igual a una palabra clave.

Vamos a conocer con más detalle cuál es la diferencia entre palabras clave y términos de búsqueda y qué necesitas saber antes de empezar a buscar información:

Las **palabras clave** describen el tema principal, los conceptos e ideas en el contenido de un documento y generalmente se escriben con tus propias palabras. Las palabras clave pueden ser una sola palabra o una frase y pueden encontrarse dentro de un título de un documento, en el texto debajo de una imagen o en cualquier lugar donde se encuentre información importante.

Cuando utilizas palabras clave dentro de una base de datos, a veces puedes encontrar palabras clave adecuadas en vocabularios o tesauros controlados que pueden ayudarte a elegir y formular palabras o frases de búsqueda apropiadas.

Al introducir una palabra clave en el cuadro de búsqueda de un catálogo de biblioteca o una base de datos, los resultados contendrán todos los registros que contengan ese término en el título, autor, tema (si estás buscando en un catálogo en línea) o dentro del resumen, cuerpo del texto, pies de foto en gráficos, imágenes, tablas, notas al pie de página, etc. (por ejemplo, si estás buscando en una base de datos a texto completo).

Si deseas obtener resultados más precisos, deberías transformar tus palabras clave o frases en términos de búsqueda. Piensa en qué palabras clave usaría un autor al escribir sobre su tema. Para crear términos de búsqueda de buena calidad, debes utilizar técnicas de búsqueda avanzadas que podrás encontrar en el Módulo 3.

El **término de búsqueda** te ayuda a refinar tu búsqueda para obtener resultados más precisos y adecuados que buscar solamente por palabras clave.

A veces las palabras clave y los términos de búsqueda son los mismos, pero a veces hay que crear nuevos términos para encontrar los resultados que necesitas.

**2.2.3**

Fíjate en el tema y las preguntas de investigación del escenario anterior ([Lesson 2.1](https://informationliteracy.eu/courses/course-v1:ILO+ILOEng+2018_1/courseware/37d9415583ff4c88ba79659e8afbf788/6b8457fdedec4fb1820442c4615a37f6/1?activate_block_id=block-v1%3AILO%2BILOEng%2B2018_1%2Btype%40vertical%2Bblock%40dbabf3a171914fccaedf15b6b634db7b)). Haz clic en Lección 2.1 para refrescar tu memoria sobre cuál era el tema.

Las sugerencias para  <span style="color: #339966;"><span style="color: #99cc00;">**palabras clave** están en verde:</span>:</span>

Tema: Aplicación de la energía solar en los hogares

➢ ¿Qué <span style="color: #99cc00;">**stecnologías solares*</span> han sido desarrolladas en los últimos cinco años para edificios de viviendas?

➢ ¿Qué requisitos existen en el diseño y la <span style="color: #99cc00;">**cconstrucción de edificios de viviendas*</span> con respecto al uso de la energía solar?

➢ ¿Cuáles son las posibles ventajas y desventajas comparativas de los diferentes<span style="color: #99cc00;">**ssistemas y tecnologías solares*</span> ien cuanto a su eficacia?

<span style="color: #3366ff;">**Los términos de búsqueda**</span> podrían ser los siguientes:

<span style="color: #000000;">✓ Energía solar en el hogar</span>

<span style="color: #000000;">✓ Eficacia de los sistemas solares</span>

<span style="color: #000000;">✓ Ventajas del uso de la energía solar</span>

<span style="color: #000000;">✓ Desventajas del uso de la energía solar</span>

<span style="color: #000000;">✓ Construcción de edificios de viviendas

</span>

**2.2.4**

Después de definir las palabras clave y los términos de búsqueda, marca la lista de sinónimos, ya que son necesarios para dirigir tu búsqueda de información.

Los sinónimos son palabras del mismo idioma que tienen el mismo o casi el mismo significado. Por ejemplo, las palabras hermoso, atractivo y bonito podrían ser sinónimos.

¡Observa algunos ejemplos!

➢ <span style="color: #0000ff;">huge </span>and <span style="color: #ff9900;">big </span>

➢ <span style="color: #3366ff;">start</span> and <span style="color: #ff9900;">begining </span>

➢ <span style="color: #0000ff;">garbage</span> and <span style="color: #ff9900;">vaste</span> and <span style="color: #ff00ff;">trash</span> ...

**2.2.5**

Más adelante, una vez tengas que iniciarte en la ejecución de las búsquedas (en el Módulo 3), ten en cuenta estos criterios:

Si obtienes demasiados resultados de búsqueda en alguna consulta de búsqueda, intenta limitar la búsqueda. Por ejemplo: si utilizas el término de búsqueda energía, es posible que obtengas demasiados resultados, lo que exige mucho tiempo para navegar por ellos. Este término de búsqueda puede acotarse, por ejemplo: energía renovable, energía solar, energía potencial, energía de campo, etc. Un ejemplo más simple es: la palabra clave animales puede ser acotada: animales salvajes o animales domésticos.

Si obtienes menos resultados de los que esperabas, la consulta puede ampliarse. Por ejemplo, si usaste el término de búsqueda rosas y obtuviste muy pocos resultados, prueba con el término flores. La búsqueda es un proceso iterativo: al mismo tiempo que reformulamos los términos de búsqueda, a veces también reformamos nuestras palabras clave, o incluso nuestras preguntas de investigación.

En el próximo Módulo 3 aprenderás más sobre cómo combinar términos de búsqueda o consultas de búsqueda mientras practicas la búsqueda de información.

**2.2.6**

Pasos básicos para estar preparado para hacer búsquedas:

Genera palabras clave y términos de búsqueda a partir de tus preguntas de investigación. Mientras lo haces:

*  Utiliza una combinación de palabras clave que represente conceptos clave
* Utiliza los sinónimos de las palabras clave
* Amplía el término con términos más generales para obtener más información en la consulta de búsqueda.
* Acota los términos si deseas obtener menos información


**2.2.7**

Test

Si tuvieras que escribir un trabajo sobre los hábitos alimentarios de los gatos salvajes europeos....


1. Elige tres palabras clave apropiadas:

( ) carnívoro, Europa, comer __¡Ups, inténtalo otra vez!}}

( ) animales, tigre, montañas __¡Ups, inténtalo otra vez!}}

(x) gato salvaje, hábitos alimentarios, Europa __¡Sí, es correcto!}}

( ) río, bosque, gato montés __¡Ups, inténtalo otra vez!}}


2. Elige dos sinónimos para el término hábitos alimentarios:

>>Debes elegir todas las respuestas correctas.<<

[x] hábitos alimenticios __ selected: ¡Sí, A es correcta, pero hay una más!}}

[ ] comida

[x] nutrición __ selected: ¡Sí, C es correcta, pero hay una más!}}

[ ] almuerzo


__ ((A C)) ¡Sí, es correcto! }}

3. Para ampliar tu búsqueda, selecciona un término de búsqueda apropiado para el término gato salvaje:

( ) Wildcat Ted Grant __¡Ups, inténtalo otra vez!}}

( ) Felis silvestris __¡Ups, inténtalo otra vez!}}

(x) gato __¡Sí, es correcto!}}

( ) gato como mascota __¡Ups, inténtalo otra vez!}}