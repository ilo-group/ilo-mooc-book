             @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } 

**2.2.0**

**Uvod**

**2.2 Najpomembnejše zamisli in ključne besede raziskovalnih vprašanj**

  

Na koncu lekcije naj bi bili sposobni:

*   uvrstiti raziskovalna vprašanja v določeno področje, kar omogoča določitev osnovnih pojmov
    
*   prepoznati najpomembnejše zamisli raziskave
    
*   oblikovati ključne besede raziskave
    

  

**2.2.1**

V tej lekciji se boste še naprej ukvarjali z raziskovalnimi vprašanji in prepoznavanjem ključnih zamisli, ki vam bodo pomagale pri nadaljnjih postopkih iskanja informacij.

  

Predstavljajte si naslednje korake, ki predstavljajo izhodišče…

*   poznam **temo**
    
*   poznam **področje** in **discipline**, ki se ukvarjajo s to temo
    
*   poznam konkretna **raziskovalna vprašanja**
    
*   določim **ključne besede** in **iskalne izraze**, ki se nanašajo na temo in raziskovalna vprašanja
    

  

\[VIDEO, tukaj\]

  

**2.2.2**

Ključne besede in iskalni izrazi niso eno in isto.

**Ključne besede** so najpomembnejše besede ali fraze določene raziskave.

**Iskalni izrazi** so izrazi, ki jih vtipkate v iskalno polje brskalnika ali podatkovne zbirke.

Včasih so iskalni izrazi hkrati tudi ključne besede.

Preberite več o razliki med ključnimi besedami in iskalnimi izrazi ter kaj storiti, preden začnete iskati informacije.

  

**Ključne besede** so izrazi, ki opišejo glavno temo, koncepte in zamisli ter vsebino dokumenta — največkrat jih zapišemo z lastnimi besedami. Ključne besede so lahko sestavljene iz ene same besede ali iz besednih zvez (fraz), ki jih najdemo v naslovu dokumenta, opisu slike ali kjerkoli v besedilu. Ko uporabljate ključne besede v podatkovnih zbirkah, vam pri izbiri ter zapisovanju ustreznih ključnih besed in besednih zvez lahko pomagajo kontrolirani slovarji in tezavri.

  

Če vtipkate ključne besede v iskalno polje knjižničnega kataloga ali podatkovne zbirke, bodo zadetki vsebovali vse zapise s temi besedami v naslovu, avtorstvu, predmetu (če iščete po spletnem knjižničnem katalogu), izvlečku, besedilu, naslovih grafikonov, slik, tabel, opombah itd. (če uproabljate podatkovno zbirko s celotnimi besedili).

  

Če želite najti boljše oziroma natančnejše zadetke, morate ključne besede ali besedne zveze (fraze) preoblikovati v iskalne izraze. Razmislite o ključnih besedah, ki jih bi uporabljal avtor, ko piše o določeni temi.

Tehnike naprednega iskanja, ki je predstavljena v modulu 3, vam lahko pomagajo pri določitvi dobrih iskalnih izrazov.

  

**Iskalni izrazi** vam pomagajo oblikovati natanšnejše iskanje ter najti ustreznejše in natančnejše zadetke, kakor zgolj uporaba ključnih besed. Čeprav se ključne besede večkrat ne razlikujejo od iskalnih izrazov, morate včasih določiti nove iskalne izraze, da najdete zadetke, ki zadovoljijo vašo potrebo.

  

**2.2.3**

Oglejte si temo in raziskovalno vprašanje prejšnje lekcije (2.1). S klikom na „lekcija 2.1“ se boste bolje spomnili te lekcije.

  

Predlogi za **ključne besede** so zeleni:

Tema: Uporaba tehnologij za izkoriščanje sončne energije v gospodinjstvih

➢ Katere **tehnologije za pridobivanje energije iz sončne svetlobe** za gospodinjstva so bile razvite v zadnjih petih letih?

➢ Kakšni so pogoji za **gradnjo stanovanjskih stavb**, ki uporabljajo tehnologije za izkoriščanje sončne energije?

➢ Kakšne so prednosti in pomanjkljivosti različnih **tehnologij za izkoriščanje sončne energije** glede njihove učinkovitosti?

  

**Iskalni izrazi** bi lahko bili naslednji:

✓ Sončna (solarna) energija v gospodinjstvih

✓ Učinkovitost tehnologij za izkoriščanje sončne energije

✓Prednosti uporabe sončne energije

*   Pomanjkljivosti uporabe sončne energije
    
*   Gradnja stanovanjskih stavb
    

  

**2.2.4**

Po določitvi ključnih besed in iskalnih izrazov si zapišite **sopomenke**, ki so potrebne za nadaljnje postopke iskanja informacij. Sopomenke so besede istega jezika, ki imajo (skoraj) enak pomen kot določena druga beseda. Besede kot lep, privlačen in atraktiven so lahko sopomenke.

Oglejte si nekatere **primere**.

  

➢ ogromno in veliko

➢ start in začetek

➢ smeti in odpadki…

➢ sončno in solarno

  

**2.2.5**

V modulu 3 se boste ukvarjali z iskanjem informacij. Tedaj pomislite na naslednje nasvete:

Če bo **seznam zadetkov preobširen**, lahko omejite zadetke s prilagoditvijo – zožanjem raziskovalnega vprašanja.

Na primer: če boste vstavili iskalni izraz „energija“, bo seznam zadetkov verjetno preobširen. Namesto izraza „energija“ lahko vpišete bolj specifične izraze, kot so obnovljivi viri energije, sončna energija, potencialna energija, električna poljska jakost itd.

Enostavnejši primer je: ključno besedo živali lahko zožimo na besede, kot so divje živali ali domače živali.

  

Če vsebuje seznam le **malo oziroma premalo zadetkov**, lahko razširite raziskovalno vprašanje.

Če ste na primer uporabili iskalni izraz vrtnice in je seznam vseboval le malo zadetkov, vtipkajte pojem rože.

  

Raziskovanje, pa tudi iskanje, je iterativni postopek: s tem, da preoblikujemo iskalne izraze, spremenimo tudi ključne besede in včasih celo raziskovalna vprašanja.

  

Medtem ko se boste v naslednjem modulu naučili kako povezati iskalne izraze in raziskovalna vprašanja, boste vadili tudi tehnike iskanja informacij.

  

**2.2.6**

  

Osnovni koraki, s katerimi se lahko **pripravite na iskanje informacij**:

Na podlagi raziskovalnega vprašanja določite ključne besede in iskalne izraze. Med tem postopkom:

  

*   Uporabljajte ključne besede, ki predstavljajo ključne koncepte
    
*   Uporabljajte sopomenke za te ključne besede
    
*   Dodajte iskalnemu izrazu širše pojme — tako boste pridobili več informacij
    
*   Omejite iskalni izraz, če bo seznam zadetkov preobširen
    

  

**2.2.7**

**Kviz**

  

Trenutno pišete seminarsko nalogo o prehranjevalnih navadah evropskih divjih mačk ...

1.  Izberite odgovor, ki vsebuje tri najustreznejše ključne besede:
    

( ) mesojedec , Evropa, jesti __Joj, poskusite znova.}}

( ) živali, tiger, gorovje __Joj, poskusite znova.}}

(x) divja mačka, prehranjevalne navade, Evropa __Da, to je pravilen odgovor.}}

( ) reka, gozd, ameriški ris __Joj, poskusite znova.}}

  

2\. Izberite dve sopomenki za izraz „prehranjevalne navade“:

Izberite vse pravilne odgovore.<<

\[x\] prehrambne navade__ izbrano: Da, odgovor A je pravilen, a obstaja še en pravilen odgovor.}}

\[ \] obrok

\[x\] prehrana __ izbrano: Da, odgovor C je pravilen, a obstaja še en pravilen odgovor.}}

\[ \] kosilo

__ ((A C)) Da, to sta pravilna odgovora.} }}

  

3.  Želite razširiti iskalni izraz „divja mačka“. Izberite ustrezne odgovore:
    

( ) divja mačka Ted Grant __Joj, poskusite znova.}}}

( ) Felis silvestris __Joj, poskusite znova.}}}

(x) mačka __Da, to je pravilen odgovor.}}}

( ) mačka kot domača žival __Joj, poskusite znova.}}}