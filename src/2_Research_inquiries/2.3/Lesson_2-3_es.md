**2.3 Pasar de la necesidad de información a identificar las fuentes de información apropiadas**

**Introducción a la Lección 2.3**

**Lección 2.3. PASAR DE LA NECESIDAD DE INFORMACIÓN A IDENTIFICAR LAS FUENTES DE INFORMACIÓN APROPIADAS**

Al final de esta lección, serás capaz de:

*   identificar posibles fuentes clave de información relevantes para tu trabajo (libro científico, libro popular, artículo científico, artículo popular, página web, etc.)
*   aplicar el conocimiento sobre las diferentes fuentes de información y establecer la fuente apropiada que coincida con las necesidades de información antes de iniciar la búsqueda de información

¡Comencemos!

[_Picture on the right side of the text, book covers, CDs, journals_]

**2.3.1**

En el Módulo 1 has aprendido que hay una gran variedad de fuentes, formatos e información que están disponibles para diferentes propósitos.

En las lecciones 2.1 y 2.2. has aprendido que antes de buscar es necesario pensar en el tema, enfocarse en las preguntas de investigación que te guiarán en la búsqueda de información, y definir los términos y palabras clave de búsqueda.

Esta lección combinará ambos aspectos: la necesidad de información y una variedad de recursos disponibles.

En esta lección, aprenderás que una vez que hayas definido y articulado tus necesidades de información, **debes determinar cuáles son las fuentes apropiadas** para llevar a cabo tu tarea de investigación y saber cómo aplicarlas a tu caso particular.

¿Cómo determinar cuáles son las fuentes apropiadas para llevar a cabo tu tarea de investigación? ¡Averígualo!

**2.3.2**

No todos los recursos son apropiados para todas las necesidades y tareas.

Existen dos grupos básicos de fuentes de información: fuentes primarias y secundarias.

Las **fuentes primarias** son obras originales de autores que representan el pensamiento original, por ejemplo, un artículo de una revista que informa sobre nuevas investigaciones o resultados, entrevistas, novelas, etc.

Las **fuentes secundarias** analizan e interpretan fuentes primarias, por ejemplo, enciclopedias, libros de texto, libros de divulgación, etc.


**2.3.3**

SAsí que si necesitas una información rápida, solo un dato (respuesta a una pregunta de investigación cerrada, por ejemplo, _cuándo ocurrió algo, el significado de la palabra, cuándo se utilizó por primera vez la tecnología solar en edificios de viviendas_, etc.), por lo general puedes encontrar esta información en enciclopedias, diccionarios y otras fuentes tales como "datos y cifras". Esta información no requiere una búsqueda estratificada de textos complicados. Wikipedia a veces puede ser útil (ten en cuenta que la información de recursos como Wikipedia, que surgen de la colaboración de personas con un nivel de conocimiento diverso, necesita ser revisada más a fondo).

Si necesita encontrar una respuesta a una pregunta de investigación abierta como, por ejemplo, _¿cuáles son las posibles ventajas y desventajas comparativas de los diferentes sistemas solares y tecnologías en cuanto a su eficacia?_ puedes intentar buscar información en artículos, bases de datos, libros que informen sobre las investigaciones, etc.

A la hora de seleccionar las fuentes, es necesario tener en cuenta si se necesitan fuentes cuyos temas estén relacionados con la historia y puedan haber sido publicados en cualquier momento, o solo aquellas que hayan aparecido recientemente y que contengan la información más actualizada.

Además, es muy importante el propósito para el que buscas la información. Por ejemplo, si escribes una tesis doctoral estarás interesado en otras tesis doctorales que hablen de temas similares y las buscarás en un repositorio de tesis doctorales. Y si escribes un trabajo en el primer año de estudios de grado probablemente no utilizarás una tesis doctoral, ya que en ese tipo de fuentes el desarrollo de un tema es demasiado complejo para ti (ten en cuenta también que aunque estén disponible públicamente, las tesis doctorales de trabajo son obras inéditas _-literatura gris-_ para las que necesitas un permiso de uso).

**2.3.4**

Imagínate que te encargan la tarea de escribir sobre _Aplicación de la energía solar en los hogares: eficacia de los sistemas y tecnologías solares_. Se espera que tu trabajo se base en la evidencia científica.

Entre las fuentes seleccionadas, escoge aquellas que consideres que serán útiles para tu tarea y piensa con qué propósito las utilizas.

**2.3.5**

**Test núm. 1**

**¿Son estos recursos adecuados para el trabajo "Aplicación de la energía solar en los hogares: eficacia de los sistemas y tecnologías solares"?**

 ¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

(x) SÍ __¡Esta es la respuesta correcta! Posible propósito: utilizar cuando se necesita información sobre el tema en general, por ejemplo, la errecta! Posible propósito: utilizar cuando se necesita información sobre el tema en general, por ejemplo, la energía solar - para obtener una visión general de ese tema.}}

( ) NO __¡No es correcto!}}

¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

(x) SÍ __¡Esta es la respuesta correcta! Posible propósito: usar cuando necesites información específica sobre tu tema para obtener evidencia científica conocida sobre el tema.}}

 ¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

( ) SÍ __¡No es correcto!}}

(x) NO __¡Esta es la respuesta correcta! Posible propósito: Esta no es una buena fuente cuando se necesita escribir un trabajo que debe basarse en la evidencia científica. Las revistas o periódicos pueden ser utilizados cuando quieres obtener información sobre proye

¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 


(x) SÍ __¡Esta es la respuesta correcta! Posible propósito: usa las enciclopedias y diccionarios para buscar información básica y definiciones de los términos que hay en tu tema como energía solar y sistemas solares}}

( ) NO __¡No es correcto!}}


¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

(x) SÍ __¡Esta es la respuesta correcta! Posible propósito: usa el catálogo de la biblioteca (en tu caso lo mejor es buscar en el catálogo de la biblioteca de tu universidad y no en una biblioteca pública) para encontrar fuentes primarias y secundarias (especialmente libros) sobre tu tema que puedes coger en préstamo o leer en línea.}}

( ) NO __¡No es correcto!}}

¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

(x) SÍ __¡Esta es la respuesta correcta! Posible propósito: utilizar bases de datos y buscar artículos que hablen del tema sobre el que estás escribiendo para obtener nueva información relevante normalmente contenida artículos de revistas científicas. Las revistas científicas se encuentran habitualmente en diferentes bases de datos.}}

( ) NO __¡No es correcto!}}

¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

( ) SÍ __¡No es correcto!}}

(x) NO __¡Esta es la respuesta correcta! Posible propósito: Esta no es una fuente recomendada si escribes un trabajo que debe estar basado en la evidencia cientí

¿Son estos recursos apropiados para el trabajo? Piensa con qué propósito puedes o no puedes usarlos. 

( ) SÍ __¡No es correcto!}}

(x) NO __¡Esta es la respuesta correcta! Posible propósito: Esta no es una fuente recomendada si escribes un trabajo que debe estar basado en la evidencia científica. Los blogs suelen 

**2.3.6**

Ten también en cuenta esto:

**No hay una respuesta única e inequívoca** a las preguntas anteriores.

Como incentivo inicial para la interpretación del tema y para la búsqueda de la información necesaria para el tema y el trabajo, puede ser útil un artículo de periódico sobre el coste o la relación coste-eficiencia de los sistemas de energía solar.

Podrían sernos útiles revistas especializadas en la industria de la construcción, en las que, por ejemplo, el jefe de una empresa habla sobre los nuevos sistemas solares que su empresa planea desarrollar.

Sin embargo, para escribir un artículo científico sobre la energía solar en los hogares y la eficacia de los sistemas y tecnologías solares, los recursos que mejor servirán para este propósito son: catálogos de bibliotecas (para saber lo que está disponible principalmente en formato impreso), revistas académicas y bases de datos.

**2.3.7**

Haz el **test núm. 2** y descubre más sobre las diferentes fuentes de información científica.

La retroalimentación se mostrará después de seleccionar las respuestas.

1. Es mejor buscar información académica actual y revisada por expertos sobre algún tema en:

( ) Libros de texto __Los libros de texto contienen información verificada y categorizada sobre un tema, pero por lo general los libros de texto no contienen la información científica más reciente.}}

(x) Bases de datos con artículos científicos __Sí, porque la información científica se publica a menudo en artículos científicos que están indexados en bases de datos.}}

( ) Enciclopedias __Las enciclopedias en general contienen solo información ampliamente conocida.}}

( ) Portales web __Los portales web suelen contener información popular, no científica.}}

2. Puedes encontrar información básica sobre algún tema científico en:

Hay dos respuestas correctas

[ ] Libros científicos __ selected: A) Solo a veces sí, depende del tema y de la estructura del libro.}}

[x] Revistas científicas __ selected: B) Sí, en algunos tipos de artículos de revistas científicas, sobre todo en artículos de revisión. }}

[x] Enciclopedias __ selected: C) Sí, porque las enciclopedias contienen información básica sobre temas generales - enciclopedias generales o información básica sobre áreas específicas - enciclopedias especializadas.}}

[ ] Redes sociales populares como Facebook, Twitter etc. __ selected: D) Las redes sociales sirven para conectarse e intercambiar información diaria. También existen redes sociales que sirven para compartir trabajos científicos, pero no son información científica básica, sino altamente especializada. }}


__ ((B C)) ¡Correcto! B) Sí, en algunos tipos de artículos de revistas científicas, principalmente en artículos de revisión. C) Sí, porque las enciclopedias contienen información básica sobre temas generales - enciclopedias generales o información básica sobre áreas específicas - enciclopedias especializadas.}}

__ ((B)) B es correcta, ¡pero hay otra más! }}

__ ((C)) C es correcta, ¡pero hay otra más! }}

