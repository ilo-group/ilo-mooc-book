**2.3 Passar de la necessitat d'informació a identificar les fonts d'informació adients**

**Introducció a la Lliçó 2.3**

Al final d'aquesta lliçó, seràs capaç de:

* identificar possibles fonts claus d'informació rellevants per al teu treball (llibre científic, llibre popular, article científic, article popular, pàgina web, etc.)
* aplicar el coneixement sobre les diferents fonts d'informació i establir la font apropiada que coincideixi amb les necessitats d'informació abans d'iniciar la recerca d'informació

Comencem!

[_Picture on the right side of the text, book covers, CDs, journals_]

**2.3.1**

En el Mòdul 1 has après que hi ha una gran varietat de fonts, formats i informació que estan disponibles per a diferents propòsits.

A les lliçons 2.1 i 2.2. has après que abans de buscar cal pensar en el tema, enfocar-se en les preguntes de recerca que et guiaran en la cerca d'informació, i definir els termes i paraules clau de cerca.

Aquesta lliçó combinarà els dos aspectes: la necessitat d'informació i una varietat de recursos disponibles.

En aquesta lliçó, aprendràs que una vegada que hagis definit i articulat les teves necessitats d'informació, **has de determinar quines són les fonts apropiades** per dur a terme la teva tasca d'investigació i saber com aplicar-les al teu cas particular.

Com determinar quines són les fonts apropiades per dur a terme la teva tasca de recerca? Esbrina-ho!

**2.3.2**

No tots els recursos són apropiats per a totes les necessitats i tasques.

Hi ha dos grups bàsics de fonts d'informació: fonts primàries i secundàries.

Les **fonts primàries** són obres originals d'autors que representen el pensament original, per exemple, un article d'una revista que informa sobre noves investigacions o resultats, entrevistes, novel·les, etc.

Les **fonts secundàries** analitzen i interpreten les fonts primàries, per exemple, enciclopèdies, llibres de text, llibres de divulgació, etc.


**2.3.3**

Així que, si necessites una informació ràpida, només una dada (resposta a una pregunta de recerca tancada, per exemple, quan va succeir alguna cosa, el significat de la paraula, _quan es va utilitzar per primera vegada la tecnologia solar en edificis d'habitatges_, etc.), en general pots trobar aquesta informació a enciclopèdies, diccionaris i altres fonts com ara "dades i xifres". Aquesta informació no requereix una cerca estratificada de textos complicats. Wikipedia de vegades pot ser útil (tingues en compte que la informació de recursos com Wikipedia, que sorgeixen de la col·laboració de persones amb un nivell de coneixement divers, necessita ser revisada més a fons).

Si necessites trobar una resposta a una pregunta de recerca oberta com, per exemple, _quins són els possibles avantatges i desavantatges comparatius dels diferents sistemes solars i tecnologies pel que fa a la seva eficàcia?_ pots intentar buscar informació en articles, bases de dades, llibres que informin sobre la recerca que s'ha dut a terme sobre el tema, etc.

A l'hora de seleccionar les fonts, cal tenir en compte si es necessiten fonts de temes relacionats amb la història i que puguin haver estat publicades en qualsevol moment, o només aquelles que hagin aparegut recentment i que continguin la informació més actualitzada.

A més, és molt important el propòsit per al que busques la informació. Per exemple, si escrius una tesi doctoral estaràs interessat en altres tesis doctorals que parlin de temes similars i les buscaràs en un repositori de tesis doctorals. I si escrius un treball en el primer any d'estudis de grau probablement no utilitzaràs una tesi doctoral, ja que en aquest tipus de fonts el desenvolupament d'un tema és massa complex per a tu (tingues en compte també que, encara que estiguin disponibles públicament, les tesis doctorals de treball són obres inèdites -literatura gris- per a les quals necessites un permís d'ús).

**2.3.4**

Imagina't que t'encarreguen la tasca d'escriure sobre _Aplicació de l'energia solar a les llars: eficàcia dels sistemes i tecnologies solars_. S'espera que el teu treball es basi en l'evidència científica.

D'entre les fonts seleccionades, tria aquelles que consideres que seran útils per a la teva tasca i pensa amb quin propòsit les utilitzes.

**2.3.5**

**Test núm. 1**

**Són aquests recursos adequats per al treball _"Aplicació de l'energia solar a les llars: eficàcia dels sistemes i tecnologies solars"?_**

1. Monografia (un llibre especialitzat sobre un sol tema; imprès o electrònic)

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

(x) SÍ __Aquesta és la resposta correcta! Possible propòsit: utilitzar quan es necessiti informació sobre el tema en general, per exemple, l'energia solar - per seleccionar un punt d'aquest tema.}}

( ) NO __No és correcte!}}

2. Revista científica (una col·lecció d'articles d'investigació acadèmica, impresos o en línia)

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

(x) SÍ __Aquesta és la resposta correcta! Possible propòsit: fer servir quan necessitis informació específica sobre el teu tema per obtenir evidència científica coneguda sobre el tema.}}

( ) NO __No és correcte!}}

3. Revista o diari (no basat en temes acadèmics; imprès o en línia)

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

( ) SÍ __No és correcte!}}

(x) NO __Aquesta és la resposta correcta! Possible propòsit: Aquesta no és una bona font quan es necessita escriure un treball que s'ha de basar en l'evidència científica. Les revistes o diaris poden ser utilitzats quan vols obtenir informació sobre projectes populars recents, productes, invents, organitzacions, etc., en el camp que t'interessa.}}

4. Enciclopèdia / diccionaris (impresos o en línia)

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

(x) SÍ __Aquesta és la resposta correcta! Possible propòsit: utilitza les enciclopèdies i diccionaris per buscar informació bàsica i definicions dels termes que hi ha al teu tema, com per exemple "energia solar" i "sistemes solars"}}

( ) NO __No és correcte!}}

5. Catàleg de biblioteca

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

(x) SÍ __Aquesta és la resposta correcta! Possible propòsit: utilitza el catàleg de la biblioteca (en el teu cas el millor és buscar en el catàleg de la biblioteca de la teva universitat i no en una biblioteca pública) per trobar fonts primàries i secundàries (especialment llibres) sobre el teu tema que pots agafar en préstec o llegir en línia.}}

( ) NO __No és correcte!}}

6. Bases de dades (treballs acadèmics teòrics i de recerca)

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

(x) SÍ __¡Aquesta és la resposta correcta! Possible propòsit: utilitzar bases de dades i buscar articles que parlin del tema sobre el qual estàs escrivint per obtenir nova informació rellevant normalment continguda articles de revistes científiques. Les revistes científiques es poden trobar a diferents bases de dades.}}

( ) NO __No és correcte!}}

7. Portals d'Internet

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

( ) SÍ __No és correcte!}}

(x) NO __Aquesta és la resposta correcta! Possible propòsit: Aquesta no és una font recomanada si escrius un treball que ha d'estar basat en l'evidència científica. Els portals d'Internet solen contenir informació popular, no informació científica.}}


8. Blogs

Són aquests recursos apropiats per al treball? Pensa amb quin propòsit pots o no pots usar-los. 

( ) SÍ __No és correcte!}}

(x) NO __Aquesta és la resposta correcta! Possible propòsit: Aquesta no és una font recomanada si escrius un treball que ha d'estar basat en l'evidència científica. Els blogs solen estar escrits per persones que són apassionades en algun tema, però que no necessàriament són experts.}}


**2.3.6**

Tingues també en compte això:

**No hi ha una resposta única i inequívoca** a les preguntes anteriors.

Com a incentiu inicial per a la interpretació del tema i per a la recerca de la informació necessària per al tema i el treball, pot ser útil un article de diari sobre el cost o la relació cost-eficiència dels sistemes d'energia solar.

Ens podrien ser d'utilitat revistes especialitzades en la indústria de la construcció, en què, per exemple, el cap d'una empresa parla sobre els nous sistemes solars que la seva empresa planeja desenvolupar.

No obstant això, per escriure un article científic sobre l'energia solar a les llars i l'eficàcia dels sistemes i tecnologies solars, els recursos que millor serviran són: catàlegs de biblioteques (per saber el que hi ha disponible principalment en format imprès) , revistes acadèmiques i bases de dades.

**2.3.7**

Fes el **test núm. 2** i descobreix més sobre les diferents fonts d'informació científica.

La retroalimentació es mostrarà després de seleccionar les respostes.

1. És millor buscar informació acadèmica actual i revisada per experts sobre algun tema en:

( ) Llibres de text __Els llibres de text contenen informació verificada i categoritzada sobre un tema, però en general els llibres de text no contenen la informació científica més recent.}}

(x) Bases de dades amb articles científics __Sí, perquè la informació científica es publica sovint en articles científics que estan indexats en bases de dades.}}

( ) Enciclopèdies __Les enciclopèdies en general contenen només informació àmpliament coneguda.}}

( ) Portals web __Els portals web solen contenir informació popular, no científica.}}

2. Pots trobar informació bàsica sobre algun tema científic en:

Hi ha dues respostes correctes 

[ ] Llibres científics __selected: A) Només de vegades sí, depèn del tema i de l'estructura del llibre.}}

[x] Revistes científiques __selected: B) Sí, en alguns tipus d'articles de revistes científiques, sobretot en articles de revisió. }}

[x] Enciclopèdies __selected: C) Sí, perquè les enciclopèdies contenen informació bàsica sobre temes generals - enciclopèdies generals o informació bàsica sobre àrees específiques - enciclopèdies especialitzades.}}

[ ] Xarxes socials populars com Facebook, Twitter etc. __Selected: D) Les xarxes socials serveixen per connectar i intercanviar informació diària. També hi ha xarxes socials que serveixen per compartir treballs científics, però no són informació científica bàsica, sinó altament especialitzada. }}


__((B C)) Correcte! B) Sí, en alguns tipus d'articles de revistes científiques, principalment en articles de revisió. C) Sí, perquè les enciclopèdies contenen informació bàsica sobre temes generals - enciclopèdies generals o informació bàsica sobre àrees específiques - enciclopèdies especialitzades.}}

__((B)) B és correcta, però n'hi ha una altra més! }}

__((C)) C és correcta, però n'hi ha una altra més! }}