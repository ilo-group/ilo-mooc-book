**2.3 Vom Informationsbedarf zur Ermittlung der geeigneten Informationsquelle**

**Am Ende dieser Lektion könnt ihr Folgendes:**

* mögliche wichtige Informationsquellen identifizieren, die für die Aufgabe relevant sind (Wissenschaftsbuch, Populärbuch, wissenschaftlicher Artikel, Populärartikel, Webpage usw.).
* Wissen über verschiedene Informationsquellen hinaus anwenden und euch für eine geeignete Quelle entscheiden, die dem Informationsbedarf entspricht, bevor mit der Suche nach Informationen begonnen wird

Los geht's!

[_Bild auf der rechten Seite des Textes, Buchumschläge, CDs, Zeitschriften_]

**2.3.1**

In Modul 1 habt ihr erfahren, dass es eine Vielzahl von Quellen, Formaten und Informationen gibt, die für verschiedene Zwecke zur Verfügung stehen.

In den Lektionen 2.1 und 2.2 habt ihr gelernt, dass es vor der Suche notwendig ist, über das Thema nachzudenken, sich auf die Forschungsfragen zu konzentrieren, welche euch bei der Suche nach Informationen leiten und Suchbegriffe und Keywords zu definieren.

Diese Lektion kombiniert beides - Informationsbedarf und eine Vielzahl von verfügbaren Ressourcen.

In dieser Lektion lernt ihr, dass ihr, sobald ihr euren Informationsbedarf definiert und artikuliert habt, **die geeigneten Quellen** für die Erfüllung eurer Forschungsaufgabe bestimmen solltet. Des Weiteren solltet ihr wissen, wie ihr diese in der jeweiligen Aufgabe anwenden könnt.

Wie kann man geeignete Quellen für die Erfüllung seiner Forschungsaufgabe ermitteln? Findet es heraus!


**2.3.2**

Nicht alle Ressourcen sind für alle Bedürfnisse und Aufgaben geeignet.

Es gibt zwei grundlegende Gruppen von Informationsquellen: Primär- und Sekundärquellen.

* **Primärquellen** sind Originalautorenwerke, die ursprüngliches Denken repräsentieren, z.B. ein Zeitschriftenartikel, der über neue Forschungen oder Ergebnisse berichtet, Interviews, Romane usw.
* **Sekundäre Quellen** analysieren und interpretieren primäre Quellen, z.B. Enzyklopädien, Lehrbücher, Bücher

**2.3.3**

Wenn ihr also schnell Information benötigt,  **<span style="color: #0000ff;">nur ein Fakt</span>** (Antwort auf eine geschlossene Forschungsfrage z.B. _wenn etwas passiert ist, die Bedeutung des Wortes, wann die Solartechnik zum ersten Mal in Wohngebäuden verwendet wurde_ etc.) findet ihr solche Informationen in der Regel in Enzyklopädien, Wörterbüchern und anderen Quellen wie "Fakten und Zahlen". Solche Informationen erfordern keine geschichtete Suche nach komplizierten Texten. Wikipedia kann manchmal nützlich sein (denkt daran, dass ein Wiki wie Wikipedia  aus der Zusammenarbeit von Menschen mit einer Vielzahl von Fachkenntnissen entstehen und die Informationen darin weiter ausgewertet werden müssen).

Wenn ihr eine Antwort auf <span style="color: #0000ff;">**eine offene Forschungsfrage**</span> finden müsst, wie z.B. _Was sind die möglichen Vor- und Nachteile verschiedener Sonnensysteme und Technologien in Bezug auf ihre Wirksamkeit?_, könnt ihr versuchen Informationen in Artikeln in Datenbanken und Büchern zu suchen, welche über die Forschung berichten usw..

Bei der Auswahl der Quellen ist es notwendig zu berücksichtigen, ob ihr Quellen benötigt, deren Themen sich auf die <span style="color: #0000ff;">**Historie**</span> beziehen und jederzeit veröffentlicht wurden, oder nur solche, die vor Kurzem erschienen sind und die aktuellsten Informationen enthalten.

Auch sehr wichtig ist der **Zweck, warum ihr Informationen sucht**.

Wenn ihr z.B. eine Doktorarbeit schreibt, werdet ihr an anderen Doktorarbeiten interessiert sein, die über ähnliche Themen schreiben und ihr werdet im Verzeichnis der Doktorarbeiten suchen.

Wenn ihr eine Seminararbeit im ersten Jahr des Grundstudiums schreibt, werdet ihr wahrscheinlich keine Doktorarbeit verwenden, da in solchen Quellen die Artikulation eines Themas für euch zu komplex ist. Berücksichtigt auch, dass es sich bei den Dissertationen, obwohl sie öffentlich zugänglich sind, um unveröffentlichte Werke (graue Literatur) handelt, für die ihr eine Nutzungserlaubnis benötigt.

**2.3.4**


Stelle dir vor, du hast die Aufgabe über _Anwendung der Solarenergie in den Haushalten: Wirksamkeit von Sonnensystemen und -technologien_ zu schreiben. Es wird erwartet, dass deine Arbeit auf wissenschaftlichen Erkenntnissen basiert.

Unter den ausgewählten Quellen wähle bitte diejenigen aus, die du für gut hältst, um sie für die Aufgabe zu nutzen, und denkt darüber nach, zu welchem Zweck die Quellen verwendet werden sollen.

**2.3.5**

**Quiz-Nr. 1**

Sind diese Ressourcen für deine Arbeit über _Anwendung der Solarenergie in den Haushalten: Wirksamkeit von Solarsystemen und -technologien_ geeignet?

>>1) Monographie (ein spezielles Buch zu einem einzelnen Thema; Druck oder E-Book)<<

(x) Ja

( ) Nein

>>2) Journal (eine Sammlung von akademischen Forschungsarbeiten; gedruckt oder online)<<

(x) Ja

( ) Nein

>>3) Zeitschrift oder Zeitung (nicht akademisch orientiert; Print oder Online)<<

( ) Ja

(X) Nein

>>4) Enzyklopädie/Wörterbücher (Print oder Online)<<

(x) Ja

( ) Nein


>>5) Bibliothekskatalog<<

(x) Ja

( ) Nein


>>6) Datenbanken (akademische Theorie- und Forschungsarbeiten)<<

(x) Ja

( ) Nein

>>7) Internetportale<<

(x) Ja

( ) Nein

>>8) Blogs<<

( ) Ja

(X) Nein


**2.3.6**

Beachte auch dies!

Es gibt **keine einzige und eindeutige Antwort** auf die vorherigen Fragen.

Als Ausgangspunkt für die Themeninterpretation und für die Suche nach den benötigten Informationen zum Thema könnte auch ein Zeitungsartikel über die Kosten oder über die Kosteneffizienz des Sonnensystems nützlich sein. 

<span style="color: #0000ff;">Magazine</span> speziell für das Baugewerbe, in denen ein Firmenchef über neue Solarsysteme spricht, die sein Unternehmen zu entwickeln plant, könnten nützlich sein.

Für das Schreiben einer wissenschaftlichen Arbeit über Solarenergie in den Haushalten und die Effektivität von Solarsystemen und -technologien sind folgende Quellen am besten für diesen Zweck geeignet: Bibliothekskataloge< (um zu erfahren, was in erster Linie im Druckformat verfügbar ist), akademische Zeitschriften und Datenbanken.

**2.3.7**

Bearbeite dieses **Quiz-Nummer. 2** und erfahre mehr über verschiedene wissenschaftliche Informationsquellen!

Das Feedback wird angezeigt, nachdem du die Antworten ausgewählt hast.

1. Aktuelle und von Experten geprüfte wissenschaftliche Informationen zu einem bestimmten Thema können am besten in … gefunden werden:

( ) Lehrbüchern __Lehrbücher enthalten verifizierte und kategorisierte Informationen zu einem Thema, aber normalerweise enthalten Lehrbücher keine neuen wissenschaftlichen Informationen.}}}}

(x) Datenbanken mit wissenschaftlichen Artikeln __Ja, da wissenschaftliche Informationen oft in wissenschaftlichen Artikeln veröffentlicht werden, die in Datenbanken indiziert sind.}}}

( ) Enzyklopädien __Enzyklopädien im Allgemeinen enthalten nur bekannte Informationen.}}}}

( ) Webportale __Webportale enthalten in der Regel populäre Informationen, nicht wissenschaftliche.}}

2. Grundlegende Informationen zu einigen wissenschaftlichen Themen findest du hier: (Es gibt zwei richtige Antworten!)

[ ] Wissenschaftliche Bücher __ ausgewählt: A) Nur manchmal ja, es kommt auf das Thema und die Struktur des Buches an. }}  

[x] Wissenschaftliche Zeitschriften __ ausgewählt: B) Ja in einigen Arten von wissenschaftlichen Zeitschriften - meist in Rezensionen. }}  

[x] Enzyklopädie __ ausgewählt: C) Ja, denn Enzyklopädien enthalten grundlegende Informationen über allgemeine Themen - allgemeine Enzyklopädien oder grundlegende Informationen über bestimmte Bereiche - spezielle Enzyklopädien. }}

[ ] Beliebte soziale Netzwerke wie Facebook, Twitter etc. __ ausgewählt: D) Soziale Netzwerke dienen der Verbindung und dem Austausch täglicher Informationen. Es gibt auch soziale Netzwerke, die dem Austausch wissenschaftlicher Arbeiten dienen, aber das sind keine grundlegenden, sondern hochspezialisierte wissenschaftliche Informationen. }}


__ ((B C))) Richtig! B) Ja, in einigen Arten von wissenschaftlichen Zeitschriften - meist in Übersichtsarbeiten. C) Ja, denn Enzyklopädien enthalten grundlegende Informationen zu allgemeinen Themen - allgemeine Enzyklopädien oder grundlegende Informationen zu einem bestimmten Bereich - spezielle Enzyklopädien.}}}} __ ((B)) B ist richtig, aber es gibt noch einen mehr! }} __ ((C)) C ist richtig, aber es gibt noch einen mehr! }}