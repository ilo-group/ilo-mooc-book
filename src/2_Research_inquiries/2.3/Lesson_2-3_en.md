﻿2.3.0

**2.3 From information need to determining appropriate information source**

At the end of this lesson, you will be able to:

*   identify possible key sources of information relevant to the task (science book, popular book, scientific article, popular article, web page etc.)
*   apply knowledge on different information sources and decide upon appropriate source that matches information need before starting search for information

Let’s go!

[_Picture on the right side of the text, book covers, CDs, journals_]

2.3.1

In Module 1 you have found out that there are a wide variety of sources, formats and information that are available for different purposes.

In lessons 2.1 and 2.2\. you have learned that before searching it is necessary to think about the topic, focus on the research questions that will lead you in searching for information, and define search terms and keywords.

This lesson will combine both - information need and a variety of available resources.

In this lesson you will learn that once you define and articulate your need for information, **you should determine appropriate sources** for accomplishing your research task and how to apply that in particular task.

How to determine appropriate sources for accomplishing your research task? Find out!

2.3.2

Not all resources are compatible for all the needs and tasks.

There are two basic groups of information sources: primary and secondary sources.

*   **Primary sources** are original author works which represent original thinking, eg. a journal article reporting new research or findings, interviews, novel etc.
*   **Secondary sources** analyze and interpret primary sources, eg. encyclopedias, textbooks, books that interpret etc.

2.3.3

So if you need a fast information, **<span style="color: #0000ff;">just a fact</span>** (answer to closed-ended research question eg. _when something happened, the meaning of the word, when was solar technology first time used in residential buildings_ etc.) usually you can find such information in encyclopedias, dictionaries and other sources such as „facts and figures“.

Such information does not demand stratified searching of complicated text.

Wikipedia can sometimes be useful (keep in mind that information from such resources as Wikipedia, that emerge from collaboration of people with a variety of expertise need to be further evaluated).

If you need to find <span style="color: #0000ff;">**answer to open-ended research question**</span> as eg. _What are the possible comparative advantages and disadvantages of different solar systems and technologies in regards to their effectiveness_? you can try to search information in articles in databases, books reporting research etc.

Upon selection of sources, it is necessary to consider whether you need sources whose topics are about <span style="color: #0000ff;">**history**</span>, and are published at any time, or only those <span style="color: #0000ff;">**recent**</span> and containing most up to date information.

Also, very important is the **purpose for which you seek** some information

For example, if you write a doctoral thesis you will be interested in other doctoral theses that talk about similar topics and you will look for them in the repository of doctoral theses.

And if you write a seminar on the first year of undergraduate studies you will probably not use somebody's doctoral thesis since in that kind of sources the articulation of a topic is too complex for you. Also take into account that although it is publicly available work doctoral theses are unpublished works (_grey literature_) for which you need permission for usage.

2.3.4

Imagine, you have an assignment to write about _Application of solar energy in the households: effectiveness of solar systems and technologies._ It is expected that your work is based on scientific evidence.

Among selected sources please choose those that you consider it will be good to use for your assignment and think for what purpose.

2.3.5

**Quiz no. 1**

**Are these resources appropriate for the assignment** **_Application of solar energy in the households: effectiveness of solar systems and technologies_?**

1) Monograph (a special book on a single subject; print or e-book)

2) Journal (a collection of academic research papers; print or online)

3) Magazine or newspaper (not based on academic subjects; print or online)

4) Encyclopedia/dictionaries (print or online)

5) Library catalogue

6) Databases (academic theoretical and research papers)

7) Internet portals

8) Blogs

Are these resources appropriate for the assignment? Think for what purpose you can/cannot use it!

YES
NO

2.3.6

Also keep in mind this!

There is **no one and unique answer** on the previous questions.

As a starting incentive for the topic interpretation and for searching for the needed information for the topic and the assignment, a <span style="color: #0000ff;">newspaper article</span> on the expensiveness, but cost-effectiveness of solar system can be useful. 

<span style="color: #0000ff;">Magazine</span> specialized for building constructors with the theme in which a company director speaks about new solar systems that his company is planning to develop could be useful.

But, for writing a scientific paper about solar energy in the households and the effectiveness of solar systems and technologies, the resources that will serve the best for this purpose are: <span style="color: #0000ff;">library catalogues</span> (to learn what is available in primarily print format), <span style="color: #0000ff;">academic journals</span> and <span style="color: #0000ff;">databases</span>.

2.3.7

Take this **quiz no. 2** and find out more about different scientific information sources!

Feedback will be shown after you select the answers.

1. Current and peer-reviewed scholarly information on some subject is best to look in:

( ) Textbooks __Textbooks contain verified and categorized information on a topic, but usually textbooks do not contain new scientific information.}}
(x) Databasest with scientific articles __Yes, because scientific information is often published in scientific articles that are indexed in databases.}}
( ) Encyclopedia __Encyclopedias in general contain only well known information.}}
( ) Web portals __Web portals usually contain popular information, not scientific.}}

2. Basic information about some scientific topic you can find in:

>>||There are two correct answers!<< 
[ ] Scientific books __ selected: A) Only sometimes yes, it depends of the topic and structure of the book. }} 
[x] Scientific journals __ selected: B) Yes in some types of scientific journal papers - mostly in review papers. }} 
[x] Encyclopedia __ selected: C) Yes, because encyclopedias contain basic information about general topics - general encyclopedias or basic information about specific field - specialized encyclopedias. }} 
[ ] Popular social networks as Facebook, Twitter etc. 
__ selected: D) Social networks serve to connect and share daily information. There are also social networks that serve to share scientific papers but these are not basic but highly specialized scientific information. }} __ ((B C)) Correct! B) Yes in some types of scientific journal papers - mostly in review papers. C) Yes, because encyclopedias contain basic information about general topics - general encyclopedias or basic information about specific field - specialized encyclopedias.}} __ ((B)) B is correct, but there is one more! }} __ ((C)) C is correct, but there is one more! }}