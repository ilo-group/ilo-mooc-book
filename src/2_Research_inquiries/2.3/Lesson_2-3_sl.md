             @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } 

**2.3.0**

**Uvod**

**2.3 Od informacijskih potreb do ustreznih informacijskih virov**

  

Na koncu lekcije boste sposobni:

\- prepoznati možne ključne vire za določeno nalogo (znanstvene knjige, poljudne knjige, znanstveni članki, poljudni članki, spletne strani itd.)

\- uporabiti znanje o različnih informacijskih virih in odločiti se za ustrezen informacijski vir, ki ustreza informacijski potrebi, preden boste začeli iskati informacije

  

Začnimo!

\[slika je na desni strani besedila, naslovnic knjig, zgoščenk, revij\]

  

**2.3.1**

V tej lekciji se boste naučili, da sledi po opredelitvi in določitvi informacijskih potreb **določitev ustreznih informacijskih virov**, ki nam pomagajo pri raziskovalnem delu.

  

**2.3.2**

Kako določiti ustrezne informacijske vire, ki nam pomagajo pri raziskovalnem delu? Tukaj boste izvedeli.

V modulu 1 ste spoznali, da obstaja mnogo informacijskih virov, formatov in informacij, ki jim pripišemo različne namene.

V lekcijah 2.1. in 2.2 ste se ukvarjali s postopki, s katerimi se pripravljamo na iskanje informacij. 

Preden iščemo informacije, moramo razmisliti o izbrani temi, postaviti raziskovalna vprašanja, ki nam pomagajo pri iskanju ustreznih informacij in določiti iskalne izraze ter ključne besede.

Ta lekcija bo to vse povezala — informacijske potrebe ter dostop do informacijskih virov.

  

Univerzalnega vira, ki zadovolji vsako informacijsko potrebo in je primeren za vsako nalogo, ni.

Razlikujemo med dvema skupinama informacijskih virov: med primarnimi in sekundarnimi viri.

  

**Primarni viri** so izvirna avtorska dela, ki vsebujejo informacije v izvirni in nekrajšani obliki, k njim spadajo članki v znanstvenih revijah, poročanje o novih raziskavah ali ugotovitvah, intervjuji, romani itd.

  

**Sekundarni viri** obnovijo, analizirajo, interpretirajo informacije primarnih virov, k njim spadajo enciklopedije, učbeniki, knjige itd.

  

**2.3.3**

Če torej želite hitro pridobiti informacije in izvedeti **le dejstvo** (odgovor na zaprto raziskovalno vprašanje, na primer: kdaj se je zgodilo kaj, pomen določene besede, kdaj so v stanovanjskih stavbah prvič uporabljali tehnologije za izkoriščanje sončne energije itd.), jih lahko običajno najdete v enciklopedijah, slovarjih in drugih virih, kot so „dejstva in številke“.

  

Za pridobitev takšnih informacij vam ni treba iskati zapletenih besedil.

  

Wikipedija vam je pri tem lahko v korist (ne pozabite, da temeljijo informacijski viri, kot je Wikipedija, na medsebojnem delovanju različnih ljudi z različnim znanjem in izkušnjami — zaradi tega moramo tovrstne vire vedno kritično oceniti).

  

Informacije, ki odgovarjajo na **odprta raziskovalna vprašanja**, kot je na primer „Kakšne so prednosti in pomanjkljivosti različnih tehnologij za izkoriščanje sončne energije glede njihove učinkovitosti?“, lahko najdete v člankih, podatkovnih zbirkah, knjigah, ki poročajo o raziskavah itd.

  

Pri izbiri informacijskih virov morate upoštevati, ali želite pridobiti informacije o **preteklih** raziskavah ali o **najaktualnejših** ugotovitvah.

Poleg tega je dobro, da določite **namen iskanja informacij.**

  

Ko pišete disertacijo, boste na primer iskali zbirko ali repozitorij disertacij, saj vas bodo zanimale disertacije drugih, ki so se ukvarjali s podobnimi temami.

  

Kot študent prvega letnika dodiplomskega študijskega programa se verjetno ne boste ukvarjali z disertacijami drugih, saj je ta informacijski vir trenutno še prezapleten za vas. Čeprav so disertacije javno dostopni informacijski viri, se morate zavedati, da spadajo med neobjavljena dela (siva literatura) — za uporabo tovrstnih znanstvenih del potrebujete dovoljenje.

  

**2.3.4**

Predstavljajte si, da morate napisati seminarsko nalogo na temo „uporaba sončne energije v gospodinjstvih: učinkovitost tehnologij in sistemov za izkoriščanje sončne energije“. Raziskava naj temelji na znanstvenih ugotovitvah.

Prosim, izberite primerne informacijske vire, ki jih bi uporabljali v seminarski nalogi. Poleg tega razmislite o namenu informacijskih virov.

  

**2.3.5**

**Kviz 1**

**Ali so navedeni informacijski viri primerni za seminarsko nalogo na temo „uporaba sončne energije v gospodinjstvih: učinkovitost tehnologij in sistemov za izkoriščanje sončne energije“?** Razmislite o uporabi in namenu navedenih virov.

**DA NE**

  

1.  Monografija (knjiga, ki se ukvarja z eno temo; tiskano ali online)
    
2.  Znanstvena revija (zbirka znanstvenih raziskav; tiskano ali online)
    
3.  Časnik (niso znanstveno usmerjeni; tiskano ali online)
    
4.  Enciklopedija/slovar (tiskano ali online)
    
5.  Knjižnični katalog
    
6.  Podatkovna zbirka (znanstveni teoretični in raziskovalni članki)
    
7.  Spletni portal
    
8.  Spletni dnevniki
    

  

  

**2.3.6**

Ne pozabite na naslednje.

Na prejšnja vprašanja ne moremo odgovoriti z **enim samim univerzalnim odgovorom.**

Članek v časopisu, ki analizira stroške ali stroškovno učinkovitost tehnologij za izkoriščanje sončne energije, vam lahko pomaga pri seznanjanju s temo, interpretaciji teme ter iskanju dodatnih informacij.

  

Revija za gradbeništvo, v kateri predstavlja direktor podjetja razvoj novih tehnologij in sistemov za izkoriščanje sončne energije, vam bi prav tako lahko bila v korist.

  

A za znanstveno delo na temo „uporaba sončne energije v gospodinjstvih: učinkovitost tehnologij in sistemov za izkoriščanje sončne energije“ je najprimernejša uporaba knjižničnih katalogov (spoznali boste knjižnično zbirko, ki je dostopna zlasti v tiskani obliki), znanstvene revije in podatkovne zbirke.

  

**2.3.7**

V **kvizu 2** boste izvedeli več o različnih znanstvenih informacijskih virih.

Iz seznama izberite pravilen odgovor. Nato boste prejeli povratno informacijo.

1.  Najnovejše in od strokovnjakov preverjene informacije določene teme najdete:
    

( ) V učbenikih __Učbeniki vsebujejo preverjene in kategorizirane podatke na določeno temo. Največkrat ne vsebujejo najnovejših ugotovitev.}}

(x) V podatkovni zbirki z znanstvenimi članki __Da, informacije na znanstveni ravni so velikokrat objavljene v znanstvenih člankih in navedene v podatkovnih zbirkah.}}

( ) V enciklopedijah. __Enciklopedije vsebujejo načeloma le informacije, ki so splošno znane.}}

( ) Na spletnih portalih {Na spletnih portalih najdemo največkrat posplošene informacije, ki v prvi vrsti niso znanstvene.}}

  

2\. Splošne informacije na določeno temo lahko najdete:

||Dva odgovora sta pravilna!<<

\[ \] V znanstvenih knjigah __ izbrano: A) Da, a le včasih - to je odvisno od teme in zgradbe knjige. }}

\[x\] V znanstvenih revijah __ izbrano: B) Da, v nekaterih specifičnih znanstvenih revijah - največkrat v recenzijah. }}

\[x\] V enciklopedijah __ izbrano: C) Da, enciklopedije vsebujejo načeloma ali informacije o osnovnih temah, ki so splošno znane - splošne enciklopedije - ali pregled nad specifičnimi informacijami - strokovne enciklopedije.}}

\[ \] Na spletnih družbenih omrežjih, kot sta Facebook, Twitter itd.

__ izbrano: D) Na spletnih družbenih omrežjih lahko komuniciramo s prijatelji in delimo informacije.

Obstajajo tudi spletna družbena omrežja, na katerih lahko delite znanstvene raziskave - a to niso osnovne informacije, marveč specifične in znanstvene informacije.}} __ ((B C)) Pravilno. B) Da, v nekaterih specifičnih znanstvenih revijah - največkrat v recenzijah. C) Da, enciklopedije vsebujejo načeloma ali informacije o osnovnih temah, ki so splošno znane - splošne enciklopedije - ali pregled nad specifičnimi informacijami - strokovne enciklopedije.}} __ ((B)) B je pravilno, a obstaja še en pravilen odgovor. }} __ ((C)) C je pravilno, a obstaja še en pravilen odgovor. }}