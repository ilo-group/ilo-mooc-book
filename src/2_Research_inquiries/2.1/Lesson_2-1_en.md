﻿---
title: How to identify topics and formulate research questions?
permalink: /2_Inquiries/2_Research_Questions_en/
eleventyNavigation:
    order: 2
    parent: 2_Inquiries
---

## 2.1 How to identify topics and formulate research questions?

At the end of this lesson, you will be able to:

*   identify information gaps in your own knowledge
*   identify the topic based on the exploration of the area of the research or task
*   recognize that the variety of research questions exist depending on the context
*   know how to determine research questions derived from the broader topic in order to focus and to narrow the topic

### 2.1.1

Let’s start with the inquiry journey
[Video no 1.]

### 2.1.2

On this journey, you will have a friend Marco [picture]

In the given scenario, through examples to which you can relate, you will follow student named Marco and observe how can he start his research from a broader topic in order to arrive to the concrete research questions that will guide him in his further research.

We hope you will enjoy your journey and we wish you a pleasant exploration.

[Updated on 13-03-19, Helena deleted]

### 2.1.3

Marco has an assignment to research and write about <span style="color: #0000ff;">new sources of energy</span>.

Marco is an undergraduate student. His assignment is to research and write about the new sources of energy.

There is a variety of information available on this topic. He knows that this topic was investigated by scientists dealing with the possibilities of applying solar energy technologies, city council consultants planning the development of the city, representatives of the city administration considering how to reduce pollution, economists calculating the possibilities of savings in production of electricity, business people developing new products in exploiting local water resources, and activists and members of various associations dealing with issues of protecting human environment. Also, the information on this issue is investigated by architects who design new buildings, people who are looking for ways to save money on electric bills in their own homes, etc.

In order to start the search for information and information resources Marco has a task of determining **what his area of interest is** and **which information and sources of information he needs**.

He is interested in exploitation of solar energy in household heating systems and in the technologies that are developed in that area. In order to focus his research toward the topic, he has to ask concrete research questions. By doing so, he can **narrow down the topic** of his work. For the purposes of this task, he is not interested in information that can be found on all possible types of sources of energy, technologies, usage, or advantages and disadvantages. Marco is interested in the application of solar energy in households. He knows that there is a lot of discussion about the effectiveness of buildings using solar systems for generating electricity, heating, cooling, etc., but he does not know enough about the application of such systems, the technology and the scientific evidence of system effectiveness.

Therefore, he **decides to direct** his work towards solar technologies in buildings and family homes, developed in the last five years.

### 2.1.4

Marco took a number of steps [_Picture on the left side of the text - stairs with Marco on the first step_]

**First step:**

In order to obtain a wider perspective of the household application of solar energy, he sought information about solar energy in the encyclopedia, for example, in the Encyclopaedia Britannica - [https://www.britannica.com/topic/solar-energy](https://www.britannica.com/topic/solar-energy), and from that concept he started researching the related concepts such as solar heating, solar technology and building construction.

While doing the research it is important to **know the broader and the narrower areas of the topics** because, further down the path, the search will return **better results**.

Where does the topic belong? Is it physics or a subfield of physics? Engineering? Earth Science? Ecology? Economics? Architecture? Technical Sciences? Social Sciences? etc.

### 2.1.5

**Second step** [Picture of the stairs with Marko on the second step]

He recorded the basic collected information in the form of a **mental map** in order to determine the **key areas and concepts** to be further explored. These areas and concepts will lead him while writing the content and while further focusing on the topic. See how he created the mind map:

[/static/Multimedia-2.1.5-1.png]

### 2.1.6

**Third step [**_Picture of the stairs with Marco on the 3rd step**]**_

Now Marco has the key ideas that will lead him in his research. To be focused in his further research, he also has to **draw the precise research questions**. He came up with the following research questions:

*   What is solar technology?
*   What solar technologies have been developed for residential buildings over the past five years?
*   What requirements are there in the design and construction of residential buildings?
*   What are the possible comparative advantages and disadvantages of different solar systems and technologies in regards to their effectiveness?

These **research questions will direct** Marco in his information seeking and in writing the assignment. Let’s see what is going on in the next step!

### 2.1.7

**Fourth step **[_Picture of the stairs with Marco on the 4th step_]

During the brainstorming about the topic and information seeking, Marco communicates with **other students** to gather more ideas about where to search in order to improve his research. Some people who know more about this field can help him so he used **social networks** for gaining new ideas. Such information can help him to enrich his mental map and take further steps.

### 2.1.8

**To sum up... ** [_Picture of the stairs with Marco on the 5th step, picture is on the left side, and the text on the right side_]

<div style="background-color: lightyellow;">

To identify the topic and formulate research questions:

1.  Think what you **already know** about the topic and what would you like to know
2.  Think about the area and **narrower field** of the topic
3.  Get a **wider picture** of the topic; search for basic information and share your ideas with others
4.  Make **notes** and organize them (it is good to draw a mind map and upgrade it continually)
5.  Ask precise and concrete **research questions** that will lead you in your research; reframe them if necessary during brainstorming and later searching. Ask open questions such as: who, where, how, how much, why?

</div>


### 2.1.9 Exercise

## Take this exercise on your own topic:

Use the _online notes with self-assesment_, below these instructions. You can save your responses and bookmark this Unit 2.1.9\. You can also take a paper and pencil if you like it. ![Paper and pencil](/static/Pencil.png)

ONLINE NOTES WITH SELF-ASSESMENT

This assignment has several steps. In the first step, you'll provide a response to the prompt. The other steps appear below the Your Response field.

Your Response

Enter your response to the prompt. You can save your progress and return to complete your response at any time. After you submit your response, you cannot edit it.¸

The prompt for this section

1. Think about a topic you are interested in or start from a topic from your actual assignment, and write down the topic you have chosen:

Your response (required)

2. What do you already know about the topic? Write down the basic information you already have about it:

3. What is the broader and narrower field your topic fits into? Write down:

4. Search for basic information on this topic and write down three key ideas worth exploring!

5. Create a mental map for your topic and put a link to it here. By using a mind map you can show the information that you have collected so far. There are many ways to present fields, terms and concepts by using a mind map.

Search the web for some examples of mental or mind maps and their design (e.g. https://www.google.hr/search?q=online+tool+for+creating+mind+maps ). There are many tools that can help you in creating your own mental or mind map.

6. Write three to five precise and concrete research questions that will lead you in your further research and in writing a paper:

7. If You are using this MOOC as part of a school / university classroom activities, share your ideas, questions and possible concerns with peers. In doing so, write down useful links that you’ve found (for example, research questions for particular topic, links to encyclopedias for gathering basic information on different topics, examples of mind maps, etc.

### 2.1.10 - Self-assesment quiz

1. When doing a research for your assigned topic it is necessary to

( ) randomly look for information _You can specify optional feedback like this, which appears after this answer is submitted._
(x) explore the assignment topic

2. To determine the topic it is necessary to know
what is broader and narrower field
basic literature about methodology
none of the above

3. Why is it important to know and understand the concepts in your area of interest?

(x) without them I can’t get to the topic that really interests me 
( ) concepts are not so crucial for determining the topic 
( ) the more concepts you include, the more topics you will cover

4. What are the first two steps in the process of getting to the topic

All correct answers must be selected.

I search the internet
I ask friends and colleagues for advice
I'm thinking about my subject
I'm making a mental map on the subject
I write what I know about the topic and what I'm really interested in


### 2.1.11 For your future work...

There are many online resources where you can find further **instructions how to formulate research questions**.

For example, try with these suggested readings:

Research Rundowns. Writing Research Questions. [https://researchrundowns.com/intro/writing-research-questions/](https://researchrundowns.com/intro/writing-research-questions/)

Center for Innovation in Research and Teaching. Writing a Good Research Question. [https://cirt.gcu.edu/research/developmentresources/tutorials/question](https://cirt.gcu.edu/research/developmentresources/tutorials/question)

