---
title: Com identificar temes i formular preguntes de recerca?
permalink: /2_Inquiries/2_Research_Questions_cat/
eleventyNavigation:
    order: 2
    parent: 2_Inquiries
---


## 2.1 Com identificar temes i formular preguntes de recerca?


**Lliçó 2.1 Com identificar els temes i formular preguntes d'investigació?**

Al final d'aquesta lliçó, podràs:

* identificar llacunes d'informació en el teu propi coneixement
* identificar el tema sobre la base de l'exploració de l'àrea de la recerca o treball
* reconèixer que la varietat de preguntes de recerca varia en funció del context
* saber com determinar les preguntes de recerca derivades del tema més ampli per poder enfocar i acotar el tema

### 2.1.1

Comencem amb el viatge de recerca

<video controls width="500">
	<source src="/videos/2-1-1-ca.mp4">
</video>

### 2.1.2


En aquest viatge, tindràs un amic, en Marco. _[picture]_

A l'escenari plantejat, a través d'exemples amb els quals puguis indentificar-te, seguiràs a l'estudiant Marco i observaràs com pot començar la seva recerca a partir d'un tema més ampli per arribar a les preguntes concretes de recerca que el guiaran en la seva recerca posterior.

Esperem que gaudeixis del teu viatge i et desitgem una agradable exploració.

### 2.1.3

En Marco ha de fer un treball sobre <span style="color: #0000ff;">noves fonts d'energia.</span>

En Marco és un estudiant de grau. Ha d'investigar i escriure sobre noves fonts d'energia.

Hi ha molta informació sobre aquest tema. Sap que aquest tema ha estat tractat per científics que estudien les possibilitats d'aplicació de les tecnologies de l'energia solar, per consultors urbanístics que planifiquen el desenvolupament de la ciutat, per representants de l'administració municipal que estudien com reduir la contaminació, per economistes que calculen les possibilitats d'estalviar la producció d'electricitat, per empresaris que desenvolupen nous productes per a l'ús dels recursos hídrics locals, per activistes i membres de diverses associacions que estudien qüestions relacionades amb la protecció del medi ambient. La informació sobre aquest tema també està sent estudiada per arquitectes que dissenyen nous edificis, persones que busquen formes d'estalviar diners en les seves pròpies factures d'electricitat, etc., així com pel públic que està interessat en l'ús dels recursos hídrics locals.

Per iniciar la recerca d'informació i recursos d'informació, en Marco ha de determinar **quina és la seva àrea d'interès i quina informació i fonts d'informació necessita.**

Està interessat en l'ús de l'energia solar en sistemes de calefacció domèstica i en les tecnologies que s'estan desenvolupant en aquesta àrea. Per poder enfocar la seva investigació sobre el tema, necessita fer preguntes concretes de recerca. D'aquesta manera, pot **acotar el tema del seu treball**. Per als fins d'aquesta tasca, no està interessat en informació sobre totes les possibles fonts d'energia, tecnologies, usos o avantatges i desavantatges. En Marco està interessat en l'ús de l'energia solar a les llars. Sap que hi ha molta literatura sobre l'eficàcia dels edificis que utilitzen sistemes solars per generar electricitat, calefacció, refrigeració, etc., però no sap prou sobre l'aplicació d'aquests sistemes, la tecnologia i l'evidència científica de la seva eficàcia.

Per tot això, decideix centrar el seu treball en les tecnologies solars en edificis i habitatges unifamiliars que s'han desenvolupat en els últims cinc anys.

### 2.1.4

En Marco va fer una sèrie de passos. [_Picture on the left side of the text - stairs with Marco on the first step_]

**Primer pas:**

Per tal d'obtenir una perspectiva més àmplia sobre l'ús domèstic de l'energia solar, va buscar informació sobre l'energia solar en una enciclopèdia, per exemple, l'Encyclopaedia Britannica - [https://www.britannica.com/topic/solar-energy](https://www.britannica.com/topic/solar-energy), i a partir d'aquest concepte va començar a investigar els conceptes relacionats com la calefacció solar, la tecnologia solar i la construcció d'edificis.

Durant la investigació és important conèixer les **àrees més generals i específiques** dels temes, ja que en el procés posterior de cerca s'obtindran **millors resultats**.

A quina àrea pertany el tema?, és física o un subcamp de la física?, enginyeria?, ciències de la terra?, ecologia?, economia?, arquitectura?, tecnologia?, ciències socials?, etc.


### 2.1.5

**Second step** _[Picture of the stairs with Marko on the second step]_

Va agafar la informació bàsica i la va recopilar en forma d'un **mapa mental** per determinar les **àrees clau** i els **conceptes** que s'havien d'explorar més a fons. Aquestes àrees i conceptes el guiaran mentre escriu el contingut i continua enfocant-se en el tema. Mira com va crear el mapa mental:

[/static/Multimedia-2.1.5-1.png]

### 2.1.6

**Tercer pas** _[Picture of the stairs with Marco on the 3rd step]_

Ara en Marco ja té les idees clau que el guiaran en la seva recerca. Per poder centrar-se en la seva recerca posterior, també ha de plantejar les preguntes exactes de la recerca. Se li han ocorregut les següents:

*  Què és la tecnologia solar?
*  Quines tecnologies solars s'han desenvolupat per a edificis residencials en els últims cinc anys?
*  Quins requisits hi ha al disseny i la construcció d'edificis residencials?
*  Quins són els possibles avantatges i desavantatges comparatius dels diferents sistemes i tecnologies solars quant a la seva eficàcia?

Aquestes preguntes de recerca orientaran a en Marc en la seva cerca d'informació i en la redacció del treball. Vegem què succeeix en el següent pas!

### 2.1.7

**Quart pas**[_Picture of the stairs with Marco on the 4th step_]

Durant el _brainstorming_ -pluja d'idees- sobre el tema i la seva cerca d'informació, en Marco es comunica amb **altres estudiants** per reunir més idees sobre on buscar per millorar la seva investigació. Algunes persones que saben més sobre aquest camp poden ajudar-lo, així que utilitza les **xarxes socials** per obtenir noves idees. Aquesta informació pot ajudar a enriquir el seu mapa mental i orientar els seus propers passos.

### 2.1.8

**Visió general dels passos anteriors** [_Picture of the stairs with Marco on the 5th step, picture is on the left side, and the text on the right side_]

<div style="background-color: lightyellow;">

Passos **bàsics** per identificar el tema i formular preguntes de recerca:

*  Pensa **en el que ja saps** sobre el tema i el que t'agradaria saber
*  Pensa en **l'àrea** i en el **camp més específic** del tema
*  Aconsegueix una **visió més àmplia** del tema; busca informació bàsica i **comparteix les teves idees amb els altres**
*  Pren **notes** i organitza-les (és bo dibuixar un mapa mental i actualitzar-lo contínuament)
*  Fes **preguntes de recerca** precises i concretes que et guiaran en la teva investigació; reformula-les si cal durant el brainstorming i la cerca posterior. Fes preguntes obertes com: qui?, on?, com?, quant?, per què?


</div>


### 2.1.9 Exercici

## FES AQUEST EXERCICI SOBRE EL TEU PROPI TEMA:

1. Pensa en un tema que t'interessi o comença a partir d'un tema del teu treball real. Pren un paper i un llapis i escriu el tema que has triat: Paper and pencil

També pots utilitzar les anotacions en línia amb autoavaluació, sota d'aquestes instruccions. Pots guardar les teves respostes i posar un marcador en aquesta Unitat 2.1.9.

2. Què és el que ja saps sobre el tema? Anota la informació bàsica que ja tens sobre el tema. 

3. Quin és el camp més general i més específic en el qual encaixa el teu tema? Anota'ls: 

4. Busca informació bàsica sobre aquest tema i escriu tres idees clau que valgui la pena explorar.

5. Crea un mapa mental per al teu tema i posa'l aquí. Gràcies al mapa mental pots fer visible la informació que ha recollit fins ara. Hi ha moltes maneres de presentar camps, termes i conceptes utilitzant un mapa mental.

Cerca a internet alguns exemples de mapes mentals i disseny de mapes mentals (per exemple: https://www.google.hr/search?q=online+tool+for+creating+mind+maps). Hi ha moltes eines que poden ajudar-te a crear el teu mapa mental.

6. Anota entre tres i cinc preguntes de recerca precises i concretes que et guiaran en la teva investigació posterior i en la redacció d'un treball: 

7. Si utilitzes aquest MOOC com a part de les activitats escolars o universitàries, comparteix les teves idees, preguntes i possibles preocupacions amb els teus companys. En fer-ho, anota els enllaços útils que hagis trobat (per exemple, preguntes de recerca sobre un tema en particular, enllaços a enciclopèdies per recopilar informació bàsica sobre diversos temes, exemples de mapes mentals, etc.). 


Anotacions en línia amb autoavaluació


1. Anota el teu tema.
2. Què és el que ja saps sobre el tema?

3. Quin és el camp més general i més específic en el qual encaixa el teu tema?

4. Busca informació bàsica sobre aquest tema i anota tres idees clau que valgui la pena explorar.
5. Crea un mapa mental per al teu tema amb alguna eina en línia per crear mapes mentals. Aquí pots escriure (enganxar) l'enllaç al teu mapa mental.
6. Escriu de tres a cinc preguntes de recerca precises i concretes.

7. Anota enllaços útils, idees i preguntes per a futures discussions amb els teus companys.


### 2.1.10 - Test d'autoavaluació

1. Quan fas una recerca per al teu tema assignat és necessari:

( ) buscar informació a l'atzar 
_You can specify optional feedback like this, which appears after this answer is submitted.__

(x) explorar el tema assignat

2. Per determinar el tema cal saber:

(x) quin és el camp més ampli i el més específic
( ) bibliografia bàsica sobre metodologia
( ) cap dels anteriors

3. Per què és important conèixer i entendre els conceptes en la teva àrea d'interès?


(x) sense ells no puc arribar al tema que realment m'interessa

( ) els conceptes no són tan crucials per determinar el tema

( ) com més conceptes incloguis, més temes cobriràs

4. Quins són els dos primers passos en el procés d'arribar al tema?

Selecciona totes les respostes correctes.

[ ] busco a internet
[ ] demano consell a amics i companys
[x] penso sobre el meu tema
[ ] faig un mapa mental sobre el tema
[x] anoto allò que sé sobre el tema i el que m'interessa d'ell


### 2.1.11 Per al teu treball futur…**

Hi ha molts recursos en línia on trobar més indicacions sobre com formular preguntes de recerca.

Per exemple, prova amb aquestes lectures que et suggerim:

Research Rundowns. Writing Research Questions.
[https://researchrundowns.com/intro/writing-research-questions/](https://researchrundowns.com/intro/writing-research-questions/)

Center for Innovation in Research and Teaching. Writing a Good Research Question. [https://cirt.gcu.edu/research/developmentresources/tutorials/question](https://cirt.gcu.edu/research/developmentresources/tutorials/question)

