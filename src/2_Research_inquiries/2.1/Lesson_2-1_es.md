---
title: ¿Cómo identificar temas y formular preguntas de investigación?
permalink: /2_Inquiries/2_Research_Questions_es/
eleventyNavigation:
    order: 2
    parent: 2_Inquiries
---


## 2.1 ¿Cómo identificar temas y formular preguntas de investigación?

**Introducción a la Lección 2.1**

Al final de esta lección, podrás:

*   dentificar lagunas de información en tu propio conocimiento

*  identificar el tema en base a la exploración del área de la investigación o trabajo

*  reconocer que la variedad de preguntas de investigación varía en función del contexto

*  saber cómo determinar las preguntas de investigación derivadas del tema más amplio para poder enfocar y acotar el tema

### 2.1.1

Comencemos con el viaje de investigación
_[Video no 1.]_

### 2.1.2

En este viaje, tendrás un amigo, Marco. [picture]

En el escenario que se plantea, a través de ejemplos con los que puedas indentificarte, seguirás al estudiante Marco y observarás cómo puede comenzar su investigación a partir de un tema más amplio para llegar a las preguntas concretas de investigación que lo guiarán en su investigación posterior.

Esperamos que disfrutes de tu viaje y te deseamos una agradable exploración

### 2.1.3

Marco tiene que hacer un trabajo sobre nuevas fuentes de energía.

Marco es un estudiante de grado. Tiene que investigar y escribir sobre nuevas fuentes de energía.

Hay mucha información sobre este tema. Sabe que este tema ha sido tratado por científicos que estudian las posibilidades de aplicación de las tecnologías de la energía solar, por consultores urbanísticos que planifican el desarrollo de la ciudad, por representantes de la administración municipal que estudian cómo reducir la contaminación, por economistas que calculan las posibilidades de ahorrar la producción de electricidad, por empresarios que desarrollan nuevos productos para el uso de los recursos hídricos locales, por activistas y miembros de diversas asociaciones que estudian cuestiones relacionadas con la protección del medio ambiente. La información sobre este tema también está siendo estudiada por arquitectos que diseñan nuevos edificios, personas que buscan formas de ahorrar dinero en sus propias facturas de electricidad, etc., así como por el público que está interesado en el uso de los recursos hídricos locales.

Para iniciar la búsqueda de información y recursos de información, Marco tiene que determinar **cuál es su área de interés y qué información y fuentes de información necesita.**

Está interesado en el uso de la energía solar en sistemas de calefacción doméstica y en las tecnologías que se están desarrollando en esta área. Para poder enfocar su investigación sobre el tema, necesita hacer preguntas concretas de investigación. De esta manera, puede **acotar el tema de su trabajo**. Para los fines de esta tarea, no está interesado en información sobre todas las posibles fuentes de energía, tecnologías, usos o ventajas y desventajas. Marco está interesado en el uso de la energía solar en los hogares. Sabe que hay mucha literatura sobre la eficacia de los edificios que utilizan sistemas solares para generar electricidad, calefacción, refrigeración, etc., pero no sabe lo suficiente sobre la aplicación de dichos sistemas, la tecnología y la evidencia científica de su eficacia.

Por todo ello, decide centrar su trabajo en las tecnologías solares en edificios y viviendas unifamiliares que se han desarrollado en los últimos cinco años.

### 2.1.4

Marco hizo una serie de pasos. [_Picture on the left side of the text - stairs with Marco on the first step_]

**Primer paso:**

Con el fin de obtener una perspectiva más amplia sobre el uso doméstico de la energía solar, buscó información sobre la energía solar en una enciclopedia, por ejemplo, en la Encyclopaedia Britannica -  [https://www.britannica.com/topic/solar-energy](https://www.britannica.com/topic/solar-energy), y a partir de ese concepto comenzó a investigar los conceptos relacionados como la calefacción solar, la tecnología solar y la construcción de edificios.

Durante la investigación es importante conocer las **áreas más generales y específicas** de los temas, ya que en el proceso posterior de búsqueda se obtendrán **mejores resultados.**

¿A qué área pertenece el tema?, ¿es física o un subcampo de la física?, ¿ingeniería?, ¿ciencias de la Tierra?, ¿ecología?, ¿economía?, ¿arquitectura?, ¿tecnología?, ¿ciencias sociales?, etc.


### 2.1.5

**Segundo paso** _[Picture of the stairs with Marko on the second step]_

Tomó la información básica recopilada en forma de un **mapa mental** para determinar las **áreas clave** y los **conceptos** que debían explorarse más a fondo. Estas áreas y conceptos lo guiarán mientras escribe el contenido y continúa enfocándose en el tema. Mira cómo creó el mapa mental:
[/static/Multimedia-2.1.5-1.png]

### 2.1.6

**Tercer paso** _[Picture of the stairs with Marco on the 3rd step]_

Ahora Marco ya tiene las ideas clave que lo guiarán en su investigación. Para poder centrarse en su investigación posterior, también debe plantear las preguntas exactas de la investigación. Se le han ocurrido las siguientes:

*  ¿Qué es la tecnología solar?
*  ¿Qué tecnologías solares se han desarrollado para edificios residenciales en los últimos cinco años?
*  ¿Qué requisitos existen en el diseño y la construcción de edificios residenciales?
*  ¿Cuáles son las posibles ventajas y desventajas comparativas de los diferentes sistemas y tecnologías solares en cuanto a su eficacia?

Estas preguntas de investigación orientarán a Marco en su búsqueda de información y en la redacción del trabajo. ¡Veamos qué sucede en el siguiente paso!

### 2.1.7

**Cuarto paso** [_Picture of the stairs with Marco on the 4th step_]

Durante el _brainstorming_ -lluvia de ideas- sobre el tema y su búsqueda de información, Marco se comunica con **otros estudiantes** para reunir más ideas sobre dónde buscar para mejorar su investigación. Algunas personas que saben más sobre este campo pueden ayudarle, así que utiliza las **redes sociales** para obtener nuevas ideas. Esta información puede ayudarle a enriquecer su mapa mental y orientar sus próximos pasos.

### 2.1.8

**Visión general de los pasos anteriores** [_Picture of the stairs with Marco on the 5th step, picture is on the left side, and the text on the right side_]

<div style="background-color: lightyellow;">

Pasos **básicos** para identificar el tema y formular preguntas de investigación::

* Piensa en **lo que ya sabes** sobre el tema y lo que te gustaría saber
* Piensa en el **área** y en el **campo más específico** del tema
* Obtén una **visión más amplia** del tema; busca información básica y **comparte tus ideas con los demás**
* Toma **notas** y organízalas (es bueno dibujar un mapa mental y actualizarlo continuamente)
* Haz **preguntas de investigación** precisas y concretas que te guiarán en tu investigación; reformúlalas si es necesario durante el brainstorming y la búsqueda posterior. Haz preguntas abiertas como: ¿quién?, ¿dónde?, ¿cómo?, ¿cuánto?, ¿por qué?

</div>


### 2.1.9 Ejercicio

## HAZ ESTE EJERCICIO SOBRE TU PROPIO TEMA:

1. Piensa en un tema que te interese o empieza a partir de un tema de tu trabajo real. Toma un papel y un lápiz y escribe el tema que has elegido: Paper and pencil

También puedes utilizar las anotaciones en línea con autoevaluación, debajo de estas instrucciones. Puedes guardar tus respuestas y poner un marcador en esta Unidad 2.1.9.

2. ¿Qué es lo que ya sabes acerca del tema? Anota la información básica que ya tienes sobre el tema.  

3. ¿Cuál es el campo más general y más específico en el que encaja tu tema? Anótalos: 

4. Busca información básica sobre este tema y escribe tres ideas clave que valga la pena explorar.

5. Crea un mapa mental para tu tema y ponlo aquí. Gracias al mapa mental puedes hacer visible la información que ha recolectado hasta ahora. Hay muchas maneras de presentar campos, términos y conceptos utilizando un mapa mental.

Busca en la web algunos ejemplos de mapas mentales y diseño de mapas mentales (por ejemplo: https://www.google.hr/search?q=online+tool+for+creating+mind+maps). Hay muchas herramientas que pueden ayudarte a crear tu mapa mental. 

6. Anota entre tres y cinco preguntas de investigación precisas y concretas que te guiarán en tu investigación posterior y en la redacción de un trabajo: 

7. Si utilizas este MOOC como parte de las actividades escolares o universitarias, comparte tus ideas, preguntas y posibles preocupaciones con tus compañeros. Al hacerlo, anota los enlaces útiles que hayas encontrado (por ejemplo, preguntas de investigación sobre un tema en particular, enlaces a enciclopedias para recopilar información básica sobre varios temas, ejemplos de mapas mentales, etc.). 
ONLINE NOTES WITH SELF-ASSESMENT

This assignment has several steps. In the first step, you'll provide a response to the prompt. The other steps appear below the Your Response field.

Your Response

Enter your response to the prompt. You can save your progress and return to complete your response at any time. After you submit your response, you cannot edit it.¸

The prompt for this section

1. Escribe tu tema.

Your response (required)

2. ¿Qué es lo que ya sabes sobre el tema?

3. ¿Cuál es el campo más general y más especifico en el que encaja tu tema?

4. Busca información básica sobre este tema y escribe tres ideas clave que valga la pena explorar.

5. Crea un mapa mental para tu tema con alguna herramienta en línea para crear mapas mentales. Aquí puedes escribir (pegar) el enlace a tu mapa mental.


Search the web for some examples of mental or mind maps and their design (e.g. https://www.google.hr/search?q=online+tool+for+creating+mind+maps ). There are many tools that can help you in creating your own mental or mind map.

6. Escribe de tres a cinco preguntas de investigación precisas y concretas.

7. Escribe enlaces útiles, ideas y preguntas para futuras discusiones con tus compañeros.


### 2.1.10 - Test de autoevaluación

1. Al hacer una investigación para tu tema asignado es necesario:

( ) buscar información al azar

(x) explorar el tema asignado


2. Para determinar el tema es necesario saber:

(x) cuál es el campo más amplio y el más específico

( )bibliografía básica sobre metodología

( )ninguno de ellos


3. ¿Por qué es importante conocer y entender los conceptos en tu área de interés?

(x) sin ellos no puedo llegar al tema que realmente me interesa

( ) los conceptos no son tan cruciales para determinar el tema

( ) cuantos más conceptos incluyas, más temas cubrirás

4. ¿Cuáles son los dos primeros pasos en el proceso de llegar al tema?

All correct answers must be selected.

[ ]busco en internet

[ ] pido consejo a amigos y colegas

[x] pienso sobre mi tema

[ ] hago un mapa mental sobre el tema

[x] anoto lo que sé sobre el tema y lo que me interesa de él


### 2.1.11 Para tu trabajo futuro....

Hay muchos recursos en línea donde puede encontrar más indicaciones sobre cómo formular preguntas de investigación.

Por ejemplo, prueba con estas lecturas que te sugerimos:

Research Rundowns. Writing Research Questions.[https://researchrundowns.com/intro/writing-research-questions/](https://researchrundowns.com/intro/writing-research-questions/)

Center for Innovation in Research and Teaching. Writing a Good Research Question. [https://cirt.gcu.edu/research/developmentresources/tutorials/question](https://cirt.gcu.edu/research/developmentresources/tutorials/question)

