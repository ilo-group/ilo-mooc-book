---
title: Kako določiti temo in raziskovalno vprašanje?
permalink: /2_Inquiries/2_Research_Questions_sl/
eleventyNavigation:
    order: 2
    parent: 2_Inquiries
---

**2.1 Kako določiti temo in raziskovalno vprašanje?**

  

Na koncu lekcije boste sposobni:

\- prepoznati vrzeli v znanju

\- določiti raziskovalno področje s tem, da raziščete in se seznanite z določeno temo ali nalogo

\- razumeti, da obstajajo različne vrste raziskovalnih vprašanj, ki so odvisne od določenega konteksta

\- določiti raziskovalno vprašanje, ki izhaja iz širše tematike, s katerim se osredotočite na določeno temo in jo s tem tudi omejite.

  

2.1.1

Začnimo s preiskovalnim potovanjem.

\[Video no 1.\]

  

2.1.2

Na potovanju vas bo spremljal Marko. \[slika\]

Študenta z imenom Marko lahko opazujete pri iskanju informacij, skozi primere, ki so vam znani. Najprej se bo ukvarjal s širšim področjem in nato določil raziskovalna vprašanja, ki mu bodo pomagala pri nadaljnjem delu – raziskovanju.

Upamo, da boste v potovanju uživali - želimo vam in Marku prijetno raziskovanje.

\[Updated on 13-03-19, Helena deleted\]

  

2.1.3

Marko mora raziskati in napisati nalogo na temo „novi viri energije“.

Marko je študent dodiplomskega študijskega programa. Njegova naloga je napisati nalogo o novih virih energije.

Obstaja mnogo informacij na to temo. Marko se zaveda, da so se znanstveniki ukvarjali z razvojem tehnologije za izkoriščanje sončne energije, svetovalci mestnega sveta z razvojem mesta, predstavniki mestne občine z ukrepi za zmanjševanje onesnaženja okolja, ekonomisti z izračunavanjem prihranka, ki nastane pri proizvodnji elektrike, poslovneži z razvojem novih proizvodov za izkoriščanje lokalnih vodotokov in izvirov in aktivisti ter člani različnih društev in združenja s težavami in vprašanji, ki se pojavijo v sklopu z varstvom okolja. Poleg tega se tej temi posvetijo prav tako arhitekti, ki načrtujejo stavbe, ljudje, ki si želijo prihraniti denar pri porabi električne energije v svojih domovih itd.

Preden se bo Marko posvetil iskanju informacij in informacijskih virov, mora določiti področje, s katerim se želi ukvarjati, ter informacije in informacijske vire, ki jih bo potreboval.

Zanima se za ogrevalne sisteme za izkoriščanje sončne energije v gospodinjstvih ter za tehnologije in orodja, ki omogočajo pretvarjanje sončne energije v električno energijo.

Marko mora oblikovati ustrezno raziskovalno vprašanje, ki mu bo omogočalo iskanje temu primernih informacij. S tem lahko zoži temo svojega dela. Namen njegove naloge ni najti čim več informacij, ki se nanašajo na vrste virov energije, tehnologij, njihovo uporabo ali na prednosti ter pomanjkljivosti. Marka zanima le uporaba tehnologij za izkoriščanje sončne energije v gospodinjstvih. On se zaveda, da se trenutno veliko razpravlja o energetski učinkovitosti stavb, ki izkoriščajo sončno energijo za proizvodnjo elektrike, ogrevanje, hlajenje itd., vendar mu uporaba tovrstnih sistemov, tehnologije ter znanstveni dokazi o učinkovitosti teh naprav ni jasna. Zaradi tega se je odločil, da se bo **osredotočil** na uporabo tehnologije za izkoriščanje sončne energije v stavbah in gospodinjstvih, ki so bili zgrajeni v zadnjih petih letih.

  

2.1.4

Marko je naredil več pomembnih korakov. \[slika na levi strani besedila - stopnice - Marko je videti na prvi stopnici\]

  

**Prvi korak:**

Da bi pridobil več informacij o uporabi tehnologij za izkoriščanje sončne energije v gospodinjstvih, je informacije o sončni energiji iskal v enciklopediji, kot je na primer Encyclopaedia Britannica [https://www.britannica.com/topic/solar-energy](https://www.britannica.com/topic/solar-energy). Nato je začel iskati in zbirati sorodne pojme, kot so ogrevanje s sončno energijo, tehnologije za izkoriščanje sončne energije in gradnja stavb.

Med iskanjem in zbiranjem informacij je dobro poznati **širše in ožje pojme področja**, saj to zagotavlja **boljše zadetke.**

Kam spada tema? Pripada fiziki ali podpodročju fizike? Strojništvu? Geologiji? Ekologiji? Ekonomiji? Arhitekturi? Tehniškim znanostim? Družboslovnim znanostim? itd.

  

2.1.5

**Drugi korak** \[slika stopnic z Markom, ki stoji na drugi stopnici\]

Marko je najpomembnejše informacije zapisal v obliki **miselnega vzorca.** Tako lahko določi **ključna področja in koncepte,** o katerih želi pridobiti več informacij. Miselni vzorec mu bo ob pisanju naloge pomagal pri osredotočenju na bistveno.

Oglejte si, kako je oblikoval miselni vzorec:

\[/static/Multimedia-2.1.5-1.png\]

  

2.1.6

**Tretji korak** \[slika stopnic z Markom, ki stoji na tretji stopnici\*\*\]\*\*

Marko je zdaj določil ključne zamisli, ki ga bodo spremljale med raziskovanjem.

Poleg tega je moral **postaviti raziskovalna vprašanja**, ki mu bodo pomagala pri osredotočenju na bistveno. Domislil se je naslednjih raziskovalnih vprašanj:

  

*   Kaj je „tehnologija za izkoriščanje sončne energije“?
    
*   Katere tehnologije za pridobivanje energije iz sončne svetlobe za gospodinjstva so bile razvite v zadnjih petih letih?
    
*   Kakšni so pogoji za načrtovanje in gradnjo stanovanjskih zgradb?
    
*   Kakšne so prednosti in pomanjkljivosti različnih tehnologij za izkoriščanje sončne energije glede njihove učinkovitosti?
    

  

Ta raziskovalna vprašanja bodo Marku **pomagala** pri iskanju ustreznih informacij in pisanju naloge.

Oglejte si naslednji korak.

  

2.1.7

**Četrti korak**\[slika stopnic z Markom, ki stoji na četrti stopnici\]

Med viharjenjem možganov in iskanjem informacij se Marko pogovarja **s svojimi kolegi**, ki mu pomagajo z idejami, kje iskati informacije — vse to prispeva k izboljšanju raziskave. Strokovnjaki, ki vedo več o tem področju, mu bi lahko prav tako pomagali — zaradi tega se je za še dodatne ideje pozanimal na **družbenih omrežjih**. Informacije, ki so dostopne na družbenih omrežjih, lahko dopiše v miselni vzorec in se nato posveti naslednjim korakom.

  

2.1.8

**K povzetku…** \[slika stopnic z Markom, ki stoji na peti stopnici — slika je na levi strani, besedilo na desni\]

  

Osnovni koraki pri določanju teme in postavljanju raziskovalnih vprašanj:

1.  Razmislite,kaj že veste in o čem želite izvedeti več.
    
2.  Razmislite o šrišem raziskovalnem področju ter sorodnih podpodročij in ožjih pojmih.
    
3.  Pridobite pregled nad temo; iščite osnovne informacije in delite svoje zamisli z drugimi.
    
4.  Zapišite pripombe in jih uredite (oblikujte miselni vzorec in mu redno pripisujte nove zamisli).
    
5.  Postavite natančna in konkretna raziskovalna vprašanja, ki vas bodo vodila ob raziskavi; če je treba, jih lahko preoblikujete med viharjenjem možganov in kasnejšim iskanjem informacij. Postavljajte odprta vprašanja, kot so: kdo, kje, kako, koliko, zakaj?
    

  

**2.1.9 Vaja**

  

**Naslednja vaja se nanaša na vašo izbrano temo:**

  

Uporabite _online zapiske za samoocenjevanje,_ ki jih najdete pod tem navodilom. Odgovore lahko shranite in dodate enoto 2.1.9 med zaznamke. Če želite, lahko uporabljate tudi list papirja in svinčnik.

  

ONLINE ZAPISKI ZA SAMOOCENJEVANJE

Ta naloga je sestavljena iz več korakov. Po pozivu morate najprej vnesti odgovor. Nadaljnje vaje se bodo pojavile pod vnosnim poljem.

  

Vaš odgovor

Po pozivu vnesite odgovor. Napredek vaje lahko shranite in se vrnite kadarkoli želite ter dopolnite odgovore. Po potrditvi oziroma pošiljanju odgovora ga ne morete več spremeniti.

  

Poziv za to vajo

1.  Razmislite o temi, ki vas zanima, ali o temi svoje seminarske naloge. Zapišite temo, ki ste si jo izbrali.
    

Vaš odgovor (obvezno)

2.  Kaj veste že o temi? Zapišite osnovne informacije, ki se nanašajo na izbrano temo in ki jih poznate.
    
3.  Zapišite splošna in specifična področja izbrane teme.
    
4.  Poiščite osnovne informacije, ki se nanašajo na to temo, in zapišite tri zamisli, ki se vam zdijo vredne raziskati.
    
5.  Oblikujte miselni vzorec za izbrano temo in dodajte tukaj povezavo do nje. Z uporabo miselnega vzorca lahko prikažete podatke, ki ste jih našli do zdaj. Miselni vzorec ponuja več možnosti, kako predstaviti področja, pojme in zamisli.
    

  

Na spletu lahko najdete več primerov, kako oblikovati miselne vzorce (npr. https://www.google.hr/search?q=online+tool+for+creating+mind+maps ). Obstajajo orodja, ki vam lahko pomagajo pri oblikovanju miselnega vzorca.

6.  Postavite tri do pet natančnih in konkretnih raziskovalnih vprašanj, ki vam bodo pomagala pri raziskovanju ter pisanju seminarske naloge.
    
7.  Če uporabljate ta MOOC kot del šolskih ali univerzitetnih dejavnosti, se lahko pogovorite s svojimi kolegi o zamislih, vprašanjih in težavah. Med pogovorom zapišite povezave, ki se vam zdijo koristne (na primer raziskovalna vprašanja za določeno temo, povezave do enciklopedij, primere za miselne vzorce itd.).
    

  

**2.1.10 - KVIZ ZA SAMOOCENJEVANJE**

1.  Ko zbirate informacije na določeno temo, iščete …
    

  

|| << ( ) informacije z naključnim iskanjem __You can specify optional feedback like this, which appears after this answer is submitted.}} (x) informacije, ki se nanašajo na vašo temo.

  

2.  Za določitev teme je potrebno poznati
    

(x) osnovno področje in podpodročja (širše in ožje pojme področja)

( ) osnovno literaturo o metodologiji

( ) odgovor ni pravilen

  

3.  Zakaj je pomembno poznati in razumeti koncepte in zamisli izbranega področja?
    

||<< (x) brez njih se ne morem ukvarjati s temo, ki me zares zanima. ( ) za določitev teme mi ni treba upoštevati njihovih konceptov. ( ) čim več konceptov in zamisli boste vključili, tem več področij boste lahko obravnavali.

  

4.  Katera dva postopka sta na vrsti, ko želite določiti raziskovalno področje?
    

  

Izberite vse pravilne odgovore.

Na spletu iščem koristne informacije.

Prijatelje in kolege prosim za nasvet.

Razmišljam o izbrani temi.

Oblikujem miselni vzorec.

Zapišem informacije, ki so mi znane in me zares zanimajo.

  

2.1.11

**Za vaše prihodnje delo...**

Na spletu lahko najdete veliko informacij in navodil, ki opišejo, **kako postaviti raziskovalna vprašanja**.

Dodali smo nekaj primerov – preizkusite jih:

Research Rundowns. Writing Research Questions. https://researchrundowns.com/intro/writing-research-questions/

Center for Innovation in Research and Teaching. Writing a Good Research Question. https://cirt.gcu.edu/research/developmentresources/tutorials/question