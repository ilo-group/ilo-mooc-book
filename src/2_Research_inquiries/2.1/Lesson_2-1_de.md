---
title: Wie werden Themen identifiziert und Forschungsfragen formuliert?
permalink: /2_Inquiries/2_Research_Questions_de/
eleventyNavigation:
    order: 2
    parent: 2_Inquiries
---


2.1.0

**2.1 Wie werden Themen identifiziert und Forschungsfragen formuliert?**

Am Ende dieser Lektion könnt ihr Folgendes:

* Informationslücken in eurem Wissen identifizieren
* Das Thema auf der Grundlage der Erforschung des Forschungsgebietes oder der Aufgabe identifizieren
* Erkennen, dass die Vielfalt der Forschungsfragen je nach Kontext unterschiedlich ist
* Forschungsfragen ermitteln, die sich aus dem breiteren Themenbereich ergeben, um das Thema zu fokussieren und einzugrenzen

**2.1.1**

Beginnen wir mit der Anfrage Reise
[Video Nr. 1.]

**2.1.2**

Auf dieser Reise wirst du einen Freund Marco haben. (Bild)

Im gegebenen Szenario folgt ihr anhand von Beispielen, auf die ihr euch beziehen könnt, den Studenten Marco. Ihr beobachtet, wie sie ihre Recherche zu einem allgemeinem Thema beginnen können, um zu den konkreten Forschungsfragen zu gelangen, welche die Studenten in ihrer weiteren Forschung leiten werden.

Wir hoffen, dass du deine Reise genießen wirst und wünschen dir, Marco eine angenehme Entdeckungsreise! :)

**2.1.3**


Marco hat den Auftrag über neue Energiequellen zu schreiben.

Marco ist ein Bachelor-Student. Seine Aufgabe ist es, über die neuen Energiequellen zu forschen und zu schreiben.

Zu diesem Thema gibt es eine Vielzahl von Informationen. Er weiß, dass dieses Thema von Wissenschaftlern untersucht wurde, die sich mit den Möglichkeiten der Anwendung von Solarenergietechnologien befassen. Aber auch Stadtratsberater, welche die Entwicklung der Stadt planen und Vertreter der Stadtverwaltung, die sich mit der Frage beschäftigen, wie man die Umweltverschmutzung reduzieren kann, sind an dem Thema interessiert. Auch von Ökonomen, welche die Möglichkeiten der Einsparung bei der Stromerzeugung berechnen und von Geschäftsleuten, die neue Produkte zur Nutzung der lokalen Wasserressourcen entwickeln und von Aktivisten und Mitgliedern verschiedener Verbände, die sich mit Fragen des Schutzes der menschlichen Umwelt befassen, wird das Thema erforscht. Die Informationen zu diesem Thema werden auch von Architekten untersucht, welche neue Gebäude entwerfen, von Menschen, die nach Möglichkeiten suchen, Geld bei Stromrechnungen im eigenen Haus zu sparen, etc.

Um die Suche nach Informations- und Informationsressourcen zu starten, soll Marco bestimmen, **was sein Interessengebiet ist** und **welche Informationen und Informationsquellen er benötigt**.

Marco interessiert sich für die Nutzung der Solarenergie in Haushaltsheizungen und für die Technologien, die in diesem Bereich entwickelt werden. Um seine Forschung auf das Thema auszurichten, muss er konkrete Forschungsfragen stellen. Auf diese Weise kann er das **Thema seiner Arbeit eingrenzen**. Für die Zwecke dieser Aufgabe interessiert er sich nicht für Informationen, die über Energieträger, Technologien, Nutzungen oder Vor- und Nachteile zu finden sind. Marco ist an der Anwendung von Solarenergie in Haushalten interessiert. Er weiß, dass viel über die Effektivität von Gebäuden diskutiert wird, die Solarsysteme zur Erzeugung von Strom, Wärme, Kälte usw. verwenden, aber er weiß nicht genug über die Anwendung solcher Systeme, die Technologie und den wissenschaftlichen Nachweis der Systemeffektivität.

Deshalb beschließt er, seine Arbeit auf Solartechnologien in Gebäuden und Einfamilienhäusern auszurichten, die in den letzten fünf Jahren entwickelt wurden.


**2.1.4**


Marco führt eine Reihe von Schritten durch [_Bild auf der linken Seite des Textes - Treppe mit Marco auf dem ersten Schritt_].

**Erster Schritt:**

Um eine breitere Perspektive für die haushaltsnahe Nutzung der Solarenergie zu erhalten, sucht er Informationen über die Solarenergie in einer Enzyklopädie, z.B. in der Encyclopaedia Britannica -[https://www.britannica.com/topic/solar-energy](https://www.britannica.com/topic/solar-energy) und von diesem Konzept aus beginnt er mit der Erforschung der damit verbundenen Konzepte wie Solarthermie, Solartechnik und Gebäudebau.

Während der Recherche ist es wichtig, **die allgemeineren und spezifischeren Bereiche der Thematik** zu kennen, denn im weiteren Verlauf der Suche werden **bessere Ergebnisse** angezeigt.

Wozu gehört das Thema? Ist es Physik oder ein Teilgebiet der Physik? Ingenieurwesen? Geowissenschaften? Ökologie? Wirtschaft? Architektur? Technische Wissenschaften? Sozialwissenschaften? etc. 



**2.1.5**

**Zweiter Schritt** [Bild der Treppe mit Marko auf dem zweiten Schritt]

Marco nimmt die gesammelten Basisinformationen in Form einer **Mind Map** auf, um die **Schlüsselbereiche und Konzepte** zu bestimmen, die weiter untersucht werden sollen. Diese Bereiche und Konzepte werden ihm beim Schreiben und bei der weiteren Fokussierung auf das Thema leiten. Seht euch an, wie er die Mind Map erstellt hat:

[/statisch/multimedia-2.1.5-1.png]


**2.1.6**

**Dritter Schritt** _[Bild der Treppe mit Marco im dritten Schritt]_

Jetzt hat Marco die Schlüsselideen, die ihn in seiner Forschung leiten werden. Um bei seiner weiteren Forschung fokussiert zu sein, muss er **die genauen Forschungsfragen definieren**. Er stellte sich die folgenden Forschungsfragen:

* Was ist Solartechnik?
* Welche Solartechnologien wurden in den letzten fünf Jahren für Wohngebäude entwickelt?
* Welche Anforderungen gibt es an die Planung und den Bau von Wohngebäuden?
* Welche sind die möglichen Vor- und Nachteile verschiedener Solarsysteme und -technologien in Bezug auf ihre Wirksamkeit?

Diese **Forschungsfragen leiten**  Marco bei der Informationssuche und beim Schreiben seiner Arbeit. Mal sehen, was im nächsten Schritt passiert!

**2.1.7**

**Vierter Schritt **[_Bild der Treppe mit Marco auf dem 4. Schritt_]

Während des Brainstormings über das Thema und der Informationssuche kommuniziert Marco mit **anderen Studenten**, um weitere Ideen zu sammeln, wo er suchen sollte, um seine Forschung zu verbessern. Einige Leute, die mehr über dieses Gebiet wissen, können ihm helfen, also nutzte er **soziale Netzwerke**, um neue Ideen zu gewinnen. Solche Informationen sind hilfreich, um seine Mind Map zu erweitern und weitere Schritte zu planen und zu unternehmen.

**2.1.8**

**Zum Fazit..... ** Bild der Treppe mit Marco auf dem 5. Schritt, Bild ist auf der linken Seite, und der Text auf der rechten Seite_].

<div style="background-color: hellgelb;">

Identifizierung des Themas und Formulierung von Forschungsfragen:

1. Denkt darüber nach, was ihr **bereits** über das Thema wisst und was ihr gerne wissen möchtet
2. Definiert den Bereich und das **spezifischere** Themenfeld
3. Verschafft euch einen Überblick über das Thema; such nach grundlegenden Informationen und teilt eure Ideen mit anderen
4. Macht euch **Notizen** und organisiert diese (es ist gut, eine Mind Map zu zeichnen und sie ständig zu aktualisieren)
5. Stellt präzise und konkrete **Forschungsfragen**, die euch in eurer Forschung leiten werden; formuliert sie gegebenenfalls während des Brainstormings und der späteren Suche neu. Stellt offene Fragen wie: Wer, wo, wie,  wie viel, warum?
</div>



**2.1.9 Übung**

## Bearbeite diese Übung zu deinem eigenen Thema:

Bearbeitet diese Übung zu eurem eigenen Thema:

Verwende die online _Notizen mit Selbstbeurteilung_ unterhalb dieser Anleitung. Du kannst die Antworten speichern und diese Einheit 2.1.9. mit einem Lesezeichen versehen. Du kannst auch mit Papier und Stift arbeiten, wenn du magst. 

ONLINE-NOTIZEN MIT SELBSTBEURTEILUNG

Diese Aufgabe erfolgt in mehreren Schritten. Im ersten Schritt gibst du eine Antwort auf die Eingabeaufforderung. Die weiteren Schritte erscheinen werden unterhalb des Eingabefeldes. 

Deine Antwort

Gib deine Antwort nach der Aufforderung ein. Du kannst deinen Fortschritt speichern und jederzeit zurückkehren, um deine Antworten zu vervollständigen. Nachdem du deine Antwort übermittelt hast, kannst du sie nicht mehr bearbeiten.¸

Die Eingabeaufforderung für diesen Abschnitt

1. Denke über ein Thema nach, das dich interessiert oder beginne mit einem Thema aus einer aktuellen Aufgabe und schreibe das von dir gewählte Thema auf:

Deine Antwort (erforderlich)

2. Was weißt du bereits über das Thema? Schreibe die grundlegenden Informationen auf, die du bereits darüber hast:	 

3. In welches allgemeinere und spezifischere Feld passt dein Thema? Schreibe es auf: 

4. Suche nach grundlegenden Informationen zu diesem Thema und schreib drei Schlüsselideen auf, die es wert sind, untersucht zu werden!	 

5. Erstelle eine Mind Map für dein Thema und poste einen Link zur Mind Map. Mithilfe einer Mind Map kannst du die Informationen aufzeigen, die du bisher gesammelt hast. Es gibt viele Möglichkeiten Felder, Begriffe und Konzepte mit Hilfe einer Mind Map darzustellen. 

Durchsuche das Web nach einigen Beispielen für mentale Modelle oder Mind Maps und deren Design (z.B. https://www.google.hr/search?q=online+tool+for+creating+mind+maps). Es gibt viele Werkzeuge, die helfen können, eigene mentale Modelle oder Mind Maps zu erstellen.

6. Schreibe drei bis fünf präzise und konkrete Forschungsfragen auf, die dich in deiner weiteren Forschung und beim Schreiben deiner Arbeit leiten werden:	 

7. Wenn du dieses MOOC im Rahmen von Schul- und Universitätsklassenaktivitäten verwendet, teile deine Ideen, Fragen und mögliche Bedenken mit Gleichaltrigen. Notiere dabei nützliche Links, die du gefunden hast (z.B. Forschungsfragen zu einem bestimmten Thema, Links zu Enzyklopädien zum Sammeln grundlegender Informationen, Beispiele von Mind Maps, etc.)

**2.1.10 - Selbstbewertungsquiz**

>>1. Wenn du eine Recherche zu einem Thema durchführst, ist es notwendig, Folgendes zu tun<<
 () nach zufälligen Informationen suchen
(x) das Aufgabenthema untersuchen

>>2. Um das Thema bestimmen zu können, ist es notwendig:
(X) zu wissen, was die Unter- und Oberbegriffe sind
( ) Grundlegende Literatur zur Methodik zu kennen
( ) Keins der genannten

>>3. Warum ist es wichtig, die Konzepte in deinem Interessengebiet zu kennen und zu verstehen?<< 
(x) Ohne sie kann ich nicht zu dem Thema kommen, das mich wirklich interessiert
( ) Konzepte sind für die Bestimmung des Themas nicht so entscheidend
( ) Je mehr Konzepte einbezogen werden, desto mehr Themen können abgedeckt werden

>>4. Welche sind die ersten beiden Schritte im Prozess, um zum Thema zu gelangen? Alle richtigen Antworten müssen ausgewählt werden.<<

( ) Ich suche im Internet
( ) Ich bitte Freunde und Kollegen um Ratschläge
(X) Ich denke über mein Thema nach
( ) Ich mache eine Mind Map zu diesem Thema
(X) Ich schreibe auf, was ich über das Thema weiß und woran ich wirklich interessiert bin



**2.1.11 Für deine zukünftige Arbeit....**

Es gibt viele Online-Ressourcen, in denen ihr weitere **Anleitungen zur Formulierung von Forschungsfragen** finden könnt.

Versucht es zum Beispiel mit diesen vorgeschlagenen Lektüren:

Research Rundowns. Writing Research Questions. [https://researchrundowns.com/intro/writing-research-questions/](https://researchrundowns.com/intro/writing-research-questions/)

Center for Innovation in Research and Teaching. Writing a Good Research Question. [https://cirt.gcu.edu/research/developmentresources/tutorials/question](https://cirt.gcu.edu/research/developmentresources/tutorials/question)