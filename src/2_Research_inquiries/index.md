---
title: Research is a journey of inquiries
permalink: /2_Inquiries/
eleventyNavigation:
    order: 2
---

# MOOC Module: Research is a journey of inquiries

Objective: In this module you will learn how to formulate the right questions to address a specific information need. It is an investigative journey of gradual reformulation of inquires, defining the topics of your interest, recognising your knowledge gaps and how to address these gaps.

