---
title: Necesidades de información
permalink: /1_Orienting/1.5_Information_needs_es/
eleventyNavigation:
    order: 5
    parent: 1_Orienting
---


## 1.5 Necesidades de información, fuentes y búsquedas

**Introducción**

En esta lección obtendrás unas nociones iniciales sobre cómo elegir las fuentes y formatos de información más apropiados para satisfacer una necesidad particular de información. Estos puntos solo se tratarán aquí de forma introductoria, y serán retomados en otras lecciones de este curso.

La lección consta de 6 unidades. Algunas de las unidades tienen un breve ejercicio en el que se te pide que pienses sobre el uso de los formatos de información. No hay respuestas correctas a estas preguntas; su objetivo es ayudarte a reflexionar sobre cómo gestionar tus necesidades de información, lo que te será útil más adelante en este curso.

**Objetivos de aprendizaje**

Al final de esta lección, los alumnos deben tener una comprensión básica de:

* la naturaleza de las necesidades de información y su relación con la elección de las fuentes y formatos de información
* el hecho de que no existe una "mejor" fuente de información para todas las circunstancias
* la influencia que tienen la necesidad de rapidez, exhaustividad, colaboración y otros factores en la forma en que se encuentra y utiliza la información
  

  

### 1.5.1 Necesidades de información

La información nunca se necesita sin un contexto. Detrás siempre hay alguna "necesidad de información" concreta. Esto determinará qué tipo de información necesitas y cómo la obtendrás.

Serían ejemplos de necesidades de información:

*  Necesito material de referencia para escribir mi trabajo.
*  Quiero saber algo sobre estos temas, de los que no sé nada, para poder elegir los cursos que haré el año que viene.
*  Quiero encontrar la fuente de esta cita.
*  Necesito algunas fotos que ilustren este tema para mi trabajo.
*  Necesito saber cómo llegar a este lugar donde tengo una reunión en 30 minutos.
*  Quiero algo interesante para leer en un largo viaje en tren.
*  Quiero algunas ideas de libros nuevos para un regalo de cumpleaños.

Podemos ver que las necesidades de información pueden ser de naturaleza muy diferente: específicas o generales; selectivas o exhaustivas; urgentes o no; y así sucesivamente. Vale la pena pensar en la naturaleza de tus necesidades de información, ya que esto limitará significativamente la elección de las fuentes y formatos que necesitas y la forma en que puedes encontrarlos.

**Ejercicio** 
Mira una presentación de Slideshare sobre "Búsqueda y fuentes de información para la investigación en literatura inglesa" en [https://www.slideshare.net/nuslibrarieshumrt/lit-hons08](https://www.slideshare.net/nuslibrarieshumrt/lit-hons08). No te preocupes por los detalles sobre búsquedas en bases de datos - serán tratados en otras lecciones de este curso- pero es interesante que te fijes en la forma sistemática de elegir las fuentes.
  

### 1.5.2 Nada es nunca "lo mejor"

Es importante darse cuenta de que no existe tal cosa como la "mejor" fuente de información; solo lo que es más apropiado para una necesidad y contexto particular.

Por ejemplo, si necesitas información urgentemente y estás lejos de casa y de la universidad, sucede que estás limitado a aquello que puedas encontrar rápidamente usando los dispositivos móviles que tengas contigo. Estas fuentes serán las mejores para tus necesidades en ese contexto, incluso si pudieras encontrar "mejor" información si estuvieras en una biblioteca o una sala de informática.

Esto significa que [como dijimos en 1.3.1] nunca debemos confiarlo todo a fuentes conocidas; incluso si sabemos que son muy buenas para algunos propósitos, no se adaptarán a todas las necesidades de información ni a todas las circunstancias.
  

### 1.5.3 ¿Necesito ser exhaustivo?

Un aspecto importante de las necesidades de información es saber si necesitamos ser exhaustivos para obtener tanta información sobre un tema como podamos. A veces podemos necesitar hacer esto; si estamos haciendo algún tipo de proyecto de investigación, o escribiendo una revisión sobre un tema. Pero normalmente no lo hacemos. En la mayoría de los casos, es más probable que queramos ser selectivos y obtener solo unos pocos elementos de información.

Si estamos siendo exhaustivos, vamos a querer hacer un proceso minucioso de recolección de información. Ello implicará empezar por las fuentes terciarias y utilizarlas para identificar las fuentes secundarias y, si es necesario, las primarias [como se indica en 1.2.6].

Si estamos siendo selectivos, entonces no deberíamos elegir los primeros elementos que encontremos sobre el tema, quizás obtenidos de un buscador. Deberíamos pensar en si queremos materiales recientes o materiales de una naturaleza o formato particular, y buscar en consecuencia.

**Ejercicio** 
Piensa en una ocasión en la que hayas tenido que realizar una búsqueda exhaustiva de información. ¿Estás satisfecho con el grado de exhaustividad que conseguiste? Si nunca antes has tenido que hacer una búsqueda de esas características, piensa en una ocasión en la que podrías necesitarlo, y cómo la abordarías. 
  

### 1.5.4 ¿De cuánto tiempo dispongo?

Si necesitas la información urgentemente, no tendrás tiempo para realizar una búsqueda detallada o sistemática. Sin embargo, no deberías buscar tu tema en un buscador y tomar la primera información que encuentres. Aquí es donde el conocimiento de unas pocas fuentes fácilmente accesibles y de buena calidad resulta muy útil. Las fuentes secundarias [presentadas en 1.2.4] probablemente serán más útiles aquí; por ejemplo, un capítulo de un libro de texto o un artículo de una enciclopedia suele ser un buen lugar para encontrar rápidamente lo que necesitas.

Si tienes más tiempo, por ejemplo, si estás trabajando en un proyecto académico durante todo un año, deberías realizar una búsqueda exhaustiva desde el principio, como preparación inicial. Es una buena idea mantenerla actualizada a lo largo del periodo en que precises la información sobre el tema, ya sea repitiendo la búsqueda a intervalos, o estableciendo alertas para el tema en fuentes relevantes [presentadas en 1.2.5].

**Ejercicio** 
Mira un breve vídeo (de 3 minutos) sobre la creación de alertas de contenidos académicos a través de Google Scholar:[https://www.youtube.com/watch?v=3kQXABU73hI](https://www.youtube.com/watch?v=3kQXABU73hI)

  

### 1.5.5 ¿Estoy trabajando solo o en equipo?

Si estás trabajando solo, podrás abordar tus necesidades de información como mejor te parezca. Si estás colaborando con otras personas, tal vez en un proyecto de grupo, tendrás que ir más allá. Obviamente, tendrás que ponerte de acuerdo con tus colegas sobre cómo dividir el trabajo de búsqueda y registro de la información que necesitáis. A continuación, tendréis que utilizar algún tipo de proceso o sistema para compartir la información que encontréis y utilizarlo para crear vuestros propios productos de información. Estos puntos serán tratados en detalle en otras lecciones de este curso.

**Ejercicio** 
Piensa en cómo has trabajado con otras personas en el pasado colaborando en el uso de la información. ¿Te gusta o prefieres trabajar solo? Esto es parte de tu "estilo de información" personal, y reflexionar sobre ello a lo largo de este curso te ayudará a decidir cuál es la mejor manera de colaborar cuando sea necesario.
  

### 1.5.6 ¿Qué es lo que realmente necesito?

En realidad, nunca queremos simplemente "información" sobre algún tema. Una necesidad de información  siempre se inserta en un contexto, y eso significa que queremos información de un tipo y nivel particular, incluso si el tema está ya especificado. Por ejemplo, si necesitamos información para preparar un trabajo para un curso universitario introductorio, la necesitaremos en el nivel apropiado: un libro de texto básico, no la bibliografía de investigación más reciente. Por el contrario, un profesor que prepara un plan para un proyecto de investigación lo querría al revés. Obtenemos el nivel del material que queremos seleccionando las fuentes apropiadas.

También hay algunas limitaciones en el tipo de información dependiendo de las circunstancias, por ejemplo:

*  ¿quiero texto, imágenes, audio, vídeo, etc?
*  idioma en el que se presenta la información: ¿puedo leerlo?
*  antigüedad del material - cómo de actualizado tiene que ser
*  disponibilidad - ¿puedo obtener el texto completo, o solo puedo ver un resumen o un extracto?

Aquí debemos considerar el formato de la información que necesitamos y elegir las fuentes y herramientas de búsqueda apropiadas.

Por último, debemos considerar los aspectos éticos sobre cómo vamos a reutilizar los materiales que encontremos. Si, por ejemplo, queremos reutilizar una imagen cuando elaboramos nuestro propio producto de información, debemos asegurarnos de que se nos permite hacerlo. Podemos, en ese caso, buscar en colecciones de imágenes específicamente elegidas para ser reutilizables, o podemos utilizar las funcionalidades de recuperación de imágenes en los buscadores para restringir los resultados a aquellas imágenes que podamos reutilizar. Estos temas serán considerados en detalle en otras lecciones de este curso.
