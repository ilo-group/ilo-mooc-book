---
title: Informationsbedarf
permalink: /1_Orienting/1.5_Information_needs_de/
eleventyNavigation:
    order: 5
    parent: 1_Orienting
---

## Lektion 1.5 Informationsbedarf, Quellen und Suche

In dieser Lektion erhaltet ihr ein erstes Verständnis dafür, wie ihr die am geeignetsten Informationsquellen und -formate auswählen könnt, um einem bestimmten Informationsbedarf gerecht zu werden. Diese Punkte werden hier vorgestellt und in anderen Lektionen dieses Kurses wieder aufgegriffen.

Diese Lektion hat 6 Einheiten. Alle Einheiten haben kurze Übungen, die euch auffordern, über die Verwendung von Informationsformaten nachzudenken. Es gibt keine richtigen Antworten auf diese Fragen; sie sollen euch helfen, darüber nachzudenken, wie ihr mit eurem Informationsbedarf umgeht, was später in diesem Kurs hilfreich sein wird.

**Lernziele**

Am Ende dieser Lektion sollten die Lernenden ein grundlegendes Verständnis von Folgendem haben:

* der Art des Informationsbedarfs und den Zusammenhang mit der Wahl der Informationsquellen und -formate
* der Tatsache, dass es keine "beste" Informationsquelle für alle Umstände gibt
* dem Einfluss der Notwendigkeit von Geschwindigkeit, Vollständigkeit, Zusammenarbeit und anderen Faktoren auf die Art und Weise, wie Informationen gefunden und verwendet werden


  

  

### 1.5.1 Informationsbedarf

Informationen werden nie ohne Kontext benötigt. Es gibt immer einen konkreten "Informationsbedarf". Der Bedarf bestimmt, welche Art von Informationen du benötigst und wie du diese erhältst.

Beispiele für den Informationsbedarf wären:

* Ich brauche Hintergrundmaterial, um mein Essay zu schreiben
    
* Ich möchte etwas über Themen herausfinden, von denen ich nichts weiß, um meine Kurse für das nächste Jahr zu wählen
    
* Ich möchte die Quelle dieses Zitats finden
    
* Ich brauche für meine Arbeit einige Bilder, die dieses Thema illustrieren
    
* Ich muss wissen, wie ich diesen Ort erreichen kann, an welchem ich in 30 Minuten ein Meeting habe
    
* Ich möchte etwas Interessantes auf einer langen Zugfahrt lesen
    
* Ich brauche ein paar Ideen für neue Bücher als Geburtstagsgeschenk
        

Wir sehen, dass der Informationsbedarf sehr unterschiedlich sein kann: Spezifisch oder allgemein, selektiv oder umfassend, dringend oder nicht. Es lohnt sich, über die Art deines Informationsbedarfs nachzudenken, da dies die Auswahl der benötigten Quellen und Formate sowie die Art und Weise, wie du sie finden kannst, erheblich einschränken wird.

**Übung** 

Schaut euch eine Slideshare-Präsentation über "Recherche- und Informationsquellen für die englische Literaturforschung" unter[https://www.slideshare.net/nuslibrarieshumrt/lit-hons08](https://www.slideshare.net/nuslibrarieshumrt/lit-hons08) an. Ignoriert die Details der Datenbankrecherche - diese werden in anderen Lektionen dieses Kurses behandelt - aber die Art und Weise, wie die Quellen systematisch ausgewählt werden, ist hier von Interesse.
  

### 1.5.2 Nichts ist jemals das Beste

Es ist wichtig zu erkennen, dass es keine "beste" Informationsquelle gibt, sondern nur das, was für einen bestimmten Bedarf und Kontext am besten geeignet ist.

Wenn ihr zum Beispiel dringend Informationen benötigt und nicht zu Hause und an der Universität  seid, dann beschränkt sich eure Suche  auf verfügbare mobile Geräte. Diese Quellen sind die besten für eure Bedürfnisse in diesem Zusammenhang, auch wenn ihr "bessere" Informationen finden könntet, sofern ihr euch in einer Bibliothek oder einem Computerraum befindet.

Das bedeutet (wie in 1.3.1 bereits erwähnt), dass wir uns niemals auf vertraute Quellen verlassen sollten. Selbst wenn wir wissen, dass diese Quellen für bestimmte Zwecke sehr gut sind, werden sie nicht allen Informationsbedürfnissen und Umständen gerecht.

### 1.5.3 Muss ich ausführlich sein?

Ein wichtiger Aspekt des Informationsbedarfs ist, ob wir ausführlich sein müssen, um so viele Informationen wie möglich zu einem Thema zu erhalten. Manchmal müssen wir dies tun, wenn wir eine Art Forschungsprojekt durchführen oder eine Rezension zu einem Thema schreiben. Aber normalerweise tun wir das nicht. In den meisten Fällen ist es wahrscheinlicher, dass wir selektiv sind und nur wenige Informationen erhalten wollen.

Wenn wir ausführlich sind, werden wir einen gründlichen Prozess der Informationsbeschaffung durchführen wollen. Das bedeutet, mit tertiären Quellen zu beginnen und sie zu nutzen, um sekundäre und gegebenenfalls primäre Quellen zu identifizieren [wie in 1.2.6 dargelegt].

Wenn wir selektiv sind, dann sollten wir nicht nur die ersten Artikel aus z.B. einer Suchmaschine auswählen, die wir zu diesem Thema finden. Wir sollten darüber nachdenken, ob wir neue Materialien oder Materialien einer bestimmten Art oder eines bestimmten Formats wollen und sie entsprechend suchen.

**Übung** 

Denkt an eine Gelegenheit, bei der ihr eine umfassende Suche nach Informationen durchführen musstet. Seid ihr sicher, dass sie wirklich ausführlich war? Wenn ihr noch nie eine umfassende Suche durchführen musstet, überlegt eine Gelegenheit, bei der es notwendig wäre und wie ihr die Informationssuche angehen würdet.


### 1.5.4 Wie viel Zeit habe ich noch?

Wenn ihr die Informationen dringend benötigt, habt ihr keine Zeit für eine detaillierte oder systematische Suche. Allerdings solltet ihr eure Suche nicht nur mit einer Suchmaschine  durchführen. Hier ist das Wissen über wenige leicht zugängliche und qualitativ hochwertige Quellen sehr nützlich. Sekundärquellen (präsentiert in 1.2.4) werden hier am nützlichsten sein; zum Beispiel ist ein Lehrbuchkapitel oder ein Enzyklopädie Artikel in der Regel ein guter Ort, um schnell das zu finden, was ihr benötigt.

Wenn ihr länger Zeit habt, z.B. ein ganzes Jahr lang an einem Universitätsprojekt arbeitet, solltet ihr zur Vorbereitung eine gründliche Suche durchführen. Es ist eine gute Idee, dies über einen bestimmten Zeitraum durchzuführen: Durch wiederholte Suche in Intervallen oder durch die Einrichtung von Benachrichtigungen über für das Thema relevante Quellen (präsentiert in 1.2.5).

**Übung** 

Schaut euch ein kurzes (3-minütiges) Video über die Einrichtung von Benachrichtigungen für akademisches Material über Google Scholar an unter [https://www.youtube.com/watch?v=3kQXABU73hI](https://www.youtube.com/watch?v=3kQXABU73hI)


### 1.5.5 Arbeite ich allein oder zusammen?

Wenn ihr alleine arbeitet, könnt ihr mit euren Informationsbedürfnissen umgehen, wie ihr es für richtig haltet. Wenn ihr mit anderen zusammenarbeitet, vielleicht in einem Gruppenprojekt, müsst ihr weiterdenken. Natürlich müsst ihr euch mit euren Kollegen darüber einigen, wie die Arbeit bei der Suche und Aufzeichnung der benötigten Informationen aufgeteilt wird. Dann solltet ihr eine Art Prozess oder System verwenden, um die gefundenen Informationen zu teilen und damit deine eigenen Informationsprodukte zu erstellen. Diese Punkte werden in anderen Lektionen dieses Kurses ausführlich behandelt.

**Übung**

Überlegt, wie ihr in der Vergangenheit mit anderen bei der Zusammenarbeit Informationen genutzt und zusammengearbeitet habt. Macht es euch Spaß oder arbeitet ihr lieber alleine? Dies ist Teil eures persönlichen „Informationsstils" und die Reflexion darüber während dieses Kurses wird euch dabei helfen zu entscheiden, wie ihr am besten zusammenarbeiten könnt.

### 1.5.6 Was brauche ich wirklich?

Wir wollen nie Informationen zu einem bestimmten Thema. Ein Informationsbedarf ist immer in einen Kontext eingebettet und bedeutet, dass wir Informationen einer bestimmten Art und Ebene wollen, auch wenn das Thema spezifiziert ist. Wenn wir zum Beispiel Informationen als Hintergrund für eine Aufgabe für ein einführendes Universitätsstudium benötigen, werden wir sie auf der entsprechenden Ebene haben wollen: Ein grundlegendes Lehrbuch und nicht die neueste Forschungsliteratur. Umgekehrt würde ein Professor, der einen Plan für ein Forschungsprojekt erstellt, ihn umgekehrt haben wollen. Wir erhalten das gewünschte Materialniveau, indem wir geeignete Quellen auswählen.

Es gibt auch einige Einschränkungen bei der Art der Informationen, die z.B. von den Umständen abhängen:

* Ich möchte Text, Bilder, Audio, Video, etc.
    
* Sprache, in der sie präsentiert wird - kann ich sie lesen?
    
* Alter des Materials - wie aktuell muss es sein?
    
* Verfügbarkeit - kann ich den Volltext erhalten, oder kann ich nur eine Zusammenfassung oder einen Auszug sehen?
    

Hier müssen wir das Format der benötigten Informationen berücksichtigen und geeignete Quellen und Suchwerkzeuge auswählen.

Schließlich müssen ethische Aspekte berücksichtigt werden, wie wir Materialien, die wir finden, wiederverwenden dürfen. Wenn wir beispielsweise ein Bild wiederverwenden wollen,  oder unser eigenes Informationsprodukt erstellen, muss sichergestellt werden, dass wir dies tun dürfen. Unter diesen Umständen können wir in Bildersammlungen suchen, die speziell für die Wiederverwendbarkeit ausgewählt wurden.  Es kann ebenso die Möglichkeit der Bildrecherche in Suchmaschinen genutzt werden, um die Suche auf diejenigen Bilder zu beschränken, die wiederverwendet werden dürfen. Diese Fragen werden in anderen Lektionen dieses Kurses ausführlich behandelt.
