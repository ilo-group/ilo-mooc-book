_**Notes:**_

_This lesson has six units, numbered 1.2.N_

_Each has 100-150 words text, plus one or two images for some; all have short exercises, which are intended for self-reflection by individual students, but could be used for discussions with other students or with an instructor._

_There is one short self-assessment quiz._

_The images are all either CC-BY or produced by the authors of this lesson._

_One YouTube video is included; this seems quite a stable resource, but would need to be checked each time the course was offered, or cached in a permanent location._

_Each unit should take 5 minutes at minimum. If the student reflected, and looked at the optional resources, it could take 10 minutes. So the whole lesson, including quiz, would take 40 minutes (maybe minimum 30), and up to 90 minutes if the student took it really seriously._


# Lesson 1.2

## Understanding information resources

In this lesson you will gain an awareness that there are several kinds of information sources, each of them useful for particular purposes.

The lesson has 6 units. All of the units have a short exercise which asks you to think about your use of information sources. There are no right answers to these questions; they are intended to help you reflect on the kinds of information sources of most value to you, which will be helpful later in this course.

  

## Learning objectives

At the end of this lesson, learners should have a basic understanding of:

*   the value of different types of sources of information for different purposes and tasks
*   the different types of information sources available
*   the value and difference between primary_,_ secondary and tertiary information sources

**1.2.1** **Different kinds of information sources**

There are many different kinds of information sources. They differ according to many factors including:

*   who produces them
*   who they are intended for
*   what they are intended to be used for
*   their subject or topic
*   their level of detail, and of knowledge assumed by the user
*   what format they are in
*   how up-to-date they are

Before digital information, and the internet in particular, became so important, it was easier to tell the difference between types of information sources. It is easy, for instance, to tell the difference between a printed textbook, an article in a printed magazine, and a handwritten note. But now that so much information comes through a web browser, and often looks very similar, it can be difficult to tell what we are dealing with, and we should use it.

Show Figure 1 with the text: it can be easy to tell what kinds of sources printed sources are.

  

**Exercise** 
Think about your own use of information which you get through the Internet - do you sometimes find it difficult to decide what kind of source you are looking at? How do you decide whether it is right for your purposes?

  

**1.2.2 Understanding different types of information source**

There are a number of ways we can categorise information sources, so as to decide whether they are useful to us, and how we should use them. Some ways of doing this include dividing sources up:

*   by their subject, for example physics, or history
*   by their intended users, for example first year university students, or the general public
*   by their format, for example web pages, or podcasts
*   by their type of material, for example textbook chapters, or magazine articles
*   by their geographic and/or time period coverage, for example Germany in the past 10 years
    

All of these may be useful, on different occasions.


There is another way of dividing up information sources, which is particularly useful with information for academic studies, can be used with any subject, and is helpful in planning a systematic approach to finding information. This divides up information sources according to their level, as primary, secondary or tertiary, depending on how much the information has been processed and packaged. Primary sources (such as articles in scientific journals and posts on social media) give the original detailed information: secondary sources (such as textbooks and encyclopaedias) give summaries and explanations; and tertiary sources (such as search engines and readings lists) give access to primary and secondary sources. We will look in more detail at these categories of sources in the next three units.

  

**Exercise** 
Think about which way/s, of any, you think of distinguishing information sources? Which are most helpful?

  

**Optional exercise** 
Watch the video (18 minutes) at https://www.youtube.com/watch?v=bhNyTIcszhA.

It is more detailed than necessary to appreciate the content of this lesson, but may be helpful for anyone wanting more on this topic.

  

**1.2.3 Primary sources**

Primary sources give the original and most detailed information, whatever the topic. Examples of primary information sources are:

*   articles in scientific journals    
*   scholarly books
  *   posts on social media
  *   laws, regulations and standards
  *   maps
*   data collections

Sources of this kind will give the fullest information, but they sometimes may be too detailed or too specialist.

Show Figure 2 with the text - Examples of primary sources



**Exercise** \- think of some primary sources that could be valuable for your needs.

  

  

**1.2.4 Secondary sources**

These kinds of sources organise and summarise information from primary sources, and often also give access to to it by references and links. Examples of secondary information sources are:

*   textbooks
*   encyclopaedia articles
*   review articles
*   collections of abstracts
    

They can often be used instead of the primary sources for study, other than at advanced levels. They are also more useful than primary sources in subjects with which you are not familiar.

Show Figure 3 with the text - Examples of secondary sources

  

**Exercise** \- think of some secondary sources that could be valuable for your needs.

  

  

**1.2.5 Tertiary sources**

These are source whose main purpose is to point to, and give access to, primary and secondary sources; they do not usually provide information themselves. They are usually some form of list. Examples are:

*   lists of web resources on a topic
*   bibliographies and reading lists
*   library catalogues
*   directories
*   guides to the literature of a subject

They are best used as a starting point, to identify useful primary and secondary sources.

Also counted as tertiary sources are 'alerting services'. These are useful if you have an interest in information on a particular topic over a long period. You can set up alerts on many different kinds of digital information services, which let you know when they have new content matching your interests.

There are also quaternary sources - 'lists of lists' on the Internet - and even 'lists of lists of lists', but we will not be concerned with these, as they are not useful for most information seeking.

Show Figure 4 with the text - Examples of tertiary sources
  

**Exercise** \- think of some tertiary sources that might be relevant to your information needs.

  

  

**1.2.6 Using different types of sources together**

Sometimes you will know exactly which primary or secondary source will be most useful to you, on a particular occasion. But if not, and you want to be systematic about finding information on a particular topic, then a good general process would be:

*   identify one or more tertiary sources for your topic
*   look at these, and from them identify one or more relevant secondary sources
*   if you need more detailed information than given by the secondary sources, then find primary sources from them, for example by following references at the end of textbook chapters.
    

Often, we will start a search for information by using a general search engine, such as Google or Bing. These will usually give a set of answers including a mix all three kinds of sources; and it is up to us to sort out which is which.

  

**Exercise** 

For the tasks listed, what kind of information source would be likely to be most useful:

1. Getting some initial information on a topic you know only a little about
( ) primary
(x) secondary
( ) tertiary

2. Finding the latest news about some issue
(x) primary
( ) secondary
( ) tertiary

3. Finding some resources to get started in a subject completely new to you
( ) primary
( ) secondary
(x) tertiary


4. Getting an initial overview of a topic
( ) primary
(x) secondary
( ) tertiary

  

A1 - secondary
A2 - primary
A3 - tertiary
A4 - secondary

  

**Exercise** \- for the search that you did in Exercise 1.1.4, try to sort the first 20 results into primary, secondary and tertiary sources. is it easy to do? If not, why not?

  

  

**Self-assessment quiz (for Lesson 1.2)**

  

**Which of the following are primary information resources?**

1. a newspaper article
2. a Wikipedia article
3. a textbook chapter
4. a blog post

A: 1 and 4, the other two are secondary

  

**Which of the following are secondary information resources?**

1. an encyclopaedia article
2. a Facebook post
3. a collection of abstracts of medical articles
4. the catalogue of a national library

A: 1 and 3. 2 is primary, 4 is tertiary

  

**Which of the following are tertiary information resources?**

1. a list of web resources in a subject area
2. the catalogue of a university library
3. a guide to engineering literature
4. a university prospectus

A: 1, 2 and 3 tertiary. 4 is primary.


If you got more than 3 wrong, you may want to go back and review this lesson.