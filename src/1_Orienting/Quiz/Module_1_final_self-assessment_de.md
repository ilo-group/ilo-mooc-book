---
title: Quiz
permalink: /1_Orienting/Quiz_de/
eleventyNavigation:
    order: 6
    parent: 1_Orienting
---


**Was habe ich aus diesem Modul gelernt?**  

Diese reflektierende Übung sollte dir helfen, dich auf das zu konzentrieren, was du aus diesem ersten Modul des ILO-Kurses gelernt hast, bevor du zu den anderen Modulen übergehst.

Denke über das Gelernte in den fünf Lektionen dieses Moduls nach und beantworte die folgenden Fragen.

Wenn du mit anderen Studenten zusammenarbeitest, vergleiche deine Antworten mit ihren und diskutiere, warum sie unterschiedlich sein könnten.

1. Was ist eine Sache, die ich über die Welt der Informationen weiß, die ich zuvor nicht wusste?
2. Welche Art von Informationsressource kann ich in Zukunft besser nutzen?
3. Welches konkrete Beispiel einer Informationsquelle kann ich in Zukunft besser nutzen?
4. Welches ist ein Informationsformat, das ich bisher nicht verwendet habe und in der Zukunft verwenden könnte?
5. Wie kann ich meine Nutzung von Social Media ändern, um "gute" Informationen zu finden?