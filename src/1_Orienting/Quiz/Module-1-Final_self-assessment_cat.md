---
title: Self-assessment?
permalink: /1_Orienting/Quiz_cat_/
eleventyNavigation:
    order: 6
    parent: 1_Orienting
---

**Què he après en aquest mòdul?**  

Aquest exercici de reflexió hauria d'ajudar-te a centrar-te en allò que has après d'aquest primer mòdul del curs d'ILO, abans de passar als següents mòduls.

Reflexiona sobre el que has après en les cinc lliçons d'aquest mòdul i respon a les següents preguntes.

Si treballes amb altres estudiants, compara les teves respostes amb les dels teus companys i analitzeu per què podrien ser diferents.

1. Què és el que ara sé sobre el món de la informació que abans no coneixia?
2. Quin tipus de recursos d'informació puc utilitzar de forma millor en el futur?
3. Quin exemple específic de recurs d'informació puc utilitzar millor en el futur?
4. Quin format d'informació no he fet servir abans i podria utilitzar en el futur?
5. Com puc canviar el meu ús de les xarxes socials per trobar bona informació?