---
title: Self-assessment?
permalink: /1_Orienting/Quiz_sl_/
eleventyNavigation:
    order: 6
    parent: 1_Orienting
---

**Kaj sem se naučil v tem modulu?**

  

Preden se boste posvetili naslednjim lekcijam, boste s to nalogo ponovili to, kar ste se naučili v prvem modulu tečaja ILO.

  

Razmislite o informacijah, ki ste jih pridobili v tej lekciji in odgovorite na naslednja vprašanja.

Če ste opravili naloge v skupini, primerjajte odgovore in se pogovorite o njih.

  

1. Kaj zdaj vem o svetu informacij, česar prej nisem vedel/vedela? Napišite vsaj eno stvar.

  

2. Katero vrsto informacijskih virov lahko bolje uporabljam v prihodnosti?

  

3. Kateri konkretni informacijski vir lahko bolje uporabljam v prihodnosti? Dodajte primer.

  

4. Ali obstaja kak format informacij, ki ga doslej še nisem uporabil/uporabila, ga pa bom mogoče v prihodnosti?

  

5. Kako (če) bom spremenil/spremenila svojo uporabo spletnih družbenih omrežij, da najdem koristne informacije?