---
title: Self-assessment?
permalink: /1_Orienting/Quiz_es_/
eleventyNavigation:
    order: 6
    parent: 1_Orienting
---
**¿Qué he aprendido en este módulo?**  

Este ejercicio de reflexión debería ayudarte a centrarte en lo que ha aprendido de este primer módulo del curso de ILO, antes de pasar a los siguientes módulos.

Reflexiona sobre lo que has aprendido en las cinco lecciones de este módulo y responde a las siguientes preguntas.

Si estás trabajando con otros estudiantes, compara tus respuestas con las de tus compañeros y analizad por qué podrían ser diferentes.

1. ¿Qué es lo que ahora sé sobre el mundo de la información que antes no conocía?
2. ¿Qué tipo de recursos de información puedo utilizar de forma mejor en el futuro?
3. ¿Qué ejemplo específico de recurso de información puedo utilizar mejor en el futuro?
4. ¿Qué formato de información no he usado antes y podría usar en el futuro?
5. ¿Cómo puedo cambiar mi uso de las redes sociales para encontrar buena información?