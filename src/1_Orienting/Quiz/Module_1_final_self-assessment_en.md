---
title: Self-assessment
permalink: /1_Orienting/Quiz_en_/
eleventyNavigation:
    order: 6
    parent: 1_Orienting
---

**What have I learned from this module?**  

This reflective exercise should help you focus on what you have learned from this first module of the ILO course, before you move on to the other modules.

Reflect on your learning over the five lessons of this module, and answer the following questions.

If you are working with other students, compare your answers with theirs, and discuss why they might be different.

1. What is one thing that I know about the world of information that I did not know before?
2. Which kind of information resource can I make better use of in future?
3. Which specific example of an information resource can I make better use of in future?
4. What is one information format I have not used before that I might use in future?
5. How might I change my use of social media to find good information?