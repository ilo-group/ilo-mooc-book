---
title: Using different information formats
permalink: /1_Orienting/1.3_Different_formats_en/
eleventyNavigation:
    order: 3
    parent: 1_Orienting
---



## Lesson 1.3: Using different information formats

In this lesson you will gain an awareness that there are many formats in which information may be presented, each of them useful for particular purposes.

The lesson has 4 units. All of the units have a short exercise which asks you to think about your use of information formats. There are no right answers to these questions; they are intended to help you reflect on the kinds of information formats of most value to you, which will be helpful later in this course.

  

**Learning objectives**

At the end of this lesson, learners should have a basic understanding of:

*   the different types of information formats (digital and analogue) available
    
*   the value of different types of formats of information for different purposes
    
*   the ways in which different information formats may be found, used, and created
    

  

### 1.3.1

The digital information environment gives us many more formats in which information can be used and created. These include:

*   print (which may be a printed document, or a print-out from a digital original)
    
*   digital text (which may be viewed on a large screen or on a mobile device)
    
*   images (including maps)
    
*   audio (including podcasts and audiobooks)
    
*   videos and video podcasts (vodcasts)
    
*   presentations in Powerpoint or similar software
    
*   animations
    
*   'mash-ups' (integrating information from different sources and formats)
    

  

Most people have a preference for which they will use, given a choice. This preference is often due to familiarity, or to convenience: materials on particular subjects are often more readily available in one format. It may also be influenced by your personal preferred learning style. It is a good idea to consciously try out different formats, particularly if you have not used them before, to see what advantages they may have.

**Exercise** \- think about which formats you prefer to use for gaining information, and why.

  

  

### 1.3.2 Finding and accessing different formats

Printed formats are generally intuitively easy to use, although there are ways in which this can be made more efficient: speed reading, for example, or annotating and notetaking for understanding lengthy texts. Advice on this is usually available through study skills materials offered by most universities.

Digital materials in all formats can usually be accessed through any web browser. Specific apps on mobile devices (phones and tablets) allow use of different formats.

Finding different formats may require some thought. Internet search engines will usually return a variety of formats, and may allow specification of particular formats: for example, images, videos or maps. There are specific searchable sources for particular formats: for example, library catalogues, and sites like Amazon and Abe Books, for books and similar materials, YouTube for videos, Listen Notes for podcasts, and Flickr and Pinterest for images.

  

Tertiary sources, particularly guides to information sources in particular subjects, can be very helpful for showing different formats in context; two examples are shown here.

Show Figure 1 with text: This is an example of a guide to art and design resources from Staffordshire University UK.

Show Figure 2 with text: This is an example of a guide to pharmacy resources from Wilkes University USA.

  

**Exercise** \- find two subject guides to information in particular subject areas. How wide a variety of formats do they include?

  

  

### 1.3.3 Using different formats

Sometimes there is no choice in what format to use: for example, the recommended reading for a university assignment may only be available as a printed book. However, very often there is a choice, and it worth thinking about why you choose one format rather than another. This will often be, as we said in an earlier, due to familiarity and convenience, but there are general considerations, such as these:

*   studies have shown that 'long reads' - book chapters or long articles - are better appreciated when read on paper rather than on screen
    
*   some ideas are better appreciated visually, so a video, an animation, or a series of images, may be more helpful than text
    
*   listening to audio on a mobile device like a phone can allow information to be accessed while one is doing something else at the same time
    
*   studies have shown that some people take in information best through multiple media, so having a transcript to refer to when using a video or podcast may be helpful
    
*   annotating a text can be helpful for understanding and remembering; this is often best done on paper (and studies have shown this to be the most effective way), but it can be done with digital materials
    
*   mobile devices are very convenient, but not good for presenting lengthy or complex information
    
*   most information formats are designed for individual learning, but some digital tools allow for collaborative reading and annotating.
    

The most important thing is to be aware that all the different formats have their own advantages and disadvantages, and be aware of these in choosing which to use.

  

**Exercise** \- Think what are the most important criteria for you in choosing which format to use to access information. Do they vary according to what use you are making of the information?

  

### 1.3.4 Creating different formats

The modern information environment allows everyone to be a creator, as well as a user, of information, and in a variety of different formats in both cases.

  

You will almost certainly have experience of creating information on social media. Perhaps you make videos or podcasts, as well as writing text and taking photographs. You should be able to make use of this experience in your creation of information as part of your studies; we will look at this more in the next lesson, 1.4. For now, it enough to remember that digital tools make it much easier to create different information formats, but thought and effort are needed to make these good quality.

  

**Exercise** \- read the advice on creating education podcasts from University College London at [https://www.ucl.ac.uk/teaching-learning/news/2014/may/5-things-you-need-create-educational-podcasts](https://www.ucl.ac.uk/teaching-learning/news/2014/may/5-things-you-need-create-educational-podcasts). If you have made podcasts yourself, do you think this advice is helpful? If you have not, would this encourage you to try? Look to see if your own university offers any advice on creating information in different formats.

  

Exercise - watch a 15 minute video on making educational videos at [https://www.youtube.com/watch?v=K81LidFRCnc](https://www.youtube.com/watch?v=K81LidFRCnc). If you have made videos yourself, do you think this advice is helpful? If you have not, would this encourage you to try? Did you prefer the video advice here, or the written advice on podcasting in the previous exercise?

**Exercise** \- think about which formats you prefer to use for creating information, and why. Does your answer differ from that for using information in unit 1.3.1? If so, why is this?