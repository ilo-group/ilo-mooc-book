---
title: Orijentacija u informacijskom okruženju
permalink: /1_Orienting/1.1_Intro_hr/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---

**Opis za sudionike:** U ovom ćete modulu upoznati  različita informacijska okruženja koja uključuju različite izvore podataka, formata, usluga i alata. Naučit ćete koliko je važno biti svjestan različitih opcija kada imate potrebu za informacijama u akademskom okruženju. Ovaj modul je pogodan za svakoga tko je zainteresiran za informacijsku pismenost, bez obzira na prethodna znanja.

  

**Ishodi učenja:**

*   Razumjeti informacijsko okruženje
    
*   Upoznati se s različitim vrstama informacijskih izvora i formata
    
*   Biti svjestan važnosti vrednovanja informacija i informacijskih izvora
    
*   Biti svjestan različitih stvaratelja i korisnika informacija
    
*   Biti svjestan veze između informacijskih potreba, izvora, formata, usluga i alata
    

  

Lekcija 1.1

_**Napomene**_:  
_Ova lekcija ima sedam jedinica koje su numerirane brojevima 1.1.N.        
Svaka jedinica ima 100-150 riječi, a neke dodatno imaju jednu ili dvije slike; većina sadrži kratke vježbe - poticaj na promišljanje, video ili vrlo kratku aktivnost na internetu. \[Htjeli smo jedinice napraviti u potpunosti standardizirane – na primjer jednu sliku i jednu vježbu za svaku jedinicu – ali smo zaključili da to studentima ne bi bilo korisno.\]  
Vježbe su namijenjene za individualno samostalno promišljanje, ali mogu biti korištene i za rasprave s drugim studentima ili s nastavnikom.  
Nadalje, ova lekcija ima i kratki kviz za samoprocjenu.  
Za sve slike ili je pribavljeno pravo korištenja (copyright licenca) ili su ih pripremili autori ovih lekcija.  
Uključen je jedan video s YouTubea. Iako je YouTube prilično stabilan izvor, dostupnost videozapisa  treba provjeriti svaki put kada se ovaj tečaj koristi/nudi ili ga treba pohraniti na stalnoj lokaciji.  
Za svaku jedinicu potrebno je najmanje 5 minuta. Ako student koristi i pregledava dodatne izvore, može potrajati i 10 minuta. Dakle, cijela lekcija uključujući kviz, traje 40 minuta (minimalno 30), pa čak i do 90 minuta ako joj student ozbiljno pristupi._

  

**Lekcija 1.1  
Razumijevanje informacijskog krajolika**  
U ovoj ćete lekciji uvidjeti kako je informacijski krajolik složen i kako se ta složenost odražava na pronalaženje i razmjenu informacija.  
Ova lekcija ima sedam jedinica. Neke od tih jedinica imaju kratku vježbu koja zahtjeva da      razmislite kojim načinima bavljenja informacijama dajete prednost. Na postavljena pitanja nema točnih odgovora. Njihova je svrha da potaknu razmišljanje o vlastitom stilu bavljenja informacijama, što će vam biti od pomoći tijekom ovoga tečaja.  
  
**Ciljevi lekcije**  
Po završetku ove lekcije polaznici bi trebali steći osnovno razumijevanje:  
• prirode suvremenog informacijskog okruženja• velike količine dostupnih informacija i raznolikosti izvora i formata• implikacije navedenog na učinkovito i etično postupanje s informacijama

**1.1.1 Razumijevanje svijeta informacija**  
Da biste pronašli prave informacije potrebne za učenje, posao, osobni život ili za bilo koju drugu svrhu, trebate razumjeti kakav je u današnje vrijeme svijet informacija. Nije dovoljno biti sposoban pretraživati Google, tražiti nešto na Wikipediji ili slati i čitati poruke na vašim omiljenim društvenim medijima. Informacijski svijet je složeniji od toga; jedinice u ovoj lekciji objašnjavaju zašto. Razumijevanje cjelokupnog informacijskog okruženja pomoći će vam u svladavanju lekcija na ovom tečaju informacijske pismenosti. Na primjer, trebat ćete upotrebljavati određene izvore i načine pretraživanja kako biste pronašli specifične vrste informacija.

**1.1.2 Polazišne točke**  
Postoji pet polazišta koja svijet informacija čine kompliciranim:  
• vrlo velika količina dostupnih informacija, koja se neprestano ubrzano povećava  
• različite vrste dostupnih izvora i formata

• svatko može biti stvaratelj, djelitelj i korisnik informacija  
• iako su digitalne informacije važne, one nisu jedino što postoji  
• novi svijet informacija donosi nove probleme, osobito one etičke prirode  
  
  

U nastavku će se svaka polazišna točka promatrati vrlo kratko u ovoj lekciji, a detaljnije u drugim lekcijama ovog tečaja.  
  
**Vježba.** Pogledajte ovaj kratki video [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://www.youtube.com/watch?v=iIKPjOuwqHo). Iako je star nekoliko godina, još uvijek daje dobru opću sliku o spomenutim pitanjima.

**1.1.3 Količina dostupnih informacija**Količina dostupnih informacija neizmjerno se povećala otkad je internet u širokoj uporabi, a i dalje se ubrzano povećava. Neki tvrde da su zapravo _podaci_ ti koji se tako brzo povećavaju, dok _informacije_ i _znanje_ ne rastu tako brzo. No, u praksi se čini da postoji sve veća količina informacija s kojima se susrećemo. Procjenjuje se da je količina informacija po jednoj osobi na Zemlji jednaka količini informacija na polici za knjige dugoj 60 metara. Kako bismo to mogli savladati, moramo naučiti kako učinkovito pronalaziti informacije koje su nam potrebne.

To ne znači da postoji mnogo informacija o svim temama, a pogotovo pisanih na svim jezicima; ponekad je jednostavno teško pronaći neku dobru informaciju kakva je potrebna.

  
**Vježba** – pretražite nešto na _Google_u na način da koristite upit od _jedne riječi_ te pogledajte broj dobivenih rezultata \[tamo gdje piše "oko NNN rezultata"\]. Što mislite, što vam to govori o količini dostupnih informacija?

  

**1.1.4 Raznolikost izvora i formata  **  
Ne samo da postoji velika količina dostupnih informacija, već se one nalaze u mnogo različitih formata i izvora: u digitalnom i u tiskanom obliku; _online_ i _offline;_ u obliku teksta, slika, audio i video zapisa, knjiga, popularnih i znanstvenih časopisa, enciklopedija i referentnih djela, baza i zbirki podataka, i tako dalje. Važno je utvrditi izvor i format za koji je najvjerojatnije da ćete u njemu pronaći informaciju koju trebate te znati kako pronaći onu vrstu izvora informacija koju želite. U sljedećim ćemo jedinicama ove lekcije pogledat ćemo načine kako to možemo učiniti. Možda ćete morati koristiti posebne strategije pretraživanja kako biste pronašli određene vrste informacija. Te strategije će biti predstavljene u drugim lekcijama.**Vježba** – Kod pretraživanja Googlea koje ste proveli u vježbi u prethodnoj jedinici 1.1.3, pogledajte prve dvije stranice rezultata. Koliko različitih vrsta izvora informacija vidite (na primjer, prilozi u Wikipediju, vijesti, poglavlje u knjizi)? Je li jednostavno odrediti o kojim se vrstama izvora radi?

**1.1.5 Stvaratelji i djelitelji informacija**Internet olakšava svima da budu i stvaratelji i djelitelji kao i korisnici informacija. To se može postati objavom informacija na društvenim medijima, kao što su Facebook i Twitter, pisanjem objava na blogu, stvaranjem videozapisa ili _podcasta_, unosom sadržaja na Wiki, kao i stvaranjem tradicionalnih materijala, kao što su akademski eseji ili izvješća o radu. Kao i kod pronalaženja informacija, internet je stvaranje i dijeljenje informacija učinio mnogo lakšim. Pritom treba znati kako biti učinkovit, izražavanjem onoga što želimo s ciljem ostvarivanja najboljih rezultata. Pronalaženje, uporaba i dijeljenje informacija idu zajedno i njima treba dobro ovladati.  
  
  
  

_Prikaz_ _**slike 1**_ _s tekstom_: Postoje mnogi dostupni sustavi za dijeljenje informacija, a njihova se popularnost brzo mijenja.  
  
**Vježba** – Razmislite o tome što činite s informacijama. Jeste li pretežito korisnik/čitatelj ili ste aktivni stvaratelj i djelitelj informacija? Ili ste oboje?

  

**1.1.6 Uglavnom digitalne informacije, ali ne samo digitalne**Digitalne informacije, posebno internet, postaju toliko važne da mnogi pretpostavljaju da je sve _online_ i da sve možemo pronaći pomoću tražilica kao što je Google. Ali to nije točno. Sve što je dostupno _online_ nije moguće pronaći putem općih tražilica. Za neke vrste informacija bit će nam potrebne specijalizirane tražilice.

Također, nisu sve informacije dostupne _online_. Još uvijek je mnogo informacija u tiskanom obliku. Stoga moramo znati kako koristiti i tiskane i digitalne izvore te kako koristiti knjižnicu s tiskanim izvorima, a kako digitalne knjižnične izvore. Razumijevanje kako postupati s tiskanim izvorima može pomoći u korištenju digitalnih informacija, jer su mnogi načini postupanja s informacijama razvijeni u tiskanom okruženju te su preneseni u digitalni svijet.

Primjer toga su knjižnični katalozi.  
_Pokaži_ _**slike 2 i 3**_ _uz tekst_: Ove slike prikazuju primjer knjižničnog kataloga na listićima koji se koristio u knjižnicama prije pojave računala i primjer zapisa u suvremenom online katalogu. Opisi knjiga su vrlo slični, jer se računalni sustavi temelje na načinima opisivanja izvora kakvi su prikazani i na kataložnim listićima.

**Vježba** – Koristite li radije tiskane ili digitalne materijale? Ili ste jednako odgovaraju jedni i drugi?

**1.1.7 Etički problemi s informacijama**Iako je sada mnogo lakše pronaći i koristiti informacije, javljaju se novi problemi s digitalnim informacijama. Neki se odnose na pronalaženje i korištenje informacija, kao što je "preopterećenje informacijama", kada je teško nositi se s količinom dostupnih informacija. No, neki od najvažnijih problema su etičke prirode, a povezani su s pravilnim načinom postupanja s informacijama. Na primjer, zbog činjenice da je toliko jednostavno preuzeti informacije s interneta u vlastiti tekst uz pomoć tehnike kopiranja i prenošenja (copy/paste) te preuzeti i uređivati slike, morate biti pažljivi pri navođenju izvora. Tekstovi drugih autora moraju biti odgovarajuće citirani te navedeni, a izvori slika koje koristite moraju biti navedeni. Lakoća dijeljenja informacija donosi probleme mogućeg gubitka privatnosti i zlouporaba u _online_ okruženju. Ove probleme ćemo detaljnije razraditi u sljedećim jedinicama ove lekcije.  
  
**Vježba** – Razmislite o nedavnoj situaciji kada ste koristili neke izvore s interneta, na primjer pri pisanju eseja ili zadaće – koliko ste bili pažljivi pri navođenju izvora iz kojih ste preuzimali sadržaje?

**Kviz za samoprocjenu (za lekciju 1.1)**  
Koje od sljedećih izjava govore o problemima u pronalaženju i korištenju informacija s kojima se danas suočavamo?  
  
1. Postoji toliko informacija da može biti teško pronaći ono što je potrebno.  
2. Količina informacija se povećava toliko brzo da to može predstavljati problem pri pronalaženju potrebne informacije.  
3. Moramo pronaći najbolje načine za stvaranje, razmjenu i korištenje informacija.

4. Moramo biti u stanju učinkovito koristiti i tiskane i digitalne izvore informacija.  
5. Tražilice poput Googlea ne mogu pronaći sve informacije na internetu.  
6. Količina različitih formata informacija može otežati njihovo korištenje.  
7. Nisu sve informacije dostupne _online_, iako mnogi to pretpostavljaju.  
8. Digitalne informacije olakšavaju nesvjesno neetično ponašanje.  
  
  

Odgovor: sve navedeno.

Ako ste mislili da bilo koja od ovih izjava ne predstavlja problem, možda biste se trebali vratiti i ponovno pročitati  ovu lekciju.