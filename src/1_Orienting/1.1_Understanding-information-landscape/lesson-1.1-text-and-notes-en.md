---
title: Introduction
permalink: /1_Orienting/1.1_Intro_en/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---



  

**Lesson 1.1**

**Understanding the information landscape**

In this lesson, you will gain a basic awareness of the complexity of the information landscape, and what means for finding and sharing information.

The lesson has 7 units. Some of the units have a short exercise which asks you to think about the ways you prefer to deal with information. There are no right answers to these questions; they are intended to help you reflect on your own information style, which will be helpful as you go through this course.

  

  

**Learning objectives**

At the end of this lesson, learners should have a basic outline understanding of:

*   the nature of the modern information environment
    
*   the great amount of information, and wide variety of sources and formats, available
    
*   the implications of this for efficient and ethical dealing with information
    

  

**1.1.1 Understanding the information world**

To find the right information that you need for your studies, work, personal life, or any other purpose, you need to understand what todays' information world is like. It is not enough to be able to do a search on Google, to look something up on Wikipedia, or to send and read messages on your favourite social media. The information world is more complicated than that; the units in this lesson explain why. These points are important because an understanding of the whole information environment will help your understanding of all the rest of the materials in this Information Literacy Online course; for example, why you would need to use special sources and searching methods to find some kinds of information.

**1.1.2 The main points**

There are five points, in particular, which make the information world a complicated place:

*   there is a very large, and rapidly increasing, amount of information now available
    
*   there is a wide variety of sources and formats now available
    
*   everyone can be a creator and sharer of information as well as a user
    
*   while digital information is important, it is not all there is
    
*   the new information world brings new problems, particularly ethical problems
    

We will now look at each of these in turn, very briefly, in this lesson, and will return to them, in more detail, in other lessons of this course.

  

**Exercise**. Watch the short video at [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://www.youtube.com/watch?v=iIKPjOuwqHo). Although made a few years ago, it still gives a good general idea of these issues.

  

**1.1.3 Amount of information**

The amount of information available has increased enormously since the Internet became widely used, and continues to increase rapidly. Some people argue that it is actually _data_ which is increasing so rapidly; _information_ and _knowledge_ do not grow so fast. But in practice it seems that there is an ever increasing amount to deal with; one estimate is that there is the equivalent of 60 meters of bookshelves of information for each person on earth. To cope with this, we have to learn to find what we need efficiently from the growing amount which is available. This does not mean that is necessarily a lot of information on all topics, and certainly not written in all languages; sometimes it can still be difficult to find any good information of the kind required.

  

**Exercise** - look something up on Google using a one word search term, and look at the numbers of results obtained \[where it says "about <number> of results"\]. What do you think this tells you about the amount of information available?

  

**1.1.4 Variety of sources and formats**

Not only is there this great volume of information available, but it is found in many different formats and sources: digital and print; online and offline; text, images, audio and video; books, magazine and academic journals; encyclopedias and reference books; databases and data collections; and so on. It is important to work out which of these is most likely to give the information you need, and to know how to find the kind of resources you want. We will look at ways of doing this in later units of this lesson. You may need to use special searching methods to find particular kinds of information; these will be covered in other lessons.

**Exercise** - for the Google searches that you did in 1.1.,3 look at the first two pages of results. How many different types of sources can you see? (for example Wikipedia entries, news items, book chapters)? Is it easy to tell what kinds of sources they are?

  

**1.1.5 Creators and sharers**

The Internet makes it easy for everyone to be both a creator and sharer of information, as well as a user. This can be by sending information on social media, such as Facebook and Twitter, by writing blog posts, making videos or podcasts, or by contributing to wikis, as well as creating more traditional materials, such as academic essays or work reports. Just as with finding information, the Internet makes creating and sharing it much easier, but it is important to know how to do it effectively, communicating what we want to get across to best effect. Finding and communicating information go together, and we need to do both well.

_Show_ _**image 1**_ _with the text_: There are many systems available for sharing information, and their popularity changes quickly.

  

**Exercise.** Think about how you deal with information. Are you mainly a user/reader, or are you an active creator and sharer? Or are you both?

  

**1.1.6 Mainly digital but not only**

Digital information, and the Internet in particular, is becoming so important that many people assume that everything is online, and that we can find it with search engines like Google. But this is not correct. Not everything that is online can be found through the main search engines; we may need special search engines for some kinds of information.

Also, not all information is online; much is still in print form, so we need to know how to use printed sources as well as digital, and how to use a library of printed materials as well as digital library resources. An understanding of printed sources can also help in dealing with digital information, as many ways of handling information have been developed in the world of print, and carried through into the digital world.

  

An example of this can be seen in library catalogues. _Show_ _**images 2 and 3**__, with the text_: These pictures show an example of the printed card catalogues used in libraries before computerisation, and a record in a modern online catalogue; the descriptions of the books are very similar, since the computer systems are based on the kinds of descriptions used on cards.

**Exercise**. Are you more comfortable using printed materials or digital materials? Or equally comfortable with both?

  

**1.1.7 Ethical problems with information**

Although it is now much easier to find and use information, new problems arise with digital information. Some are to do with finding and using information; such as "information overload", when it is difficult to cope with the amount of information available. But some of the most important of these problems are ethical, to do with the right way of use information. For example, because it is so easy to reuse information from the Internet in your own writing, by cutting and pasting text, and by including and editing images, you need to be careful to acknowledge sources, to quote and cite other writers properly, to acknowledge the source of images you use, and so on. The ease with which information can be shared brings problems of possible loss of privacy, and of online abuse. We will look in more detail at these problems in later units of this lesson.

  

**Exercise.** Think of a recent occasion when you have used material from the Internet in your own writing; say for an essay or a report; how careful were you about crediting where it came from?

  

  

  

**Self-assessment quiz (for Lesson 1.1)**

Which of the following are problems for finding and using information today:

  

1. there is so much information, it can be difficult to find what is needed

2. the amount of information increases so rapidly it can be difficult to keep up

3. we have to think about the best ways to create and share information, as well as to use it

4. we need to be be able to use both printed and digital sources of information effectively

5. search engines like Google cannot find all the information on the Internet

6. the number of different formats of information can make it difficult to cope with them all

7. not all information is available online, although many people assume it is

8. digital information makes it easier to behave unethically by mistake

  

A: all of them. If you thought any of these was not a problem, you might want to go back and review this lesson.