---
title: Introducció
permalink: /1_Orienting/1.1_Intro_cat/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---


## Lesson 1.1 Introducció

En aquesta lliçó adquiriràs una consciència bàsica de la complexitat del món de la informació i aprendràs què significa trobar i compartir informació.

La lliçó consta de 7 unitats. Algunes de les unitats inclouen breus exercicis on se't pregunta sobre els teus mètodes preferits per analitzar la informació. No hi ha respostes correctes a aquestes preguntes; el seu objectiu és ajudar-te a reflexionar sobre el teu propi estil informatiu, cosa que et serà útil a mesura que avancis en aquest curs.

  

**Objectius d'aprenentatge**

Al final d'aquesta lliçó, els alumnes han de tenir una comprensió bàsica de:

* La naturalesa dels entorns d'informació moderns.
* La gran quantitat d'informació i la gran varietat de fonts i formats disponibles.
* Les implicacions que això té per a una gestió eficient i ètica de la informació.

  

### 1.1.1 Entendre el món de la informació

Per tal de trobar la informació adient que necessites per als teus estudis, treball, vida personal o per a qualsevol altre propòsit, necessites entendre com és avui el món de la informació. No n'hi ha prou amb saber fer una cerca a Google, consultar alguna cosa a la Wikipedia, o enviar i llegir missatges a les teves xarxes socials preferides. El món de la informació és bastant més complicat; en les unitats d'aquesta lliçó t'explicarem el perquè. Els punts que tractarem en aquesta lliçó són importants perquè entendre l'entorn de la informació t'ajudarà a entendre tots els altres materials del curs Information Literacy Online; per exemple, per què és necessari utilitzar fonts i mètodes de cerca especials per trobar determinats tipus d'informació.

### 1.1.2 Els punts principals

Hi ha cinc punts, en particular, que fan que el món de la informació sigui complex: 

* Existeix una gran quantitat d'informació disponible en l'actualitat, que va en ràpid augment.
* Existeix una gran varietat de fonts i formats disponibles en l'actualitat. 
* Tothom pot ser creador i distribuïdor d'informació, així com usuari. 
* Per bé que la informació digital és important, no és tota la informació existent. 
* El nou món de la informació planteja nous problemes, en particular problemes ètics. 

Examinarem cadascun d'aquests punts, molt breument, en aquest mòdul. Tornarem a ells, amb més detall, en propers mòduls. 

  

**Exercici**. Mira el vídeo de sota. [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://informationliteracy.eu/encoded/1_Orienting/Videos-Module1-Catalan-Translation/1_Orienting_1.1.2_world-info-capacity-animation_CAT/orig_1_Orienting_1.1.2_world-info-capacity-animation_CAT.mp4). Encara que va ser realitzat fa anys, dona una bona visió general sobre el tema.
  

### 1.1.3 Quantitat d'informació

La quantitat d'informació disponible ha augmentat enormement des que es va generalitzar l'ús d'Internet, i segueix augmentant ràpidament. Algunes persones argumenten que en realitat és la quantitat de dades el que està augmentant tan ràpidament: la informació i el coneixement no creixen tan ràpid. Però, en la pràctica, sembla que hi ha una quantitat cada vegada més gran d'informació per tractar; una estimació és que hi ha l'equivalent a 60 metres de prestatgeries amb informació per cada persona a la terra. Per fer front a això, hem d'aprendre a trobar allò que necessitem de forma eficient a partir de la quantitat creixent d'informació disponible. Això no significa que hi hagi molta informació sobre tots els temes, i certament no està escrita en tots els idiomes; de fet, de vegades és difícil trobar bona informació del tipus requerit. 
  

**Exercici** 
Busca alguna cosa en Google usant un terme de cerca d'una sola paraula, i mira el nombre de resultats obtinguts. Què creus que ens diu això sobre la quantitat d'informació disponible?
  

### 1.1.4 Varietat de fonts i formats

No només hi ha un gran volum d'informació disponible, sinó que aquesta es troba en molts diferents formats i fonts: digital i impresa; en línia i fora de línia; text, imatges, àudio i vídeo; llibres, revistes i revistes acadèmiques; enciclopèdies i llibres de referència; bases de dades i col·leccions de dades; i així successivament. És important determinar quina d'aquestes fonts o formats és més probable que proporcioni la informació que necessites i saber com trobar el tipus de recursos que desitges. Examinarem les maneres de fer això en les lliçons posteriors d'aquest mòdul. És possible que necessitis utilitzar mètodes especials de cerca per trobar tipus específics d'informació; aquests mètodes seran coberts en altres mòduls.

**Exercici** 

Mira les dues primeres pàgines de resultats de les cerques de Google que has fet en el punt 1.1.3. Quants tipus diferents de fonts ets capaç d'identificar (per exemple, articles de Wikipedia, notícies, capítols de llibres)? És fàcil saber quin tipus de fonts són?

### 1.1.5 Creadors i compartidors

Internet fa que sigui fàcil per a tots ser alhora creadors i compartidors d'informació, així com usuaris. Això passa quan enviem informació en mitjans socials, com Facebook i Twitter, escrivim articles en blogs, fem vídeos o podcasts, o contribuïm a wikis, així com quan elaborem materials més tradicionals, com assaigs acadèmics o informes de treball. Igual que amb la recerca d'informació, Internet facilita molt la creació i l'intercanvi d'informació, però és important saber com fer-ho de manera eficaç, comunicant el que volem transmetre de la millor manera possible. Trobar i comunicar informació són dos elements que van de la mà, i necessitem fer ambdues coses bé.


![THi ha molts sistemes disponibles per a compartir informació, i la seva popularitat canvia ràpidament:](/1_Orienting/1.1_Understanding-information-landscape/Image-1.jpg)

  

**Exercici** 

Pensa en com et relaciones amb la informació. Ets fonamentalment un usuari / lector, o ets un creador i compartidor actiu? O totes dues coses?
  

### 1.1.6 Predominantement digital, però no exclusiva

La informació digital, i Internet en particular, s'està tornant tan important que molta gent assumeix que tot està en línia, i que podem trobar-ho amb cercadors com Google. Però això no és així. No tot el que està en línia es pot trobar a través dels principals motors de cerca; és possible que necessitem cercadors especials per a determinats tipus d'informació.

A més, no tota la informació està en línia; encara hi ha molta informació en forma impresa, així que necessitem conèixer per igual com utilitzar les fonts impreses i les digitals, i com consultar una biblioteca de materials impresos així com els recursos d'una biblioteca digital. Comprendre les fonts impreses també pot ajudar-nos a tractar amb la informació digital, ja que en el món analògic de la impremta la impressió s'han desenvolupat moltes formes de tractar la informació que s'han transferit al món digital.

Un exemple d'això es pot veure en els catàlegs de la biblioteca. 

![Aquestes imatges mostren un exemple dels catàlegs de fitxes impreses utilitzats a les biblioteques abans de la informatització, i un registre en un modern catàleg en línia; les descripcions dels llibres són molt similars, ja que els sistemes informàtics es basen en el tipus de descripcions utilitzades en les antigues fitxes.](/1_Orienting/1.1_Understanding-information-landscape/Image-2.jpg)

![Aquestes imatges mostren un exemple dels catàlegs de fitxes impreses utilitzats a les biblioteques abans de la informatització, i un registre en un modern catàleg en línia; les descripcions dels llibres són molt similars, ja que els sistemes informàtics es basen en el tipus de descripcions utilitzades en les antigues fitxes.](/1_Orienting/1.1_Understanding-information-landscape/Image-3.jpg)
**Exercici**
Et sents més còmode usant materials impresos o digitals? O tots dos per igual?
  

### 1.1.7 Problemes ètics amb la informació

Encara que avui dia és molt més fàcil trobar i utilitzar la informació, la informació digital fa sorgir nous problemes. Alguns d'ells tenen a veure amb la cerca i l'ús de la informació, com ara la "infoxicació", quan és difícil gestionar la quantitat d'informació disponible. Però alguns dels problemes més importants són de caire ètic, i estan relacionats amb la forma correcta d'utilitzar la informació. Per exemple, donat que és tan fàcil reutilitzar la informació d'Internet quan escrivim, retallant i enganxant text, i editant i inserint imatges, necessitem tenir prou cura d'especificar bé les fonts, citar altres escriptors de forma correcta, reconèixer les fonts de les imatges que fem servir, etcètera. La facilitat amb què es pot compartir la informació comporta problemes de possible pèrdua de privacitat, usos indeguts i ciberassetjament. Examinarem amb més detall aquests problemes en les lliçons posteriors d'aquest mòdul.
  

**Exercici** 

  
Pensa en una ocasió recent en què hagis utilitzat materials d'Internet en els teus propis escrits; per exemple, per a un treball acadèmic o un informe. Com d'acurat vas ser a l'hora d'esmentar les fonts?
  

  

**Test d'autoavaluació**

 Quins dels següents punts són problemes actuals per trobar i utilitzar la informació? 
 Marca **totes** les respostes correctes 

[x] Hi ha tanta informació que pot ser difícil trobar allò que es necessita.

[x] La quantitat d'informació augmenta tan ràpidament que pot ser difícil estar al dia.

[x] Hem de pensar en les millors maneres de crear i compartir la informació, així com d'utilitzar-la.

[x] Hem de ser capaços d'utilitzar eficaçment les fonts d'informació impreses i digitals.

[x] Els cercadors com Google no poden trobar tota la informació a Internet.

[x] El nombre de formats diferents d'informació pot fer difícil abastar-los tots.

[x] No tota la informació està disponible en línia, encara que moltes persones assumeixen que sí que ho està.

[x] La informació digital fa que sigui més fàcil comportar-se de forma poc ètica sense adonar-nos.

[Explanation]
Tots ells. Si has pensat que algun d'aquests punts no era un problema, potser vulguis tornar a repassar aquesta lliçó.