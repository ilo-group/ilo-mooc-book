---
title: Verstehen der Informationslandschaft
permalink: /1_Orienting/1.1_Intro_de/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---


**Lektion 1.1**

**Verstehen der Informationslandschaft**

In dieser Lektion werdet ihr ein grundlegendes Bewusstsein für die Komplexität der Informationslandschaften entwickeln und lernen, was es bedeutet, Informationen zu finden und zu teilen.

Diese Lektion hat 7 Einheiten. Einige dieser Einheiten beinhalten kurze Aufgaben, die danach fragen, welche Methoden ihr bei der Analyse von Informationen bevorzugt. Dabei gibt es keine richtigen Antworten auf diese Fragen. Sie sollen euch dabei helfen, euren eigenen Informationsstil zu reflektieren, was im Verlauf dieses Kurses hilfreich sein wird.

 
  

**Lernerfolge**

Am Ende dieser Lektionen sollten die Lernenden ein grundlegendes Verständnis von Folgendem haben: 


*   der Natur der modernen Informationsumgebungen
    
*   der Verfügbarkeit von großen Mengen an Informationen und der Vielfalt an Quellen und Formaten
    
*   der Implikation, für einen effizienten und ethischen Umgang mit Informationen
    

  

**1.1.1 Verstehen der Informationswelt**

Um die richtigen Informationen zu finden, die ihr für euer Studium, Arbeit, Privatleben oder einen anderen Zweck benötigt, müsst ihr die heutige Informationswelt verstehen. Es reicht nicht aus, eine Suche auf Google durchzuführen, etwas auf Wikipedia nachzuschlagen oder Nachrichten auf deiner bevorzugten Social Media Plattform zu versenden und zu lesen. Die Informationswelt ist komplizierter als das; die Einheiten in dieser Lektion werden erklären, warum. Diese Punkte sind wichtig, da ein Verständnis der gesamten Informationsumgebung euch helfen wird, alle anderen Materialien in diesem Information Literacy Online Kurs zu verstehen; zum Beispiel, warum du spezielle Quellen und Suchmethoden verwenden musst, um spezifische Arten von Informationen zu finden.

**1.1.2 Die Hauptpunkte**

Es gibt fünf Punkte, welche insbesondere die Informationswelt zu einem komplizierten Ort machen:

* es gibt eine sehr große und schnell wachsende Menge an Informationen, die jetzt verfügbar sind
    
* es gibt eine Vielzahl von Quellen und Formaten, die jetzt verfügbar sind
    
* Jeder kann ein Verfasser und Teilhaber von Informationen sowie ein Benutzer sein
    
* Obwohl digitale Informationen wichtig sind, gibt es noch mehr Informationsformen, die berücksichtigt werden sollten 
    
* Die neue Informationswelt bringt neue, insbesondere ethische Probleme

Wir werden uns nun in dieser Lektion sehr kurz mit jedem einzelnen Aspekt befassen und in anderen Lektionen dieses Kurses ausführlicher auf diese zurückkommen.
  

**Übung**. Schaut euch das kurze Video unter diesem Link an: [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://www.youtube.com/watch?v=iIKPjOuwqHo). Obwohl es schon vor eignen Jahren erstellt wurde, gibt es eine gute allgemeine Vorstellung zum Thema Informationswelt. 

  

**1.1.3 Menge an Informationen**

Die Menge der verfügbaren Informationen hat seit der Verbreitung des Internets enorm zugenommen und steigt weiter rasant an. Einige Wissenschaftler argumentieren, dass es eigentlich die Datenmengen sind, welche so schnell ansteigen; Informationen und Wissen wachsen nicht so schnell.  In der Praxis scheint es,  dass die Auseinandersetzung mit Information wichtiger wird; eine Einschätzung ist, dass es das Äquivalent von 60 Metern Bücherregalen mit Informationen für jeden Menschen auf der Erde gibt. Um dies zu bewältigen, müssen wir lernen, aus der wachsenden Menge an verfügbaren Mitteln effizient das zu finden, was wir brauchen. Das bedeutet nicht, dass eine Vielzahl von Informationen zu allen Themen vorliegt, welche sicherlich nicht in allen Sprachen verfasst sind; manchmal ist es trotzdem schwierig, gute Informationen zu finden.

**Übung** - Sucht etwas bei Google mit einem Ein-Wort-Suchbegriff und seht euch die Anzahl der erzielten Ergebnisse an (wo es heißt "Ungefähr XXX Ergebnisse"). Was sagt es eure Meinung nach über die Menge der verfügbaren Informationen aus?


**1.1.4 Vielfalt der Quellen und Formaten**

Es gibt nicht nur Informationen in großen Mengen, sondern auch in verschiedenen Formaten und Quellen: Digital und gedruckt; online und offline; Text, Bild, Audio und Video; Bücher, Zeitschriften und wissenschaftliche Zeitschriften; Enzyklopädien und Fachbücher, Datenbanken und Datensammlungen und so weiter. Es ist wichtig, herauszufinden, welche davon am ehesten die Informationen liefert, die ihr benötigt  um zu lernen, wie ihr Ressourcen  findet, die ihr braucht. Wir werden in späteren Kapiteln dieser Lektion verschiedene Verfahren vorstellen. Möglicherweise müsst ihr spezielle Suchmethoden verwenden, um bestimmte Arten von Informationen zu finden; diese werden in anderen Lektionen behandelt.

**Übung** -  für die Google-Suche, die ihr in Modul 1.1.3 durchgeführt  habt:  Schaut euch die ersten beiden Ergebnisseiten an. Wie viele verschiedene Arten von Quellen  könnt ihr  finden? (z.B. Wikipedia-Einträge, Nachrichten, Buchkapitel)? Ist es leicht herauszufinden, um welche Arten von Quellen es  sich handelt?

**1.1.5 Ersteller und Teiler**

Das Internet macht es jedem leicht, sowohl Produzent und Teiler von Informationen als auch Nutzer zu sein. Dies kann durch das Senden von Informationen über soziale Medien wie Facebook und Twitter, durch das Schreiben von Blog-Posts, das Erstellen von Videos oder Podcasts oder durch Mitwirken bei Wikis sowie durch die Erstellung traditioneller Materialien wie akademische Essays oder Arbeitsberichte geschehen. Wie beim Auffinden von Informationen macht das Internet das Erstellen und Teilen einfacher.  Dennoch ist es notwendig zu lernen, wie die Informationssuche effektiv durchgeführt wird. Eine effektive Informationssuche, bestehend aus dem Auffinden und Teilen von Information, gehört zusammen. 

_Bild 1 mit Text_: Es gibt viele Systeme, mit denen Informationen geteilt werden können, und ihre Beliebtheit verändert sich schnell.
  

**Übung** Überlegt, wie ihr mit Informationen  umgeht. Seid ihr hauptsächlich ein Benutzer/Leser, oder ein aktiver Ersteller und Teiler von Information? Oder seid ihr beides?

**1.1.6 Überwiegend digital, aber nicht ausschließlich**

Digitale Informationen und insbesondere das Internet  sind wichtig,. Viele Menschen  gehen davon aus, dass  alle Informationen online verfügbar und mit Suchmaschinen wie Google schnell auffindbar sind.  Diese Annahme ist nicht vollständig korrekt. Nicht alles, was im Internet verfügbar ist, kann über die bekanntesten Suchmaschinen gefunden werden; wir benötigen möglicherweise spezielle Suchmaschinen für spezifische Arten von Informationen.

Außerdem sind nicht alle Informationen online verfügbar.  Viele Informationen sind noch in gedruckter Form vorhanden, so dass wir lernen müssen, wie sowohl gedruckte als auch digitale Quellen verwendet werden, und wie eine Bibliothek mit gedruckten Materialien sowie digitale Bibliotheksressourcen gebraucht wird. Ein Verständnis von gedruckten Quellen kann ebenfalls im Umgang mit digitalen Informationen hilfreich sein, da viele Möglichkeiten des Umgangs mit Informationen in der Druckwelt entwickelt und in die digitale Welt übertragen wurden.  

Ein Beispiel dafür sind die Bibliothekskataloge. _Bild 2 und 3 mit Text_ Diese Bilder zeigen ein Beispiel für gedruckten Zettelkataloge, die in Bibliotheken vor der Computerisierung verwendet wurden, sowie eine Aufzeichnung in einem modernen Online-Katalog; die Beschreibungen der Bücher sind sehr ähnlich, da die Computersysteme auf den auf den Karten verwendeten Beschreibungen basieren.

**Übung** Seid ihr mehr vertraut im Umgang mit gedrucktem oder digitalen Materialien? Oder seid ihr mit beidem gleich vertraut?


**1.1.7 Ethische Probleme mit Informationen**

Obwohl es heute viel einfacher ist, Informationen zu finden und zu nutzen, entstehen neue Probleme mit digitalen Informationen. Einige haben mit dem Auffinden und Verwenden von Informationen zu tun, wie z.B. "information overload", wenn es schwierig ist, mit der Menge der verfügbaren Informationen umzugehen. Wichtigste Probleme sind ethischer Natur, die mit der richtigen Art und Weise der Nutzung von Informationen zu tun haben. Zum Beispiel ist es einfach, Informationen aus dem Internet im eigenen Dokument wiederzuverwenden, indem du den Text ausschneidest und einfügst sowie Bilder bearbeitest und einbindest. Ihr müsst darauf achten, die Quellen korrekt anzugeben, andere Autoren richtig zu zitieren, sowie die Quelle, der von dir verwendeten Bilder anzugeben und so weiter. Die Leichtigkeit, mit der Informationen ausgetauscht werden können, bringt Probleme des möglichen Verlustes der Privatsphäre und des Online-Missbrauchs mit sich. Wir werden uns mit diesen Problemen in späteren Lektionen befassen.
  

**Übung** Denkt an eine aktuelle Situation, bei der ihr Material aus dem Internet in eurem eigenen Aufsatz verwendet habt; z.B. für einen Text oder einen Bericht;  wart ihr vorsichtig bei der Quellenangabe?

  

  

**Quiz zum Selbsttest (für Lektion 1.1)**

Welche der folgenden Punkte sind heute Probleme beim Auffinden und Verwenden von Informationen?

1. Es gibt viele Informationen, so dass es schwierig sein kann, herauszufinden, was benötigt wird 

2. Die Menge der Informationen nimmt so schnell zu, dass es schwierig sein kann, diese zu verarbeiten

3. Wir müssen überlegen, wie man Informationen effektiv erstellt,  teilt und nutzt

4. Wir müssen in der Lage sein, sowohl gedruckte als auch digitale Informationsquellen effektiv zu nutzen

5. Suchmaschinen wie Google verfügen  nicht über alle Informationen im Internet 

6. Die Anzahl verschiedener Informationsformate kann den Umgang mit diesen schwierig gestalten 

7. Nicht alle Informationen sind online verfügbar sondern auch in gedruckter Form vorhanden. 

8. Digitale Informationen erleichtern es, sich versehentlich unethisch zu verhalten
  

A: Alle von ihnen. Wenn ihr dachtet, dass eines davon kein Problem darstellt,  solltet ihr vielleicht diese Lektion wiederholen.