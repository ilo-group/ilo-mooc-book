---
title: Orientiranje v informacijski pokrajini
permalink: /1_Orienting/1.1_Intro_sl/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---

**Uvod v Modul 1**

  

**Orientiranje v informacijski pokrajini**

  

V tem modulu se boste seznanili z različnimi informaicjskimi okolji, ki vključujejo raznotere informacijske vire, formate, storitve in orodja. Naučili se boste, kako pomembno je, da se zavedamo različnih možnosti, kadar imamo v akademskem svetu neko informacijsko potrebo.

  

Učni cilji:

*   Razumevanje informacijskega okolja
    
*   Poznavanje informacijskih virov
    
*   Zavedanje potrebe po vrednotenju informacij in informacijskih virov
    
*   Zavedanje različnih vrst ustvarjalcev in uporabnikov informacij
    
*   Zavedanje povezanosti informacijskih potreb z viri, formati, storitvami, orodji.
    

  

**Lekcija 1.1**

**Razumevanje informacijske pokrajine**

Uvod

V tej lekciji boste pridobili splošne informacije o zapletenosti informacijske pokrajine ter kaj to pomeni za iskanje in izmenjavo informacij.

Lekcija je sestavljena iz 7 enot. Nekatere enote so sestavljene iz kratke naloge, ki spodbuja razmišljanje o tem, kako najraje ravnate z informacijami. Pravilnih in napačnih odgovorov na ta vprašanja ni, naloga vas samo spodbuja k razmišljanju o vašem osebnem informacijskem stilu. Vse to vam bo pomagalo pri nadaljnjih vajah tega tečaja.

  

**Učni cilji**

Na koncu lekcije naj bi študentje razvili razumevanje o:

*   naravi sodobnih informacijskih okolij
    
*   ogromni količini dostopnih informacij, raznolikostih virov in formatov
    
*   pomenu vsega tegapri učinkovitosti in etičnem ravnanju z informacijami.
    

  

1.  **Razumevanje informacijskega sveta**
    

Dandanes živimo v informacijski dobi, v kateri je pomembno, da razumemo, kako deluje sodobna informacijska pokrajina, da lahko najdemo informacije za študij, delo vsakdanje življenje ali kateri koli drug namen.

Ni dovolj, da znamo uporabljati iskalnik Google ali Wikipedijo in pošiljati in brati sporočila na socialnih medijih. Informacijsksvet je bolj zapleten. V tej lekciji boste izvedeli zakaj. Razumevanje o celotnem informacijskem okolju je pomembno, da boste razumeli celoten tečaj Information Literacy Online. Izvedeli boste, zakaj zahtevajo določene informacije posebne vire in s tem tudi posebne metode iskanja.

  

**1.1.2 Glavne točke**

Poznamo 5 glavnih točk, zaradi katerih je informacijski svet zapleten:

*   Količina informacij je velika in se hitro povečuje.
    
*   Veliko različnih virov in formatov je na voljo
    
*   Vsakdo lahko ustvari in deli informacije in je hkrati tudi uporabnik drugih informacij
    
*   Čeprav so digitalne informacije pomembne, to niso vse informacije
    
*   V sodobnem informacijskem svetu se moramo spoprijemati z novimi težavami, zlasti etičnimi
    

  

V tej lekciji se bomo samo kratko ukvarjali z omenjenimi problemi, v naslednjih lekcijah pa se bomo podrobneje srečali z njimi.

  

**Vaja**. Oglejte si naslednji videoposnetek [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://www.youtube.com/watch?v=iIKPjOuwqHo). Čeprav je bil objavljen pred nekaj leti, omogoča dober pregled nad težavami.

  

**1.1.3 Količina informacij**

Količina informacij se je zaradi vedno pogostejše rabe interneta močno povečala. Nekateri trdijo, da se širi predvsem količina podatkov, ne pa dejanskih informacij in znanja. V praksi pa se zdi, da se soočamo z vse večjo količino; neka ocena pravi,, da je za vsakega posameznika na voljo količina informacij v obsegu 60 metrov knjižnih polic. Prav zaradi tega se moramo naučiti, kako ločiti med koristnimi in manj koristnimi informacijami. Čeprav imamo dostop do ogromnega števila informacij, ne obstajajo nujno informacije o vseh področjih in temah. Poleg tega informacije zagotovo niso napisane v vseh jezikih. Zato je velikokrat težko najti dobre informacije.

  

**Vaja** – Vpišite v iskalnik Google eno besedo in preverite število zadetkov \[tam, kjer piše "približno <število> zadetkov."\]. Kakšno je vaše mnenje o tem velikem številu dostopnih informacij?

  

**1.1.4 Raznolikost virov in formatov**

Dandanes pa nimamo le dostopa do veliko informacij, tudi veliko formatov in virov je na voljo: digitalno ali tiskano,  virtualno ali realno, besedila, slike, (video-)posnetki, knjige, revije, znanstveni članki, enciklopedije in drugo referenčno gradivo podatkovne zbirke in tako naprej. Pomembno je spoznati, kaj so najboljše možnosti, priti do zaželenih informacij in kako najti najkoristnejše vire. V tej lekciji vam bomo predstavili metode, ki vam bodo pomagale pri iskanju dobrih informacij. Da boste našli zaželene informacije, boste morali morda uporabljati posebne metode iskanja. S temi metodami se bomo ukvarjali v drugih lekcijah.

  

**Vaja** – Pri prejšnji vaji (1.1.3) ste vpisali besedo v iskalnik Google. Zdaj si oglejte prvi dve strani zadetkov. Koliko različnih vrst virov ste našli? (na primer objave na Wikipediji, novice, poglavja v knjigah itd.)? Ali je po vašem mnenju enostavno prepoznati in povedati, za kakšne vire gre?

  

  

**1.1.5 Ustvarjalci in delilci**

Internet nam omogoča igranje treh vlog: vlogo ustvarjalca, delilca in uporabnika. Izmenjujemo informacije na družbenih omrežjih, kot sta Facebook in Twitter, objavljamo osebne misli v spletnih dnevnikih, zapišemo nam znana dejstva v Wikipedijo, naložimo videoposnetke ali podcaste, objavljamo bolj tradicionalne znanstvene članke ali delovna poročila. Pojav interneta je poleg lažjega iskanja omogočil lažje ustvarjanje in širjenje različnih informacij, a treba je vedeti, kako to početi učinkovito in kako doseči najboljči učinek te komunikacije.

Obstaja tesna povezava med iskanjem in posredovanjem informacij – oboje moramo dobro obvladati.

_Pokažite sliko_ _**1**_ _in besedilo_: Imamo dostop do veliko sistemov, ki nam omogočajo objavo oziroma delitev informacij, a njihova priljubljenost se hitro spreminja.

  

**Vaja.** Razmislite, kakšen je vaš dostop do informacij in kako ravnate z njimi. Ste večinoma uporabnik oz. bralec, ali ste aktivni izvajalec in delilec informacij? Ali ste oboje?

  

**1.1.6 Večinoma digitalno, a ne samo to**

Internet in informacije v digitalni obliki so postale tako pomembne, da so mnogi ljudje prepričani, da je vse online in da lahko s spletnimi iskalniki, kot je na primer Google, najdejo vse. Toda to ni res. Z običajnimi spletnimi iskalniki nimamo dostopa do vseh online virov. Za nekatere določene informacije potrebujemo posebne spletne iskalnike.

Poleg tega niso vsi viri dostopni online, saj obstaja tudi veliko gradiva v tiskani obliki. Pomembno je, da vemo, kako uporabljati tiskane in digitalne vire in kako uporabiti knjižnico tiskanega gradiva, pa tudi digitalno knjižnico.

Razumevanje tiskanih virov nam lahko pomaga pri ravnanju z digitalnimi informacijami. Mnoge metode, ki se nanašajo na ravnanje z informacijami, so nastale v obdobju tiska – iste metode uporabljamo tudi v digitalni dobi.

  

Knjižnični katalog je dober primer za to. _Pokažite_ _**sliki 2 in 3**_ _ter besedilo_: Na slikah vidite knjižnični katalog v tiskani obliki (listkovni katalog), ki smo ga uporabljali pred obdobjem digitalizacije ter zapis v sodobnem online knjižničnem katalogu.

Opisi knjig se ne razlikujejo, saj temeljijo opisi v računalniškem katalogu na knjižničnem katalogu v tiskani obliki.

  

**Vaja**. Uporabljate raje tiskane ali digitalne vire? Ali uporabljate oboje?

  

**1.1.7 Etični izzivi z informacijami**

Čeprav dandanes hitro najdemo in uporabimo nove informacije, se pojavijo v povezavi z digitalnimi informacijami nove težave. Nekatere nastanejo zaradi iskanja in uporabe informacij, kot na primer “preobloženost z informacijami”, ki nastane zaradiprevelike količine informacij.. A največji problem predstavljajo etični izzivi, kot na primer pravilna uporaba dosegljivih informacij.

Informacije na internetu je enostavno ponovno uporabiti oziroma preoblikovati s tem, da izbrišemo in dodamo dele besedila ali vključimo in obdelamo slike. Paziti moramo, da s pravilnim citiranjem priznamo avtorstvo besedila, navedemo vire slik in s tem tudi avtorske pravice. Težave nastanejo predvsem zaradi enostavne objave podatkov, ki lahko ogroža zasebnost ali varnost na internetu. Z navedenimi težavami se bomo podrobneje ukvarjali v naslednjih lekcijah.

  

**Vaja.** Pomislite na kako nedavno situacijo, v kateri ste sami pri svojem pisanju uporabljali preko interneta dosegljivo gradivo.  Ste bili natančni pri navajanju virov?

  

  

**Kviz za preverjanje samoocenjevanja**  **(za lekcijo 1.1)**

  

Kateri od naslednjih problemov se dandanes pojavijo, ko iščemo in uporabljamo informacije:

  

1\. obstaja toliko informacij, da je težko najti prav tisto, ki jo potrebujem

2\. število dosegljivih informacij se hitro povečuje, težko jim sledimo

3\. razmisliti moramo o najboljših načinih, kako ustvariti in deliti ter uporabljati informacije

4\. znati moramo učinkovito uporabljati tako digitalne kakor tudi tiskane vire.

5\. na spletnih iskalnikih, kot je Google, ne najdemo vseh digitalnih informacij

6\. informacije bodo predstavljene v različnih formatih – težko se znajdemo pri vseh teh oblikah

7\. ljudje so prepričani, da lahko najdemo vse informacije online, a temu ni tako

8\. digitalne informacije lažje vodijo pomotoma k neetičnemu vedenju

  

Odg.: Vse izjave. Če menite, da nobena izmed navedenih problemov ne predstavlja posebnih težav, preglejte še enkrat to lekcijo.