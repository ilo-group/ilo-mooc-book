---
title: Entender el universo de la información
permalink: /1_Orienting/1.1_Intro_es/
eleventyNavigation:
    order: 1
    parent: 1_Orienting
---

*1.1 Entender el universo de la información**

**Introducción**

En esta lección adquirirás una conciencia básica de la complejidad del mundo de la información y aprenderás qué significa encontrar y compartir información. 

La lección consta de 7 unidades. Algunas de las unidades incluyen breves ejercicios donde se te pregunta sobre tus métodos preferidos para analizar la información. No hay respuestas correctas a estas preguntas; su objetivo es ayudarte a reflexionar sobre tu propio estilo informativo, lo que te será útil a medida que avances en este curso. 
  

  

**Objetivos de aprendizaje**

Al final de esta lección, los alumnos deben tener una comprensión básica de: 

* La naturaleza de los entornos de información modernos.
* La gran cantidad de información y la gran variedad de fuentes y formatos disponibles.
* Las implicaciones que esto tiene para una gestión eficaz y ética de la información.
    

  

**1.1.1 Entender el mundo de la información**

Para encontrar la información adecuada que necesitas para tus estudios, trabajo, vida personal o cualquier otro propósito, necesitas entender cómo es hoy el mundo de la información. No basta saber hacer una búsqueda en Google, consultar algo en Wikipedia, o enviar y leer mensajes en tus redes sociales favoritas. El mundo de la información es bastante más complicado; las unidades en esta lección te explicarán por qué. Los puntos que vamos a tratar en esta lección son importantes porque entender el entorno de la información te ayudará a entender todos los demás materiales del curso _Information Literacy Online_; por ejemplo, por qué es necesario usar fuentes y métodos de búsqueda especiales para encontrar determinados tipos de información.


**1.1.2 Los puntos principales**

Hay cinco puntos, en particular, que hacen que el mundo de la información sea complejo:

* Existe una gran cantidad de información disponible en la actualidad, que va en rápido aumento.
* Existe una gran variedad de fuentes y formatos disponibles en la actualidad.
* Cualquiera puede ser un creador y un distribuidor de información, así como un usuario.
* Si bien la información digital es importante, no es toda la información existente.
* El nuevo mundo de la información plantea nuevos problemas, en particular problemas éticos.

Ahora examinaremos cada uno de estos puntos, muy brevemente, en este módulo. Volveremos a ellos, con más detalle, en próximos módulos.

**Ejercicio**
Mira el vídeo de abajo [https://www.youtube.com/watch?v=iIKPjOuwqHo](https://informationliteracy.eu/encoded/1_Orienting/Videos-Module1-Spanish-Translation/1_Orienting_1.1.2_world-info-capacity-animation_ES/orig_1_Orienting_1.1.2_world-info-capacity-animation_ES.mp4). Aunque fue realizado hace años, da una buena idea general sobre el tema.
  

**1.1.3 Cantidad de información**

La cantidad de información disponible ha aumentado enormemente desde que se generalizó el uso de Internet, y sigue aumentando rápidamente. Algunas personas argumentan que en realidad es la cantidad de datos lo que están aumentando tan rápidamente; la información y el conocimiento no crecen tan rápido. Pero, en la práctica, parece que hay una cantidad cada vez mayor de información que tratar; una estimación es que hay el equivalente a 60 metros de estanterías con información para cada persona en la tierra. Para hacer frente a esto, tenemos que aprender a encontrar lo que necesitamos de forma eficiente a partir de la cantidad creciente de información disponible. Esto no significa que haya mucha información sobre todos los temas, y ciertamente no está escrita en todos los idiomas; de hecho, a veces es difícil encontrar buena información del tipo requerido.


**Ejercicio** 

Busca algo en Google usando un término de búsqueda de una sola palabra, y mira el número de resultados obtenidos. ¿Qué crees que dice esto sobre la cantidad de información disponible?
  

**1.1.4 Variedad de fuentes y formatos**

No solo existe un gran volumen de información disponible, sino que esta se encuentra en muchos diferentes formatos y fuentes: digital e impresa; en línea y fuera de línea; texto, imágenes, audio y vídeo; libros, revistas y revistas académicas; enciclopedias y libros de referencia; bases de datos y colecciones de datos; y así sucesivamente. Es importante determinar cuál de estas fuentes o formatos es más probable que proporcione la información que necesitas y saber cómo encontrar el tipo de recursos que deseas. Examinaremos las maneras de hacer esto en las lecciones posteriores de este módulo. Es posible que necesites utilizar métodos especiales de búsqueda para encontrar tipos específicos de información; estos métodos serán cubiertos en otros módulos.

**Ejercicio** 

Mira las dos primeras páginas de resultados de las búsquedas de Google que has hecho en el punto 1.1.3. ¿Cuántos tipos diferentes de fuentes eres capaz de identificar (por ejemplo, artículos de Wikipedia, noticias, capítulos de libros)? ¿Es fácil saber qué tipo de fuentes son?
  

**1.1.5 Creadores y compartidores**

Internet hace que sea fácil para todos ser a la vez creadores y compartidores de información, así como usuarios. Esto pasa cuando enviamos información en medios sociales, como Facebook y Twitter, escribimos artículos en blogs, hacemos vídeos o podcasts, o contribuimos a wikis, así como cuando elaboramos materiales más tradicionales, como ensayos académicos o informes de trabajo. Al igual que con la búsqueda de información, Internet facilita mucho la creación y el intercambio de información, pero es importante saber cómo hacerlo de manera eficaz, comunicando lo que queremos transmitir de la mejor manera posible. Encontrar y comunicar información son dos elementos que van de la mano, y necesitamos hacer ambas cosas bien.

_Show_ _**image 1**_ _with the text_: Hay muchos sistemas disponibles para compartir información, y su popularidad cambia rápidamente:
  

**Ejercicio** 

Piensa en cómo te relacionas con la información. ¿Eres fundamentalmente un usuario/lector, o eres un creador y compartidor activo? ¿O ambas cosas?
  

**1.1.6 Predominantemente digital, pero no exclusivamente**

La información digital, e Internet en particular, se está volviendo tan importante que mucha gente asume que todo está en línea, y que podemos encontrarlo con buscadores como Google. Pero esto no es así. No todo lo que está en línea se puede encontrar a través de los principales motores de búsqueda; es posible que necesitemos buscadores especiales para determinados tipos de información.

Además, no toda la información está en línea; todavía hay mucha información en forma impresa, así que necesitamos conocer por igual cómo usar las fuentes impresas y las digitales, y cómo consultar una biblioteca de materiales impresos así como los recursos de una biblioteca digital. Comprender las fuentes impresas también puede ayudarnos a tratar con la información digital, ya que en el mundo analógico de la imprenta se han desarrollado muchas formas de tratar la información que se han transferido al mundo digital. 

Un ejemplo de esto se puede ver en los catálogos de la biblioteca. _Show_ _**images 2 and 3**, with the text_:_ Estas imágenes muestran un ejemplo de los catálogos de fichas impresas utilizados en las bibliotecas antes de la informatización, y un registro en un moderno catálogo en línea; las descripciones de los libros son muy similares, ya que los sistemas informáticos se basan en el tipo de descripciones utilizadas en las antiguas fichas.

**Ejercicio**

¿Te sientes más cómodo usando materiales impresos o digitales? ¿O ambos por igual?

**1.1.7 Problemas éticos con la información**

Aunque hoy en día es mucho más fácil encontrar y utilizar la información, la información digital hace surgir nuevos problemas. Algunos de ellos tienen que ver con la búsqueda y el uso de la información, como por ejemplo la "infoxicación", cuando es difícil manejar la cantidad de información disponible. Pero algunos de los problemas más importantes son de índole ética, y están relacionados con la forma correcta de utilizar la información. Por ejemplo, debido a que es tan fácil reutilizar la información de Internet cuando escribimos, cortando y pegando texto, y editando e incrustando imágenes, necesitamos tener el cuidado de especificar bien las fuentes, citar a otros escritores de forma correcta, reconocer las fuentes de las imágenes que usamos, etcétera. La facilidad con la que se puede compartir la información trae consigo problemas de posible pérdida de privacidad, usos indebidos y ciberacoso. Examinaremos con más detalle estos problemas en las lecciones posteriores de este módulo.

**Ejercicio** 

Piensa en una ocasión reciente en la que hayas utilizado materiales de Internet en tus propios escritos; por ejemplo, para un trabajo académico o un informe. ¿Cómo de cuidadoso fuiste al acreditar las fuentes?

  

  

**Test de autoevaluación**

¿Cuáles de los siguientes puntos son problemas actuales para encontrar y utilizar la información?
Marca **todas** las respuestas correctas

[x] Hay tanta información que puede ser difícil encontrar lo que uno necesita.

[x] La cantidad de información aumenta tan rápidamente que puede ser difícil estar al día.

[x] Tenemos que pensar en las mejores maneras de crear y compartir la información, así como de utilizarla.

[x] Debemos ser capaces de utilizar eficazmente las fuentes de información impresas y digitales.

[x] Los motores de búsqueda como Google no pueden encontrar toda la información en Internet.

[x] El número de formatos diferentes de información puede hacer difícil abordarlos todos.

[x] No toda la información está disponible en línea, aunque muchas personas asumen que sí lo está.

[x] La información digital hace que sea más fácil comportarse de forma poco ética sin darnos cuenta.

[explanation]
Todos ellos. Si has pensado que alguno de estos puntos no era un problema, tal vez quieras volver a repasar esta lección.
[explanation]
