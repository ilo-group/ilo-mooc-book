---
title: Uporaba informacij v družbenih omrežjih
permalink: /1_Orienting/1.4_Using_social_media_sl/
eleventyNavigation:
    order: 4
    parent: 1_Orienting
---




## Lekcija 1.4 Uporaba informacij v družbenih omrežjih

**Uvod**

V tej lekciji boste spoznali, da so izkušnje, ki ste jih pridobili na spletnih družbenih omrežjih, lahko koristne pri zbiranju in izmenjavi informacij med študijem ali na delovnem mestu.

Lekcija je sestavljena iz 4 enot. V vsako lekcijo je vključena kratka vaja, ki vas bo spodbujala h kritičnemu razmišljanju o osebni rabi družbenih medijev. Pravilnih in napačnih odgovorov na ta vprašanja ni, naloga vas naj spodbuja k razmišljanju o pomembnosti družbenih omrežij v zvezi z zbiranjem in izmenjavo informacij med študijem ali na delovnem mestu. Vse to vam bo pomagalo pri nadaljevanju tega tečaja.

  

**Učni cilji**

Na koncu lekcije naj bi študentje razumeli:

*   uporabnost in potencial spletnih družbenih omrežij pri ravnanju z informacijami za študijske in profesionalne namene
    
*   pomembnost/uporabnost spretnosti, ki so jih pridobili med osebno rabo družbenih omrežij, za ravnanje z informacijami med študijem in na delovnem mestu.
    

  

### 1.4.1 Zbiranje in posredovanje informacij na družbenih omrežjih
    

Spletna družbena omrežja ste verjetno uporabili za komuniciranje s prijatelji. Družbeni mediji se v mnogih državah zelo pogosto uporabljajo. Najpriljubljenejše platforme pa se razlikujejo od države do države.

![koraj univerzalna uporaba družbenih omrežij - Raziskave Edison Research so ugotovile, katera družbena omrežja najpogosteje uporabljajo mladi Američani, stari od 12 do 24 let.](/1_Orienting/1.4_Using-social-media/1.4_Figure-1.png)

Čeprav se tega mogoče ne zavedate, ste se z rabo spletnih družbenih omrežij naučili veliko o iskanju in posredovanju informacij. To vam je lahko v pomoč pri uporabi informacij med študijem, predvsem pri skupnem učenju ali skupinskih nalogah.

Naučili ste se, kako ...

*   izbrati najboljši medij za posredovanje določene vrste informacij.
    
*   posredovati informacije hitro in učinkovito.
    
*   poiskati informacije in nasvete.
    
*   ustvarjati in deliti informacije v različnih formatih (na primer snemanje in objava videoposnetkov).
    

  

Čeprav se boste med študijem soočali z drugimi viri in drugo vsebino, vam bodo tudi izkušnje na družbenih omrežjih v pomoč pri uporabi različnih informacij.

  

**Vaja** – Razmislite o osebni rabi spletnih družbenih omrežij. Ali večinoma zbirate in posredujete informacije? Ali raje iščete informacije in nasvete, ali jih delite? Ali se ukvarjate z vsemi naštetimi stvarmi? Bi informacije na isti način uporabljali tudi med študijem ali na delovnem mestu?

  

### 1.4.2 Spletna družbena omrežja in izbira informacij
    

Na spletnih družabnih omrežjih najdemo veliko informacij. Medtem ko izbiramo informacije, ki nas zanimajo, se odločamo tudi, komu želimo slediti in s kom bi radi prišli v stik. iz tega se lahko veliko naučimo o izbiri informacij med študijem ali na delovnem mestu. Zavedati se moramo, da določamo na družbenih omrežjih sami, s kom se družimo.

Tudi med študijem ali na delovnem mestu moramo izbrati ustrezne vire in formate, ki so vezani na določene informacije. Izbira naj bi bila zmeraj zavestna, saj ne smemo dati stvarem, ki jih že poznamo, prednost zaradi udobja.

Raziskave so potrdile, da imajo družbena omrežja učinek filtrirnega mehurčka. Osebno pozornost posvečamo le ljudem in virom, ki nas zanimajo in s katerimi se strinjamo. Rezultat tega je lahko, da se ne ukvarjamo s prepričanji in mnenji drugih. V spletnih družbenih omrežjih se lahko ukvarjamo samo s stališči, s katerimi se strinjamo tudi sami. Na znanstvenih področjih ali na delovnem mestu pa se moramo soočati z vsemi stališči – prebrati in razumeti moramo vse pomembne izjave in zamisli.

Družbena omrežja temeljijo na zaupanju. Sami odločate, kateri nasveti in katera stališča se vam zdijo koristna. Isto velja za informacije, ki jih uporabljate med študijem ali na delovnem mestu. Določiti moramo, katere informacije se nam zdijo primerne. S tem se bomo ukvarjali v naslednjih lekcijah.

  

**Vaja** – Razmislite o osebnem vedenju na spletnih družbenih omrežjih. Pomislite, koga dodate, komur sledite in s kom ste v stiku na družbenih omrežjih. Mislite, da bi lahko izbrali informacije na enak način tudi pri študiju ali na delovnem mestu?

  

### 1.4.3 Spletna družbena omrežja in informacije na znanstveni ravni

Informacije na družbenih omrežjih ter strokovne in znanstveneinformacije se trenutno močno prekrivajo. Mnogi profesorji, univerze in akademske skupnosti so aktivni na spletnih družbenih omrežjih, predvsem na Facebooku in Twitterju.

![“Royal Society of Chemistry” je dober primer za akademsko skupnost, ki je aktivna na družbenih omrežjih, kot sta Facebook in Twitter.](/1_Orienting/1.4_Using-social-media/1.4_Figure-2.png)

Obstajajo spletne strani, ki objavljajo tudi znanstvene članke in učna gradiva ter jih uporabljajo za oglaševanje preko socialnih medijev. Dober primer predstavlja [Humanities Commons](https://hcommons.org/).

![Spletna stran “The Humanities Commons” povezuje izmenjavo informacijskih virov in komunikacijo na družbenih omrežjih.](/1_Orienting/1.4_Using-social-media/1.4_Figure-3.png)

Prepričani smo, da bo komunikacija na znanstveni ravni v prihodnosti postala vse bolj podobna družbenim omrežjem. Izkušnje, ki ste jih pridobili z osebno rabo spletnih družbenih omrežij, vam bodo pomagale pri učenju in na delovnem mestu v tem novem okolju.

  

**Vaja** – Poiščite 3 primere družbenih omrežij, ki se ukvarjajo z znanstvenimi temami. Lahko izberete bodisi stran na Facebooku ali objave profesorja na Twitterju bodisi oddelek na univerzi. Poglejte si objave. Bi vam spremljanje izbranega družbenega omrežja pomagalo pri študiju?

  

**Vaja** – Oglejte si spletno stran [Humanities Commons](https://hcommons.org/). Poiščite in izberite skupino, ki vas zanima. Katere informacije so objavljene in kako člani komunicirajo?

  

  

### 1.4.4 Socialni viri znanstvenih in strokovnih informacij

  

V prejšnji lekciji smo se ukvarjali z družbenimi omrežji za znanstvene namene. Obstajajo tudi druge oblike virov, kot so spletni dnevniki ali wikiji.

  

Wikiji so informacijski viri, ki jih ureja in spreminja veliko število zainteresiranih ljudi. Na drugi strani obstaja tradicionalno ustvarjanje tovrstnih virov s strani maloštevilnih strokovnjakov. Najbolj znana je Wikipedija, prosto dostopna spletna splošna enciklopedija. Spada med najbolj uporabljene spletne strani, saj jo v določeni situaciji uporabi marsikdo. Uporaba Wikipedije je razmeroma kontroverzna, nekateri profesorji ne dovolijo njene uporabe kot vir. Kritika se nanaša predvsem na to, da nima konsistentnega preverjanja s strani strokovnega uredništva, kar povzroča, da ostanejo napake dolga leta v sistemu, ne da bi jih popravili. Problem pa je tudi neenakomerno pokrivanje, obravnava in predstavitev tem in stališč. Veliko ljudi pa vseeno meni, da je Wikipedija dobro izhodišče za iskanje informacij o neki tematiki. V prihodnosti bomo verjetno imeli več tovrstnih virov. Čeprav nam lahko pomagajo pri zbiranju informacij, jih moramo kritično oceniti, jih morda uporabiti za začetek iskanja, a se nanje ne zanašati kot na edini pomemben vir.

  

Spletni dnevniki, tako imenovani blogi, so nova oblika informacijskih virov. Tudi mnogi znanstveniki in profesorji objavijo besedila ali osebne misli v spletnih dnevnikih. Čeprav blogov ne recenziramo in urejamo na enak način kot knjige ali članke v revijah, nam vseeno omogočajo vpogled v ugotovitve različnih raziskav ali razna znanstvena področja. Objave na spletnih dnevnikih najdemo s spletnimi iskalniki, kot je Google. Obstajajo tudi posebne spletne strani za spletne dnevnike, kot so Wordpress, Blogger ali Tumblr. Priporočljivo je slediti spletnim dnevnikom, ki vas zanimajo in jih objavijo strokovnjaki.

  

**Vaja** – Wikipedija je primer za informacijski vir wiki. Vtipkajte v iskalnik glavne Wikipedije v angleškem jeziku temo ali besedo, ki je značilna za državo, v kateri živite. Ali je članek na Wikipediji dobro opisal vtipkano temo? Obstaja tudi Wikipedija v vašem jeziku? Vtipkajte isti pojem v iskalnik regionalne Wikipedije in preberite še ta članek. Ali je opis vtipkane teme ali besede boljši?