              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } a:link { color: #0000ff; text-decoration: underline } 

**Einheit 6.4 Grafische Darstellung von Daten**

**Lernziele**

In dieser Einheit werdet ihr lernen, wie man Daten effektiv in einer visuellen Form präsentieren kann und wie man verschiedene Werkzeuge und Technologien verwenden kann, um Daten grafisch zu präsentieren.

Am Ende dieser Einheit werdet ihr:

*   In der Lage sein, passende und präzise grafische Darstellungen von Daten und Informationen zu erstellen
    
*   Grafische Darstellungen für ein unterschiedliches Zielpublikum erstellen zu können
    
*   Im Stande sein, mit verschiedenen Werkzeugen zur Erstellung von grafischen Darstellungen zu experimentieren
    
*   Im Stande sein, die besten Werkzeuge und Technologien zur Erstellung von Grafiken zu identifizieren
    

**6.4.1**

Abbildungen sind ein essentieller Bestandteil von wissenschaftlichen Publikationen, um Informationen visuell darzustellen. Beispielsweise sind das Zeichnungen, die komplexe Zusammenhänge darstellen, oder Bilder von Artefakten. Gute Veranschaulichungen helfen dem Leser, ein Thema besser zu verstehen und es sich zu merken. Eine häufige Form von Abbildungen sind Diagramme, die zur Präsentation von Daten in verschiedenen Arten verwendet werden. Ein paar Beispiele von Diagrammen sind:

*   Balkendiagramm: Darstellung durch rechteckige, vertikale oder horizontale Balken. \_einfügen Balkendiagramm\__
    
*   Histogramm: Darstellung von Häufigkeiten durch Rechtecke_. \_einfügen Histogramm\__
    
*   Kreisdiagramm: Darstellung von Prozentsätzen anhand von Tortenstücken _\_Einfügen Kreisdiagramm\__
    
*   Kurvendiagramm: Daten werden in zeitlicher Reihenfolge dargestellt. Verbindungen werden zwischen den verschiedenen Datenpunkten gezeichnet, was zu einer horizontalen Kurve führt, die Veränderungen über eine Zeitspanne zeigt. \_Einfügen Kurvendiagramm\_
    

**6.4.2**

Bevor ihr mit der Darstellung eurer Daten beginnt, solltet ihr euch des Ziels eurer Arbeit sicher sein und wissen, welchen Inhalt ihr in welchem Format und welcher Struktur zeigen wollt. Das bedeutet auch, dass ihr in manchen Fällen eure Daten auf die wichtigsten Werte reduzieren müsst. Eure Darstellungen sollten euren Text unterstützen, aber gleichzeitig sollten sie auch ohne zusätzlichen Text verständlich sein. Wenn ihr eure Veranschaulichungen formatiert, solltet ihr ablenkende Element vermeiden – das bedeutet keine unnötigen Beschriftungen, Symbole, Schattierungen oder Farbverläufe. Vergesst niemals auf das Ziel eurer Darstellung. Es kann sein, dass Darstellungen anders aussehen, wenn man sie beispielsweise für Marketingzwecke produziert als für wissenschaftliche Veröffentlichungen. In einem wissenschaftlichen Umfeld ist es sehr wichtig, dass eure Darstellung vollkommen verständlich und replizierbar ist. Das bedeutet, dass in einem Diagramm alle Daten beschriftet sein sollten.

**6.4.3**

Um eure visuellen Abbildungen zu produzieren, gibt es verschiedene Software-Produkte. Eine der am häufigsten verwendeten Anwendung zur Erstellung von Diagrammen ist die Verwendung von Microsoft Excel oder macOS Numbers. Es gibt aber auch kostenlose Alternativen wie OpenOffice Calc oder Google Spreadsheets. Wenn ihr mit empirischen Daten arbeitet, könnt ihr den Output von spezieller Software wie IBM SPSS Statistics oder der kostenlosen Software JASP verwenden. Um Abbildungen zu bearbeiten, könnt ihr eine große Vielzahl an kommerzieller und kostenloser Grafikbearbeitungs-Software verwenden. Ein paar kostenlose Lösungen sind auch direkt im Webbrowser nutzbar, ohne dass diese auf einem lokalen Rechner installiert werden müssen. Außerdem bietet euch normalerweise euer Betriebssystem bereits ein paar grundlegende Werkzeuge an, wie beispielsweise Paint in Microsoft Windows. Eine Referenzliste zu ein paar hilfreichen Werkzeugen findet ihr am Ende dieser Einheit.

Bedenkt stets auch das Copyright, wenn ihr Bilder verwendet, die ihr nicht selbst erstellt habt. Das Erstellen von guten grafischen Darstellungen ist nicht immer einfach – nehmt euch Zeit, um verschiedene Tools kennenzulernen und experimentiert ein wenig mit verschiedenen Daten - nach einiger Zeit bekommt ihr ein viel besseres Gefühl dafür, wie ihr eure Daten bestmöglich darstellen könnt.

**6.4.4 Beispiel**

In einem Kurs an der Universität musstet ihr eine kurze Befragung unter euren Kollegen zum Thema Nutzung von Streamingdiensten durchführen. Ihr müsst nun eure Ergebnisse in dem Kurs präsentieren und die Präsentation als eine PDF-Datei an den Lehrveranstaltungsleiter schicken. Ihr habt bereits den Fragebogen erstellt, die erhaltenen Daten analysiert und die Prozentangaben der Werte in ein Microsoft Excel Datenblatt eingetragen:

| Nutzung von Video Streamingdiensten | Prozent der befragten Studenten |
| ----------------------------------------- | -------------------------------- |
| Mehr als 20 Stunden                       |                               6% |
| 10 - 20 Stunden                           |                              16% |
| 5 - 10 Stunden                            |                              32% |
| Unter 5 Stunden                           |                              26% |
| Ich verwende Video Streamingdienste nicht |                              20% |

  
  

Um diese Daten in eurer Präsentation übersichtlicher darzustellen, entschließt ihr euch, ein Diagramm zu erstellen. Da ihr hier Prozentangaben habt, ist die optimale Variante, die Ergebnisse mit einem Kreisdiagramm darzustellen. Um dieses Diagramm zu erstellen, müsst ihr eure Daten in Microsoft Excel markieren und danach in der Menüleiste unter Punkt Einfügen das Kreisdiagramm auswählen. Somit wir das Diagramm automatisch generiert. _\_Einfügen Beispiel-2\__

Ihr könnt jetzt euer Diagramm nach euren Wünschen formatieren. Da ihr das Diagramm für ein wissenschaftliches Umfeld erstellt, ist es sehr wichtig, dass eure Veranschaulichung vollkommen verständlich und replizierbar ist. Deshalb solltet ihr durch einen Rechtklick auf das Diagramm die Beschriftungsoptionen öffnen und die Prozentwerte anzeigen lassen. Wir ändern auch den Titel des Diagramms mit einem Doppelklick auf den Text, um einen aussagekräftigeren Titel anzugeben und die Zahl der befragten Personen zu nennen. _\_Einfügen Beispiel-3\__

Schlussendlich sollte das Diagramm so aussehen_: \_Einfügen Beispiel-4\__

Ihr könnt jetzt euer Diagramm in die Präsentation einfügen. In diesem Fall werden wir die Präsentation in Microsoft PowerPoint vorbereiten. Es gibt allerdings auch zahlreiche andere Lösungen diese zu präsentieren. Um das Diagramm zu übertragen, müsst ihr es in Microsoft Excel markieren und kopieren und dann in der Folie in Microsoft PowerPoint einfügen. Ihr müsst nun nur noch der Folie einen Titel geben und die Folie ist fertig. _\_Einfügen Beispiel-5\__

**6.4.5 Übung**

Für eure Präsentation wollt ihr noch ein weiteres Diagramm erstellen. Ihr habt auch schon die nötigen Daten erhoben:

| Kenntnis von Video Streamingdiensten | Kenne ich und benütze ich | Kenne ich, aber ich habe es noch nie benutzt| Kenne ich nicht|
| ---------------------------------------- | --------------------- | ------------------------------ | ---------------- |
| Amazon Prime Video | 45% | 39% | 16% |
| Netflix | 51% | 41% | 8% |
| Maxdome | 8% | 32% | 60% |
| Sky Go | 11% | 38% | 51% |
| Andere Dienste | 21% | 35% | 44% |

  
  

1.  Sucht euch eine angemessene Diagrammart aus und erstellt ein Diagramm mit einer Software nach eurer Wahl (z.B. Microsoft Excel)
    
2.  Übertragt euer Diagramm auf eine Folie in einer Präsentation und speichert es im PDF-Format ab. Ihr könnt Microsoft PowerPoint oder eines der am Ende dieses Kapitels aufgelisteten Tools verwenden (z.B. Prezi)
    
3.  Experimentiert ein wenig mit anderen Diagrammarten und Formatierungsfunktionen, um ein Gefühl dafür zu bekommen, was alles möglich ist. Versucht auch einige der Tools, die am Ende dieser Einheit aufgelistet sind.
    

Euer Lehrveranstaltungsleiter möchte mit eurem Einverständnis euer Diagramm im Internet publizieren und bittet euch deshalb, ihm euer Diagramm in einem Bildformat zu schicken. Erstellt ein Bild im .jpg Format von eurem Diagramm. (Hinweis: Es gibt mehrere Wege dies zu tun: In Microsoft Windows könnt ihr das Programm Snipping Tool verwenden, das euch ermöglicht, direkt ein .jpg Bild von eurem Diagramm zu erstellen; ihr könnt aber auch das Diagramm aus Microsoft Excel in ein Bildbearbeitungsprogramm einfügen wie beispielsweise Microsoft Paint und es dann als .jpg speichern; oder ihr könnt einen Screenshot mit der „Druck“- Taste auf eurer Tastatur machen und es dann in ein Bildbearbeitungsprogramm einfügen, zuschneiden und dann im .jpg Format speichern.)

**6.4.6**

Es gibt einige online Tools, die helfen können, Abbildungen oder interaktive Präsentationen für wissenschaftliche Zwecke zu erstellen. Einige davon sind:

Prezi – Online erstellte Präsentationen: [https://prezi.com/](https://prezi.com/)

Generieren von Infografiken direkt im Webbrowser: [https://piktochart.com/](https://piktochart.com/)

Generieren von Infografiken direkt im Webbrowser: [https://infogram.com/](https://infogram.com/)

Generator für interaktive Diagramme, welche mittels Quellcode auch in eigene Blogs oder Websites eingefügt werden können: [https://developers.google.com/chart/](https://developers.google.com/chart/)

Online PDF Editor: [https://www.pdf2go.com/edit-pdf](https://www.pdf2go.com/edit-pdf)

Kostenlose Grafikbearbeitungssoftware für Microsoft Windows: [http://www.irfanview.com/](http://www.irfanview.com/)

Diese Einheit hat ein paar Aufgaben in Microsoft Office erfordert. Für weitere Informationen bezüglich der Funktionalitäten der Software, könnt ihr die Office-Hilfe Funktion verwenden. Eine einleitende Übersicht zu Microsoft Office findet ihr hier: https://support.office.com/de-de/article/office-%E2%80%93-schnellstarts-25f909da-3e76-443d-94f4-6cdf7dedc51e

Eine kostenlose, webbasierte Alternative zu Microsoft Word ist:  [https://www.google.com/docs/about/](https://www.google.com/docs/about/)

JASP Statistics: [https://jasp-stats.org/](https://jasp-stats.org/)

  
  

Wenn ihr Diagramme aus statistischen Daten entwerft, sind auch ein paar grundlegende statistische Kenntnisse sehr wichtig. Ihr findet hier ein paar einführende MOOCs zu Statistik:

[https://www.udacity.com/course/intro-to-statistics--st101#](https://www.udacity.com/course/intro-to-statistics--st101)

[https://www.edx.org/course/introduction-statistics-descriptive-uc-berkeleyx-stat2-1x](https://www.edx.org/course/introduction-statistics-descriptive-uc-berkeleyx-stat2-1x)