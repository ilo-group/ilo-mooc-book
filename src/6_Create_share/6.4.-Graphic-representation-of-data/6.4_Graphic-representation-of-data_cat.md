              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } 
# **6.4 Representar dades visualment**

**Introducció**

**Objectius d'aprenentatge**

En aquesta lliçó aprendràs com presentar les teves dades visualment de manera efectiva i com utilitzar una varietat d'eines i tecnologies per produir representacions gràfiques de dades.

Al final d'aquesta lliçó, seràs capaç de:

*   construir representacions gràfiques precises i apropiades de dades i informació (per exemple, quadres, mapes, gràfics, models)
*   produir representacions gràfiques per a diferents audiències
*   experimentar amb diferents eines per crear representacions gràfiques
*   identificar les millors eines i tecnologies per generar el gràfic


**6.4.1**

Les imatges poden ser un element essencial en les publicacions científiques per organitzar i comunicar informació visualment. Per exemple, poden utilitzar-se per il·lustrar relacions complexes o artefactes. Les bones visualitzacions ajuden el lector a comprendre i recordar millor un tema. Una forma comuna d'imatge és el gràfic, utilitzat per presentar les dades de diverses maneres. Alguns exemples de gràfics són:

(1) Gràfics de barres: Són barres rectangulars que poden ser verticals o horitzontals. Les seves longituds són proporcionals als valors. _Insert Charts_Barchart.png_

(2) Histogrames: Representen freqüències mitjançant rectangles. _Insert Charts_Histogram.png_

(3) Gràfics circulars: Representen valors percentuals com si fossin porcions d'un pastís. _Insert Charts_PieChart.png_

(4) Gràfics de línies: Tracen les dades d'acord al seu ordre. Es dibuixen les connexions entre dos eixos de dades, això formarà una corba que mostra els canvis al llarg del temps. _Insert Charts_LinChart.png_

  
  

**6.4.2**

Abans de començar a visualitzar les teves dades, has de tenir clar l'objectiu del teu treball i el contingut proposat, així com l'estructura i el format de la imatge. Això també vol dir que en alguns casos hauràs de simplificar les dades que es presenten, reduint-les als valors més importants. La seva visualització ha de donar suport al text, però d'altra banda també ha de ser possible entendre el gràfic per separat. En donar format a la visualització, has d'intentar evitar els elements que distreguin, això implica que no hi hagi etiquetes, icones, textures o degradats de color innecessaris. Tingues sempre en compte el propòsit de la teva visualització. Podria tenir un aspecte diferent, per exemple, si es produeix amb finalitats de màrqueting o si és per a una publicació científica. En un entorn acadèmic i científic és molt important que la teva visualització sigui totalment comprensible i replicable. Això vol dir que en un gràfic totes les dades han de ser etiquetades.


**6.4.3**

Hi ha gran quantitat de programari disponible per produir les teves representacions visuals. Una de les formes més comunes de produir gràfics a partir de dades és l'ús de Microsoft Excel o macOS Numbers. També hi ha alternatives gratuïtes com OpenOffice Calc o els fulls de càlcul de Google. Si treballes amb dades empíriques, també pots utilitzar programari especial com IBM SPSS Statistics o el programari gratuït JASP. Per produir imatges pots utilitzar una àmplia gamma de programari gratuït i comercial d'edició d'imatges. Algunes solucions gratuïtes estan fins i tot disponibles per al seu ús al navegador web sense necessitat d'instal·lar res a l'ordinador. També el teu sistema operatiu sol tenir ja instal·lades algunes eines bàsiques disponibles, com Paint a Microsoft Windows. Al final d'aquesta lliçó trobaràs una referència a algunes eines útils.

Assegura't de respectar sempre els drets d'autor si utilitzes imatges que no hagin estat produïdes per tu mateix. Produir bones imatges visuals no sempre és fàcil. Pren-te el teu temps per experimentar amb diferents eines i jugar amb algunes dades, amb el temps milloraràs la teva destresa en la visualització de dades.


**6.4.4 Exemple**

Com a tasca per a un curs de la teva universitat, has hagut de realitzar una breu enquesta entre els teus companys sobre l'ús dels serveis de streaming de vídeo. Has de presentar els teus resultats a classe i enviar la presentació en un arxiu PDF al teu professor. Ja has acabat el qüestionari, analitzat les dades recollides i introduït els valors percentuals en un full de Microsoft Excel:

| Use of video streaming services     | Percent of questioned students    |
| ----------------------------------  | --------------------------------  |
| Over 20 hours                       |  6% |
| 10 - 20 hours                       | 16% |
|  5 - 10 hours                       | 32% |
| Under 5 hours                       | 26% |
| I do not use video streaming        | 20% |


Per poder representar les dades de forma més clara en la teva presentació, has decidit produir un gràfic. Com es tracta de valors percentuals, un gràfic circular és la manera òptima de presentar-les. Per produir aquest gràfic, selecciona les dades a Microsoft Excel, ves al menú Inserir i tria un gràfic circular. El gràfic es genera automàticament. _Insert Example-2.jpg_

Ara pots donar format al gràfic segons desitgis. Com que produeixes el gràfic en un entorn acadèmic, és molt important que la seva visualització sigui totalment comprensible i replicable. Per tant, afegirem amb un clic dreter sobre el gràfic una etiqueta de dades, perquè els valors percentuals siguin visibles. També canviarem el títol de la taula fent doble clic sobre el text perquè sigui més descriptiu i esmenti el nombre d'estudiants enquestats. _Insert Example-3.jpg_

Finalment, el gràfic té aquest aspecte: _Insert Example-4.jpg_

Ara ja pots incorporar el gràfic a la teva presentació. En aquest cas prepararem la presentació amb Microsoft PowerPoint, però també hi ha moltes altres eines possibles disponibles. Per incorporar el gràfic, només has de seleccionar-lo i copiar-lo a Microsoft Excel i després enganxar-lo a la diapositiva de Microsoft PowerPoint. Ara només has de afegir un títol a la diapositiva i ja estaria acabat. _Insert Example-5.jpg_


**6.4.5 Exercici**

T'agradaria produir un altre gràfic per a la teva presentació. Ja has agregat les dades necessàries:

| Coneixement sobre els serveis de streaming de vídeo | El conec i l'utilitzo  | El conec, però no l'he utilitzat mai  | No el conec |
| --------------------------------------------------- | ---------------------- | ------------------------------------- | ----------- |
| Amozon Prime Video                                  | 45%                    | 39%                                   |       16%   |
| Netflix                                             | 51%                    | 41%                                   |       8%    |
| Maxdome                                             | 8%                     | 32%                                   |       60%   |
| Sky Go                                              | 11%                    | 38%                                   |       51%   |
| Other Services                                      | 21%                    | 35%                                   |       44%   |


1.  Tria una forma adient de gràfic i genera el gràfic amb qualsevol programa que vulguis (per exemple, Microsoft Excel).
2.  Col·loca el gràfic a la diapositiva d'una presentació i desa-ho també en format PDF. Pots utilitzar Microsoft PowerPoint o provar una de les eines enumerades al final d'aquesta lliçó (per exemple, Prezi).
3.  Juga amb altres tipus de gràfics i altres opcions de format per tenir una idea de les possibilitats; prova també algunes de les eines que s'esmenten en els enllaços al final d'aquesta lliçó.

El teu professor vol publicar el teu gràfic a la web i per tant et demana que l'hi enviïs en un format d'imatge. Genera una imatge en format jpg a partir del teu gràfic. (Consell: Hi ha diverses maneres de fer això: A Microsoft Windows pots fer servir el programa Snipping Tool per produir directament una imatge jpg; també pots copiar el gràfic de Microsoft Excel a un programa d'edició d'imatges com Microsoft Paint i guardar-ho com jpg; o pots fer una captura de pantalla amb el botó d'impressió del teu teclat i enganxar-lo en un programari d'edició d'imatges, retallar-lo i guardar-lo en format jpg).

  

**6.4.6**

Hi ha algunes eines en línia que t'ajuden a produir imatges i presentacions interactives amb finalitats acadèmiques. Algunes d'elles són:

Prezi – Presentacions creades en línia: https://prezi.com/

Generar infografies directament al navegador web: https://piktochart.com/

Generar infografies i informes directament al navegador web: https://infogram.com/

Google Charts, generador de diagrames interactius, que també poden ser inserits als teus propis blogs o llocs web utilitzant el codi font: https://developers.google.com/chart/

Editor en línia de PDF https://www.pdf2go.com/edit-pdf

Programari bàsic gratuït d'edició i visualització d'imatges per a Microsoft Windows: http://www.irfanview.com/

Aquesta lliçó inclou algunes tasques amb Microsoft Office. Per obtenir més informació sobre les funcionalitats d'aquest programari, prova la funció d'ajuda o fes una cerca a Internet. Una visió general introductòria del programari de Microsoft Office es pot trobar aquí: https://www.hq.nasa.gov/office/itcd/ctc/documents/Intro-MS-Office.pdf

Una alternativa gratuïta a Microsoft Office basada en el núvol és Google Docs: https://www.google.com/docs/about/

JASP Statistics: https://jasp-stats.org/

A l'hora de dissenyar gràfics a partir de dades estadístiques, també és important comptar amb un cert coneixement sobre estadística. Pots trobar alguns MOOCs d'introducció a l'estadística aquí:

https://www.udacity.com/course/intro-to-statistics--st101#

https://www.edx.org/course/introduction-statistics-descriptive-uc-berkeleyx-stat2-1x