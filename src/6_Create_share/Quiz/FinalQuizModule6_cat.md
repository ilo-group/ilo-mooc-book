              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } 
#Test final

**Test Mòdul 6**

**Quina part d'un article has de mirar primer si vols saber si has llegir-lo en detall?**

(x) Resum

( ) Bibliografia

( ) Cos

**Quina és la millor manera d'arribar al públic amb el teu treball acadèmic?**

( ) Publicar en revistes acadèmiques d'alta qualitat

(x) Realitzar comunicats de premsa

( ) Presentar la teva feina en conferències professionals


**Com pots millorar l'eficiència de les teves publicacions en una xarxa social com Facebook?**

[ ] Publicant textos llargs per donar als teus seguidors una comprensió més profunda

[x] Utilitzant textos breus i imatges o vídeos

[x] Incloent un enllaç a la teva publicació

**Segons el cicle d'informació, quins són els canals típics de comunicació utilitzats el mateix dia d'un esdeveniment?**

(x) Xarxes socials i informatius de televisió

( ) Revistes setmanals

( ) Publicacions científiques

**Quin seria un exemple de xarxa social acadèmica?**

( ) Facebook

(x) ResearchGate

( ) Blogger.com

**Què volen dir les sigles RSS?**

(x) Rich Site Summary

( ) Rich Search Summary

( ) Rapid Search Supply

**Quin gràfic divideix un cercle?**

( ) Histograma

( ) Gràfic de barres

(x) Gràfic circular

( ) Gràfic d'àrees

**Els gràfics de línies mostren…**

(x) Canvis en el temps horitzontalment

( ) Canvis en el temps verticalment

**Els gràfics de barres poden ser…**

(x) Horitzontals o verticals

( ) Només horitzontals

( ) Només verticals

**Hi pot haver diferències entre gràfics amb finalitats científiques i de màrqueting?**

(x) Sí

( ) No

**Quin podria ser un element de distracció en un gràfic, que hauria de ser evitat?**

( ) Valors percentuals

(x) Textures

( ) Text