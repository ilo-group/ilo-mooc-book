              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } a:link { color: #0000ff; text-decoration: underline } 

**Quiz für das Modul 6**

1\. Welchen Teil eines wissenschaftlichen Aufsatzes solltet ihr zuerst lesen, um abschätzen zu können, ob dieser für eure Suche relevant ist?

(x) Abstract

( ) Literatur

( ) Gesamten Textkörper

2\. Was ist eine typische Struktur von wissenschaftlichen Arbeiten? (Drag und drop Übung)

Einleitung – Hauptteil – Zusammenfassung

3\. Was ist die beste Möglichkeit, die breite Öffentlichkeit mit eurer wissenschaftlichen Arbeit zu erreichen?

( ) Publikation in einer hochkarätigen wissenschaftlichen Zeitschrift

(x) Presseaussendungen

( ) Präsentation der Arbeit bei Fachkonferenzen

4\. Wie könnt ihr die Effizienz eurer Postings in sozialen Netzwerken wie z.B. Facebook erhöhen?

( ) Lange Texte, um die Thematik umfangreich beschreiben zu können

(x) Kurze Texte und Bilder oder Videos

(x) Einen Link in das Posting einfügen

5\. Was sind laut dem Informationskreislauf typische Medienkanäle der Berichterstattung am Tag eines Ereignisses?

(x) Soziale Netzwerke und Nachrichtenkanäle

( ) Wochenzeitschriften

( ) Wissenschaftliche Publikationen

6\. Was ist ein Beispiel für ein wissenschaftliches soziales Netzwerk?

( ) Facebook

(x) ResearchGate

( ) Blogger.com

7\. Wofür steht RSS?

(x) Rich Site summary

( ) Rich Search Summary

( ) Rapid Search Supply

8\. Welches Diagramm stellt einen aufgeteilten Kreis dar?

( ) Histogramm

( ) Balkendiagramm

(x) Kreisdiagramm

( ) Flächendiagramm

9\. Kurvendiagramme zeigen…

(x) Änderungen im Zeitverlauf auf einer horizontalen Ebene

( ) Änderungen im Zeitverlauf auf einer vertikalen Ebene

10\. Balkendiagramme können…

(x) Vertikal und horizontal ausgerichtet sein

( ) Nur horizontal ausgerichtet sein

( ) Nur vertikal ausgerichtet sein

11\. Können Unterschiede zwischen Diagrammen für Marketingzwecke und für wissenschaftliche Zwecke bestehen?

(X) Ja

( ) Nein

12\. Was können ablenkende Elemente in Diagrammen sein, die vermieden werden sollten?

( ) Prozentangaben

(x) Farbverläufe

( ) Text