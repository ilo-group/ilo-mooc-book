              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } 

Quiz for Module 6

**At which part of a paper should you look first if you would like to find out if you should read it in detail?**

1.  Abstract
    
2.  Literature
    
3.  Body
    

A: 1

**What is a typical structure of a research paper (drag-and-drop order exercise)?**

Introduction – Body – Conclusion

**What is the best way to reach the public with your academic work?**

1.  Publish in high-quality academic journals
    
2.  Make press releases
    
3.  Present your work at professional conferences
    

A: 2

**How can you enhance efficiency of your posting in a social network like Facebook?**

1.  Post long texts to give your followers a deeper understanding
    
2.  Use short texts and image or video
    
3.  Include a link in your posting
    

A: 2 and 3

**According to the information cycle, what are the typical media channels on the day of the event?**

1.  Social networks and news channels
    
2.  Weekly magazines
    
3.  Scientific publications
    

A: 1

**What is an example for a scholarly social network?**

1.  Facebook
2.  ResearchGate 
3.  Blogger.com
    

A: 2

**What does RSS stand for?**

1.  Rich Site Summary
    
2.  Rich Search Summary
    
3.  Rapid Search Supply
    

A: 1

**Which Chart divides a circle?**

1.  Histogram
    
2.  Bar Chart
    
3.  Pie Chart
    
4.  Area Chart
    

A:3

**Line graphs show…**

1.  changes over time horizontally
    
2.  changes over time vertically
    

A: 1

**Bar charts can be…**

1.  Vertically or horizontally
    
2.  Only horizontally
    
3.  Only vertically
    

A: 1

**Might be there a difference between charts for scientific and marketing purposes?**

1.  Yes
    
2.  No
    

A: 1

**What could be a distracting element of a chart, which should be avoided?**

1.  Percent values
    
2.  Texturing
    
3.  Text
    

A: 2