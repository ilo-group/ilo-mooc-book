
**Kviz modula 6**

**Kateri del raziskovalnega dela** **vam omogoča vpogled v najpomembnejše ugotovitve raziskave?**

1.  izvleček
    
2.  literatura
    
3.  glavni del
    

A: 1

**Katera so osnovna poglavja raziskovalnega dela****?**

uvod – glavni del – sklepne ugotovitve

**Kako seznanite javnost s svojim znanstvenim delom?**

1.  Objava v znanstvenih revijah
    
2.  Sporočilo za javnost
    
3.  Predstavitev dela na znanstvenih konferencah
    

A: 2

**Kako lahko povečate učinkovitost svojih objav na družbenih omrežjih, kot je Facebook?**

1.  Objavite dolgo besedilo s podrobnimi informacijami
    
2.  Objavite kratko besedilo s sliko ali videom
    
3.  V objavo vstavite povezavo
    

A: 2 in 3

**Predstavili smo življenjski cikel informacijskega sistema. Kateri mediji so najbolj značilni na dan dogodka?**

1.  Družbena omrežja in televizija
    
2.  Tedenski časopisi
    
3.  Znanstvene revije
    

A: 1

**Katero izmed naslednjih omrežij je družbeno omrežje za znanstvenike?**

1.  Facebook
    
2.  ResearchGate
    
3.  Blogger.com
    

A: 2

**Kakšen je pomen kratice RSS?**

1.  Rich Site Summary
    
2.  Rich Search Summary
    
3.  Rapid Search Supply
    

A: 1

**Kateri grafikon vsebuje krožne izseke?**

1.  Histogram
    
2.  Palični grafikon
    
3.  Tortni grafikon
    
4.  Črtni grafikon
    

A: 3

**Črtni grafikon prikaže …**

1.  spremembe določenega časovnega razpona v horizontalni obliki
    
2.  spremembe določenega časovnega razpona v vertikalni obliki
    

A: 1

**Palični grafikon** **je lahko …**

1.  vertikalen ali horizontalen
    
2.  samo horizontalen
    
3.  samo vertikalen
    

A: 1

**Ali se razlikujejo grafikoni, ki so namenjeni trženju, od tistih, ki so objavljeni v znanstvenih revijah?**

1.  Da
    
2.  Ne
    

A: 1

**Grafikoni lahko vsebujejo moteče elemente. Kateri so to?**

1.  Odstotni deleži
    
2.  Nepotrebni napisi
    
3.  Besedilo
    

A: 2