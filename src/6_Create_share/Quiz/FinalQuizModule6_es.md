              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } p.western { so-language: hr-HR } p.cjk { font-family: ; so-language: hr-HR } 
# **Test final**


**Test Módulo 6**

**¿Qué parte de un artículo tienes que mirar primero si deseas saber si debes leerlo en detalle?**

(x) Resumen

( ) Bibliografía

( ) Cuerpo

**¿Cuál es la mejor manera de llegar al público con tu trabajo académico?**

( ) Publicar en revistas académicas de alta calidad

(x) Realizar comunicados de prensa

( ) Presentar tu trabajo en conferencias profesionales


**¿Cómo puedes mejorar la eficiencia de tus publicaciones en una red social como Facebook?**

[ ] Publicando textos largos para dar a tus seguidores una comprensión más profunda

[x] Utilizando textos breves e imágenes o vídeos

[x] Incluyendo un enlace en tu publicación


**Según el ciclo de información, ¿cuáles son los canales típicos de comunicación utilizados el mismo día de un evento?**

(x) Redes sociales e informativos de televisión

( ) Revistas semanales

( ) Publicaciones científicas


**¿Cuál sería un ejemplo de red social académica?**

( ) Facebook

(x) ResearchGate

( ) Blogger.com

**¿Qué significan las siglas RSS?**

(x) Rich Site Summary

( ) Rich Search Summary

( ) Rapid Search Supply


**¿Qué gráfico divide un círculo?**

( ) Histograma

( ) Gráfico de barras

(x) Gráfico circular

( ) Gráfico de áreas

**Los gráficos de líneas muestran…**

(x) Cambios en el tiempo horizontalmente

( ) Cambios en el tiempo verticalmente



**Los gráficos de barras pueden ser…**

(x) Horizontales o verticales

( ) Solo horizontales

( ) Solo verticales


**Puede haber diferencias entre gráficos con fines científicos y de marketing?**

(x) Sí

( ) No

**¿Cuál podría ser un elemento de distracción en un gráfico, que debería ser evitado?**

( ) Valores porcentuales

(x) Texturas

( ) Texto
