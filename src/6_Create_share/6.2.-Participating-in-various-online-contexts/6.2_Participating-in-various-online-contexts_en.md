              @page { margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 } a:link { color: #0563c1 } 

**Lesson 6.2. Participating in various online contexts**

**Learning objectives**

In this lesson you will learn how to become more confident with creating and communicating information with others via social networking, blogs and other tools, the importance of taking into account your audience when posting in online spaces, and learn the practical basics of self-presentation.

At the end of this lesson, you will:

*   be aware of the audience when posting in online spaces, and learn the basics of the effective self – presentation
    
*   be able to develop content for a personal website, blog or social media channel
    
*   be able to disseminate information through informal and formal information sources
    
*   be aware of the need to customize information content and writing style to the format of communication
    

  

**6.2.1**

The starting point of your communication – no matter if it is in a scientific or private context – is to define the intended audience. Knowing your audience helps making decisions about what information to include, how to arrange the information, and what kind of supporting details will be necessary for the reader to understand what you are presenting. It also influences the structure.

The most common audiences are:

*   Public: Communication to the public means breaking down complex terms to very easy descriptions. Also the whole wording should much simpler.
    
*   Professionals: Communication can go more in detail and use professional terms. Nevertheless, it should not go into too many details about the research process.
    
*   Scholars: Communication should follow academic standards and be provable. You should also emphasize previous research and describe your methodology in detail.
    

Depending on the group different channels will be used. If you would like to address the public you might use social media channels, use your own blog or website or send out press releases. If you would like to reach the professional community you will use communication organs of professional organizations like newsletters, journals for practitioners or conferences. For reaching the scientific community you can use all of the before mentioned channels, but might want to extend them through academic social networks like ResearchGate, scientific journals and conferences.

  
  

**6.2.2**

If you are planning to use social media channels, there are several aspects that will make your postings more efficient.

When you are using Facebook for professional purposes it is a good idea to create a page additionally to your private profile, so that you can separate your postings. When creating a new page think of a good name for your page and provide a description of your page. Choose also a good cover photo and profile picture. It is easier to get new likes for your page if you have already some followers. Therefore, ask your friends to follow your site. You can also promote your site by defining a budget in the Facebook ads manager. The design of your postings will finally influence how many persons you will reach. Try to keep the text short and to include an image or video (and also link, if appropriate). Such postings have been proven to be more efficient. Additionally, you can try to engage your followers by asking a question.

Another tool you might use is Twitter. The huge difference is, that people do not need to follow you in order to see your posting. It is common to use hashtags on Twitter, which have the form #topic. By clicking on such hashtag you will see the postings of all Twitter users using this hashtag. You can create new hashtags for your ongoing projects and events. E.g. posting a tweet about your project in connection with the hashtag of a scientific event might draw attention to your own work. Postings on Twitter must not exceed 280 characters.

There are tools that allow you to create automatic posts in all of your social media channels. A tool that is free – at least as long as you are not getting a heavy user – is https://dlvrit.com/.

For researchers there exist also special social networks. Some examples are [ResearchGate](https://www.researchgate.net/), and [Academia.edu](https://www.academia.edu/). Another non-commercial alternative is [Humanities Commons](https://hcommons.org/). There you can post your most recent work, allow others to follow your research project and connect with other researchers. They also allow you to make your full texts available (if you have not waived the copyright to a publisher), which raises the chance for your work to be cited. There are also other networks you can use: You can also make available full-texts trough [scribd.com](scribd.com) or can share your presentations through [slideshare.net](slideshare.net). You could use the links to these resources finally for your postings in social media.


**Exercise** 
Think of the social networks you are already using. How could you use them to promote your academic work?

**6.2.3**

If you would like to create longer postings with more content, social networks are not the right place. In this case you might want to create your own blog or website.

Creating a blog or a website today does not require technical skills and can be done using convenient online tools. For creating your own blog you can use for example the website [blogger.com](https://www.blogger.com/about/?r=1-null_user). After you have registered for free you can start to fill your page with your content. Editing can be done in an easy web editor. Blogger.com even enables you to earn money with your blog by integrating Google advertisements.

Another widely used platform is [wordpress.com](https://en.wordpress.com/). Originally started as a blogging tool, WordPress is today also a widely-used platform for creating websites. You can create your own webpage through wordpress.com or install it on your own webserver. Also many hosting providers allow you to pre-install WordPress on your personal webspace. WordPress allows you to create modern websites through a catalogue of themes. All editing can be done straightforwardly in your admin interface. If you are more technically skilled you can also customize all the source code by yourself to give your website individual functionalities.

After you have made a new posting in your blog or on your website you might share the link to the page through a social media posting.

Posting into online spaces and engaging with other users can be a lot of fun. But always keep in mind your privacy when doing so. Anything you posted to the web is hard to remove afterwards. Even if you delete a posting later, it might be already archived somewhere else. When you use social networks, and Facebook in particular, also be aware of attempts to steal your online identity for criminal purposes. Therefore, be always aware of your friend requests and do not accept requests from people you do not know and profiles that seem fake. Also be aware which external applications you allow to access your profile.


**Exercise**
Try to create your first personal blog or website through blogger.com or wordpress.com.

**Additional resources:**

Official video tutorials for blogger.com: [https://www.youtube.com/user/BloggerHelp?hl=en](https://www.youtube.com/user/BloggerHelp?hl=en)

Official instructions to WordPress through WordPress support: [https://en.support.wordpress.com/](https://en.support.wordpress.com/)

_Introduction into creation of blogs and websites will be given through screencasts – maybe use existing open-licenced YouTube video_