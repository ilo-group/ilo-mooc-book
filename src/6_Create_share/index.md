---
title: Create and Share
permalink: /6_Create_share/
eleventyNavigation:
    order: 6
---

# MOOC Module: Let’s create something new based on information and share it!
 
In this module you will learn how to synthesize information and produce new knowledge, independently or in a collaborative environment, and share information and knowledge with others.
