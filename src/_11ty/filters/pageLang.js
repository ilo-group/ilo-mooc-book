module.exports = (page) => {
    let n = page.filePathStem + ""
    let k = n.replace(/.*[_-]([a-z][a-z][a-z]?)/, "$1")
    if(k.length===2 || k.length===3) return k
    return "??"
}