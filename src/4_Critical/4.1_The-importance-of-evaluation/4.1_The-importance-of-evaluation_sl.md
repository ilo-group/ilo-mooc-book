             @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } a:link { color: #000080; so-language: zxx; text-decoration: underline } 

**Lekcija 4.1: Pomembost evalvacije**

  

**Uvod**

V tej lekciji boste spoznali pomembnost evalvacije informacij, ki ste jih zbrali in delili.

Predstavili vam bomo različne situacije, v katerih naj bi preverili informacije glede njihove kakovosti. Tako boste spoznali in ozavestili, v katerih situacijah je evalvacija informacij nujna.

  

**Učni cilji**

  

*   spoznati pomembnosti evalvacije informacij.
    
*   zavedati se, da obstajajo različni motivi tistih, ki so ustvarili in delili informacije.
    
*   razumeti in razlikovati med zanesljivimi in primernimi informacijami.
    
*   zavedati se, da obstajajo pristranske in nezanesljive informacije.
    

  

_Navodila za oblikovanje._

_Besedilo_

  

**4.1.1 ŠE POPRAVITI!**

Oglejte si naslednji video. [https://www.youtube.com/watch?v=p-81Mk3WxbI](https://www.youtube.com/watch?v=p-81Mk3WxbI)

Pomislite na prednosti povezane s kritičnim ovrednotenjem informacij, ki jih posredujejo oglasi. Kakšen bi bil vaš odziv na takšno situacijo? Kako (če sploh) bi lahko vplivali na kakovost informacij?

Kako bi opisali situacijo — kot situacijo, s katero se soočamo vsak dan, v šoli ali na univerzi, na delovnem mestu itd.?

  

_Navodila za oblikovanje._

_Besedilo + video_

  

**4.1.2**


Oglejte si naslednji video. [https://www.youtube.com/watch?v=n7Wjf7Vtmp0](https://www.youtube.com/watch?v=n7Wjf7Vtmp0)

Čemu je namenjena informacija? Kako ste prepoznali namen informacije? Kako to veste? Kako bi opisali informacije — ali so odkrite, zaupne, zavajajoče ali kaj drugega? Zakaj? Ali menite, da se vedenje posameznika lahko zaradi dodatnih informacij spremeni? Zakaj se strinjate, zakaj ne?

Kako bi opisali situacijo — kot situacijo, s katero se soočamo vsak dan, v šoli ali na univerzi, na delovnem mestu itd.?

  

_Navodila za oblikovanje._

_Besedilo + animacija/video_

  

**4.1.3**


  

Oglejte si zdaj še naslednji video.

https://www.youtube.com/watch?v=l9IaSiQ\_Xvc

Čemu je namenjena informacija? Kako ste prepoznali namen informacije? Kako to veste? Kako bi opisali informacije — ali so odkrite, zaupne, zavajajoče ali kaj drugega? Zakaj? Ali menite, da se vedenje posameznika lahko zaradi dodatnih informacij spremeni? Zakaj se strinjate, zakaj ne?

Kako bi opisali situacijo — kot situacijo, s katero se soočamo vsak dan, v šoli ali na univerzi, na delovnem mestu itd.?

  

_Navodila za oblikovanje._

_Besedilo + video_

  

**4.1.4**

Oglejte si naslednji video in ga primerjajte s prejšnjima videoma. https://www.youtube.com/watch?v=04GI7YEGotM

Čemu je namenjena informacija? Kako ste prepoznali namen informacije? Kako to veste? Kako bi opisali informacije — ali so odkrite, zaupne, zavajajoče ali kaj drugega? Zakaj? Ali menite, da se vedenje posameznika lahko zaradi dodatnih informacij spremeni? Zakaj se strinjate, zakaj ne?

Kako bi opisali situacijo — kot situacijo, s katero se soočamo vsak dan, v šoli ali na univerzi, na delovnem mestu itd.?

  

_Navodila za oblikovanje._

_Besedilo + video_

  

**4.1.5**

Ali poznate izraz „lažne novice“ („fake news“)? Tukaj je kratek video, v katerem se boste naučili, kako jih prepoznati.

[https://www.youtube.com/watch?v=xf8mjbVRqao](https://www.youtube.com/watch?v=xf8mjbVRqao)

  

_Navodila za oblikovanje._

_Besedilo + video_

  

**4.1.6**

Oglejte si naslednji video. https://www.youtube.com/watch?v=aOq5obAZBFw

Kako bi videoposnetek ocenili z vašega vidika? Kako bi ocenili informacije, ki se pojavijo v videu? Ocenite jih s kriteriji, ki ste se jih naučili v prejšnjem videu. Se vam zdijo informacije lažne ali ne?

  

Zdaj oglejte si še naslednji video.

[https://www.youtube.com/watch?v=vA87muteUUk](https://www.youtube.com/watch?v=vA87muteUUk)

  

Ukvarjali se bomo z istimi vprašanji:

Kako bi videoposnetek ocenili z vašega vidika?

Kako bi ocenili informacije, ki se pojavijo v videu? Ocenite jih s kriteriji, ki ste se jih naučili v prejšnjem videu. Se vam zdijo informacije lažne ali ne?

Kako bi sami opredelili pojem „lažne novice“? Zakaj postajajo lažne novice del našega vsakdana? Zakaj so lažne novice vse bolj naraščajoča težava današnjega časa? Zakaj je pogosto težko prepoznati lažne novice?

  

_Navodila za oblikovanje._

_Besedilo + video_

  

**4.1.7**

To je zadnja vaja te lekcije. Napišite prispevek, ki ga bi objavili v spletnem dnevniku.

Opišite situacijo, v kateri ste sprejeli slabo odločitev zaradi pomanjkljivih ali nekakovostnih informacij (opišite, zakaj so bile informacije pomanjkljive/nekakovostne). Poleg tega opišite, zakaj je bila odločitev slaba, kakšen vpliv je imela, katere druge možnosti bi obstajale in kaj bi bilo drugače, če bi imeli kakovostne informacije.

  

_Vstavite pogovorno okno._

  

Preberite prispevke svojih kolegov in odgovorite na objave s predlogi, v katerih opišete, kaj bi kolegi lahko storili v situaciji, tako da bi se vse obrnilo na boljše.

Vaši kolegi bodo prebrali tudi vaš prispevek in storili isto.

  

_Navodila za oblikovanje._

_Besedilo + video_