_**Lesson Plan 4.1:**_ _**The importance of evaluation**_

**Introduction**

In this lesson you will learn about the importance of evaluating gathered and shared information.

We will present various situations and scenarios to make you aware of the circumstances that lead to the needs to evaluate information.

  **Learning outcomes:**
* to understand the importance of evaluating information
* to be aware of different motives of those who create and share information
* to comprehend and distinguish between reliability and relevance of information
* to be aware of the fact that information can be biased and inaccurate 


_Guidelines for design_

_text_

  

**4.1.1**

Take a look at this video. [](https://www.youtube.com/watch?v=CUnvuRHxqU8) [https://www.youtube.com/watch?v=CUnvuRHxqU8](https://www.youtube.com/watch?v=CUnvuRHxqU8)

Think why it is good for people to think critically about information they get in diet ads. What would be your response to this situation? How (if at all) could you affect the quality of information?

How would you characterize the situation – as everyday life, school-related, work-related etc.?

  

_Guidelines for design_

_text + video_

  

**4.1.2**

Take a look at this video. [](https://www.youtube.com/watch?v=n7Wjf7Vtmp0) [https://www.youtube.com/watch?v=n7Wjf7Vtmp0](https://www.youtube.com/watch?v=n7Wjf7Vtmp0)

What is the motive behind this information? How do you know?

Would you characterize the information as sincere, as misleading, something else? Why?

Do you think that getting more information changes one’s behaviour? Why would you answer yes or no?

How would you characterize the situation – as everyday life, school-related, work-related etc.?

  

_Guidelines for design_

_text + animation/video_

  

**4.1.3**


  
Now, take a look at this video.

[ https://www.youtube.com/watch?v=l9IaSiQ_Xvc]( https://www.youtube.com/watch?v=l9IaSiQ_Xvc)

What is the motive behind this information? How do you know?

Would you characterize the information as sincere, as misleading, something else? Why?

Do you think that getting more information changes one’s behaviour? Why would you answer yes or no?

How would you characterize the situation – as everyday life, school-related, work-related etc.?

  

_Guidelines for design_

_text + video_

  

**4.1.4**

In comparison with the previous two, take a look at this video. [](https://www.youtube.com/watch?v=04GI7YEGotM) [https://www.youtube.com/watch?v=04GI7YEGotM](https://www.youtube.com/watch?v=04GI7YEGotM)

What is the motive behind this information? How do you know?

Would you characterize the information as sincere, as misleading, something else? Why?

Do you think that getting more information changes one’s behaviour? Why would you answer yes or no?

How would you characterize the situation – as everyday life, school-related, work-related etc.?

  

_Guidelines for design_

_text + video_

  

**4.1.5**


Have you heard  the term 'fake news'? Here is a short video on how you might recognize it.

[https://www.youtube.com/watch?v=xf8mjbVRqao](https://www.youtube.com/watch?v=xf8mjbVRqao)

  

_Guidelines for design_

_text + video_

  

**4.1.6**


Take a look at this video. [https://www.youtube.com/watch?v=aOq5obAZBFw ](https://www.youtube.com/watch?v=aOq5obAZBFw )

How would you evaluate the video from your personal point of view?

How would you evaluate the information in it? Rate it from the point of view of the fake news criteria you watched in the previous video. Is it fake or not?

Now watch the next video:

[https://www.youtube.com/watch?v=vA87muteUUk](https://www.youtube.com/watch?v=vA87muteUUk )

We start with the same questions:

How would you evaluate the video from your personal point of view?

How would you evaluate the information in it? Rate it from the point of view of the fake news criteria you watched in the previous video. Is it fake or not?

How would you yourself define fake news? Why is fake news an issue in today’s world? Why is fake news sometimes hard to recognize?

  

_Guidelines for design_

_text + video_

  

**4.1.7**


This is the last activity in this lesson. Your final task is to write a blog entry:

Describe one situation where you did not make a good decision because the information you had was not of good quality (describe _why_ the information was not good). In your presentation, describe how and why the decision was bad, which other alternatives were possible, what could have been different, had you had better information.

  _Include window for discussion_

Read the blog posts of your colleagues and respond with additional suggestions how they could have handled their situations differently to make a better outcome.

Your colleagues will read your description and do the same for your situation.

  

_Guidelines for design_

_text + blog_


  

  
