**Einleitung**

In dieser Lektion wirst du etwas über die Bedeutung der Evaluierung von gesammelten und geteilten Informationen lernen.

Wir werden verschiedene Situationen und Szenarien präsentieren, um auf Sachverhalte zu verweisen, welche einer Evaluierung von Informationen bedürfen.

**Lernerfolge:**

* Die Wichtigkeit der Evaluierung von Informationen verstehen
* Kenntnis von verschiedenen Absichten derer, die Informationen bereitstellen und teilen
* Zuverlässigkeit und Relevanz von Informationen unterscheiden und nachvollziehen
* Kenntnis darüber haben, dass Informationen verzerrt und fehlerhaft sein können


**4.1.1**

Seht euch das Video an!  [https://www.youtube.com/watch?v=p-81Mk3WxbI](https://www.youtube.com/watch?v=p-81Mk3WxbI)

Denkt darüber nach, wieso es gut ist Informationen die über Werbung zu Ernährung verbreitet werden kritisch zu reflektieren. Was wäre eure Reaktion in dieser Situation? Wie (wenn überhaupt) könntet ihr die Qualität der Informationen beeinflussen?


 Wie würdet ihr diese Situation beschreiben – als Alltagsituation, Situation im Zusammenhang mit Schule, Situation im Zusammenhang mit Beruf etc.?


  

**4.1.2**


Seht euch dieses Video an! Link (https://www.youtube.com/watch?v=n7Wjf7Vtmp0) [https://www.youtube.com/watch?v=n7Wjf7Vtmp0](https://www.youtube.com/watch?v=n7Wjf7Vtmp0)

Was ist das Motiv hinter dieser Information? Wie könnt ihr dieses erkennen?

Empfindet ihr die Information als aufrichtig, irreführend oder etwas anderes? Wieso?

Denkt ihr, dass der Gewinn weiterer Informationen das Verhalten ändert? Wieso würdet ihr mit ja oder nein antworten?

Wie würdet ihr die Situation beschreiben – als Alltagsituation, Situation im Zusammenhang mit Schule, Situation im Zusammenhang mit Beruf etc.?

  

_Guidelines for design_

_text + animation/video_

  

**4.1.3**



Seht euch jetzt dieses Video an! Link [https://www.youtube.com/watch?v=nFnoLpKrTyE](https://www.youtube.com/watch?v=nFnoLpKrTyE)

Was ist das Motiv hinter dieser Information? Wie könnt ihr dieses erkennen?

Empfindet ihr die Information als aufrichtig, irreführend oder etwas anderes? Wieso?

Denkt ihr, dass der Gewinn weiterer Informationen das Verhalten ändert? Wieso würdet ihr mit ja oder nein antworten?

Wie würdet ihr die Situation beschreiben – als Alltagsituation, Situation im Zusammenhang mit Schule, Situation im Zusammenhang mit Beruf etc.?


**4.1.4**

Im Vergleich mit den vorigen zwei Videos, seht euch jetzt dieses Video an! [Video](https://www.youtube.com/watch?v=04GI7YEGotM) [https://www.youtube.com/watch?v=04GI7YEGotM](https://www.youtube.com/watch?v=04GI7YEGotM)

Was ist der das Motiv hinter dieser Information? Wie könnt ihr dieses erkennen?

Empfindet ihr die Information als aufrichtig, irreführend oder etwas anderes? Wieso?

Denkt ihr, dass der Gewinn weiterer Informationen das Verhalten ändert? Wieso würdet ihr mit ja oder nein antworten?

Wie würdet ihr die Situation beschreiben – als Alltagsituation, Situation im Zusammenhang mit Schule, Situation im Zusammenhang mit Beruf etc.?

**4.1.5**


Habt ihr schon den Begriff „Fake News“ gehört? Hier findet ihr ein kurzes Video, das euch helfen kann, sogenannte Fake News zu erkennen.

[https://www.youtube.com/watch?v=xf8mjbVRqao](https://www.youtube.com/watch?v=xf8mjbVRqao)


**4.1.6**


Seht euch jetzt dieses Video an!(https://www.youtube.com/watch?v=JCs35T-dvis) [https://www.youtube.com/watch?v=JCs35T-dvis](https://www.youtube.com/watch?v=JCs35T-dvis)

Wie würdet ihr dieses Video eurer persönlichen Meinung nach beurteilen?

Wie würdet ihr die vermittelten Informationen darin bewerten? Bewertet es nach den Fake News Kriterien, die ihr im vorigen Video gesehen habt. Ist es Fake oder nicht?




Seht euch nun dieses Video an: [Video](https://www.youtube.com/watch?v=tAr6tSdqk1Q)

Wir fangen mit denselben Fragen an:

Wie würdet ihr dieses Video eurer persönlichen Meinung nach beurteilen?

Wie würdet ihr die vermittelten Informationen darin bewerten? Bewertet es nach den Fake News Kriterien, die ihr im vorigen Video gesehen habt. Ist es Fake oder nicht?

Wie würdet ihr selbst Fake News definieren? Wieso sind Fake News so ein ernstzunehmendes Thema in der heutigen Zeit? Wieso ist es manchmal schwer Fake News zu erkennen?


**4.1.7**

Das ist die letzte Aufgabe in dieser Einheit: Schreibt einen Blogeintrag!

Beschreibt eine Situation, in der ihr eine schlechte Entscheidung getroffen habt, weil ihr keine qualitativ hochwertigen Informationen hattet (beschreibt, wieso die Informationen nicht gut waren). Beschreibt, wie und wieso die Entscheidung schlecht war, welche Alternativen es gegeben hat, was ihr besser machen würdet, wenn ihr bessere Informationen hättet.

_[Fenster]_

Liest euch die Blogeinträge eurer Kollegen durch und antwortet darauf, wie sie die Situation anders/besser handhaben hätten können.

Eure Kollegen werden sich euren Eintrag durchlesen und dasselbe tun. 


  

  
