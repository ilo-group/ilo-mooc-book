---
title: Critical information appraisal
permalink: /4_Critical/
eleventyNavigation:
    order: 4
---

## MOOC Module: OCritical information appraisalrienting in the information landscape


# Critical information appraisal

In this module you will become more aware of the different criteria for evaluating information sources and to apply those criteria. Not all information needs, tasks and inquiries require the same criteria and some critical appraisal skills are needed to evaluate information.
