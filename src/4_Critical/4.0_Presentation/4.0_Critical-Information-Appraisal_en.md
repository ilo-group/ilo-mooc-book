**MODULE 4: CRITICAL INFORMATION APPRAISAL**

**Lesson plans**

  **Critical information appraisal**
  
  In this module you will become more aware of the different criteria for evaluating information sources and how to apply those criteria. Not all information needs, tasks and inquiries require the same criteria and some critical appraisal skills are needed to evaluate information.
 
**Learning outcomes:**
* to know how to evaluate information and information sources
* to know how to evaluate online tools and online interactions
* to be able to distinguish between different information resources and the information they provide
* to be able to assess the quality, accuracy, relevance, bias, reputation and credibility of the information resources and the data gathered  



