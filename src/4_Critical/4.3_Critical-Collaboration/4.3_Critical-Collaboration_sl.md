             @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } a:link { color: #0563c1; text-decoration: underline } 

**Lekcija 4.3: Nadzor kakovosti in sodelovanje z drugimi**

  

**Uvod**

V tej lekciji se boste naučili, kako preveriti kakovost in primernost lastnih zadetkov ter zadetkov vaših kolegov.

  

**Učni cilji**

*   sposobnost kritičnega preverjanja in vrednotenja lastnih zadetkov ter utemeljiti izbiro virov in orodij.
    
*   sposobnost oceniti prispevke drugih, ki so nastali v sodelovalnih okoljih, z uporabo različnih meril.
    

  

Lekcija je sestavljena iz dveh delov.

V prvem delu se boste ukvarjali z razvojem in utemeljitvijo meril za vrednotenje virov, ki jih nameravate uporabljati.

Drugi del vam bo pomagal razširiti ozaveščenost o tem, da informacije niso statične, a se lahko spremenijo s časom.

  

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.1**

Za ogrevanje: Videoposnetek o enem od okrivjev standarda ACRL „Scholarship as conversation“:

[https://youtu.be/YGia3gNyHDM](https://youtu.be/YGia3gNyHDM).

  

_Navodila za oblikovanje._

_Besedilo in video._

  

**Prvi del**

Prvi del: Merila za sodelovalno vrednotenje informacij

(prirejeno po “What to do with what you find” [http://lis101.com/what-to-do-with-what-you-find/](http://lis101.com/what-to-do-with-what-you-find/))

  

_Navodila za oblikovanje._

_Besedilo_

  

  

**4.3.3**

Ko ste na začetku iskali informacije v knjižnici, ste našli nekaj zanimivih člankov, ki ste jih poslali na svoj e-poštni naslov. Zdaj sedite pred računalnikom. Zvečer želite preveriti članke in z njimi sestaviti prvi osnutek raziskovalnega dela. Nenadoma se pojavijo težave, ki jih niste pričakovali. Spoznali ste, da ste se v knjižnici le površno ukvarjali s članki. Medtem ko ste v knjižnici bili prepričani, da članki obravnavajo prav določeno temo, ste doma ugotovili, da temu ni tako. Oglejte si naslednje primere in razmislite o tem, kaj bi v tej situaciji lahko storili.

(Primeri na naslednjih straneh iz [http://lis101.com/what-to-do-with-what-you-find/](http://lis101.com/what-to-do-with-what-you-find/)).

  

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.4**

Oglejmo si naslednji primer:

Kaj lahko storite, kadar vsebuje članek ustrezne ključne besede, a se vseeno ne ukvarja z določeno temo? Medtem ko želite najti članke na temo „fantomska podjetja“ oziroma „podjetja poštni nabiralniki“, najdete le prispevke o podjetjih, ki proizvajajo poštne nabiralnike.

  

Razmislite o naslednjih vprašanjih:

*   Kako bi rešili to na najbolj pragmatičen način?
    
*   Kakšen način bi bil etično pravilen?
    
*   Kateri izmed njih bi bil najprimernejši za znanstveno delo?
    

  

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.5**

Na ta vprašanja je možno odgovoriti takole:

V tem primeru boste morali iskati nove članke, saj ne boste mogli citirati tistih, ki ste jih doslej našli, kajti s temi informacijami ne morete dokazati ali zagovarjati svojega stališča. To pomeni, da boste morali verjetno preoblikovati iskalne izraze in jih znova določiti, ali naj bodo bolj specifični ali splošni. Tako ali tako boste morali knjižnico obiskati še enkrat.

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.6**

**Vaja 1:** Tukaj najdete izroček, ki vam bo pomagal v različnih situacijah:

(povezava k datoteki/digitalna oblika).

  

V polje zapišite svoje misli.

_Izroček za študente_

  

Kaj storiti s tem, kar najdete?

(prilagojeno po: http://lis101.com/what-to-do-with-what-you-find/)

  

Med iskanjem informacij boste našli veliko različnih člankov, pri čemer bodo nekateri koristnejši od drugih. Kaj storiti v naslednjih situacijah?

  

1.  **Članek, ki ste ga našli, vsebuje sicer ključne besede, a obravnava drug vidik teme. Kaj storite?**
    

V svojem raziskovalnem delu želite obravnavati socialne programe namenjeni preprečevanju mladoletniškega prestopništva. Našli pa ste članek o protestu ene soseske proti odprtju rehabilitacijskega centra za mladoletne prestopnike.

  

*   Kako bi to rešili na najbolj pragmatičen način?
    
*   Kakšen način bi bil etično pravilen?
    
*   Kateri izmed njih bi bil najprimernejši za znanstveno delo?
    

  

**2\. Članek, ki ste ga našli, obravnava sicer isti vidik vaše teme, a z drugega zornega kota. Kaj storite?**

Želite dokazati, da popoldanska oskrba, ki jo finančno podpira vlada, poveča kriminaliteto/nasilna dejanja — našli pa ste le informacije, ki potrdijo nasprotje.

  

● Kako bi to rešili na najbolj pragmatičen način?

*   Kakšen način bi bil etično pravilen?
    
*   Kateri izmed njih bi bil najprimernejši za znanstveno delo?
    

  

**3\. Članki, ki ste jih našli, ovržejo vašo postavljeno hipotezo oziroma trditev. Kaj storite?**

Naj se še tako trudite, niste našli ničesar, kar bi lahko verodostojno zavrnilo, kar ste našli najprej.

  

● Kako bi to rešili na najbolj pragmatičen način?

*   Kakšen način bi bil etično pravilen?
    
*   Kateri izmed njih bi bil najprimernejši za znanstveno delo?
    

  

**4\. Članek, ki ste ga našli, zagovarja oziroma podpira vašo postavljeno hipotezo oziroma trditev. Kaj storite?**

S člankom, ki podkrepi vaše stališče, lahko odlično zagovarjate postavljeno hipotezo.

  

● Kako bi rešili to na najbolj pragmatičen način?

*   Kakšen način bi bil etično pravilen?
    
*   Kateri izmed njih bi bil najprimernejši za znanstveno delo?
    

  

**Drugi del**

**Drugi del: Informacije niso statične**

(zgled:

[http://sandbox.acrl.org/library-collection/scholarship-conversation-case-study-coffee-super-food-or-health-threat](http://sandbox.acrl.org/library-collection/scholarship-conversation-case-study-coffee-super-food-or-health-threat))

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.7**

Oglejte si NPR epizodo podcasta z naslovom NPR“The Hidden Brain: A Conversation about Life’s Unseen Patterns”, gostitelj Shankar Vedantam, naslov “Scientific Findings Often Fail To Be Replicated, Researchers Say” ki so jo predvajali na NPR 28. avgusta 2015 ([http://www.npr.org/2015/08/28/435416046/research-results-often-fail-to-be-replicated-researchers-say](http://www.npr.org/2015/08/28/435416046/research-results-often-fail-to-be-replicated-researchers-say))

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.8**

Zdaj pa preberite članek “Health Effects of Coffee: Where Do We Stand?”, ki ga najdete na naslednji spletni strani [http://www.cnn.com/2015/08/14/health/coffee-health/index.html](http://www.cnn.com/2015/08/14/health/coffee-health/index.html), ki ga je napisalaSandee LaMotte in je bil objavljen na spletni strani CNN 14. avgusta 2015.

_Navodila za oblikovanje._

_Besedilo_

  

**4.3.9**

**Vaja 1:**

Misli, ki so se pojavile ob branju članka na temo kave, zapišite kot prispevek, ki ga bi objavili v spletnem dnevniku. Uporabljajte informacije, ki ste jih ravno prebrali oz. slišali in videli.

  

_Vstavite pogovorno okno._

  

**Vaja 2:**

Preberite prispevke svojih kolegov in jih pokomentirajte. Na ta način boste spoznali ideje, stališča, analize in sklepe svojih kolegov. Isto bodo storili tudi vaši kolegi.

  

_Navodila za oblikovanje._

_Besedilo in prispevek v spletnem dnevniku._

  

**4.3.10**

Pomislite na svoje študijske naloge. Vsakič, ko delate eno od teh nalog, ste v vlogi nastajajočega raziskovalca, porajajočega se znanstvenika. Zato ste hkrati tudi bodoči udeleženci znanstvenih razprav. V prihodnosti boste mogoče celo obogatili znanstvena poznavanja.

_Navodila za oblikovanje._

_Besedilo_