
_**Final quiz**_

  

**SCREEN 1**

**Visible to the student**

  

Answer in your own words in a few sentences:

  

Why is it important to evaluate information?

  

In which situations is it especially important to evaluate information? Why?

  

What can be the problems/issues with information that dictate the need to evaluate it? Which motives can guide various providers of information?

  

Think of some consequences that may happen (in everyday life, at school, at work) if we don’t think about the information that we find/receive?

  

Choose all criteria of good quality information from the list below:

*   It is true.
    
*   It’s free.
    
*   It is current / not outdated.
    
*   It’s already known to me.
    
*   It is relevant.
    
*   It is of appropriate size.
    
*   It is in appropriate language.
    
*   It’s all contained in one single piece (like one book, one web page etc.).
    
*   It is on an appropriate media, in an appropriate format.
    
*   It’s digital.
    

  

Choose one of the three main criteria (truth, relevance, currency) and explain in your own words what it is.

  

Explain in your own words what is fake news?

  

What does it mean that information is not static but can evolve over time?

  

  

**Guidelines for design**

text