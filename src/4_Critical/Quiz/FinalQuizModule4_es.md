**Test Módulo 4**

  

**Preguntas de respuesta abierta**

  

* ¿Por qué es importante evaluar la información?
* ¿En qué situaciones es especialmente importante evaluar la información? ¿Por qué?
* ¿Cuáles pueden ser los problemas con la información que hacen necesario evaluarla? ¿Qué motivos o intereses pueden guiar a los distintos proveedores de información?
* Piensa en las consecuencias que pueden ocurrir (en la vida cotidiana, en la escuela, en el trabajo) si no reflexionamos sobre la información que encontramos/recibimos.

Elige todos los criterios que cumple información de buena calidad de la siguiente lista:

[x] Es veraz.

[ ] Es gratis.

[x] Está actualizada / no desactualizada.

[ ] Es ya conocida por mí.

[x] Es relevante.

[x] Es de la extensión adecuada.

[x] Está en el lenguaje apropiado.

[ ] Está contenida toda en una sola pieza (como un libro, una página web, etc.).

[x] Está en un medio y formato apropiados.

[ ] Es digital.

**Preguntas de respuesta abierta**

* Escoge uno de los tres criterios principales de calidad de la información (veracidad, relevancia, actualidad) y defínelo con tus propias palabras.
* Explica en tus propias palabras qué son las fake news.
* ¿Qué significa que la información no es estática sino que puede evolucionar con el tiempo?
