**Test Mòdul 4**
  

Preguntes de resposta oberta

  
* Per què és important avaluar la informació?
* En quines situacions és especialment important avaluar la informació? Per què?
* Quins poden ser els problemes amb la informació que fan necessari avaluar-la? Quins motius o interessos poden guiar els diferents proveïdors d'informació?
* Pensa en les conseqüències que poden haver-hi (en la vida quotidiana, a l'escola, a la feina) si no reflexionem sobre la informació que trobem / rebem.

Escull tots els criteris que compleix informació de bona qualitat de la següent llista:

[x] És veraç.

[ ] És gratuïta.

[x] Està actualitzada / no desactualitzada.

[ ] És coneguda ja per mi.

[x] És rellevant.

[x] És de l'extensió adequada.

[x] Està en el llenguatge apropiat.

[ ] Està continguda tota en una sola peça (com un llibre, una pàgina web, etc.).

[x] Està en un mitjà i format apropiats.

[ ] És digital.

Preguntes de resposta oberta

* Escull un dels tres criteris principals de qualitat de la informació (veracitat, rellevància, actualitat) i defineix-lo amb les teves paraules.
* Explica en les teves pròpies paraules què són les fake news.
* Què vol dir que la informació no és estàtica sinó que pot evolucionar amb el temps?
  

**Guidelines for design**

text