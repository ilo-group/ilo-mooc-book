              @page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.79in } p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { so-language: en-GB } 

**Quiz Modul 4**

Wieso ist es wichtig, Informationen zu bewerten?

In welchen Situationen ist es besonders wichtig, Informationen zu bewerten? Warum?

Welche Probleme/Aspekte im Zusammenhang mit Informationen zwingen uns dazu, Informationen zu bewerten? Welche Motive können verschiedene Anbieter von Informationen verfolgen?

Denkt über möglichen Konsequenzen nach, die im Alltag, Berufsleben oder in der Schule eintreten könnten, wenn wir erhaltene Informationen nicht überdenken.

Wählt alle Kriterien für Qualitätsinformationen aus der folgenden Liste aus:

(X)  Sie ist wahr
    

( ) Sie ist kostenlos

(X)  Sie ist aktuell/ nicht veraltet
    

( ) Sie ist mir bereits bekannt

(X)  Sie ist relevant
    

(X) Sie ist in angemessener Größe
    

(X)  Sie ist in angemessener Sprache
    

( ) Sie ist gesammelt in einem einzelnen Medium (z.B. Buch, einer Webseite etc.)

(X) Sie ist ein angemessenes Medium, in einem angemessenen Format

( ) Sie ist digital

  

Wählt eine der drei Hauptkriterien (Wahrhaftigkeit, Relevanz oder Aktualität) und beschreibt diese in euren eigenen Worten.

Erklärt in eigenen Worten, was _Fake News_ sind.

Was hat es für mich für Auswirkungen, dass Informationen nicht statisch sind, sondern sich im Laufe der Zeit weiterentwickeln?