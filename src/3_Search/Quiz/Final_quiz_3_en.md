**Quiz for Module 3**

1. What are the advantages of the general search engines?
( )	transparent
(x) quick
(x)	comprehensive
( )	privacy

2. Which aspect of the following list is not an advantage of the modern library catalogues?
( )	specific search options
( )	human involvement
( )	scientific data included
(x)	many fulltexts available 
( )	quality controlled

3. What are the advantages of the Google Scholar search engine?
(x)	Controlled quality
( )	Transparent
(x)	Many results can be gained
( )	No good searching skills necessary

4. What are the advantages of Social Media?
( )	controlled quality
( )	clear structure
(x)	many search options
( )	no advertisement
(x)	browsing possible

5. What is an OPAC? 
(x)	a modern library catalogue
( )	a discovery system
( )	a scientific search engine


6. You are conducting a research on the impact of climate change on the economy in Canada. Which search strategy do you use?  
(x)	“climate change” AND econom* AND (Canada OR Canadian)
()  	Canada AND (“climate change” OR “economy”)
( )	(Canada OR Canadian) AND climate AND econom*

 7. What is the best way to truncate the word “innovation” in order to search for variant words: innovations, innovative, innovating?
( )	Innovation*
( )	Innovativ*
(X)	Innovat*
( )	None of the above.

8. What are possibilities to broaden the search?
(X)	Multilingual search terms
(X)	Truncation
( )	Filters
( )	AND operator
(X)	Synonyms
( )	Phrase search

9. Look at the following query. Which titles of documents could be delivered using that query? (Imagine that the search system doesn’t truncate automatically and that it only searches in the title field.)
(sharing economy OR collaborative consumption) AND sustainab*
(X) Collaborative consumption and sustainability: a discursive analysis of consumer representations and collaborative website narratives
( ) Sharing economy: for an economic taxonomy
(X) Collaborative consumption as a new trend of sustainable consumption
(X) The social sustainability of the sharing economy
( ) Sustainability of an economy relying on two reproducible assets

 10. Serendipity Search is a search technique that you can plan beforehand.
( )  True
(X) False

11. You are using the term “renewable energy”. Choose the right relation to the following terms. 
Options: Broader term, narrower term, synonym, translation, related term.
- Renewable energy policy (related term)
- Renewable resources (broader term)
- Bioenergy (narrower term)
- Erneuerbare Energie (translation)
- Solar energy (narrower term)
- Wind energy (narrower term)
