**Test Módulo 3**

1. ¿Cuáles son las ventajas de los buscadores generales?

[ ]	transparentes

[X] rápidos

[X]	exhaustivos

[ ]	privacidad

2. ¿Qué aspecto de la siguiente lista no es una ventaja de los catálogos modernos de las bibliotecas?

( ) opciones de búsqueda específicas

( ) intervención humana

( ) se incluyen datos científicos

(x) numerosos textos completos disponibles 

( ) calidad controlada

3. ¿Cuáles son las ventajas del buscador Google Scholar?

[X]	control de calidad

[ ]	transparente

[X]	se pueden obtener muchos resultados

[ ]	no es necesario tener buenas habilidades de búsqueda

4. ¿Cuáles son las ventajas de los medios sociales?

[ ]	control de calidad

[ ]	estructura clara

[X]	muchas opciones de búsqueda

[ ]	no tienen anuncios

[X]	es posible navegar

5. ¿Qué es un OPAC? 

(x)	un catálogo moderno de biblioteca

( )	una herramienta de descubrimiento

( )	un buscador científico



6. Estás llevando a cabo una investigación sobre el impacto del cambio climático en la economía de Canadá. ¿Qué estrategia de búsqueda utilizas?

(x)	"cambio climatico" AND econom* AND (Canada OR canadiense)

( ) Canada AND ("cambio climatico" OR "economia")

( )	(Canada OR canadiense) AND clima AND econom*


7. ¿Cuál es la mejor manera de truncar la palabra "innovación" para buscar palabras variantes: innovaciones, innovador, innovar?

( )	innovacion*

( )	innovad*

(X)	innova*

( )	ninguna de ellas

8. ¿Cuáles son las posibilidades de ampliar la búsqueda?

[X]	términos de búsqueda multilingües

[X]	truncamiento

[ ]	filtros

[ ]	el operador AND

[X]	sinónimos

[ ]	búsqueda por frase exacta

9. Mira la siguiente consulta. ¿Qué títulos de documentos podrían encontrarse utilizando esta consulta? (Imagina que el sistema de búsqueda solo busca en el campo de título).

(economia compartida OR consumo colaborativo) AND sostenib*

====


[X] Consumo colaborativo y sostenibilidad: un análisis discursivo de las representaciones de los consumidores y las narrativas de sitios web colaborativos

[ ] Economía compartida: para una taxonomía económica

[X] El consumo colaborativo como nueva tendencia del consumo sostenible

[X] La sostenibilidad social de la economía compartida

[ ] Sostenibilidad de una economía basada en dos activos reproducibles

10. La búsqueda por serendipia es una técnica de búsqueda que se puede planificar de antemano.

( )	Verdadero

(X) Falso

11. Estás utilizando el término "energía renovable". Elige la relación correcta con los siguientes términos. Opciones: Término más amplio, término más específico, sinónimo, traducción, término relacionado

- Política de energía renovable [[término más amplio
narrower term
sinónimo
traducción
(término relacionado)]]

- Recursos renovables [[(término más amplio)
narrower term
sinónimo
traducción
término relacionado]]

- Bioenergía [[término más amplio
(término más específico)
sinónimo
traducción
término relacionado]]

- Renewable energy [[término más amplio
narrower term
sinónimo
(traducción)
término relacionado]]

- Energía solar [[término más amplio
(término más específico)
sinónimo
traducción
término relacionado]]

- Energía eólica [[término más amplio
(término más específico)
sinónimo
traducción
término relacionado]]