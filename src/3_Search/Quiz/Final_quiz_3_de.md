**Quiz für Modul 6**
1. Was sind Vorteile von allgemeinen Suchmaschinen?
[] Transparent 
[x] Schnell 
[x] Umfassend 
[] Datenschutz

2. Welcher Aspekt der folgenden Liste ist kein Vorteil der modernen Bibliothekskataloge?
() Spezifische Suchoptionen
() Menschliche Beteiligung
() Wissenschaftliche Daten enthalten
(x) Viele Volltexte verfügbar 
() Qualitätsgeprüft

3. Welche Vorteile hat die Suchmaschine Google Scholar?
[x] Kontrollierte Qualität 
[] Transparent
[x] Viele Ergebnisse können erzielt werden
[] Keine guten Suchfähigkeiten erforderlich

4. Was sind Vorteile von Social Media?
[] Kontrollierte Qualität
[] Klare Struktur
[x] Viele Suchmöglichkeiten
[] Keine Werbung 
[x] Browsing möglich

5. Was ist OPAC? 
(x) Einen modernen Bibliothekskatalog
() Ein Discoverysystem 
() eine wissenschaftliche Suchmaschine


6. Du führst eine Studie über die Auswirkungen des Klimawandels auf die Wirtschaft in Kanada durch. Welche Suchstrategie verwendest du?  
(x) "Klimawandel" AND Ökonomie* AND (Kanada OR Kanadier)
() Kanada AND ("Klimawandel" OR "Wirtschaft")
() (Kanada OR Kanadier) AND Klima AND Wirtschaft*.

 7. Wie kann man das Wort "Innovation" am besten trennen, um nach unterschiedlichen Wörtern zu suchen: innovations, innovative, innovating ?
( )	Innovation*
( )	Innovativ*
(X)	Innovat*
() Keine der oben genannten Möglichkeiten.

8. Welche Möglichkeiten gibt es, die Suche zu erweitern?
[x]Mehrsprachige Suchbegriffe
[x] Trunkierung 
[] Filter
[] AND-Operator
[x] Synonyme
[] Phrasensuche

9. Schaue dir folgende Abfragen an. Welche Titel von Dokumenten könnten mit dieser Abfrage geliefert werden? (Stell dir vor, dass das Suchsystem nicht automatisch abtrennt und nur im Titelfeld sucht.)
(sharing economy OR collaborative consumption) AND sustainab*
[x] Collaborative consumption and sustainability: a discursive analysis of consumer representations and collaborative website narratives 
[] Sharing economy: for an economic taxonomy
[x] Collaborative consumption as a new trend of sustainable consumption 
[x] The social sustainability of the sharing economy
[] Sustainability of an economy relying on two reproducible assets


 10. Serendipity Search ist eine Suchtechnik, die du im Voraus planen kannst.
() Wahr
(X) Falsch

11. Du verwendest den Begriff "erneuerbare Energien". Wähle das richtige Verhältnis zu den folgenden Begriffen. 
Optionen: Allgemeinere Begriffe, spezifischere Begriffe, Synonyme, Übersetzungen, verwandte Begriffe.
- Politik für erneuerbare Energien (verwandter Begriff)
- Erneuerbare Rohstoffe (allgemeinerer Begriff)
- Bioenergie (spezifischerer Begriff)
- Erneuerbare Energie (Übersetzung)
- Solarenergie (spezifischerer Begriff)
- Windenergie (spezifischerer Begriff)