**Test Mòdul 3**

Marca **totes** les respostes correctes

1. Quins són els avantatges dels buscadors generals?

[ ] transparents

[x] ràpids

[x] exhaustius

[ ] privacitat

2. Quin aspecte de la següent llista no és un avantatge dels catàlegs moderns de les biblioteques?

( ) opcions de cerca específiques

( ) intervenció humana

( ) s'inclouen dades científiques

(x) nombrosos textos complets disponibles

( ) qualitat controlada

3. Quins són els avantatges del buscador Google Scholar?

[x] control de qualitat

[ ] transparent

[x] es poden obtenir molts resultats

[ ] no és necessari tenir bones habilitats de cerca

4. Quins són els avantatges dels mitjans socials?

[ ] control de qualitat

[ ] estructura clara

[x] moltes opcions de cerca

[ ] no tenen anuncis

[x] és possible navegar

5. Què és un OPAC?

(x) un catàleg modern de biblioteca

( ) una eina de descobriment

( ) un buscador científic


6. Estàs duent a terme una investigació sobre l'impacte del canvi climàtic en l'economia de Canadà. Quina estratègia de cerca fas servir?

(x) "canvi climàtic" AND econom* AND (Canadà OR canadenc)

( ) Canadà AND ("canvi climàtic" OR "economia")

( ) (Canadà OR canadenc) AND clima AND econom*

7. Quina és la millor manera de truncar la paraula "innovació" per buscar paraules variants: innovacions, innovador, innovar? 

( ) innovacio*

( ) innovad*

(x) innova*

( ) cap d'elles


 8. Quines són les possibilitats d'ampliar la cerca? 

[x] termes de cerca multilingües

[x] truncament

[ ] filtres

[ ] l'operador AND

[x] sinònims

[ ] cerca per frase exacta

9. Mira la següent consulta. Quins títols de documents es podrien trobar utilitzant aquesta consulta? (Imagina que el sistema de cerca només busca en el camp de títol).

(economia compartida OR consum col·laboratiu) AND sostenib*
====
<<

[x] Consum col·laboratiu i sostenibilitat: una anàlisi discursiva de les representacions dels consumidors i les narratives de llocs web col·laboratius

[ ] Economia compartida: per a una taxonomia econòmica

[x] El consum col·laboratiu com a nova tendència del consum sostenible

[x] La sostenibilitat social de l'economia compartida

[ ] Sostenibilitat d'una economia basada en dos actius reproduïbles


10. La cerca per serendípia és una tècnica de recerca que es pot planificar amb antel·lació.

( ) Verdader

(X) Fals

11. Estàs utilitzant el terme "energia renovable". Tria la relació correcta amb els següents termes. Opcions: Terme més ample, terme més específic, sinònim, traducció, terme relacionat

- Política d'energia renovable 
[[terme més ampli
terme més específic
sinònim
traducció
(terme relacionat)]]

- Recursos renovables 
- [[(terme més ampli)
terme més específic
sinònim
traducció
terme relacionat]]

- Bioenergía [[terme més ampli
(terme més específic)
sinònim
traducció
terme relacionat]]
- Renewable energy [[terme més ampli
terme més específic
sinònim
(traducció)
terme relacionat]]

- Energia solar [[terme més ampli
(terme més específic)
sinònim
traducció
terme relacionat]]

- Energia eòlica [[terme més ampli
(terme més específic)
sinònim
traducció
terme relacionat]]