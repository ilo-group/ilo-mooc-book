
**Kviz** **modula 3**

**1\.** **Kakšne so prednosti splošnih** **spletnih iskalnikov?**

( ) transparentnost

(x) hitrost

(x) obširnost

( ) zasebnost

  
  

**2\. Kateri izmed naslednjih dejavnikov ne spada k prednostim spletnih knjižničnih katalogov?**

( ) posebne možnosti za iskanje informacij

( ) človekova dejavnost

( ) dostop do znanstvenih podatkov

(x) dostop do celotnega besedila

( ) nadzor kakovosti

  
  

**3\. Katere so prednosti iskalnika Google Učenjak?**

(x) nadzor kakovosti

( ) transparentnost

(x) veliko zadetkov

( ) niso potrebne posebne spretnosti iskanja

  
  

**4\. Katere so prednosti spletnih družbenih omrežij?**

( ) nadzor kakovosti

( ) jasna struktura

(x) veliko možnosti za iskanje informacij

( ) ni reklam

(x) brskanje

  
  

**5\. Kaj je OPAC?**

(x) Spletni knjižnični katalog

( ) Orodje za odkrivanje informacij

( ) Iskalnik za znanstvene informacije

  

**6\. Želite raziskovati, na kakšen način vplivajo spremembe podnebja na gospodarstvo v Kanadi. Kaj bi vtipkali v iskalnik?**

( ) “klimatske spremembe” AND gospodars\* AND (Kanada OR Kanadski)

(X) Kanada AND (“klimatske spremembe” OR “gospodarstvo”)

( ) (Kanada OR kanadski) AND podnebje AND gospodars\*

  
  

**7\. Kako skrajšati besedo “inovacija” tako, da boste našli z iskalnikom tudi podobne besede, kot sta “inovacije, inovacijski”?**

( ) inovacija\*

( ) inovac\*

(X) inovacij\*

( ) noben odgovor ni pravilen.

  
  

**8\. Kako lahko raz****širimo možnosti iskanje****?**

(X) izraz v drugih jezikih

(X) krajšanje

( ) filtri

( ) operator IN

(X) sopomenke

( ) frazno iskanje

  
  

**9\. Oglejte si iskalni niz. Katere naslove datotek bi lahko s tem iskalnim nizom našli? (****Predstavljajte si, da išče iskalni sistem le izraze v naslovih.)**

**(Ekonomija delitve OR sodelovalna potrošnja) AND trajnost\***

(X) Sodelovalna potrošnja in trajnostnost: diskurzivna analiza potrošnikov in spletnih straneh

( ) Ekonomija delitve: za ekonomsko taksonomijo

(X) Sodelovalna potrošnja – novi trend trajnostne potrošnje

(X) Družbena trajnostnost ekonomije delitve

( ) Ekonomska trajnost, ki temelji na obnovljivih virih

  
  

**10\. Naključno iskanje je strategija iskanja, ki je ne moremo načrtovati.**

( ) Da

(X) Ne

  
  

**11.** **Želite uporabiti iskalni niz “obnovljivi viri energije”. Povežite pravilne tehnike iskanja z danimi izrazi. Možnosti: širši pojem, ožji pojem, sopomenka, prevod, pomensko soroden izraz.**

*   Načela energetske politike (pomensko soroden izraz)
    
*   Obnovljivi viri  (širši pojem)
    
*   Biomasa (ožji pojem)
    
*   Renewable energy (prevod)
    
*   Sončna energija (ožji pojem)
    
*   Vetrna energija (ožji pojem)