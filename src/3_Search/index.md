---
title: The power of search
permalink: /3_Search/
eleventyNavigation:
    order: 3
---

## MOOC Module: The power of search

In this module you will learn about finding and gathering of information as well as the effective use of relevant information sources and tools, in order to find specific targets. You will also improve your task and time management skills, and develop a proactive approach to searching and gathering in order to prevent information overload. 

