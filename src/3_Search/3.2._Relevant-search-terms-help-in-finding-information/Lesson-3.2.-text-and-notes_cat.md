**3.2 Utilitzar termes de cerca rellevants ajuda a trobar la informació**

*Introducció**

En aquesta lliçó aprendràs a identificar els termes de cerca que t'ajudaran a trobar la informació que realment necessites.
  

**Resultats d'aprenentatge:**

Al final d'aquesta lliçó, seràs capaç de:

* definir termes i conceptes de recerca adequats
* identificar sinònims, termes de cerca relacionats, més genèrics i més específics

  
  

**Contingut:**

  
Bàsicament, el procés de cerca consisteix en trobar les paraules clau adequades que condueixen a la font que satisfà la necessitat d'informació específica.

És de gran ajut anotar immediatament les paraules clau que podrien ser apropiades per a la cerca quan estem llegint sobre un tema. Els mapes mentals poden ajudar a ordenar els teus pensaments mentre et actualitzes sobre el tema i t'ajuden a veure on pot haver-hi un buit o si necessites centrar-te en un altre aspecte.

Un altre consell és crear una llista de paraules. Pots utilitzar les paraules clau que has anotat abans com a paraules clau "principals" i començar a buscar traduccions (si cal), sinònims, així com termes més generals i més específics. Un tesaure [http://zbw.eu/stwversion/latest/about.en.html](http://zbw.eu/stwversion/latest/about.en.html) és perfecte per a aquest propòsit.

Un cop tinguis clara la pregunta de recerca, pots començar una pluja d'idees sobre el tema per identificar els termes de cerca apropiats. Pot ser útil fer servir termes de fonts que ja has llegit per el teu tema, o sinònims de termes que has trobat.
  
  

**Optimització del terme de cerca**

  

Després d'haver decidit un terme de cerca i executar la recerca en el sistema, pots avaluar la llista de resultats. Depenent dels resultats, pots modificar els termes de cerca. Aprèn més sobre estratègies i conceptes de cerca fent clic a les següents paraules clau.

1. _Stemming_ o lematització
2. Truncaments
3. Comodins (_wildcards_)
4. Operadors booleans
5. Parèntesis
6. Cerca per frase exacta
    

  
  

**3.2.1 Stemming o lematització**
    

  

“_«El terme "stemming" es refereix al procés de reduir les paraules als seus lemes o arrels. L'arrel és la porció d'una paraula que queda després d'eliminar els seus prefixos i sufixos» (Liu, 2011: 228)._

  

Exemple: competències => competència
  

**3.2.2 Truncaments**

  

Un truncament vol dir que pots utilitzar un símbol que substitueixi qualsevol sèrie de caràcters. En la majoria de les bases de dades, el símbol de truncament és *, però pot variar segons la base de dades, així que has de consultar el text d'ajuda. És una tècnica que amplia la teva cerca en incloure diversos finals o començaments de paraules.

Un truncament (*) permet cercar terminacions desconegudes de paraules.

  

**Exemples**

        cavall* => cavalls,…
        
        malalt* => malalt, malalta, malaltia...
        
        *comunicacions => comunicacions, telecomunicacions...

  
  

**3.2.3 Comodins o 'wildcards'**

Els comodins o _wildcards_ funcionen de forma similar als truncaments, però només substitueixen un caràcter. Són molt útils si una paraula es pot escriure de diferents maneres. El símbol ? s'utilitza sovint per indicar un comodí, però de nou, pot variar d'una base de dades a una altra.

  

**Exemple:**

        G?rona => Girona, Gerona
  

**3.2.4 Operadors booleans**

Els operadors booleans es poden utilitzar en gairebé totes les bases de dades. Ofereixen la possibilitat de combinar diversos termes de cerca.

  

**AND:** Cada terme ha d'aparèixer en algun lloc del document. Exemple: calefacció **AND** solar -> à retorna tots els documents que contenen tots dos termes.

  

**OR:**  Un dels termes ha d'aparèixer en algun lloc del document. Exemple: facebook **OR** marketing -> à tots els documents que continguin almenys un dels termes.

  

**NOT:** Exclou termes. Exemple: marketing **NOT** facebook -> à tots els documents que contenen el terme "marketing" però no el terme "facebook".
  

**Consells:**

*  La majoria de les bases de dades utilitzen automàticament l'operador AND entre tots els termes que introdueixis en el camp de cerca. Desafortunadament, no existeix un estàndard per a això. Per tant, pots verificar-ho introduint les condicions sense l'operador AND un cop i, a continuació, introduint-les amb l'operador AND. Si els resultats són els mateixos, la base de dades utilitza automàticament l'operador AND. 
*  Consulta el text d'ajuda de l'eina de cerca per comprovar si l'eina utilitza "AND", "OR" i "NOT". A vegades les eines utilitzen símbols diferents (per exemple, els símbols + o -).


  

**3.2.5 Parèntesis**

  

Hi ha dues opcions per interpretar la cadena de cerca "marketing AND Facebook OR twitter". La primera opció és: "Mostra'm tots els documents sobre marketing a Facebook o al twitter", mentre que la segona opció és completament diferent: "Mostra'm tots els documents sobre marketing a Facebook o documents sobre twitter en general". Els operadors booleans tenen la seva pròpia jerarquia: NOT> AND> OR. Per tant, el nostre exemple de cadena de cerca seria interpretat com la segona opció pel nostre sistema de cerca, però aquesta interpretació no té sentit. Per això, has de fer servir parèntesis per aclarir la teva intenció. La cadena de cerca ha de canviar-se a: marketing AND (Facebook OR twitter).

A continuació, les dues opcions es visualitzen en un diagrama:

_Include Image 3.2-1 and Image 3.2-2_

**3.2.6 Cerca per frase exacta**

De vegades els termes de cerca contenen diverses paraules i si desitges buscar-les en aquesta combinació exacta, has d'utilitzar la cerca per frase. Per fer-ho, posa el grup de paraules entre cometes dobles.


**Exemple:** "social media marketing“


Posar els termes o consultes entre cometes tornarà només els resultats que continguin aquesta combinació exacta. Per exemple:

_Include Image 3.2-3_


**3.2.7 Test d'avaluació**

1.	Connectes dos termes amb AND i després fas el mateix amb OR. Quina connexió troba més resultats? I per què? 
 Marca **totes** les respostes correctes 

[x] OR troba més resultats perquè mostra tots els registres que contenen un dels termes de cerca

[ ] OR troba menys resultats perquè defineix més vagament el terme de cerca

[ ] AND troba més resultats perquè mostra tots els registres que contenen tots dos termes de cerca

[x] AND troba menys resultats perquè mostra els registres que contenen els dos termes de cerca

2.	Suposa que busques documents que continguin totes les formes gramaticals de "comercial" però la base de dades no ofereix truncaments. Quin operador booleà utilitzaries en el seu lloc? 

= OR

[explanation]
Exemple: comercial OR comercialment OR comercialitzat
[explanation]

3. 	Marca **totes** les respostes correctes.

Ara, se't donen els títols d'una sèrie de documents i has de dir quina de les consultes és l'adequada per mostrar-los. Les paraules en negreta destaquen els termes de cerca. (Suposarem que el sistema de cerca només indexa el camp del títol).

a. Managing **globalization**: new business models, **strategies** and innovation

b. Evolution of **strategic** interactions from the triple to quad helix innovation models for sustainable development in the era of **globalization**

c. **Globalization** contained: the economic and **strategic** consequences of the container

d. The role of innovation and **globalization** strategies in post-crisis recovery

e. Competition as a Response **Strategy** to **Globalization** by Manufacturing Firms in Kenya


 
[ ]	Globalization AND strategy

[x]  Globalization AND strateg*

[ ] Globali?ation AND strateg?

[x]  Globali?ation AND strateg*

a. **Crowdfunding:** its potential for stimulating financial **development** in the ECCU

b. What determines the **growth** expectations of early-stage entrepreneurs? : evidence from crowdfunding

c. Web 2.0 as Platform for the Development of **Crowdfunding**

d. Crowdfunding economic **development**

e.  Modes, flows and networks : the promise of crowdfunding in documentary filmmaking and audience **development**


[x] crowdfunding AND (development OR growth)
[ ] development OR crowdfunding OR growth
[ ] crowdfunding AND development
[x]  crowdfund* AND (growth OR development)



