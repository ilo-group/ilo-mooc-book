**3.2 Usar términos de búsqueda relevantes ayuda a encontrar la información**

**Introducción**

En esta lección aprenderás a identificar los términos de búsqueda que te ayudarán a encontrar la información que realmente necesitas. 

**Resultados de aprendizaje:*

Al final de esta lección, serás capaz de:

* definir términos y conceptos de búsqueda adecuados
* identificar sinónimos, términos de búsqueda relacionados, más genéricos y más específicos 
  

**Contenido:**

Básicamente, el proceso de búsqueda consiste en encontrar las palabras clave adecuadas que conducen a la fuente que satisface la necesidad de información específica.

Es de gran ayuda anotar inmediatamente las palabras clave que podrían ser apropiadas para la búsqueda cuando estamos leyendo sobre un tema. Los mapas mentales pueden ayudar a ordenar tus pensamientos mientras te actualizas sobre el tema y te ayudan a ver dónde puede haber un hueco o si necesitas centrarte en otro aspecto.

Otro consejo es crear una lista de palabras. Puedes utilizar las palabras clave que has anotado antes como palabras clave "principales" y empezar a buscar traducciones (si es necesario), sinónimos, así como términos más generales y más específicos. Un tesauro [http://zbw.eu/stwversion/latest/about.en.html](http://zbw.eu/stwversion/latest/about.en.html) es perfecto para este propósito.

Una vez que tengas clara la pregunta de investigación, puedes comenzar una lluvia de ideas sobre el tema para identificar los términos de búsqueda apropiados. Puede ser útil usar términos de fuentes que ya has leído para tu tema, o sinónimos de términos que has encontrado.


**Optimización del término de búsqueda**

Después de haber decidido un término de búsqueda y ejecutar la búsqueda en el sistema, puedes evaluar la lista de resultados. Dependiendo de los resultados, puedes modificar los términos de búsqueda. Aprende más sobre estrategias y conceptos de búsqueda haciendo clic en las siguientes palabras clave.

1. _Stemming_ o lematización
2. Truncamientos
3. Comodines (_wildcards_)
4. Operadores booleanos
5. Paréntesis
6. Búsqueda por frase exacta
  

**3.2.1 Stemming o lematización**
    

  

_«El término "stemming" se refiere al proceso de reducir las palabras a sus lemas o  raíces . La raíz es la porción de una palabra que queda después de eliminar sus prefijos y sufijos» (Liu, 2011: 228)._

Ejemplo: competencias => competencia


**3.2.2 Truncamientos**


Un truncamiento significa que puedes utilizar un símbolo que sustituya a cualquier serie de caracteres. En la mayoría de las bases de datos, el símbolo de truncamiento es *, pero puede variar según la base de datos, así que debes consultar el texto de ayuda. Es una técnica que amplía tu búsqueda al incluir varios finales o comienzos de palabras.

Un truncamiento (*) permite buscar terminaciones desconocidas de palabras.  

**Ejemplos**

        casa* => casas,…
        
        enferm* => enfermo, enfermedad, enfermería...
        
        *comunicaciones => comunicaciones, telecomunicaciones...

  
  

**3.2.3 Comodines o 'wildcards'**

Los comodines o _wildcards_ funcionan de forma similar al truncamiento, pero sólo sustituyen a un carácter. Son muy útiles si una palabra se puede deletrear de diferentes maneras. El símbolo ? se utiliza a menudo para indicar un comodín, pero de nuevo, puede variar de una base de datos a otra. 

**Ejemplos:**

        Mé?ico => México, Méjico
        
        abogad? => abogado, abogada
  

**3.2.4 Operadores booleanos**

Los operadores booleanos se pueden utilizar en casi todas las bases de datos. Ofrecen la posibilidad de combinar varios términos de búsqueda.

**AND:** Cada término tiene que aparecer en algún lugar del documento. Ejemplo: calefacción **AND** solar -> devuelve todos los documentos que contienen ambos términos.

  

**OR:** Uno de los términos tiene que aparecer en algún lugar del documento. Ejemplo: facebook **OR** marketing ->  todos los documentos que contienen al menos uno de los términos.

**NOT:** Excluye términos. Ejemplo: marketing **NOT** facebook -> todos los documentos que contienen el término "marketing" pero no el término "facebook".

**Consejos:**

*  La mayoría de las bases de datos utilizan automáticamente el operador AND entre todos los términos que introduzcas en el campo de búsqueda. Desafortunadamente, no existe un estándar para esto. Por lo tanto, puedes verificarlo introduciendo las condiciones sin el operador AND una vez y, a continuación, introduciéndolas con el operador AND. Si los resultados son los mismos, la base de datos utiliza automáticamente el operador AND.
*  Consulta el texto de ayuda de la herramienta de búsqueda para comprobar si la herramienta utiliza "AND", "OR" y "NOT". A veces las herramientas utilizan símbolos diferentes (por ejemplo, los símbolos + o -).

  

**3.2.5 Paréntesis**

Hay dos opciones para interpretar la cadena de búsqueda "marketing AND Facebook OR twitter". La primera opción es: "Muéstrame todos los documentos sobre marketing en Facebook o en twitter" mientras que la segunda opción es completamente diferente: "Muéstrame todos los documentos sobre marketing en Facebook o documentos sobre twitter en general". Los operadores booleanos tienen su propia jerarquía: NOT > AND > OR. Por lo tanto, nuestro ejemplo de cadena de búsqueda sería interpretado como la segunda opción por nuestro sistema de búsqueda, pero esta interpretación no tiene sentido. Por eso, debes usar paréntesis para aclarar tu intención. La cadena de búsqueda debe cambiarse a: marketing AND (Facebook OR twitter).

A continuación, las dos opciones se visualizan en un diagrama:

_Include Image 3.2-1 and Image 3.2-2_

**3.2.6 Búsqueda por frase exactah**

A veces los términos de búsqueda contienen varias palabras y si deseas buscarlas en esa combinación exacta, debes utilizar la búsqueda por frase. Para ello, pon el grupo de palabras entre comillas dobles.


**Ejemplo:** "social media marketing“

Poner los términos o consultas entre comillas devolverá solamente los resultados que contengan esa combinación exacta. Por ejemplo:


_Include Image 3.2-3_

  

**3.2.7 Test de evaluación**

1.	Conectas dos términos con AND y después haces lo mismo con OR. ¿Qué conexión encuentra más resultados? ¿Y por qué? 
Marca **todas** las respuestas correctas

[x] OR encuentra más resultados porque muestra todos los registros que contienen uno de los términos de búsqueda

[ ] OR encuentra menos resultados porque define más vagamente el término de búsqueda

[ ] AND encuentra más resultados porque muestra todos los registros que contienen ambos términos de búsqueda

[x] AND encuentra menos resultados porque muestra los registros que contienen los dos términos de búsqueda

2.	Supón que buscas documentos que contengan todas las formas gramaticales de "comercial" pero la base de datos no ofrece truncamientos. ¿Qué operador booleano utilizarías en su lugar?

= OR

[explanation]
Ejemplo: comercial OR comercialmente OR comercializado
[explanation]



3. 	Ahora, se te dan los títulos de una serie de documentos y tienes que decir cuál de las consultas es la adecuada para mostrarlos. Las palabras en negrita destacan los términos de búsqueda. (Supón que el sistema de búsqueda solo indexa el campo del título).

a. Managing **globalization**: new business models, **strategie**s and innovation
b. Evolution of **strategic** interactions from the triple to quad helix innovation models for sustainable development in the era of **globalization**
c.** Globalization** contained: the economic and **strategic** consequences of the container
d. The role of innovation and **globalization** strategies in post-crisis recovery
e. Competition as a Response **Strategy** to **Globalization** by Manufacturing Firms in Kenya

 
( )	Globalization AND strategy

(X) Globalization AND strateg*

( ) Globali?ation AND strateg?

(x) Globali?ation AND strateg*

a. **Crowdfunding**: its potential for stimulating financial **development** in the ECCU
b. What determines the **growth**** expectations of early-stage entrepreneurs? : evidence from crowdfunding
c. Web 2.0 as Platform for the Development of **Crowdfunding**
d. **Crowdfunding** economic **development**
e.  Modes, flows and networks : the promise of **crowdfunding** in documentary filmmaking and audience **development**


(x) crowdfunding AND (development OR growth)

( ) development OR crowdfunding OR growth

( ) crowdfunding AND development

(x) crowdfund* AND (growth OR development)



