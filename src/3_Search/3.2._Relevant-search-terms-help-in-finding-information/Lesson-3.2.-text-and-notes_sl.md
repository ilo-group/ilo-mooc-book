              @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } a:link { color: #0563c1; text-decoration: underline } 

**Lekcija 3**

  

**3.2 Relevantni iskalni izrazi pomagajo pri iskanju informacij**

V tej lekciji se boste naučili, kako določiti iskalne izraze, ki vam bodo pomagali pri iskanju bistvenih informacij.

  

**Učni cilji:**

*   Določanje primernih iskalnih izrazov in konceptov
    
*   Določanje sopomenk ter izrazov s širšim in ožjim pomenom
    

  
  

**Vsebina:**

  

Pri postopku iskanja informacij so najpomembnejše primerne ključne besede, s katerimi najdemo informacijske vire, ki zadovoljijo našo informacijsko potrebo.

  

Pomembno je, da si ob branju nove teme zabeležite ključne besede, ki vam bi lahko pomagale pri iskanju informacij. Poleg tega je priporočljiva izdelava miselnega vzorca, ki vam pomaga pri beleženju in urejanju osebnih misli. Prav tako vidite s pomočjo miselnega vzorca, na katerem področju vam manjkajo informacije oz. ali bi bila potrebna zamenjava raziskovalnega vprašanja ali raziskovalne teme.

  

Dodatno vam svetujemo, da napišete seznam vseh ključnih besed. Uporabljate lahko ključne besede, ki ste si jih zabeležili že prej, kot glavne izraze. Potem iščete prevod (če je potrebno), sinonime in izraze s širšim in ožjim pomenom. Spletna stran thesaurus ([http://zbw.eu/stw/version/latest/about.en.html](http://zbw.eu/stw/version/latest/about.en.html)) vam je lahko v pomoč.

  

Po določitvi raziskovalnega področja in raziskovalnega vprašanja vam lahko pomaga metoda viharjenja možganov pri določanju ustreznih iskalnih izrazov. Zapišite sopomenke ali ključne besede virov, ki ste jih prebrali.

  

**Postopek iskanja iskalnih izrazov**

  

Po določitvi iskalnega izraza ter njegovemu preizkušanju v sistemu je treba vrednotiti seznam zadetkov. Spreminjanje/prilagajanje iskalnih izrazov je odvisno od seznama zadetkov. S klikom na nasldenje ključne besede lahko izveste več o navedenih strategijah in konceptih.

  

1.  Krnjenje
    
2.  Krajšanje
    
3.  Nadomestni znaki
    
4.  Boolovi operatorji
    
5.  Oklepaji
    
6.  Frazno iskanje
    

  
  

**3.2.1** **Krnjenje**
    

  

“Krnjenje je postopek krajšanja besed na njihov koren. Koren je del besede, ki ostane, ko odstranimo predpono ali pripono.” (Liu, 2011: 228).

  

Primer: sposobnosti -> sposobnost

  
  

**3.2.2** **Krajšanje**

  

Krajšanje je postopek, pri katerem nadomešča določen znak poljubno število znakov. Večina baz podatkov uporablja za ta postopek znak **\***. Znak pa je odvisen od podatkovne zbirke, tako da je treba preveriti navodila za iskanje. Ta metoda vam omogoča razširitev iskanja z iskanjem vseh podobno sestavljenih besed.

  

Znak (\*) nadomešča poljubno število znakov.

  

**Primer**

Na primer: hiš\* -> hiše,…

gospodar\*-> gospodarski, gospodaren, gospodarstvo,…

\*enakost -> enakost, neenakost

  
  

**3.2.3**  **Nadomestni znak**

Nadomestni znaki delujejo podobno kot metoda krajšanja, vendar nadomeščajo samo en znak. Pomagajo nam lahko, če besede lahko napišemo na različne načine.

Večina podatkovnih zbirk uporablja za ta postopek znak **?**. Kot že omenjeno, je znak odvisen od posamezne zbirke uporabljajo za to različne znake.

  

**Primer:**

bar?a -> barva, barka

  

**3.2.4\. Boolovi operatorji (IN ALI NE)**

Boolove operatorje, tako imenovane logične operatorje, lahko uporabljamo v skoraj vsaki podatkovni zbirki. Zagotavljajo možnost kombiniranja več iskalnih izrazov.

  

**IN:** Vsak izraz se mora pojaviti v dokumentu. Primer: korist IN trženje -> vidimo dokumente, ki vsebujejo oba izraza

**ALI:** Vsaj eden izmed izrazov se mora pojaviti nekje v dokumentu. Primer: Facebook ALI trženje -> vidimo vse dokumente, ki vsebujejo vsaj enega izmed izrazov

  

**NE:** Izključitev iskalnih izrazov. Primer: trženje NE Facebook -> vidimo vse dokumente, ki vsebujejo besedo “trženje”, izraz “Facebook” pa izključimo iz iskanja.

  

**Nasveti:**

*   Večina podatkovnih zbirk uporablja operator IN, če vpišete izraz v eno iskalno polje. Ker pa ta postopek ni standardiziran, ga morate preveriti sami. Najprej vtipkajte izraze brez operatorja IN, potem pa z operatorjem IN. Če so zadetki isti, uporablja podatkovna zbirka operator IN avtomatsko.
    
*   Poiščite navodila za iskanje (pomoč) in preverite, ali je možno iskanje z logičnimi operatorji “IN”, “ALI” in “NE”. Uporaba znakov je lahko odvisna od podatkovne zbirke (na primer znak za minus ali plus).
    

  

**3.2.5** **Oklepaji**

  

Iskalni niz “trženje IN Facebook ALI Twitter” lahko razložimo na dva načina.

Prvi način je naslednji: “Pokaži mi vse dokumente o trženju na Facebooku ali o trženju na Twitterju”. Iskalni niz pa lahko razumemo tudi na naslednji način: “Pokaži mi vse dokumente o trženju na Facebooku ali dokumente na splošno o Twitterju”. Boolovi operatorji imajo določen vrstni red: NE > IN > ALI. Računalniški sistem bi naš iskalni niz razumel na drugi način.

Ker pa želimo iskati iskalni niz na prvi način, moramo uporabljati oklepaje. Iskalni niz moramo spremeniti v: trženje IN (Facebook ali Twitter).

  

Tukaj vidite shematični prikaz obeh možnosti.

_Prikažite sliko 3.2-1 in sliko 3.2-2_

**3.2.6. Frazno iskanje**

Besedne zveze so sestavljene iz več besed. Če želite poiskati določeno kombinacijo besed (besedno zvezo oz. frazo), je treba uporabljati frazno iskanje. Za to je treba zamejiti besedne zveze z dvojnimi narekovaji.

**Primer:** "družbeno omrežje“

  

Z zamejitvijo besednih zvez z dvojnimi narekovaji bomo videli le zadetke, ki vsebujejo vse izraze v natanko takem zaporedju (torej frazo). Na primer:

**#**

_Prikažite sliko 3.2-3_

  

_V videoposnetku se boste naučili določanja iskalnih nizov:_

  

_Videoposnetek:_

  

_**Vir:**_ _ScreenCaptor, ProQuest (ali druge baze podatkov)_

-> _V ProQuest je možno iskanje z logičnimi operatorji._

  

_**Raziskovalno vprašanje:**_

_Kako se je (opredelitev pojma) informacijska pismenost spremenila zaradi digitalizacije?_

_**Videoposnetek predstavlja naslednje strategije:**_

*   _Krnjenje_
    
*   _Uporaba sopomenk_
    
*   _Iskanje besednih zvez_
    
*   _Evalvacija kakovosti_
    
*   _Boolovi operatorji in nadomestni znaki_
*   

**3.2.7 Vaja**

1.	Najprej povežete dva izraza z logičnim operatorjem AND in nato z operatorjem OR. Kdaj boste pridobili več zadetkov? Zakaj?

[x] Z logičnim operatorjem OR boste pridobili več zadetkov, saj boste našli vse dokumente, ki vsebujejo vsaj enega izmed izrazov.

[ ]Z logičnim operatorjem OR boste pridobili manj zadetkov

[ ]Z logičnim operatorjem AND najdemo le dokumente, ki vsebujejo oba izraza. 

[x]Z logičnim operatorjem AND noste pridobili manj zadetkov, ker najdemo le dokumente, ki vsebujejo oba izraza.

2.	Predstavljajte si, da želite najti datoteke, ki vsebujejo vse slovnične oblike izraza „komercialen”, a  podatkovna zbirka ne ponuja metode krajšanja. Katerega izmed Boolovih operatorjev bi izbrali namesto metode krajšanja?

OR. Primer: komercialen
OR komercialen 
OR komercialen 
OR komercializirati.

3.	Tukaj vidite naslove datotek – izbrati morate datoteke, ki se vam zdijo primerne. Besede, označene s krepko pisavo, predstavljajo iskalne izraze. (Predstavljajte si, da išče iskalni sistem le izraze v naslovih.) 

a. Managing **globalization**: new business models, **strategies** and innovation 

b. Evolution of **strategic** interactions from the triple to quad helix innovation models for sustainable development in the era of **globalization** 

c. **Globalization** contained: the economic and **strategic** consequences of the container 

d. The role of innovation and **globalization** strategies in post-crisis recovery

e. Competition as a Response **Strategy** to **Globalization** by Manufacturing Firms in Kenya

[ ] Globalization AND strategy

[X] Globalization AND strateg*

[ ] Globali?ation AND strateg?

[x] Globali?ation AND strateg*



a.	**Crowdfunding**: its potential for stimulating financial **development** in the ECCU 

b.	What determines the **growth** expectations of early-stage entrepreneurs? : evidence from **crowdfunding**

c.	Web 2.0 as Platform for the Development of **Crowdfunding**

d.	**Crowdfunding** economic **development**

e.	Modes, flows and networks : the promise of **crowdfunding** in documentary filmmaking and audience **development**

[X] crowdfunding AND (development OR growth)

[ ] development OR crowdfunding OR growth

[ ] crowdfunding AND development

[X] crowdfund* AND (growth OR development)



