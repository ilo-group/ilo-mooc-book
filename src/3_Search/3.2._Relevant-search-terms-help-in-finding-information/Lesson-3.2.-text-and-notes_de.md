**Lektion 2**

  

_**3.2 Relevante Suchbegriffe helfen bei der Informationssuche**_

In dieser Lektion lernt ihr, wie ihr Suchbegriffe identifizieren könnt, die euch helfen, Informationen zu finden, die ihr wirklich benötigt.

**Lernergebnisse:**

* Definieren geeigneter Suchbegriffe und Konzepte
    
* Identifizieren von Synonymen, verwandten, allgemeineren und spezifischeren Suchbegriffen
    

  
  

**Inhalt:**

  

Im Grunde geht es bei der Suche darum, geeignete Keywords zu finden, die zu der Quelle führen, die das Informationsbedürfnis befriedigt.

  

Es ist sehr hilfreich, Schlagwörter, welche für die Suche geeignet sein könnten, sofort beim Nachlesen eines Themas zu notieren. Mind Maps können helfen, deine Gedanken zu sortieren, während du dich über das Thema informierst, und sie helfen dir ebenfalls dabei Lücken zu erkennen oder herauszufinden, ob du den Fokus auf einen anderen Aspekt legen musst.

  

Ein weiterer Ratschlag ist, eine Wortliste zu erstellen. Ihr könnt die zuvor notierten Keywords als "Hauptschlagwörter" verwenden und nach Übersetzungen (falls erforderlich), Synonymen sowie allgemeineren und spezifischeren Begriffen suchen. Ein Thesaurus (http://zbw.eu/stwversion/latest/about.en.html) ist dafür perfekt geeignet.
  

Sobald ihr euch über eine Forschungsfrage im Klaren seid, könnt ihr mit dem Brainstorming zu eurem Thema beginnen, um geeignete Suchbegriffe zu identifizieren. Die Verwendung von Begriffen aus Quellen, die ihr bereits zum Thema gelesen habt, oder Synonyme von Begriffen, die ihr gefunden habt, können hilfreich sein.

**Optimieren des Suchbegriffs**



Nachdem du dich für einen Suchbegriff entschieden und diesen in das System eingegeben hast, kannst du die Ergebnisliste auswerten. Abhängig von deinen Ergebnissen kannst du die Suchbegriffe verändern. Erfahre mehr über Suchstrategien und -konzepte, indem du dich durch die folgenden Schlüsselwörter klickst.

  

1.  Stemming
    
2.  Trunkierung/Truncation
    
3.  Wildcards
    
4.  Boolesche Operatoren
    
5.  Brackets (Klammern)
    
6.  Phrasensuche
    

  
  


**3.2.1.1.Stemming**
    

  

_"Stemming bezieht sich auf den Prozess der Reduzierung von Wörtern auf ihre Stiele oder Wurzeln. Ein Stamm ist der Teil eines Wortes, der nach dem Entfernen seiner Präfixe und Suffixe übrig bleibt" (Liu, 2011: 228; aus dem englischen Original übersetzt)._
_“Stemming refers the process of reducing words to their stems or roots. A stem is the portion of a word that is left after removing its prefixes and suffixes” (Liu, 2011: 228)._
  

Beispiel: Kompetenzen - Kompetenz
  

**3.2.2. Trunkierung/Truncation**

Trunkierung bedeutet, dass ihr ein Symbol verwenden könnt, das dann eine beliebige Anzahl von Zeichen ersetzt. In den meisten Datenbanken ist das Symbol * für die Kürzung, aber es kann je nach Datenbank variieren, also überprüft dies bitte im Hilfetext. Es ist eine Technik, die eure Suche erweitert, indem sie verschiedene Wortendungen oder -anfänge miteinschließt.

Eine Trunkierung (*) ermöglicht die Suche nach unbekannten Wortendungen.   

 **Beispiele**

z.B.: Haus* => Häuser,.....
economic* => economic, economical, economics
*Gleichheit => Gleichheit, Ungleichheit 
  

_**3.2.3** **Wildcards**_

Wildcards funktionieren ähnlich wie Trunkierungen, ersetzen aber nur ein Zeichen. Sie sind sehr nützlich, wenn ein Wort auf verschiedene Weise geschrieben werden kann. Das Symbol **?** wird oft dafür verwendet, aber auch hier kann es von Datenbank zu Datenbank variieren.

  

**Beispiel:**

colo?r => color, colour
  

**3.2.4 Boolesche Operatoren (AND OR NOT)**

Boolesche Operatoren können in fast jeder Datenbank verwendet werden. Sie bieten die Möglichkeit, mehrere Suchbegriffe zu kombinieren.

  

**AND:** Jeder Begriff muss irgendwo im Dokument vorkommen. Beispiel: Nutzen UND Marketing.  Alle Dokumente, die beide Begriffe enthalten, werden angegeben.

  

**OR:** Einer der Begriffe muss irgendwo im Dokument vorkommen. Beispiel: Facebook OR Marketing. Alle Dokumente, die einen der Begriffe enthalten, werden angegeben.

  

**NOT:** Schließt Bedingungen aus. Beispiel: Marketing NICHT Facebook. Alle Dokumente, die den Begriff "Marketing", aber nicht den Begriff "Facebook" enthalten, werden angegeben.

  

**Tipps:**

* Die meisten Datenbanken verwenden den Operator AND zwischen Begriffen, wenn ihr diese in ein Suchfeld eingebt. Leider gibt es dafür keine Norm. Ihr könntet dies also überprüfen, indem ihr die Begriffe jeweils ohne den Operator AND eingebt und danach mit dem Operator AND. Wenn die Ergebnisse gleich sind, verwendet die Datenbank automatisch den Operator AND!
* Lest den Hilfetext des Suchwerkzeugs, um zu überprüfen, ob das Werkzeug "AND", "OR" und "NOT" verwendet. Manchmal verwenden Werkzeuge unterschiedliche Symbole (z.B. das Plus- oder Minuszeichen).

  

_**3.2.5** **Brackets** (Klammern)_

  

Es gibt zwei Möglichkeiten, den Suchbegriff "Marketing AND Facebook OR Twitter" zu interpretieren. Die erste Option ist: "Zeige mir alle Dokumente über Facebook-Marketing oder Twitter-Marketing", während die zweite Option völlig anders ist: "Zeige mir alle Dokumente über Facebook-Marketing oder Dokumente über Twitter im Allgemeinen". Boolesche Operatoren haben ihre eigene Hierarchie: NOT > AND > OR. Unser Beispielsuchbegriff würde also von unserem Suchsystem als zweite Option interpretiert werden, aber diese Interpretation macht keinen Sinn. Aus diesem Grund solltet ihr Klammern verwenden, um eure Absicht zu verdeutlichen. Der Suchbegriff sollte geändert werden in: Marketing AND (Facebook oder Twitter).
  

Im Folgenden werden die beiden Möglichkeiten in einem Baumdiagramm visualisiert:

_Bild 3.2-1 und Bild 3.2-2_ einbinden_

**3.2.6 Phrasensuche**

Manchmal enthalten Begriffe mehrere Wörter. Wenn ihr danach in einer bestimmten Kombination suchen wollt, verwendet die Phrasensuche. Dazu müsst ihr die Wortgruppe in doppelte Anführungszeichen setzen.

**Beispiel: ** "Social Media Marketing".

  

  

Wenn ihr Begriffe oder Abfragen in Anführungszeichen setzt, erhaltet ihr nur Ergebnisse, die alle Wörter enthalten. z.B...:
**#**

Bild 3.2-3_ einbinden

_Include Image 3.2-3_

  

  

_**Der folgende Screencast zeigt ein Beispiel zur Definition von Suchbegriffen**_.

  

_SCREENCAST:_ 


_**Ressource:** ScreenCaptor, ProQuest (oder eine andere wissenschaftliche Datenbank)_.

_ProQuest bietet eine Schnittstelle zum Betreiben einer Booleschen Suche_.

  

_**Forschungsfrage:**_

_Wie hat sich die Informationskompetenz und ihre Definitionen durch die Digitalisierung verändert_?

  


_**Folgende Strategien werden im Screencast angewendet:**_

* _Stemming_ 
    
* _Synonyme_ _verwenden_
    
* _Manipulieren Sie Suchanfragen/ Begriffe_.
    
* _Bewertung der Qualität/Stärke der Suchanfrage/ -begriffe/ -ergebnisse_
    
* _Boolesche Operatoren und Wildcards_


**3.2.7 Test**

1.	Du verbindest zwei Begriffe mit AND und danach mit OR. Welche Verbindung findet mehr Ergebnisse? Und warum? 

	_OR findet mehr Ergebnisse, weil es alle Datensätze anzeigt, die einen der Suchbegriffe enthalten. AND zeigt nur die Datensätze an, die beide Suchbegriffe enthalten._

2.	Angenommen, du suchst nach Dokumenten, die alle grammatikalischen Formen von "commercial" enthalten, aber die Datenbank bietet keine Trunkierung. Welchen booleschen Operator würdest du stattdessen verwenden? 

 OR. Beispiel: kommerziell OR kommerziell OR kommerziell OR kommerzialisiert.

3. Diesmal erhältst du die Titel der Dokumente und musst sagen, welche der Abfragen geeignet ist. Die fettgedruckten Begriffe beziehen sich auf die Suchbegriffe. (Stell dir vor, dass das Suchsystem nicht automatisch abschneidet und nur im Titelfeld sucht.)

a. Managing **globalization**: new business models, **strategies** and innovation

b. Evolution of **strategic** interactions from the triple to quad helix innovation models for sustainable development in the era of **globalization**

c. **Globalization** contained: the economic and **strategic** consequences of the container

d. The role of innovation and **globalization** strategies in post-crisis recovery

e. Competition as a Response **Strategy** to **Globalization** by Manufacturing Firms in Kenya


 
[ ]	Globalization AND strategy

[x]  Globalization AND strateg*

[ ] Globali?ation AND strateg?

[x]  Globali?ation AND strateg*

a. **Crowdfunding:** its potential for stimulating financial **development** in the ECCU

b. What determines the **growth** expectations of early-stage entrepreneurs? : evidence from crowdfunding

c. Web 2.0 as Platform for the Development of **Crowdfunding**

d. Crowdfunding economic **development**

e.  Modes, flows and networks : the promise of crowdfunding in documentary filmmaking and audience **development**


[x] crowdfunding AND (development OR growth)
[ ] development OR crowdfunding OR growth
[ ] crowdfunding AND development
[x]  crowdfund* AND (growth OR development)





