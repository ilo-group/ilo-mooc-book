---
title: Relevant search terms help in finding information
permalink: /3_Search/3.2_Relevant_terms/
eleventyNavigation:
    order: 3
    parent: 3_Search
---

**Lesson 2**

  

_**3.2 Relevant search terms help in finding information**_

In this lesson you will learn how to identify search terms that will help in finding information that you really need.

  

_**Learning outcomes:**_

*   to define appropriate search terms and concepts
    
*   to identify synonyms, related, broader and narrower search terms
    

  
  

**Content:**

  

Basically the search process is about finding out appropriate keywords which lead to the source that satisfies one's information need.

  

It is very helpful to note keywords, that might be appropriate for the search, right away when reading up on a subject. Mind maps can help to sort your thoughts while catching up on the topic and they help you to see where there might be gap or if you need to lay the focus on another aspect._

  

Another advice is to create a word list. You can use the keywords that you have noted before as “main” keywords and start looking for translations (if necessary), synonyms as well as broader and narrower terms. A thesaurus ([http://zbw.eu/stwversion/latest/about.en.html](http://zbw.eu/stw/version/latest/about.en.html)) is perfect for this._

  

Once you are clear about a research question, you can start brainstorming about your topic to identify appropriate search terms. Using terms from sources you have already read for your topic, or synonyms of terms you have found can be helpful.

  
  

**Search term operation**

  

After you decided on a search term and have run it through the system, you can evaluate the result list. Depending on your results you can modify the search terms. Learn more about search strategies and concepts by clicking through the following keywords.

  

1.  Stemming
    
2.  Truncation
    
3.  Wildcards
    
4.  Boolean Operators
    
5.  Brackets
    
6.  Phrase Search
    

  
  

_**3.2.1.Stemming**_
    

  

“_Stemming refers the process of reducing words to their stems or roots. A stem is the portion of a word that is left after removing its prefixes and suffixes” (Liu, 2011: 228)._

  

Example: competencies  competency
  

_**3.2.2 Truncation**_

  

Truncation means that you can use a symbol which then substitutes any number of characters. In most databases, the symbol for truncation is * but it may vary by database, so please check the help text. It’s a technique that broadens your search by including various word endings or beginnings.

  

  

A truncation (*) allows to search for unknown word endings

  

**Examples**

e.g: house* => houses,…

economic* => economic, economical, economics

*equality =>  equality, inequality

  
  

_**3.2.3** **Wildcards**_

Wildcards work similarly to truncation but they only substitute one character. They are very useful if a word can be spelled in different ways. The symbol **?** is often used for that but again, it may vary from database to database.

  

**Example:**

colo?r => color, colour
  

**3.2.4 Boolean Operators (AND OR NOT)**

Boolean Operators can be used in almost every database. They provide an opportunity to combine several search terms.

  

**AND:** Every term has to appear somewhere in the document. Example: benefit AND marketing  all documents, that contain both terms, are given.

  

**OR:** One of the terms has to appear somewhere in the document. Example: facebook OR marketing  all documents, that contain one of the terms, are given.

  

**NOT:** Excludes Terms. Example: marketing NOT facebook  all documents, that contain the term “marketing” but not the term “facebook” are given.

  

**Tips:**

*  Most databases use the AND operator between terms if you enter them in one search field. Unfortunately, there is no standard for this. So, you could check this by entering the terms without the AND operator at a time and after that, you enter them with the AND operator. If the results are the same, the database uses automatically the AND operator!
*  Consult the help text of the search tool to check if the tool uses “AND”, “OR” and “NOT”. Sometimes tools use different symbols (e.g. the plus or minus symbol).
    

  

_**3.2.5** **Brackets**_

  

There are two options to interpret the search string “marketing AND Facebook OR twitter”. The first option is: “Show me all documents about Facebook marketing or twitter marketing” whereas the second option is completely different: “Show me all documents about Facebook marketing or documents about twitter in general”. Boolean operators have their own hierarchy: NOT > AND > OR. So, our example search string would be interpreted as the second option by our search system but this interpretation does not make sense. Because of that, you should use brackets to clarify your intention. The search string should be changed to: marketing AND (Facebook or twitter).

  

In the following, the two options are visualized in a tree diagram:

_Include Image 3.2-1 and Image 3.2-2_

**3.2.6 Phrase search**

Sometimes terms contain several words and if you want to look for them in that exact combination, you use the phrase search. For this, you put the word group in double quotation marks.

  

**Example:** "social media marketing“

  

  

Putting terms or queries in quotation marks will only bring back results that contain all the words. E.g.:

**#**

_Include Image 3.2-3_

  

_**The following screencast will show an example on how to define search terms**_

  

_SCREENCAST:_

  

_**Resource:** ScreenCaptor, ProQuest (or other scientific data base)_

 _ProQuest provides an interface to operate a Boolean search_

  

_**Research question:**_

_How did information literacy and its definitions change as a result of digitisation_

  

_**The following strategies will be applied in the screencast:**_

*   _Stemming_
    
*   _Using synonyms_
    
*   _Manipulate search query/ terms_
    
*   _Evaluate quality/ strength of search query/ terms/ outcome_
    
*   _Boolean operators and Wildcards_
*   

**3.2.7 Assessment**

1.	You connect two terms with AND and after that with OR. Which connection finds more results? And why? 
	_OR finds more results because it shows all records that contain one of the search terms. AND just shows the records that contain both of the search terms._
2.	Supposedly, you look for documents that contain all grammatical forms of “commercial” but the database doesn’t offer truncation. Which Boolean operator would you use instead? 
 _OR. Example: commercial OR commercially OR commercialized_

3. 	This time, you are given the titles of documents and have to say, which one of the queries is suitable. The bold terms refer to the search terms. (Imagine that the search system doesn’t truncate automatically and that it only searches in the title field.)

a. Managing globalization: new business models, strategies and innovation
b. Evolution of strategic interactions from the triple to quad helix innovation models for sustainable development in the era of globalization
c. Globalization contained: the economic and strategic consequences of the container
d. The role of innovation and globalization strategies in post-crisis recovery
e. Competition as a Response Strategy to Globalization by Manufacturing Firms in Kenya

 
( )	Globalization AND strategy
(X) Globalization AND strateg*
( ) Globali?ation AND strateg?
(x) Globali?ation AND strateg*

1. Crowdfunding: its potential for stimulating financial development in the ECCU
2. What determines the growth expectations of early-stage entrepreneurs? : evidence from crowdfunding
3. Web 2.0 as Platform for the Development of Crowdfunding
4. Crowdfunding economic development
5.  Modes, flows and networks : the promise of crowdfunding in documentary filmmaking and audience development


(x) crowdfunding AND (development OR growth)
( ) development OR crowdfunding OR growth
( ) crowdfunding AND development
(x) crowdfund* AND (growth OR development)



