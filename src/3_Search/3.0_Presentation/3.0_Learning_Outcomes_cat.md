# **Mòdul 3: El poder de la cerca: Introducció, orientacions i objectius**

En aquest mòdul aprendràs com **trobar i recopilar informació** i com **utilitzar eficaçment les fonts i eines d'informació rellevants** per aconseguir objectius específics. També milloraràs les teves habilitats de gestió de tasques i temps, i desenvoluparàs un enfocament proactiu de la cerca i recopilació d'informació per evitar la sobrecàrrega d'informació.

**Resultats d'aprenentatge:**

*  conèixer els recursos d'informació rellevants
*  ser capaç d'utilitzar una varietat de fonts, incloent xarxes socials, contactes, bases de dades, etc.
*  utilitzar eficaçment les eines digitals
*  trobar i accedir a la informació (sobre un tema o tasca definits)
*  adquirir les habilitats i estratègies per trobar un objectiu específic (font, informació, persona, grup).
*  avaluar la idoneïtat de la informació i de les fonts d'informació i identificar les principals fonts d'informació pertinents per a la tasca
*  gestionar la informació (què fer amb els resultats de cerca)
*  ser capaç de replantejar una cerca quan sigui necessari, d'acord amb la reflexió sobre l'ús de les estratègies de cerca
*  convertir-se en un aprenent independent i en un aprenent per tota la vida