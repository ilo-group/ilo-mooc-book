---
title: Where to search and look for?
permalink: /3_Search/3.1_Where_to_search_en/
eleventyNavigation:
    order: 2
    parent: 3_Search
---




**3.1. Where to search and look for?**

  

In this lesson you will learn where to search for needed information and which tools to use.

  

**Learning outcome:**

* to identify the appropriate search tool/s (search engines, discovery tools, library catalogues, databases, social networks etc.) regarding the information sources that meet the information need_
    
* to be able to use a variety of tools including social networks, contacts, databases, etc. in order to find information_
    
* to choose appropriate information resources_
    

  

**Presentation: Text and Images**

  

First of all it is important to define a research question before searching for information. If the research question is clear, an appropriate tool can be identified relevant to the information need._

Choose one of the following links to learn more about search tools depending on a specific information need:_

Information is hidden behind these links, learners can click through them to learn more about search tools and sources. Examples will emphasise the information needs that can be covered by the specific tools:_

  

1. General search engines_
    
2. Library and online catalogues_
    
3. Scientific search engines_
    
4. Domain specific search services (databases, discovery systems)_
    
5. Social Media_
    

  

1.**General search engines**
    

General search engines are often used in everyday life and they help in satisfying almost every information need ( e.g. recipes, finding surrounding places or shops…). Users have free access at any time. They can gain quickly and easily a lot of results which are presented in a ranking. Finding scientific sources in general search engines could be difficult, because most of these are not transparent and not quality controlled._

  
**Examples:**

*   Google, Bing
    
    *   Public search engine
    
    *   free access at any time
        
    *   most widely used
        
    
*   DuckDuckGo
    
    *   Public search engine
        
    *   maintain that they do not collect information about the user’s search behaviour
        
    *   less frequently used
        
    

  

  

**Advantages**

*  quick
    
*  many results can be found
    
*  comprehensive
    
*  no specific searching and evaluation skills necessary
    
*  ranked results: individually by the user preferences
    
*  helps finding research questions
    

  

**Disadvantages**

* privacy: collect a lot of personal data
    
* not transparent: Covered resources - unclear which resources are covered
    
* sponsored announcements: Economic interest may influence results
    
* not controlled for quality
    

  
  

  

**3.1.2 Library and online catalogues**

Online catalogues are modern library catalogues which cover books and materials available in a library.

  

**Examples:**

*  OPAC (Online Public Access Catalogue):
    
    *   Indexing occurs by a database. Libraries are presenting their general catalogue of their stock
        
    
*   Union catalogue: WorldCat  the largest international catalogue
    
    *   Union catalogues widen the search domain. They contain the stock of different library catalogues
        
    

  

**Advantages**

* specific search options (filters)
    
* human involvement: books are indexed by experts
    
* scientific data included
    
* quality controlled


  

**Disadvantages**

* often no fulltext/limited access
    
* often unstructured results
    
* not as many results as other search systems (often only internal data)


  
  

**3.1.3 Scientific search engines**

Search engine for literature with a limited search domain. This tool provides a full text indexing and analyses of quotations. Also features and filters to modify the search are available._

  

**Examples:** Google Scholar, Semantic Scholar

  

**Advantages**

* Many results can be found, quick
    
* Mostly controlled quality

* Scientific data included
    
* Information about references (e.g. for Latex)
    

  

  

**Disadvantage**

* Specific searching and evaluation skills necessary
    
* Non-transparent: coverage unclear
    
* Fulltexts not always available
    
* False or uncomplete reference information
    

  
  

  

  

**3.1.4 Domain specific search engines**

  

**Publisher Systems:**

These systems are typically offered by a publisher and allow access to single articles e.g. in a journal.

The index of Discovery Systems is based on search engine technologies. The system accesses very heterogeneous sources and integrates different kinds of media.

  

Discovery Systems are provided by a publishing service like EBSO, Ex Libris or ProQuest, ACM and Pubmed

  

**Advantages**

* flexible search
    
* different kinds of media integrated
    
* ranked results
    
* helps finding research questions
    

  

**Disadvantages**

* not transparent
    
* coverage may be limited


  
  

**Databases:**

Databases are collection of e.g. books, articles, publications. Some organisation offer access to many databases especially to single articles.

  

**Example:** Hosts, STN international, Web of Science

  

**Advantages**

* controlled quality
    
* compact
    
* better access to the content of documents (key terms)
    
* domain specific
    

  

**Disadvantages**

* good searching skills necessary
    
* user access is more difficult
    
* browsing is difficult
    
* payed service
    

**3.1.5 Social Media**

Social Media offer a wide range of search options. Humans can search through hashtags, looking for persons, looking for locations, looking for products and share their knowledge in social networks. Other than that, people with an information need can also ask their questions actively in posts or forums to get information.

  

**Examples:** e.g. Facebook, Instagram, Xing, LinkedIn (professional social network)

  

**Advantages**

* many search options
    
* international
    
* searching without a clear research question (through hashtags..—> browsing)
    
* help by other users
    
* support finding research questions
   
* different kinds of media integrated

* explorative search
    

  

**Disadvantages**

* fake information
    
* uncontrolled quality
    
* not transparent
    
* can be easily manipulated
    
* no clear structure of the results
    
* much advertisement
    

  

  

  

  

**3.1.6 Choosing appropriate information resources**

Before you start looking for information resources, it is important to know if your topic for the academic achievement  is already accurately defined or if you are just looking for a broad overview of the subject area. The difference lies in the information resources and the tools you use for the research. Textbooks are great to get an overview but they do not offer current discussions about the topic. Papers, however, do offer current discussions and therefore they are the most important information resource if your topic for the term paper is already accurately defined.

So please keep in mind that different resources are published with different time delays. Also, not every resource can be found in any search tool. Common resources and where to find them are listed below:

* **Paper (journal):** Papers that have been published in a journal are the most important resource because they are very topical, their quality is certified and the content is specialized. Databases
    
* **Paper (collected volume):** Papers from a collected volume are often hard to find individually. In most cases you will have to look for the anthology itself in catalogues or bibliographies of other publications. Catalogues, bibliographies
    
* **Book:** Books can be monographs or collected volumes. Usually, monographs cover one issue whereas collected volumes comprise papers concerning one issue which are written by different authors. Catalogues
    
* **Working Paper:** Working papers belong to the so called grey literature. This is literature which is published outside of a publisher. Working papers are often published as a singular booklet or online for free. They are written by scientists but not peer-reviewed. Since they are not published by a publisher, they are published faster and therefore very topical. Specialist portals, scientific search engines, databases, catalogues
    
* **Website:** Any kind of internet resource is diverse and fast moving. Websites are not reviewed by a publisher; therefore, you have to do it by yourself before you use such a resource in your term paper ( module 4). Also, you should check if it is really necessary to use a website as a resource in your paper or if you could use a scientific resource to prove your statement. Search engines
    
* **Article in a newspaper or magazine:** Articles in a newspaper or magazine are updated daily or weekly. In contrast to the other resources above, the main audience is the general public and not scientists. For that reason, you should not use these articles in a term paper. The only exception is when you work on an up-to-the-minute topic which has not been scientifically reflected yet.  
     Press database, archives of newspapers and magazines.


**3.1.7 Assesment**

1. Which Google tool can you use to find scientific papers? 
( ) Google News
( ) Google Hangout
( ) Google+
(X) Google Scholar

2. True/false
TRUE OR FALSE?  You can use articles from a newspaper without concerns in your term paper.
( ) True
(x) False

3. TRUE OR FALSE? Journal articles are certified. 
(x) True
( ) False

4. TRUE OR FALSE? Internet resources are very diverse.
(x) True
( ) False

5. TRUE OR FALSE?  Working papers are certified.
( ) True
(x) False


6. Identify search tools/ sources regarding to the following information need scenarios, also define a research question of each scenario. 

Scenario 1: Max wants to cook a fancy dinner for his friends. He decides to serve a pumpkin soup. He has the recipe but has no idea how to arrange his dinner. He quickly needs some inspiration. But where to look? 

_[Answer: social media (e.g. Instagram, Facebook], looking through hashtags, Google, general search engines]_

Scenario 2: Lisa is writing her thesis about Information literacy. She is just starting with her research. She only wants to look for scientific literature. Which tools should Lisa use? 

_[Answer: Online catalogues like OPACs, Discovery Systems, Google Scholar]_
