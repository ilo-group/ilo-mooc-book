              @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; border: none; padding: 0cm; direction: ltr</span>; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US } p.cjk { font-family: "Arial Unicode MS"; font-size: 12pt } p.ctl { font-family: "Times New Roman"; font-size: 12pt } 

**3.1.** **Kje iščemo?**

  

V tej lekciji se boste naučili iskanja informacij in uporabe ustreznih orodij.

  

**Učni cilji:**

*   Prepoznavanje ustreznih orodij za iskanje (spletni iskalniki, orodja za odkrivanje informacij, knjižnični katalogi, baze podatkov, spletna družabna omrežja itd.), ki se nanašajo na določene informacijske vire in potrebe
    
*   Sposobnost uporabljati socialne medije, baze podatkov itd., ki pomagajo pri iskanju informacij
    
*   Sposobnost izbrati ustrezne informacijske vire
    

  

**Predstavitev: Besedila in slike**

Preden se lotite iskanja informacij, morate določiti raziskovalno vprašanje. Po določitvi raziskovalnega področja morate najti informacijsko orodje, ki ustreza informacijskim potrebam.

Izberite eno izmed naslednjih povezav, če želite izvedeti več o informacijskih orodjih, ki ustrezajo določenim informacijskim potrebam:

S klikom na povezavo se odpre novo okno z informacijami. Učenci se tako naučijo več o iskalnih orodjih in informacijskih virih. Določena orodja so namenjena določenim informacijskim potrebam:

  

1.  Splošni spletni iskalniki
    
2.  Knjižnični in spletni katalogi
    
3.  Iskalniki znanstvenih informacij
    
4.  Za področje specifični iskalniki (baze podatkov, orodja za odkrivanje informacij
    
5.  Družbena omrežja
    

  

1.  **Splošni spletni iskalniki**
    

Splošne spletne iskalnike pogosto uporabljamo v svojem vsakdanjiku. Večinoma pokrivajo vse informacijske potrebe (iskanje receptov, krajev, trgovin …). Uporabniki imajo kadarkoli neomejen dostop do njih ter hitro in preprosto pridejo do zadetkov. S splošnimi spletnimi iskalniki težje najdemo znanstvene vire, saj večinoma ne obstaja niti nadzor kakovosti niti niso transparentni.

  

**Primeri:**

*   Google, Bing
    
    *   Javni iskalnik
        
    *   kjerkoli in kadarkoli prost dostop
        
    *   Najbolj razširjena
        
    
*   DuckDuckGo
    
    *   Javni iskalnik
        
    *   Trdijo, da ne zbirajo podatkov o uporabnikovem vedenju
        
    *   Manj pogosto uporabljen
        
    

  

**Prednosti**

*   hitrost
    
*   veliko zadetkov
    
*   obširno
    
*   posebne spretnosti iskanja niso potrebne
    
*   razvrščeni rezultati: individualno glede na uporabnikove preference
    
*   pomagajo pri iskanju raziskovalnih vprašanj
    

  

**Pomanjkljivosti**

*   zasebnost: zbiranje veliko osebnih podatkov
    
*   ni transparentno: zajeti viri – ni jasno kateri viri so zajeti
    
*   sponzorirane objave: ekonomski interesi lahko vplivajo na zadetke
    
*   ni nadzora kakovosti
    

  

**3.1.2 Knjižnični katalogi**

Online katalogi so sodobni knjižnični katalogi, s katerimi najdemo knjige in gradiva določene knjižnice.

  

**Primeri:**

*   OPAC (Online Public Access Catalogue):
    
    *   Indeksirani glede na podatkovno zbirko. Knjižnice v katalogu predstavljajo svojo knjižnično zbirko.
        
    
*   Vzajemni katalog: WorldCat -> največji mednarodni katalog
    
    *   Z vzajemnimi katalogi razširimo iskalno področje. Vzajemni katalogi nam omogočajo dostop do knjižničnih katalogov (knjižničnega gradiva in zaloge) različnih knjižnic.
        
    

  

**Prednosti**

*   Posebne možnosti za iskanje informacij (filtri)
    
*   Človekova dejavnost: knjige obdelajo strokovnjaki
    
*   Dostop do znanstvenih podatkov
    
*   Nadzor kakovosti
    

  

**Pomanjkljivosti**

*   pogosto ni dostopa do celotnega besedila/oz. je omejen dostop
    
*   nestrukturirani zadetki
    
*   manj zadetkov kot drugi iskalniki (pogosto le notranji knjižnični podatki)
    

  

2.  **Iskalniki znanstvenih informacij**
    

So iskalniki za iskanje literature z omejenimi iskalnimi področji. S tem orodjem lahko iščemo po celotnem besedilu. Prav tako nam omogoča analizo citatov.

Z različnimi funkcijami in filtri lahko prilagodite iskalne zahteve.

  

**Primeri:** Google Učenjak, Semantic Scholar

  

**Prednosti**

*   hitro iskanje, mnogo zadetkov
    
*   Večinoma obstaja nadzor kakovosti
    
*   Dostop do znanstvenih podatkov
    
*   Informacije o referencah (na primer za Latex)
    

  

**Pomanjkljivosti**

*   Potrebne so posebne sposobnosti iskanja in vrednotenja
    
*   Ni transparentno: pokritost podatkov ni jasna
    
*   Ni vedno dostopa do celotnega besedila
    
*   Napačne ali nepopolne informacije o referencah
    

  
  

  

  

**3.1.4 Za področje specifični iskalniki**

  

**Založniški sistemi:**

Sisteme ponujajo založniki – omogočajo nam dostop do posameznih člankov (kot na primer v reviji). Iskalni seznami teh orodij za odkrivanje informacij temeljijo na tehnologijah spletnih iskalnikov in nam omogočajo dostop do raznovrstnih virov in različnih vrst medijev.

  

Orodja za odkrivanje informacij ponujajo založniki kot so EBSCO, Ex Libris ali ProQuest, ACM in PubMed.

  

**Prednosti**

*   fleksibilno iskanje
    
*   integrirane različne vrste medijev
    
*   razvrščeni zadetki
    
*   pomagajo pri iskanju raziskovalnih vprašanj
    

  

**Pomanjkljivosti**

*   ni transparentno
    
*   pokritost področja je lahko omejena
    

  
  

**Podatkovne zbirke:**

Podatkovne zbirke so npr. zbirke knjig, člankov, objav. Nekatere organizacije ponujajo dostop do veliko podatkovnih zbirk, posebno do posameznih člankov.

  

**Primeri:** Hosts, STN international, Web of Science

  

**Prednosti**

*   nadzor kakovosti
    
*   jedrnate
    
*   boljši dostop do vsebine datotek (ključni pojmi)
    
*   za specifično področje
    

  

**Pomanjkljivosti**

*   potrebne so dobre sposobnosti iskanja
    
*   težji dostop
    
*   brskanje ni enostavno
    
*   plačljive
    

**3.1.5 Družbena omrežja**

V spletnih družbenih omrežjih najdemo veliko različnih možnosti za iskanje informacij. Družbeni mediji nam omogočajo iskanje oseb, lokacij, izdelkov. Informacije lahko iščemo z uporabo ključnikov. Prav tako lahko delimo svoje znanje na družbenih omrežjih. Poleg tega se lahko vsakdo aktivno pozanima o informacijah na primer z objavo vprašanja na spletnih družbenih omrežjih ali v klepetalnicah.

  

**Primeri:** na primer Facebook, Instagram, Xing, LinkedIn (strokovno omrežje)

  

**Prednosti**

*   veliko možnosti za iskanje informacij
    
*   mednarodno
    
*   iskanje brez raziskovalnega vprašanja (preko ključnikov —> brskanje)
    
*   pomoč drugih uporabnikov
    
*   pomoč pri iskanju raziskovalnega vprašanja
    
*   različne vrste medijev
    
*   eksplorativno iskanje
    

  

**Pomanjkljivosti**

*   lažne informacije
    
*   ni nadzora kakovosti
    
*   ni transparentno
    
*   manipulacija je možna
    
*   ni jasne strukture zadetkov
    
*   veliko reklam
    

  

  

  

  

**3.1.6 Izbira ustreznih informacijskih virov**

Preden se lotite iskanja informacijskih virov, razmislite, ali je tema vaše raziskave jasno opredeljena ali iščete le širok pregled nad temo. Informacijski viri in orodja se namreč razlikujejo in so odvisni od namena iskanja. Učbeniki predstavljajo na primer dober vir za pregled nad temo. Na žalost pa ne ponujajo aktualnih diskusij. Znanstveni članki so najpomembnejši informacijski vir, če ste že določili raziskovalno področje, saj povzamejo sodobne razprave o določeni temi. Treba je upoštevati, da se lahko objava različnih virov zavleče. Poleg tega ni mogoče najti vsakega vira z le enim iskalnim orodjem. Navedli smo nekaj pogostih skupin virov:

*   **Članek (revija):** Članki objavljeni v revijah, so najpomembnejši viri, saj so aktualni, kakovostni in njihova vsebina je specializirana. Podatkovne zbirke
    
*   **Članek (zbirka):** Težko je najti posamezne članke, ki so bili objavljeni v zbirkah. Največkrat boste morali iskati že samo antologijo v katalogih ali bibliografijah drugih publikacij. Katalogi, bibliografije
    
*   **Knjiga:** H knjigam spadajo tako monografije kakor tudi zbirke. Medtem ko monografije obravnavajo določeno temo, so zbirke sestavljene iz več znanstvenih razprav na temo, s katero se ukvarjajo različni avtorji. Katalogi
    
*   **Delovni dokument:** Delovni dokumenti spadajo k tako imenovani sivi literaturi in običajno nimajo založnika. Po navadi so objavljeni kot knjižice. Velikokrat pa so tudi brezplačno dostopni na spletu. Avtorji teh nerecenziranih publikacij so znanstveniki. Delovne dokumente objavijo avtorji sami (ne založniki). Zato je postopek objave po navadi zelo hiter, vsebujejo pa zelo ažurne informacije.
    

Portali za strokovnjake, spletni iskalniki za znanstveno literaturo, podatkovne zbirke, katalogi

*   **Spletna stran:** Spletni viri so zelo raznoliki in se hitro spreminjajo. Pomembno je vedeti, da spletnih strani ni preveril noben založnik. Preden uporabljamo to vrsto virov, jih moramo sami preveriti in vrednotiti njihovo kakovost (modul 4). Prav tako morate preveriti, ali je uporaba te spletne strani zares potrebna oziroma lahko namesto nje uporabite informacijske vire na znanstveni ravni, ki podpirajo vaše stališče. Spletni iskalniki
    
*   **Članki v časnikih ali revijah:** Članke v časopisih ali revijah posodabljajo dnevno ali tedensko. Medtem ko so prej predstavljeni viri namenjeni predvsem znanstvenikom, so časniki namenjeni vsem ljudem. Zaradi tega vam ne priporočamo, da jih citirate v svojem znanstvenem delu. Navajanje teh virov je izjemoma dovoljeno, če se v svoji razpravi ukvarjate z aktualno temo, ki še ni bila utemeljena. Podatkovne zbirke člankov iz časnikov, arhivi časnikov in revij.


**3.1.7 Vaja**
1.	S katerim Googlovim orodjem lahko najdete znanstvene objave? 

 () Google News
 
 () Google Hangout
 
 () Google+
 
 (X) Google Učenjak
 
2.	Da/Ne

Da ali ne? Članke iz časnika lahko brez skrbi uporabljate v seminarskih nalogah.

() Da

(x) Ne

3.	Da ali ne? Članki v časniku so certificirani.

(x) Da

() Ne

4.	Da ali ne? Spletni viri so raznoliki.

(x) Da

() Ne

5.	Da ali ne? Delovni dokumenti so certificirani.

() Da

(x) Ne

6.	Poimenujte informacijske vire, ki zadovoljijo informacijsko potrebo naslednjih scenarijev. Prav tako postavite raziskovalno vprašanje vsakega scenarija.

Scenarij 1: Maks želi povabiti prijatelje na večerjo – želi skuhati gobovo juho. Ima recept, a ne ve, kako bi jo serviral – potrebuje malo inspiracije. Kje bo jo našel?

Odgovor: Spletna družbena omrežja (z.B. Instagram, Facebook), ključniki, Google, splošni spletni iskalniki]

Scenarij 2: Sonja piše seminarsko nalogo na temo „informacijska pismenost”. Pravkar se je začela ukvarjati s to temo. Najprej želi najti znanstveno literaturo. Katera orodja naj uporablja?

[Odgovor: Spletne knjižnične kataloge kot je OPAC, orodja za odkrivanje informacij, Google Učenjak]
