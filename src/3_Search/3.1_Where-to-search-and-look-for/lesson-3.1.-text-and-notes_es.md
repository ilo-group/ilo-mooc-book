**3.1 ¿Dónde buscar?**

**Introducción**  

En esta lección aprenderás dónde buscar la información necesaria y qué herramientas utilizar.

**Resultados de aprendizaje:**

Al final de esta lección serás capaz de:

* identificar la herramienta de búsqueda apropiada (motores de búsqueda, herramientas de descubrimiento, catálogos de bibliotecas, bases de datos, redes sociales, etc.) en relación con la fuente de información que satisface las necesidades de información
* usar una variedad de herramientas incluyendo redes sociales, contactos, bases de datos, etc. para encontrar información

 

**3.1.1 Buscadores generalistas**
    
Los buscadores generalistas se utilizan a menudo en nuestra vida cotidiana y ayudan a satisfacer casi todas las necesidades de información (por ejemplo, recetas, búsqueda de lugares o tiendas cercanas...). Los usuarios tienen acceso libre en cualquier momento. Pueden obtener rápida y fácilmente una gran cantidad de resultados que se presentan en forma de ranking. Encontrar fuentes científicas en los motores de búsqueda generalistas puede ser difícil, porque la mayoría de ellos son intransparentes y no tienen controles de calidad.

  
**Ejemplos:**

*   Google, Bing
    
    *   buscador de uso público
    *   acceso libre en cualquier momento
    *   el más utilizado
        
    
*   DuckDuckGo
    
    *   buscador de uso público
    *   según la empresa, no recopilan información sobre el comportamiento de búsqueda del usuario
    *   utilizado con menos frecuencia
    

  

**Ventajas**

*  rápidos
*  se pueden encontrar muchos resultados
*  exhaustivos
*  no se precisan habilidades específicas de búsqueda y evaluación de información
*  resultados ordenados en forma de ranking: individualmente según las preferencias del usuario
*  ayudan a encontrar preguntas de investigación
    

**Desventajas**

* privacidad: recogen muchos datos personales
* no son transparentes: existencia de recursos ocultos - no está claro qué recursos abarca el buscador
* anuncios patrocinados: hay intereses económicos que pueden influir en los resultados
* sin controles de calidad

  

**3.1.2 Catálogos de biblioteca y catálogos en línea**

Los catálogos en línea son catálogos modernos de bibliotecas que contienen libros y materiales que están disponibles en una biblioteca.

**Ejemplos:**

*  OPAC (Online Public Access Catalogue):
    
    *   La indexación se realiza mediante una base de datos. Las bibliotecas presentan el catálogo completo de sus fondos.
        
    
*  Catálogos colectivos: WorldCat -> el mayor catálogo de ámbito internacional
    
    *   Los catálogos colectivos amplían el ámbito de búsqueda. Contienen el stock de diferentes catálogos de la biblioteca.
        
    

**Ventajas**

* opciones de búsqueda específicas (filtros)
* intervención humana: los libros son indexados por expertos
* incluyen datos científicos
* con controles de calidad


**Desventajas**

* a menudo no son a texto completo / acceso limitado
* resultados a menudo no estructurados
* no tantos resultados como otros sistemas de búsqueda (a menudo solo datos internos)



**3.1.3 Buscadores científicos**

## GOOGLE SCHOLAR
Buscador de literatura científica con posibilidades de búsqueda limitadas. Esta herramienta proporciona una indexación de texto completo y análisis de citas. También hay disponibles funciones y filtros para modificar la búsqueda.

  
**Ventajas**

* se pueden encontrar muchos resultados, de forma rápida
* en la mayoría de ocasiones, se controla la calidad
* se incluyen datos científicos
* información sobre referencias (por ejemplo, para Latex)
  

**Desventajas**

* son necesarias habilidades específicas de búsqueda y evaluación de la información
* no es transparente: no está claro qué recursos abarca el buscador
* el texto completo de los artículos no siempre está disponible
* la información de las referencias a menudo es incorrecta o incompleta
  
  

**3.1.4 Buscadores de dominio específico**

## SISTEMAS DE BÚSQUEDA DE EDITORIALES

Estos sistemas suelen ser ofrecidos por un editor o distribuidor y permiten el acceso a artículos individuales, por ejemplo, en una revista.

Los índices de las herramientas de descubrimiento (en inglés, discovery systems o discovery tools) se basan en tecnologías de motores de búsqueda. El sistema accede a fuentes muy heterogéneas e integra diferentes tipos de medios.

Los discovery systems son proporcionados por servicios editoriales como EBSO, Ex Libris o ProQuest, ACM y Pubmed.

**Ventajas**

* búsqueda flexible
* diferentes tipos de medios integrados
* resultados clasificados
* ayudan a encontrar preguntas de investigación

  

**Desventajas**

* no son transparentes
* su cobertura, es decir, los recursos que abarca el buscador, puede ser limitada
  

## BASES DE DATOS

Las bases de datos son colecciones de, por ejemplo, libros, artículos y publicaciones. Algunas organizaciones ofrecen acceso a muchas bases de datos, especialmente a artículos individuales.

**Ejemplos:** STN international, Web of Science


**Ventajas**

* calidad controlada
* concisos
* un mejor acceso al contenido de los documentos (palabras clave)
* dominio específico
    
  

**Desventajas**

* se necesitan buenas habilidades de búsqueda
* el acceso de los usuarios es más difícil
* la navegación es difícil
* servicios de pago
    

**3.1.5 Redes sociales**

Las redes sociales ofrecen una amplia gama de opciones de búsqueda. Podemos buscar a través de hashtags, buscar personas, lugares o productos y compartir nuestros conocimientos en las  redes sociales. Aparte de eso, las personas con una necesidad de información también pueden preguntar activamente en mensajes o foros para obtener información.

**Ejemplos:** Facebook, Instagram, Xing, LinkedIn (red social profesional)

 
**Ventajas**

* muchas opciones de búsqueda
* ámbito internacional
* permiten buscar sin una pregunta de investigación clara (a través de hashtags...-> navegación)
* ayuda por parte de otros usuarios
* son de ayuda para encontrar preguntas de investigación
* integran diferentes tipos de medios
* búsqueda explorativa

**Desventajas**

* informaciones falsas
* calidad no controlada
* no son transparentes
* pueden ser fácilmente manipulados
* los resultados no tienen una estructura clara
* hay mucha publicidad
  

  

**3.1.6 Escoger recursos de información adecuados**

Antes de empezar a buscar recursos de información, es importante saber si tu tema ya está definido con precisión o si solo estás buscando una visión general de la materia. La diferencia radica en los recursos de información y las herramientas que utilizarás para la investigación. Los libros de texto son excelentes para obtener una visión general, pero no ofrecen análisis o investigaciones actuales sobre el tema. Sin embargo, los documentos ofrecen análisis e investigaciones actuales y, por lo tanto, son la fuente de información más importante si el tema de tu trabajo o proyecto ya está definido con precisión.

Así que por favor ten en cuenta que los diferentes recursos se publican con diferentes intervalos de tiempo. Además, no todos los recursos se pueden encontrar en cualquier herramienta de búsqueda. A continuación se enumeran los recursos más comunes y dónde encontrarlos:

* **Artículo (revista científica):** Los artículos publicados en una revista científica son el recurso más importante porque están muy actualizados, su calidad está certificada y su contenido es especializado. Dónde buscarlos: bases de datos.

* **Artículo (obra colectiva):** Los artículos que aparecen en una obra colectiva son a menudo difíciles de encontrar individualmente. En la mayoría de los casos tendrás que buscar la monografía que los recoge en catálogos o bibliografías de otras publicaciones. Dónde buscarlos:  catálogos, bibliografías.

* **Libro:** Los libros pueden ser monografías u obras colectivas. Por lo general, las monografías cubren un tema, mientras que las obras colectivas incluyen artículos sobre un tema escritos por diferentes autores. Dónde buscarlos:  catálogos.

* **Documento de trabajo (working paper):** Los documentos de trabajo pertenecen a la denominada literatura gris. Se trata de literatura que se publica fuera del circuito editorial. Los documentos de trabajo se publican a menudo como un folleto singular o en línea de forma gratuita. Están escritos por científicos, pero no están revisados por expertos. Dado que no son publicados por un editor, se publican más rápido y, por lo tanto, son de gran actualidad. Dónde buscarlos: portales especializados, buscadores científicos, bases de datos, catálogos.

* **Sitio web:** Cualquier tipo de recurso de Internet es diverso y cambia rápidamente. Los sitios web no son revisados por un editor; por lo tanto, tienes que revisarlos por ti mismo antes de utilizar un recurso de este tipo en tu trabajo (módulo 4). Además, debes comprobar si es realmente necesario utilizar un sitio web como recurso en tu trabajo o si podrías utilizar un recurso científico para contrastar la información. Dónde buscarlos: buscadores.

* **Artículo de un periódico o revista:** Los artículos de un periódico o revista se actualizan diaria o semanalmente. A diferencia de los otros recursos mencionados anteriormente, la audiencia a la que se dirigen es el público en general, no los científicos. Por esta razón, no debes utilizar estos artículos en un trabajo académico. La única excepción es cuando se está trabajando en un tema actual que aún no se ha reflejado científicamente. Dónde buscarlos: bases de datos de prensa, archivos de periódicos y revistas.



**3.1.7 Test de evaluación**

1. ¿Qué herramienta de Google puedes utilizar para encontrar artículos científicos?

( ) Google News

( ) Google Hangouts 

( ) Google+ 

(X) Google Scholar

2. ¿VERDADERO O FALSO? Puedes usar artículos de un periódico en tu trabajo sin preocuparte por nada.

( ) Verdadero

(x) Falso

3. ¿VERDADERO O FALSO? Los artículos de revistas científicas son revisados por expertos.

(x) Verdadero

( ) Falso

4. ¿VERDADERO O FALSO? Los recursos de Internet son muy diversos.

(x) Verdadero

( ) Falso


5. ¿VERDADERO O FALSO? Los documentos de trabajo son revisados por expertos.

( ) Verdadero

(x) Falso


6. Identifica herramientas/fuentes de búsqueda en relación con los siguientes escenarios, y define una pregunta de investigación para cada escenario.

Escenario 1: Max quiere cocinar una cena elegante para sus amigos. Decide servir una sopa de calabaza. Él tiene la receta, pero no tiene idea de cómo preparar su cena. Necesita algo de inspiración de forma rápida. ¿Pero dónde buscar?

_[Answer: social media (e.g. Instagram, Facebook], looking through hashtags, Google, general search engines]_

Escenario 2: Lisa está escribiendo su tesis sobre alfabetización informacional. Justo está empezando con su investigación. Solo quiere buscar literatura científica. ¿Qué herramientas debe usar Lisa?

_[Answer: Online catalogues like OPACs, Discovery Systems, Google Scholar]_
