**3.1. Wo wird gesucht und wonach?**



In dieser Lektion lernt ihr, wo ihr nach benötigten Informationen suchen könnt und welche Werkzeuge ihr verwenden solltet.

  
**Lernergebnisse:**

* Identifizieren von geeigneten Suchwerkzeugen (Suchmaschinen, Discovery-Tools, Bibliothekskataloge, Datenbanken, soziale Netzwerke usw.) hinsichtlich der Informationsquellen, die dem Informationsbedarf entsprechen
    
* Fähig sein, eine Vielzahl von Tools einschließlich sozialer Netzwerke, Kontakte und Datenbanken usw. zu nutzen, um Informationen zu finden
    
* Geeignete Informationsressourcen auswählen


**Präsentation: Text und Bilder**

Bevor nach Informationen gesucht wird, ist es zunächst wichtig, ein Suchziel zu definieren. Wenn das Suchziel klar definiert ist, kann ein geeignetes Instrument identifiziert werden, das für diesen Informationsbedarf geeignet ist.

Wählt einen der folgenden Links, um mehr über Suchwerkzeuge zu erfahren, die von einem bestimmten Informationsbedarf abhängen: 

Die Informationen sind hinter den folgenden Links verborgen, die Lernenden können sich durchklicken, um mehr über die Suchwerkzeuge und -quellen zu erfahren. Durch Beispiele wird der Informationsbedarf unterstrichen, der durch die spezifischen Instrumente abgedeckt werden kann:

1. Allgemeine Suchmaschinen
    
2. Bibliotheks- und Online-Kataloge
    
3. Wissenschaftliche Suchmaschinen
    
4. Domänenspezifische Suchdienste (Datenbanken, Discovery-Systeme)
    
5. Soziale Medien
    

  

**3.1.1 Allgemeine Suchmaschinen**
    

Allgemeine Suchmaschinen werden im Alltag häufig eingesetzt und helfen fast alle Informationsbedürfnisse zu befriedigen (z.B. Rezepte, Suche nach Orten in der Umgebung oder Geschäften...). Die Nutzer haben jederzeit freien Zugang. Sie können schnell und einfach viele Ergebnisse erzielen, die in einem Ranking dargestellt werden. Die Suche nach wissenschaftlichen Quellen in allgemeinen Suchmaschinen könnte schwierig sein, da die meisten weder transparent noch qualitätskontrolliert sind.

  
**Beispiele:**

* Google, Bing
    
    * Öffentliche Suchmaschine
    
    * Freier Zugang zu jeder Zeit
        
    * Am weitesten verbreitet
        
    
* DuckDuckGo
    
    * Öffentliche Suchmaschine
        
    * Stellt sicher, dass keine Informationen über das Suchverhalten des Benutzers gesammelt werden
        
    * Weniger häufig verwendet
        
    

  

  

**Vorteile**

* Schnell
    
* Viele Ergebnisse werden gefunden
    
* Umfassend
    
* Keine spezifischen Such- und Bewertungskenntnisse erforderlich
    
* Ranglistenergebnisse: individuell nach den Benutzereinstellungen
    
* Hilft bei der Suche nach Forschungsfragen
    

  

 **Nachteile**

* Datenschutz: Sammeln viele persönliche Daten
    
* nicht transparent: Verborgene Ressourcen - unklar, welche Ressourcen abgedeckt sind
    
* Gesponserte Anzeigen: Wirtschaftliches Interesse kann die Ergebnisse beeinflussen
    
* Nicht qualitätsgeprüft
    

  
  

  

**3.1.2 Bibliotheks- und Online-Kataloge**

Online-Kataloge sind moderne Bibliothekskataloge, die Bücher und Materialien umfassen, welche in einer Bibliothek vorhanden sind.

  
**Beispiele:**

* OPAC (Online Public Access Catalogue):
    
    * Die Indizierung erfolgt durch eine Datenbank. Bibliotheken präsentieren den Gesamtkatalog ihres Bestandes.
        
    
* Gesamtkatalog: WorldCat  der größte internationale Katalog
    
    * Verbundkataloge erweitern den Suchbereich. Sie enthalten den Bestand verschiedener Bibliothekskataloge.   

  

**Vorteile**

* Spezifische Suchoptionen (Filter)
    
* Menschliches Engagement: Bücher werden von Experten indexiert
    
* Wissenschaftliche Daten enthalten
    
* Qualitätsgeprüft


  

**Nachteile**

* Oft kein Volltext/eingeschränkter Zugriff
    
* Oft unstrukturierte Ergebnisse
    
* Nicht so viele Ergebnisse wie bei anderen Suchsystemen (oft nur interne Daten)



  
  

**3.1.3 Wissenschaftliche Suchmaschinen**

Suchmaschine für Literatur mit eingeschränkter Suchmöglichkeit. Dieses Tool stellt Volltext-Indexierungen zur Verfügung und ermöglicht die Übernahme von Zitaten. Es stehen auch Funktionen und Filter zur Verfügung, um die Suche zu modifizieren.

  

**Beispiele:** Google Scholar, Semantic Scholar

  
**Vorteile**

* Viele Ergebnisse können schnell und einfach gefunden werden
    
* Meistens kontrollierte Qualität

* Enthalten wissenschaftliche Daten
    
* Informationen über Referenzen (z.B. für Latex)
    

  

  

**Nachteil**

* Spezifische Such- und Bewertungskenntnisse erforderlich
    
* Intransparent: Abdeckung unklar
    
* Volltexte nicht immer verfügbar
    
* Falsche oder unvollständige Referenzinformationen
    

  
  

  

  

**3.1.4 Domänenspezifische Suchmaschinen**

  

**Verlagssysteme:**

Diese Systeme werden typischerweise von einem „Publisher“ angeboten und ermöglichen den Zugriff auf einzelne Artikel z.B. in einer Zeitschrift.

Der Index von Discovery-Systemen basiert auf Suchmaschinentechnologien. Das System greift auf sehr heterogene Quellen zu und integriert verschiedene Arten von Medien.


Discovery-Systeme werden von einem Publishing-Service wie EBSO, Ex Libris oder ProQuest, ACM und PubMed bereitgestellt.

  
**Vorteile**

* Flexible Suche
    
* Verschiedene Arten von Medien integriert
    
* Geordnete Ergebnisse
    
* Hilft bei der Suche nach Forschungsfragen
    

  

**Nachteile**

* Nicht transparent
    
* Der Umfang ist begrenzt


  

  

**Datenbanken:**

Datenbanken sind Sammlungen von z.B. Büchern, Artikeln, Publikationen. Einige Unternehmen bieten Zugang zu vielen Datenbanken, insbesondere zu einzelnen Fachartikeln.

  

**Beispiel:** Gastgeber, STN international, Web of Science

  

**Vorteile**

* Kontrollierte Qualität
    
* Kompakt
    
* Besserer Zugang zum Inhalt von Dokumenten (Schlüsselbegriffe)
    
* Domänenspezifisch

  

**Nachteile**

* Gute Suchkenntnisse erforderlich
    
* Der Benutzerzugriff ist schwieriger
    
* „Browsing“ gestaltet sich schwierig
    
* Bezahlte Dienstleistung
    

**3.1.5 Soziale Medien**

Social Media bietet eine Vielzahl von Suchmöglichkeiten. Menschen können Hashtags durchsuchen und nach Personen, Standorten und Produkten suchen sowie ihr Wissen in sozialen Netzwerken teilen. Darüber hinaus können Menschen mit einem Informationsbedarf ihre Fragen auch aktiv in Beiträgen oder Foren stellen, um Informationen zu erhalten.

  

**Beispiele:** z.B. Facebook, Instagram, Xing, LinkedIn (professionelles soziales Netzwerk)

  

**Vorteile**

* Viele Suchoptionen
    
* International
    
* Suche ohne klare Recherchefrage möglich (durch Hashtags -> Browsen)
    
* Hilfe durch andere Benutzer
    
* Unterstützung bei der Suche nach Forschungsfragen
   
* Verschiedene Arten von Medien integriert

* Explorative Suche
    

  

**Nachteile**

* Gefälschte Informationen
    
* Unkontrollierte Qualität
    
* Nicht transparent
    
* Kann leicht manipuliert werden
    
* Keine klare Struktur der Ergebnisse
    
* Viel Werbung
    

  

  

  

  

**3.1.6 Auswahl geeigneter Informationsressourcen**

Bevor ihr mit der Suche nach Informationsressourcen beginnt, ist es wichtig zu wissen, ob das Thema für z.B. eure Semesterarbeit bereits genau definiert ist oder ob ihr zuerst nach einem Überblick des jeweiligen Fachgebiets sucht. Je nachdem was euer Ziel ist, gibt es unterschiedliche  Informationsressourcen und  Werkzeuge, die ihr für die Recherche verwenden könntet. Lehrbücher sind gut, um sich einen Überblick zu verschaffen, bieten aber keine aktuellen Diskussionen zum Thema. Paper (Fachartikel) enthalten hingegen aktuelle Diskussionen und sind daher die wichtigste Informationsquelle, wenn z.B. für eine Seminararbeit gesucht wird.

Bitte beachtet außerdem, dass verschiedene Ressourcen mit unterschiedlichen Zeitverzögerungen veröffentlicht werden. Außerdem kann nicht jede Ressource in jedem Suchwerkzeug gefunden werden. Allgemeine Ressourcen und die Stellen, an denen sie zu finden sind, sind unten aufgeführt:

* **Papers (Zeitschrift):** Papers, die in einer Zeitschrift veröffentlicht wurden, sind die wichtigste Ressource, da sie sehr aktuell sind, ihre Qualität sichergestellt ist und die Inhalte spezialisiert sind
    
* **Paper (Sammelband):** Paper aus einem Sammelband sind oft schwer einzeln zu finden. In den meisten Fällen müsst ihr den Sammelband selbst in Katalogen oder Bibliographien anderer Publikationen suchen
    
*  **Buch:** Bücher können Monographien oder Sammelbände sein. In der Regel decken Monographien ein Thema ab, während Sammelbände Beiträge zu einem Thema umfassen, die von verschiedenen Autoren verfasst wurden
    
* **Working Paper:** Working Papers gehören zur so genannten grauen Literatur. Dies ist Literatur, die außerhalb eines Verlages veröffentlicht wird. Working Papers werden oft als Einzelheft oder kostenlos online veröffentlicht. Sie wurden von Wissenschaftlern verfasst, aber nicht von Experten begutachtet. Da sie nicht von einem Verlag veröffentlicht werden, werden sie schneller und damit sehr aktuell veröffentlicht: Fachportale, wissenschaftliche Suchmaschinen, Datenbanken, Kataloge.    

* **Website:** Jede Art von Internetressource ist vielfältig und schnelllebig. Websites werden nicht von einem Publisher überprüft; daher müsst ihr dies selbst tun, bevor ihr eine solche Ressource in eurer Hausarbeit nutzt (Modul 4). Außerdem solltet ihr prüfen, ob es wirklich notwendig ist, eine Website als Ressource in eurer Arbeit zu verwenden, oder ob ihr eine wissenschaftliche Ressource verwenden könntet, um eure Aussage zu beweisen

* **Artikel in einer Zeitung oder Zeitschrift:** Artikel in einer Tageszeitung oder Zeitschrift werden täglich oder wöchentlich aktualisiert. Im Gegensatz zu den anderen oben genannten Ressourcen ist das Hauptpublikum die breite Öffentlichkeit und nicht die Wissenschaftler. Aus diesem Grund solltet ihr diese Artikel nicht in einer Hausarbeit verwenden. Die einzige Ausnahme ist, wenn ihr an einem aktuellen Thema arbeitet, das noch nicht wissenschaftlich reflektiert wurde.
    Pressedatenbank, Archive von Zeitungen und Zeitschriften.  

**3.1.7 Test**

1. Mit welchem Google-Tool kannst du wissenschaftliche Arbeiten finden? 

() Google News

() Google Hangout

() Google+

(X) Google Scholar

2. Wahr/Falsch

WAHR ODER FALSCH?  Du kannst Artikel aus einer Zeitung bedenkenlos in deiner Hausarbeit verwenden.

() Wahr

(x) Falsch

3. WAHR ODER FALSCH? Zeitschriftenartikel sind zertifiziert. 

(x) Wahr

() Falsch

4. WAHR ODER FALSCH? Internetressourcen sind sehr vielfältig.

(x) Wahr

() Falsch

5. WAHR ODER FALSCH?  Die Arbeitspapiere sind zertifiziert.

() Wahr

(x) Falsch


6. Identifiziere Suchwerkzeuge/Quellen zu den folgenden Informationsbedarfsszenarien. Definiere auch eine Forschungsfrage zu jedem Szenario. 

Szenario 1: Max will seinen Freunden ein ausgefallenes Abendessen zubereiten. Er beschließt, eine Kürbissuppe zu servieren. Er hat das Rezept, hat aber keine Ahnung, wie er sein Abendessen gestalten soll. Er braucht schnell etwas Inspiration. Aber wo soll man suchen? 

_Antwort: Social Media (z.B. Instagram, Facebook), Hashtags durchsuchen, Google, allgemeine Suchmaschinen]_ 

Szenario 2: Lisa schreibt ihre Arbeit über Informationskompetenz. Sie fängt gerade erst mit ihrer Forschung an. Sie will nur nach wissenschaftlicher Literatur suchen. Welche Werkzeuge sollte Lisa verwenden? 

_[Antwort: Online-Kataloge wie OPACs, Discovery Systeme, Google Scholar]_ 
