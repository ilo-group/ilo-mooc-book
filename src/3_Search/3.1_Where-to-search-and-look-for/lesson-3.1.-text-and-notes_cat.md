**3.1 On buscar?**

**Introducció**

  

En aquesta lliçó aprendràs on buscar la informació necessària i quines eines utilitzar.

  

**Resultats d'aprenentatge:**

Al final d'aquesta lliçó seràs capaç de:

* identificar l'eina de cerca apropiada (motors de cerca, eines de descobriment, catàlegs de biblioteques, bases de dades, xarxes socials, etc.) en relació amb la font d'informació que satisfà les necessitats d'informació
* utilitzar una varietat d'eines incloent xarxes socials, contactes, bases de dades, etc. per trobar informació
    

  

**Presentation: Text and Images**

  

First of all it is important to define a research question before searching for information. If the research question is clear, an appropriate tool can be identified relevant to the information need._

Choose one of the following links to learn more about search tools depending on a specific information need:_

Information is hidden behind these links, learners can click through them to learn more about search tools and sources. Examples will emphasise the information needs that can be covered by the specific tools:_

  

1. General search engines_
    
2. Library and online catalogues_
    
3. Scientific search engines_
    
4. Domain specific search services (databases, discovery systems)_
    
5. Social Media_
    

  

**3.1.1 Cercadors generalistes**
    

Els cercadors generalistes s'utilitzen sovint en la nostra vida quotidiana i ajuden a satisfer gairebé totes les necessitats d'informació (per exemple, receptes, cerca de llocs o botigues properes ...). Els usuaris hi tenen accés lliure en qualsevol moment. Poden obtenir ràpida i fàcilment una gran quantitat de resultats que es presenten en forma de rànquing. Trobar fonts científiques en els motors de cerca generalistes pot ser difícil, perquè la majoria d'ells són intransparentes i no tenen controls de qualitat.

  
**Exemples:**

*   Google, Bing
    *  cercadors d'ús públic
    *  accés lliure en qualsevol moment
    *  els més utilitzats
*   DuckDuckGo
    *  cercador d'ús públic
    *  segons l'empresa, no recopilen informació sobre el comportament de cerca de l'usuari
    *  utilitzat amb menys freqüència
    

  

  

**Avantatges**

*  ràpids
*  podem trobar molts resultats
*  exhaustius
*  no es necessiten habilitats específiques de cerca i avaluació d'informació
*  resultats ordenats en forma de rànquing: individualment segons les preferències de l'usuari
*  ajuden a trobar preguntes de recerca
    

  

**Inconvenients**

* privacitat: recullen moltes dades personals
* no són transparents: existència de recursos ocults - no està clar quins recursos abasta el cercador
* anuncis patrocinats: hi ha interessos econòmics que poden influir en els resultats
* sense controls de qualitat

  
  

  

**3.1.2 Catàlegs de biblioteca i catàlegs en línia**

Els catàlegs en línia són catàlegs moderns de biblioteques que recullen els llibres i materials que estan disponibles en una biblioteca.

  

**Exemples:**

*  OPAC (Online Public Access Catalogue)
    
    *   La indexació es realitza mitjançant una base de dades. Les biblioteques presenten el catàleg complet dels seus fons.
        
    
*   Catàlegs col·lectius: WorldCat -> el catàleg més gran d'àmbit internacional
    
    *   Els catàlegs col·lectius amplien l'àmbit de cerca. Contenen l'estoc de diferents catàlegs de la biblioteca.

        
    

  

**Avantatges**

* opcions de cerca específiques (filtres)
* intervenció humana: els llibres són indexats per experts
* inclouen dades científiques
* amb controls de qualitat

  

**Inconvenients**

* sovint no són a text complet / accés limitat
* resultats sovint no estructurats
* no tants resultats com altres sistemes de cerca (sovint només dades internes)

  
  

**3.1.3 Cercadors científics**

## GOOGLE SCHOLAR

Cercador de literatura científica amb possibilitats de recerca limitades. Aquesta eina proporciona una indexació de text complet i anàlisi de cites. També hi ha disponibles funcions i filtres per modificar la cerca.
  


**Avantatges**

* es poden trobar molts resultats, de forma ràpida
* en la majoria d'ocasions, es controla la qualitat
* s'inclouen dades científiques
* informació sobre referències (per exemple, per LaTex)

  

  

**Inconvenients**

* són necessàries habilitats específiques de cerca i avaluació de la informació
* no és transparent: no està clar quins recursos abasta el cercador
* el text complet dels articles no sempre està disponible
* la informació de les referències sovint és incorrecta o incompleta
  
  
**3.1.4 Cercadors de domini específic**

  

## **SISTEMES DE CERCA D'EDITORIALS:**

Aquests sistemes solen ser oferts per un editor o distribuïdor i permeten l'accés a articles individuals, per exemple, en una revista.

Els índexs de les eines de descobriment (en anglès, discovery systems o discovery tools) es basen en tecnologies de motors de cerca. El sistema accedeix a fonts molt heterogènies i integra diferents tipus de mitjans.

Els discovery systems són proporcionats per serveis editorials com EBSO, Ex Libris o ProQuest, ACM i Pubmed.
  

**Avantatges**

* cerca flexible
* diferents tipus de mitjans integrats
* resultats classificats
* ajuden a trobar preguntes de recerca
    

  

**Inconvenients**

* no són transparents
* la seva cobertura, és a dir, els recursos que abasta el cercador, pot ser limitada

  
  

##**BASES DE DADES:**

Les bases de dades són col·leccions de, per exemple, llibres, articles i publicacions. Algunes organitzacions ofereixen accés a moltes bases de dades, especialment a articles individuals.

**Exemples:** STN international, Web of Science
  


  

**Avantatges**

* qualitat controlada
* concisos
* un millor accés al contingut dels documents (paraules clau)
* domini específic
    

  

**Inconvenients**

* cal tenir bones habilitats de cerca
* l'accés dels usuaris és més difícil
* la navegació és difícil
* serveis de pagament
    

**3.1.5 Xarxes socials**

Les xarxes socials ofereixen una àmplia gamma d'opcions de cerca. Podem cercar a través de hashtags, buscar persones, llocs o productes i compartir els nostres coneixements a les xarxes socials. A part d'això, les persones amb una necessitat d'informació també poden preguntar activament en missatges o fòrums per obtenir informació.

**Exemples:** Facebook, Instagram, Xing, LinkedIn (xarxa social professional).

  

**Avantatges**

* moltes opcions de cerca
* àmbit internacional
* permeten buscar sense una pregunta d'investigació clara (a través d'hashtags ...-> navegació)
* ajuda per part d'altres usuaris
* són d'ajuda per trobar preguntes de recerca
* integren diferents tipus de mitjans
* cerca explorativa
    

  

**Inconvenients**

* informacions falses
* qualitat no controlada
* no són transparents
* poden ser fàcilment manipulats
* els resultats no tenen una estructura clara
* hi ha molta publicitat
    

  
**3.1.6 Escollir recursos d'informació adequats**

Abans de començar a buscar recursos d'informació, és important saber si el teu tema ja està definit amb precisió o si només estàs buscant una visió general de la matèria. La diferència rau en els recursos d'informació i les eines que utilitzaràs per a la investigació. Els llibres de text són excel·lents per seleccionar un punt, però no ofereixen anàlisis o recerca actual sobre el tema. No obstant això, els documents ofereixen anàlisis i recerca actual i, per tant, són la font d'informació més important si el tema del teu treball o projecte ja està definit amb precisió.

Així que si us plau tingues en compte que els diferents recursos es publiquen amb diferents intervals de temps. A més, no tots els recursos es poden trobar en qualsevol eina de cerca. A continuació s'enumeren els recursos més comuns i on trobar-los:

* **Article (revista científica):** Els articles publicats en una revista científica són el recurs més important perquè estan molt actualitzats, la seva qualitat està certificada i el seu contingut és especialitzat. On buscar-los: bases de dades.

* **Article (obra col·lectiva):** Els articles que apareixen en una obra col·lectiva són sovint difícils de trobar individualment. En la majoria dels casos hauràs de buscar la monografia que els recull en catàlegs o bibliografies d'altres publicacions. On buscar-los: catàlegs, bibliografies.

* **Llibre: Els llibres poden ser monografies o obres col·lectives.** En general, les monografies cobreixen un tema, mentre que les obres col·lectives inclouen articles sobre un tema escrits per diferents autors. On buscar-los: catàlegs.

* **Document de treball (working paper):** Els documents de treball pertanyen a l'anomenada literatura grisa. Es tracta de literatura que es publica fora del circuit editorial. Els documents de treball es publiquen sovint com un fullet singular o en línia de forma gratuïta. Estan escrits per científics, però no estan revisats per experts. Atès que no són publicats per un editor, es publiquen més ràpid i, per tant, són de gran actualitat. On buscar-los: portals especialitzats, cercadors científics, bases de dades, catàlegs.

* **Lloc web:** Qualsevol tipus de recurs d'Internet és divers i canvia ràpidament. Els llocs web no són revisats per un editor; per tant, has de revisar-los per tu mateix abans d'utilitzar un recurs d'aquest tipus en el teu treball (mòdul 4). A més, has de comprovar si és realment necessari utilitzar un lloc web com a recurs en el teu treball o si podries utilitzar un recurs científic per contrastar la informació. On buscar-los: cercadors.

* **Article d'un diari o revista:** Els articles d'un diari o revista s'actualitzen diàriament o de forma setmanal. A diferència dels altres recursos esmentats anteriorment, l'audiència a la qual es dirigeixen és el públic en general, no els científics. Per aquesta raó, no has d'utilitzar aquests articles en un treball acadèmic. L'única excepció és quan s'està treballant en un tema actual que encara no s'ha reflectit científicament. On buscar-los: bases de dades de premsa, arxius de diaris i revistes.



**3.1.7 Test d'avaluació**

1. Quina eina de Google pots utilitzar per trobar articles científics? 

( ) Google News

( ) Google Hangout

( ) Google+

(X) Google Scholar

 2. ¿VERITABLE O FALS? Pots fer servir articles d'un diari en el teu treball sense preocupar-te per res.

( ) Veritable

(x) Fals

3. ¿VERITABLE O FALS? Els articles de revistes científiques són revisats per experts.

(x) Veritable

( ) Fals

4. ¿VERITABLE O FALS? Els recursos d'Internet són molt diversos.

(x) Veritable

( ) Fals

5. ¿VERITABLE O FALS? Els documents de treball són revisats per experts.

( ) Veritable

(x) Fals


6. Identifica eines / fonts de cerca en relació amb els següents escenaris, i defineix una pregunta de recerca per a cada escenari.

Escenari 1: En Max vol cuinar un sopar elegant per als seus amics. Decideix servir una sopa de carbassa. Ell té la recepta, però no té idea de com preparar el seu sopar. Necessita una mica d'inspiració de forma ràpida. Però on buscar?

_[Answer: social media (e.g. Instagram, Facebook], looking through hashtags, Google, general search engines]_

Escenari 2: La Lisa està escrivint la seva tesi sobre alfabetització informacional. Tot just està començant amb la seva recerca. Només vol buscar literatura científica. Quines eines ha d'usar la Lisa?

_[Answer: Online catalogues like OPACs, Discovery Systems, Google Scholar]_
