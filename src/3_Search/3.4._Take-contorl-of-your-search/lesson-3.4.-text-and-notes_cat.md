**3.4 Controla la teva cerca**

  

**Introducció**

**Resultats d'aprenentatge:**

Al final d'aquesta lliçó:

*  sabràs com relacionar la informació trobada amb l'estratègia de cerca original
*  podràs ordenar i limitar els resultats de la teva cerca utilitzant diferents criteris que s'ajustin a les teves necessitats per obtenir fonts d'informació rellevants (font, format, any, mètriques de citació, etc.)
*  seràs conscient de l'efecte positiu de la serendípia en les cerques
*  seràs conscient de la importància de gestionar la informació que trobis i les eines de suport

  

**3.4.1 Relacionar les estratègies de cerca amb el resultat de la cerca (1/2)**

  

Diferents estratègies de cerca retornen diferents resultats. Hem après que 'facebook AND marketing' dona resultats totalment diferents que 'facebook OR marketing'. Però també canvis molt més petits en la consulta poden alterar la llista de resultats, com ara ' "facebook marketing" ' (entrat com una frase) i 'facebook marketing'. En aquest capítol ens centrarem a relacionar les estratègies de cerca amb la informació trobada. És molt útil pensar en com vols que es vegi la llista de resultats i després crear la consulta de cerca.

Observa les següents tres consultes. Pensa en els documents que retornaran.

1. creixement economic xina

2. econom* xina

3. (creixement OR economic) xina

  

  

Solució:_

1. Aquesta consulta buscarà documents que continguin els tres termes de cerca.
2. Aquesta consulta buscarà qualsevol terme que comenci amb "econom" (com economia, economista, etc.) i el terme "xina".
3. Aquesta consulta buscarà documents que continguin el terme "xina" i un dels altres termes. Per tant, la consulta també podria ser "(xina economic) OR (xina creixement)". La llista de resultats seria la mateixa.
  

**3.4.1 Relacionar les estratègies de cerca amb el resultat de la cerca (2/2)**

També podem tractar de fer-ho a l'inrevés. Aquí tens cinc títols de documents. Quina estratègia de cerca és l'adequada? Imagina que el sistema de cerca només busca els termes del títol (els termes en negreta es refereixen als termes de cerca).


1. Determinants comportamentals del suport a les polítiques de **protecció** del medi **ambient.**

2. Avaluació de l'impacte de les activitats de **protecció** del medi **ambient** a les regions russes.

3. Formació del mecanisme d'activitats de **protecció ambiental** en l'àmbit de la gestió **ambiental** agrícola.

4. Finançament de l'activitat de **protecció** de la **natura** a nivell regional: principals tendències i perspectives de millora.

5. Turisme, **protecció** de la **natura** i responsabilitat.

El terme "protecció" està present en els cinc títols. En conseqüència, és un dels termes de cerca. Després tenim "medi ambient" i "ambiental", per la qual cosa podem assumir que el segon terme és "ambient*". El tercer terme que trobem és "natura". En els títols, es tracta de "natura" o d'"ambient*" i, per tant, podem suposar que els dos termes es combinen amb un operador OR. Per tant, la consulta que és adequada per obtenir la llista de títols, és: "(ambient* OR natura) AND protecció".


**3.4.2 Adaptar la cerca**

De vegades no s'obtenen prou resultats o no són suficientment bons. És important no frustrar-se! En la majoria dels casos, la forma més fàcil d'obtenir millors resultats és adaptar l'estratègia de cerca. A continuació, et mostrarem què fer si tens molt pocs o massa resultats.

**1. Tens molt pocs resultats.**

En primer lloc, has de verificar si has comès algun error formal en la teva cerca. És correcta l'ortografia? Has utilitzat els camps de cerca i els operadors booleans correctes? Has utilitzat termes de cerca rellevants? Si tot i així hi ha pocs resultats, t'oferim alguns consells:

Hauries d'ampliar la teva cerca. Si has utilitzat opcions de filtre, comprova si pots eliminar-les. Tracta de fer servir termes més amplis o truncaments al final dels termes. Utilitza termes alternatius (sinònims) i combina'ls amb un operador OR. Fes la cerca en diferents llengües. Si malgrat tot aquests consells no són suficients, pots fer un cop d'ull a les referències que es troben al final de la lliçó.


**2. Tens massa resultats.**

Has utilitzat la connexió correcta entre els termes? L'operador AND entre termes ajuda a trobar menys resultats. Utilitza també l'operador NOT. Pots utilitzar la cerca avançada per aplicar filtres o pots filtrar la llista de resultats amb posterioritat. Intenta utilitzar termes més específics i combina'ls amb els altres termes de cerca. A més a més, fes servir la cerca per frase exacta quan convingui. 


**3.4.3 Limitar i ordenar els resultats de la cerca**

Pot passar que obtinguis massa resultats de cerca, fins i tot si la teva estratègia de cerca anterior era correcta. En aquest cas, molts sistemes donen l'oportunitat de limitar i ordenar els resultats de la cerca.

A continuació, pots veure les opcions d'ordenació de resultats d'EconBiz.

_Include Image 3.4.-1_

**3.4.4 Cerca per serendípia**

 _"La serendípia és l'art de trobar coses que no sabíem que estàvem buscant". - Glauco Ortolano_

La cerca per serendípia descriu una cerca en la qual es troba informació interessant però que no estava prevista en l'estratègia de cerca original. També pot passar sovint quan es busca alguna cosa que no té res a veure. Es podria dir que algú "ensopega" amb informació útil. Donat que la cerca per serendípia és un concepte un tant abstracte per explicar-lo aquí, mira aquest vídeo que mostra una troballa afortunada de nova informació:

_Video on Serendipity Search (Video Ende)_

**Referències**
Referències que et suggerim per aprendre més:

Association of College and Research Libraries ACRL (2015): Framework for Information Literacy for Higher Education. [http://www.ala.org/acrl/sites/ala.org.acrl/files/content/issues/infolit/Framework_ILHE.pdf] [Consulta: 06 Sep 2017].

Bent, Moira; Stubbings, Ruth (2011): The SCONUL Seven Pillars of Information Literacy. Core model for higher education. London: SCONUL. [http://www.sconul.ac.uk/sites/default/files/documents/coremodel.pdf] [Consulta: 10 Sep 2017].

Coonan, Emma; Secker, Jane (2011): A New Curriculum for Information Literacy (ANCIL)- Executive Summary. [http://ccfil.pbworks.com/f/Executive_summary.pdf] [Consulta: 10 Sep 2017].

Jacobson, Trudi; Mackey, Thomas P. (eds.) (2016): Metaliteracy in practice. Chicago: Neal-Schuman, an imprint of the American Library Association.

Lauber-Reymann, Magrit (2017): Informationsressourcen. Ein Handbuch für Bibliothekare und Informationsspezialisten. Berlin: Walter de Gruyter & Co (Bibliotheks- und Informationspraxis, 49).

Sühl-Strohmenger, Wilfried (ed.) (2012a): Handbuch Informationskompetenz. Berlin: De Gruyter (De Gruyter Handbuch).



**3.4.5 Exercici**

Diferents estratègies de cerca retornen diferents resultats. Hem après que 'facebook AND marketing' dona resultats totalment diferents que 'facebook OR marketing'. Però també canvis molt més petits en la consulta poden alterar la llista de resultats, com ara ' "facebook marketing" ' (entrat com una frase) i 'facebook marketing'. En aquest capítol ens centrarem a relacionar les estratègies de cerca amb la informació trobada. És molt útil pensar en com vols que es vegi la llista de resultats i després crear la consulta de cerca.

Observa les següents tres consultes. Pensa en els documents que retornaran.
1. creixement economic xina

( ) Aquesta consulta buscarà documents que continguin informació econòmica, però no sobre el creixement de Xina.
(x) Aquesta consulta buscarà documents que continguin els tres termes de cerca.
( ) Aquesta consulta buscarà documents que continguin alguns dels tres termes de cerca.

2. econom* xina

(x) Aquesta consulta buscarà qualsevol terme que comenci per "econom" (com economia, economista, etc.) i el terme "xina".
( ) Aquesta consulta buscarà qualsevol terme que finalitzi amb "econom" (com economia, economista, etc.) i el terme "xina".
( ) Aquesta consulta buscarà qualsevol terme que no inclogui "econom", però sí el terme "xina".

3. (creixement OR economic) xina

( ) Aquesta consulta buscarà documents que continguin el terme "xina" i els dos altres termes.
(x) Aquesta consulta buscarà documents que continguin el terme "xina" i un dels altres termes.
( ) Aquesta consulta buscarà documents que continguin "economic" o "creixement", però no "xina".

També podem tractar de fer-ho a l'inrevés. Aquí tens cinc títols de documents. Quina estratègia de cerca és l'adequada? Imagina que el sistema de cerca només busca els termes del títol (els termes en negreta es refereixen als termes de cerca).

1. Determinants comportamentals del suport a les polítiques de **protecció** del medi **ambient**.
2. Avaluació de l'impacte de les activitats de **protecció** del medi **ambient** a les regions russes.
3. Formació del mecanisme d'activitats de **protecció ambiental** en l'àmbit de la gestió **ambiental** agrícola.
4. Finançament de l'activitat de **protecció** de la **natura** a nivell regional: principals tendències i perspectives de millora.
5. Turisme, **protecció** de la **natura** i responsabilitat.

= (ambient* OR natura) AND protecció

[explanation]
El terme "protecció" està present en els cinc títols. En conseqüència, és un dels termes de cerca. Després tenim "medi ambient" i "ambiental", per la qual cosa podem assumir que el segon terme és "ambient*". El tercer terme que trobem és "natura". En els títols, es tracta de "natura" o d'"ambient*" i, per tant, podem suposar que els dos termes es combinen amb un operador OR. Per tant, la consulta que és adequada per obtenir la llista de títols, és: "(ambient* OR natura) AND protecció".
[explanation]

