_**Lektion 4**_

  

_**3.4. Kontrolliere deine Suche**_


**Lernergebnisse:**

Am Ende dieser Lektion werdet ihr:

* wissen, wie man die gefundenen Informationen mit der ursprünglichen Suchstrategie in Beziehung setzt
    
* die Suchergebnisse nach verschiedenen Kriterien sortieren und einschränken können, die deinen Bedürfnissen entsprechen, um relevante Informationsquellen zu erhalten (Quelle, Format, Jahr, Zitierkennzahlen usw.).
    
* auf den positiven Effekt des Serendipity Searchs achten
    
* euch der Bedeutung der Verwaltung der gefundenen Informationen und Support-Tools bewusst sein -> Link zu Modul 4    

  

_**3.4.1** **Beziehung von Suchstrategie und Suchergebnis (1/2)**_

  

Unterschiedliche Suchstrategien liefern unterschiedliche Ergebnisse. Ihr habt gelernt, dass "facebook AND marketing" völlig andere Ergebnisse liefert als "facebook OR marketing". Viel kleinere Änderungen an der Abfrage können ebenfalls die Ergebnisliste verändern, wie z.B. "facebook marketing" ' (als Phrase) und ‚facebook marketing’. Dieses Kapitel konzentriert sich daher auf die Beziehung zwischen Suchstrategien und den gefundenen Informationen . Es ist sehr hilfreich, darüber nachzudenken, wie die Ergebnisliste aussehen soll und dann die Suchanfrage zu erstellen.

  

Betrachtet die folgenden drei Abfragen. Überlegt, welche Dokumente sie liefern würden.

1. economic growth china
2. econom* china
3. (economic OR growth) china


  

_Lösung:_

1\. Diese Abfrage sucht nach Dokumenten, die alle drei Suchbegriffe enthalten.

2\. Diese Suche sucht nach Begriffen, die mit "econom" (wie economics, economical etc.) und dem Begriff "china" beginnen.

3\. Diese Abfrage sucht nach Dokumenten, die den Begriff "china" und einen der anderen Begriffe enthalten. So könnte die Abfrage auch "(economic china) OR (growth china)" lauten. Die Ergebnisliste wäre die gleiche.
  

_**3.4.1** **Beziehung von Suchstrategie und Suchergebnis (2/2)**_

Ihr könnt ebenfalls versuchen, die Sache andersherum anzugehen. Hier habt ihr fünf Titel von Dokumenten. Welche Suchstrategie ist geeignet? Stellt euch vor, dass das Suchsystem nicht automatisch kürzt und nur nach den Begriffen im Titel sucht. (Die fettgedruckten Begriffe beziehen sich auf die Suchbegriffe.)
  

1. Behavioral Determinants of Proclaimed Support for **Environment Protection** Policies.
2. Assessment of **environment protection** activity impact in Russian regions.
3. Formation of the environmental protection activities mechanism in the field of agricultural **environmental** management.
4. Financing Of **Nature Protection** Activity At Regional Level: Main Trends And Improvement Prospects.
5. Tourism, **Nature Protection** and Responsibility.


Der Begriff **„protection“** kommt in allen fünf Titeln vor. Deshalb ist es auch einer der Suchbegriffe. Danach haben wir **„environment“** und **„environmental“**,  deshalb können wir davon ausgehen, dass unser zweiter Suchbegriff **„environment*“** ist. Der dritte Begriff, den wir finden ist **„nature“**. In den Titeln kommt entweder **„nature“** oder **„environment*“** vor und aus diesem Grund können wir davon ausgehen, dass die Begriffe durch den OR Operator kombiniert sind. Das bedeutet das unsere Suchanfrage schlussendlich so aussieht: **“(environment* OR nature) AND protection”**


**3.4.2** **Anpassung der Suche**

Manchmal werden nicht genügend oder qualitativ ausreichende Ergebnisse geliefert. Dann ist es wichtig, nicht frustriert zu sein! Meistens ist der einfachste Weg, um bessere Ergebnisse zu erzielen, die Anpassung eurer Suchstrategie. Im Folgenden wird euch gezeigt, was zu tun ist, wenn ihr zu wenig oder aber zu viele Ergebnisse erhaltet.

**1\. Ihr habt zu wenig Ergebnisse.**

Zuerst solltet ihr prüfen, ob ihr bei der Suche formale Fehler gemacht habt. Ist die Schreibweise korrekt? Habt ihr die richtigen Suchfelder und booleschen Operatoren verwendet? Habt ihr relevante Suchbegriffe verwendet? Wenn all dies zutrifft, sind hier einige Ratschläge:

Ihr solltet eure Suche erweitern. Wenn ihr Filteroptionen verwendet habt, prüft ob ihr diese lockern könnt. Versucht allgemeinere Begriffe zu verwenden oder Trunkierung am Ende von Begriffen zu verwenden. Verwendet alternative Begriffe (Synonyme) und kombiniert diese mit einem OR-Operator. Versucht es mit mehrsprachiger Suche. Wenn diese Ratschläge nicht geholfen haben, schaut euch die Referenzen der relevanten Literatur an, die ihr bereits gefunden habt.
  

**2\. Ihr habt zu viele Ergebnisse.**

Habt ihr die richtige Verbindung zwischen den Begriffen genutzt? Der Operator AND zwischen den Begriffen hilft, weniger Ergebnisse zu finden. Verwendet auch den NOT-Operator. Ihr könnt die Erweiterte Suche verwenden, um Filter anzuwenden, oder ihr wendet Filter in der Ergebnisliste an. Versucht spezifischere Begriffe (engere Begriffe) zu verwenden und diese mit euren anderen Suchbegriffen zu kombinieren. Verwendet außerdem die Phrasensuche.

_**3.4.3** **Einschränkung und Sortierung der Suchergebnisse**_

Es kann vorkommen, dass ihr zu viele Suchergebnisse erhaltet, obwohl eure bisherige Suchstrategie ausreichend war. In diesem Fall bieten viele Systeme die Möglichkeit, die Suchergebnisse einzuschränken und zu sortieren.

Nachfolgend seht ihr die Sortierungsmöglichkeiten von EconBiz.

Bild einbinden 3.4.-1

  _**Google Scholar**_


Im folgenden Screencast zeigen wir euch, wie ihr die Suchergebnisse reduzieren könnt, indem ihr die Suchanfrage anpasst und die Ergebnisse anschließend einschränkt.  

Video zur Reduzierung der Suchergebnisse_

  
**3.4.4 Serendipity Search**

  
“_Serendipity is the gift of finding things we did not know we were looking for.” – Glauco Ortolano_

  
Serendipity Search beschreibt eine Suche, bei der ihr interessante Informationen findet, bei denen ihr feststellt, dass sie nicht in der ursprünglichen Suchstrategie vorgesehen waren. Dies kann oft passieren, wenn nach etwas ganz anderem gesucht wird. Man könnte sagen, dass jemand über hilfreiche Informationen "stolpert". Da die Serendipity-Suche etwas abstrakt zu erklären ist, könnt ihr euch das folgende Video angucken, was das zufällig geglückte Auffinden neuer Informationen zeigt:


Video zur Serendipity-Suche (Video Ende)_  

  
**3.4.5 Übung**

**Beziehung von Suchstrategie und Suchergebnis**

A. Unterschiedliche Suchstrategien liefern unterschiedliche Ergebnisse. Ihr habt gelernt, dass "facebook AND marketing" völlig andere Ergebnisse liefert als "facebook OR marketing". Viel kleinere Änderungen an der Abfrage können ebenfalls die Ergebnisliste verändern, wie z.B. "facebook marketing" ' (als Phrase) und ‚facebook marketing’. In diesem Kapitel werden wir uns daher auf die Beziehung von Suchstrategien zu den gefundenen Informationen konzentrieren. Es ist sehr hilfreich, darüber nachzudenken, wie die Ergebnisliste aussehen soll und dann die Suchanfrage zu erstellen.

Betrachtet die folgenden drei Abfragen. Überlege, welche Dokumente sie liefern würden.

1. economic growth china
2. econom* china
3. (economic OR growth) china

Wir können auch versuchen, es andersherum zu machen. Hier habt ihr fünf Titel von Dokumenten. Welche Suchstrategie ist geeignet? Stell euch vor, dass das Suchsystem nicht automatisch kürzt und nur nach den Begriffen im Titel sucht. (Die fettgedruckten Begriffe beziehen sich auf die Suchbegriffe
1. Behavioral Determinants of Proclaimed Support for **Environment Protection** Policies.
2. Assessment of **environment protection** activity impact in Russian regions.
3. Formation of the environmental protection activities mechanism in the field of agricultural **environmental** management.
4. Financing Of **Nature Protection** Activity At Regional Level: Main Trends And Improvement Prospects.
5. Tourism, **Nature Protection** and Responsibility.


_Lösung_

_A._
1\. Diese Abfrage sucht nach Dokumenten, die alle drei Suchbegriffe enthalten.

2\. Diese Suche sucht nach Begriffen, die mit "econom" (wie economics, economical etc.) und dem Begriff "china" beginnen.

3\. Diese Abfrage sucht nach Dokumenten, die den Begriff "china" und einen der anderen Begriffe enthalten. So könnte die Abfrage auch "(economic china) OR (growth china)" lauten. Die Ergebnisliste wäre die gleiche.

_B._
Der Begriff "protection" ist in allen fünf Titeln enthalten. Folglich ist es einer der Suchbegriffe. Dann haben wir "environment und "environmental", so dass wir davon ausgehen können, dass der andere Begriff „environment*" ist. Der dritte Begriff, den wir finden, ist "nature". In den Titeln ist es entweder "nature" oder "environment*" und deshalb können wir davon ausgehen, dass die beiden Begriffe mit einem OR-Operator kombiniert werden. Also, unsere letzte Frage, die sich für die Liste der Titel eignet, ist: "(environment* OR nature) AND protection”.

**Quellen**

 Empfohlene Literatur für weiteres Lernen

  
  

Association of College and Research Libraries ACRL (2015): Framework for Information Literacy for Higher Education. Online available at: http://www.ala.org/acrl/sites/ala.org.acrl/files/content/issues/infolit/Framework_ILHE.pdf, zuletzt geprüft am 06.09.2017.

Bent, Moira; Stubbings, Ruth (2011): The SCONUL Seven Pillars of Information Literacy. Core model for higher education. Hg. v. SCONUL. London. Online available at: http://www.sconul.ac.uk/sites/default/files/documents/coremodel.pdf, zuletzt geprüft am 10.09.2017.

Coonan, Emma; Secker, Jane (2011): A New Curriculum for Information Literacy (ANCIL)- Executive Summary. Online available at: http://ccfil.pbworks.com/f/Executive_summary.pdf, zuletzt geprüft am 10.09.2017.

Jacobson, Trudi; Mackey, Thomas P. (eds.) (2016): Metaliteracy in practice. Chicago: Neal-Schuman, an imprint of the American Library Association.

Lauber-Reymann, Magrit (2017): Informationsressourcen. Ein Handbuch für Bibliothekare und Informationsspezialisten. Berlin: Walter de Gruyter & Co (Bibliotheks- und Informationspraxis, 49).

Sühl-Strohmenger, Wilfried (Hg.) (2012a): Handbuch Informationskompetenz. Berlin: De Gruyter (De Gruyter Handbuch).

  
  

_Include Exit Image InformationLiteratePerson_