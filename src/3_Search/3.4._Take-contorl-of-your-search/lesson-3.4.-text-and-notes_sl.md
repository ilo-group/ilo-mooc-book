              @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; border: none; padding: 0cm; direction: ltr</span>; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } p.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US } p.cjk { font-family: "Arial Unicode MS"; font-size: 12pt } p.ctl { font-family: "Times New Roman"; font-size: 12pt } 

**Lekcija 4**

  

**3.4. Nadzor nad iskanjem informacij**

  

**Učni cilji:**

Na koncu te lekcije boste razvili:

*   Razumevanje povezave prvotne strategije iskanja informacij z najdenimi informacijami
    
*   sposobnost razvrščanja in omejevanja zadetkov z uporabo raznolikih kriterijev, ki ustrezajo osebnim potrebam za pridobivanje relevantnih virov (viri, oblike, leto, navajanje itd.)
    
*   zavedanje pozitivnega učinka naključnega iskanja informacij
    
*   zavedanje pomembnosti upravljanja z informacijami in informacijskimi orodji -> povezava modula
    

  

**3.4.1 Povezava strategij iskanja informacij in seznama zadetkov** **(1/2)**

  

Z različnimi strategijami iskanja dosežemo različne zadetke. Ugotovili smo, da se razlikujeta iskalna niza “Facebook IN trženje” in “Facebook ALI trženje”. Prav tako se spremeni seznam zadetkov, če spremenimo le majhne stvari in vpišemo v iskalnik na primer “trženje na Facebooku” (kot frazo) ali ‚trženje na facebooku‘ kot niz nepovezanih besed. V tem poglavju se bomo ukvarjali s povezavo strategij iskanja informacij in njihovih zadetkov. Pomembno je, da razmišljate o seznamu zadetkov, preden vtipkatepoizvedbo v iskalno polje.

  

Poglejte si naslednje poizvedbe. Razmišljajte o tem, kako bi bil seznam zadetkov sestavljen.

1\. gospodarska rast Kitajske

2\. gospodars\* Kitajska

3\. (gospodarstvo ALI rast) Kitajska

  

  

_Rešitev:_

1\. Ta iskalni niz bo iskal dokumente z vsemi tremi izrazi.

2\. Ta iskalni niz bo iskal vse izraze, ki se začnejo z “gospodarsk” (kot na primer gospodarstvo, gospodarski itd.) in izraz “Kitajska”.

3\. Ta iskalni niz bo iskal dokumente, ki vsebujejo izraz “Kitajska” in enega izmed ostalih izrazov. Vtipkali bi lahko iskalni niz “(gospodarstvo Kitajske) ALI (rast Kitajske)”. V seznamu bi našli iste zadetke.

  

**3.4.1 Povezava strategij iskanja informacij in seznama zadetkov** **(2/2)**

To pa deluje tudi na obraten način. Sestavili smo seznam z naslovi dokumentov. Katera strategija iskanja informacij je primerna? Predstavljajte si, da išče iskalni sistem le izraze v naslovih. (Znaki, ki so zapisani v krepki pisavi, se nanašajo na iskalne nize.)

  

1\. Determinante vedenja pri izraženi podpori načelom **varstva okolja**.

2\. Evalvacija vpliva na dejavnosti **varstva okolja** v ruskih regijah.

3\. Oblikovanje mehanizmov **varstva okolja** s kmetijsko-okoljskimi ukrepi.

4\. Financiranje aktivnosti **varstva narave** na regionalni ravni: glavne usmeritve in možnosti izboljšanja.

5\. Turizem, **varstvo narave** in odgovornost.

  

Izraz “varstvo” je sestavni del vseh naslovov. Zato je to tudi eden izmed iskalnih izrazov. V naslovih najdemo tudi besedi “okolje” in “okoljskimi”, zato domnevamo, da je drugi iskalni izraz “okolj\*”. Tretji iskalni izraz pa je “narava”. V naslovih najdemo, ali besedo “narava” ali izraz “okolj\*”. Zaradi tega lahko domnevamo, da bomo izraza povezali z logičnim operatorjem ALI. Iskalni niz, ki se nanaša na naslove, je: “(okolj\* ALI narava) IN varstvo”.

  

**3.4.2**  **Prilagoditev nastavitev iskanja**

Včasih ni dovolj zadetkov, pogosto pa niso primerni. Najpomembnejše je, da obvladamo v tem trenutku neprijetna čustva. Treba je samo prilagoditi strategijo iskanja informacij. Pokazali vam bomo, kaj je treba storiti, če ste dobili preveč ali premalo zadetkov.

  

**1\. Imate premalo zadetkov.**

-> Preverite najprej, ali v poizvedbi opazite kakršnokoli napako. Ste zagrešili kakršnokoli formalno napako? Ste naredili pravopisno napako? Ste uporabljali pravilna iskalna polja in logične operatorje? Ste uporabljali primerne iskalne izraze? Če ste vse preverili in niste našli nobenih napak, imamo nekaj nasvetov za vas:

Uporabljajte razširitev iskanja. Če ste uporabili iskalne filtre, jih izklopite ali omilite, izberite širše pojme ali krajšajte bolj na koncu besed. Spremenite možnosti iskanja ali iskalna polja. Pomislite tudi na sopomenke in jih zapišite z logičnim operatorjem ALI v iskalno polje. Za iskanje informacij lahko uporabljate tudi druge jezike. Oglejte si bibliografijo virov, ki ste jih doslej našli, če vam prej omenjeni nasveti ne pomagajo pri rešitvi težave.

  

**2\. Imate preveč zadetkov.**

-> Ste povezali izraze s pravilnim Boolovim operatorjem? Z logičnim operatorjem IN boste našli manj zadetkov. Priporočljiva je tudi uporaba logičnega operatorja NE. Izberete lahko napredno iskanje, s katerim spremenite nastavitve iskanja, uporabite dodatne filtre ali filtrirajte seznam zadetkov. Uporabljajte več specifičnih izrazov (ožajte pomen). Uporabite frazno iskanje.

  

**3.4.3** **Omejevanje in razvrščanje zadetkov**

  

Čeprav ste dobro izbrali strategijo iskanja, se lahko vseeno zgodi, da bo seznam zadetkov preobširen.  V tem primeru omogočajo mnogi sistemi možnosti za omejitev in razvrstitev zadetkov.

  

Tukaj vidite možnosti iskanja na EconBiz.

_Vključite sliko1 3.4.3_

  

**Google Učenjak**

  

**V videoposnetku se boste naučili, kako omejiti zadetke s prilagoditvijo raziskovalnega vprašanja. Rezultate iskalnikov lahko omejite tudi pozneje.**

  

**Video, ki se ukvarja z omejitvijo zadetkov.**

  

**3.4.4**  **Naključno iskanje**

  

“Naključnost je dar za iskanje stvari, za katere se ne zavedamo, da jih iščemo.”– Glauco Ortolano

  

Z naključnim iskanjem najdemo zanimive informacije, o katerih nismo razmišljali pri določanju prvotne strategije iskanja. To se lahko zgodi, ko iščemo popolnoma druge informacije. Lahko bi rekli, da se nekdo spotakne ob koristne informacije.

Ker pa je naključnost precej abstraktna stvar, vam priporočamo, da si ogledate videoposnetek, ki prikazuje srečno najdenje novih informacij.

_Videoposnetek na Serendipity Search (konec videa)_

  

  

**Reference:**

Literatura, ki jo vam predlagamo za nadaljnje učenje:

  
  

Association of College and Research Libraries ACRL (2015): Framework for Information Literacy for Higher Education. Dostopno na: http://www.ala.org/acrl/sites/ala.org.acrl/files/content/issues/infolit/Framework\_ILHE.pdf, preverjeno 06.09.2017.

Bent, Moira; Stubbings, Ruth (2011): The SCONUL Seven Pillars of Information Literacy. Core model for higher education. Hg. v. SCONUL. London. Dostopno na: http://www.sconul.ac.uk/sites/default/files/documents/coremodel.pdf, preverjeno 10.09.2017.

Coonan, Emma; Secker, Jane (2011): A New Curriculum for Information Literacy (ANCIL)- Executive Summary. Dostopno na: http://ccfil.pbworks.com/f/Executive\_summary.pdf, preverjeno 10.09.2017.

Jacobson, Trudi; Mackey, Thomas P. (eds.) (2016): Metaliteracy in practice. Chicago: Neal-Schuman, an imprint of the American Library Association.

Lauber-Reymann, Magrit (2017): Informationsressourcen. Ein Handbuch für Bibliothekare und Informationsspezialisten. Berlin: Walter de Gruyter & Co (Bibliotheks- und Informationspraxis, 49).

Sühl-Strohmenger, Wilfried (Hg.) (2012a): Handbuch Informationskompetenz. Berlin: De Gruyter (De Gruyter Handbuch).

  
  

_Include Exit Image InformationLiteratePerson_