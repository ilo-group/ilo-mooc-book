**3.4 Controla tu búsqueda**

**Introducción**

  


**Resultados de aprendizaje:**

Al final de esta lección:

* sabrás cómo relacionar la información encontrada con la estrategia de búsqueda original
* podrás ordenar y limitar los resultados de tu búsqueda utilizando diferentes criterios que se ajusten a tus necesidades para obtener fuentes de información relevantes (fuente, formato, año, métricas de citación, etc.)
* serás consciente del efecto positivo de la serendipia en la búsqueda
* serás consciente de la importancia de gestionar la información encontrada y las herramientas de apoyo
    

**3.4.1 Relacionar las estrategias de búsqueda con el resultado de la búsqueda (1/2)**

  

Diferentes estrategias de búsqueda producen diferentes resultados. Hemos aprendido que 'facebook AND marketing' da resultados completamente diferentes que 'facebook OR marketing'. Pero también cambios mucho más pequeños en la consulta pueden cambiar la lista de resultados, tales como ' "facebook marketing" ' (entrado como una frase) y 'facebook marketing'. En este capítulo nos centraremos en relacionar las estrategias de búsqueda con la información encontrada. Es muy útil pensar en cómo quieres que se vea la lista de resultados y luego crear la consulta de búsqueda.

Observa las siguientes tres consultas. Piensa en los documentos que devolverían.

1. crecimiento economico china

2. econom* china

3. (crecimiento OR economico) china
  

  

Solución:_

1. Esta consulta buscará documentos que contengan los tres términos de búsqueda.
2. Esta consulta buscará cualquier término que comience con "econom" (como economía, economista, etc.) y el término "china".
3. Esta consulta buscará documentos que contengan el término "china" y uno de los otros términos. Por lo tanto, la consulta también podría ser "(china economico) OR (china  crecimiento)". La lista de resultados sería la misma.


**3.4.1 Relacionar las estrategias de búsqueda con el resultado de la búsqueda (2/2)**

También podemos tratar de hacerlo al revés. Aquí tienes cinco títulos de documentos. ¿Qué estrategia de búsqueda es la adecuada? Imagina que el sistema de búsqueda solo busca los términos en el título (los términos en negrita se refieren a los términos de búsqueda).


1. Determinantes comportamentales del apoyo a las políticas de **protección** del medio **ambiente**.

2. Evaluación del impacto de las actividades de **protección** del medio **ambiente** en las regiones rusas.

3. Formación del mecanismo de actividades de **protección ambiental** en el ámbito de la gestión **ambiental** agrícola.

4. Financiación de la actividad de **protección** de la **naturaleza** a nivel regional: principales tendencias y perspectivas de mejora.

5. Turismo, **protección** de la **naturaleza** y responsabilidad.

  
El término "protección" está presente en los cinco títulos. En consecuencia, es uno de los términos de búsqueda. Luego tenemos "medio ambiente" y "ambiental", por lo que podemos asumir que el otro término es "ambient*". El tercer término que encontramos es "naturaleza". En los títulos, se trata de "naturaleza" o de "ambient*" y, por lo tanto, podemos suponer que los dos términos se combinan con un operador OR. Por lo tanto, la consulta que es adecuada para la lista de títulos es: "(ambient* OR naturaleza) AND protección".

  

**3.4.2 Adaptar la búsqueda**

A veces no se obtienen suficientes resultados o no son lo suficientemente buenos. ¡Es importante no frustrarse! En la mayoría de los casos, la forma más fácil de obtener mejores resultados es adaptar la estrategia de búsqueda. A continuación, te mostraremos qué hacer si tienes muy pocos o demasiados resultados.


**1. Tienes muy pocos resultados.**

En primer lugar, debes comprobar si has cometido algún error formal en tu búsqueda. ¿Es correcta la ortografía? ¿Has utilizado los campos de búsqueda y los operadores booleanos correctos? ¿Has utilizado términos de búsqueda relevantes? Si aun así hay pocos resultados, te ofrecemos algunos consejos:

Deberías ampliar tu búsqueda. Si has utilizado opciones de filtro, comprueba si puedes quitarlas. Trata de usar términos más amplios o usa truncamientos al final de los términos. Utiliza términos alternativos (sinónimos) y combínalos con un operador OR. Haz la búsqueda en distintas lenguas. Si pese a todo estos consejos no son suficientes, puedes echar un vistazo a las referencias que se encuentran al final de la lección.


**2. Tienes demasiados resultados.**

¿Has utilizado la conexión correcta entre los términos? El operador AND entre términos ayuda a encontrar menos resultados. Utiliza también el operador NOT. Puedes utilizar la Búsqueda Avanzada para aplicar filtros o puedes filtrar la lista de resultados con posterioridad. Intenta utilizar términos más específicos (términos más acotados) y combínalos con los demás términos de búsqueda. Además, haz uso de la búsqueda por frase exacta.


**3.4.3 Limitar y ordenar los resultados de la búsqueda**

Puede suceder que obtengas demasiados resultados de búsqueda aunque tu estrategia de búsqueda anterior fuera correcta. En este caso, muchos sistemas dan la oportunidad de limitar y ordenar los resultados de la búsqueda.

A continuación, puedes ver las opciones de ordenación de resultados de EconBiz.

_Include Image 3.4.-1_

  
**3.4.4 Búsqueda por serendipia**

  

_“La serendipia es el arte de encontrar cosas que no sabíamos estábamos buscando.” – Glauco Ortolano_

  

La búsqueda por serendipia describe una búsqueda en la que se encuentra información interesante pero que no estaba prevista en la estrategia de búsqueda original. También puede ocurrir a menudo cuando se busca algo que no tiene nada que ver. Se podría decir que alguien "tropieza" con información útil. Debido a que la búsqueda por serendipia es un concepto un poco abstracto para explicarlo aquí, mira este vídeo que muestra un hallazgo afortunado de nueva información:

_Video on Serendipity Search (Video Ende)_

  
**3.4.5 Ejercicio**

**Relating search strategies to the search result**

Diferentes estrategias de búsqueda producen diferentes resultados. Hemos aprendido que 'facebook AND marketing' da resultados completamente diferentes que 'facebook OR marketing'. Pero también cambios mucho más pequeños en la consulta pueden cambiar la lista de resultados, tales como '"facebook marketing"' (entrado como una frase) y 'facebook marketing'. En este capítulo nos centraremos en relacionar las estrategias de búsqueda con la información encontrada. Es muy útil pensar en cómo quieres que se vea la lista de resultados y luego crear la consulta de búsqueda.

Observa las siguientes tres consultas. Piensa en los documentos que devolverían.

1. crecimiento economico china

( ) Esta consulta buscará documentos que contengan información económica, pero no sobre el crecimiento de China.

(x) Esta consulta buscará documentos que contengan los tres términos de búsqueda.

( ) Esta consulta buscará documentos que contengan algunos de los tres términos de búsqueda.

2. econom* china

(x) Esta consulta buscará cualquier término que comience con "econom" (como economía, economista, etc.) y el término "china".

( ) Esta consulta buscará cualquier término que termine con "econom" (como economía, economista, etc.) y el término "china".

( ) Esta consulta buscará cualquier término que no incluya "econom", pero sí el término "china".

3. (crecimiento OR economico) china

( ) Esta consulta buscará documentos que contengan el término "china" y los dos otros términos.

(x) Esta consulta buscará documentos que contengan el término "china" y uno de los otros términos.

( ) Esta consulta buscará documentos que contengan "economico" o "crecimiento", pero no "China".

También podemos tratar de hacerlo al revés. Aquí tienes cinco títulos de documentos. ¿Qué estrategia de búsqueda es la adecuada? Imagina que el sistema de búsqueda solo busca los términos en el título (los términos en negrita se refieren a los términos de búsqueda).

1. Determinantes comportamentales del apoyo a las políticas de **protección** del medio **ambiente**.
2. Evaluación del impacto de las actividades de **protección** del medio **ambiente** en las regiones rusas.
3. Formación del mecanismo de actividades de **protección ambiental** en el ámbito de la gestión **ambiental** agrícola.
4. Financiación de la actividad de **protección** de la **naturaleza** a nivel regional: principales tendencias y perspectivas de mejora.
5. Turismo, **protección** de la **naturaleza** y responsabilidad.

= (ambient* OR naturaleza) AND protección

[explanation]
El término "protección" está presente en los cinco títulos. En consecuencia, es uno de los términos de búsqueda. Luego tenemos "medio ambiente" y "ambiental", por lo que podemos asumir que el otro término es "ambient*". El tercer término que encontramos es "naturaleza". En los títulos, se trata de "naturaleza" o de "ambient*" y, por lo tanto, podemos suponer que los dos términos se combinan con un operador OR. Por lo tanto, nuestra última consulta que es adecuada para la lista de títulos, es: "(ambient* OR naturaleza) AND protección".
[explanation]



**Referencias**

Referencias sugeridas para aprender más:

Association of College and Research Libraries ACRL (2015): Framework for Information Literacy for Higher Education. [http://www.ala.org/acrl/sites/ala.org.acrl/files/content/issues/infolit/Framework_ILHE.pdf] [Consulta: 06 Sep 2017].

Bent, Moira; Stubbings, Ruth (2011): The SCONUL Seven Pillars of Information Literacy. Core model for higher education. London: SCONUL. [http://www.sconul.ac.uk/sites/default/files/documents/coremodel.pdf] [Consulta: 10 Sep 2017].

Coonan, Emma; Secker, Jane (2011): A New Curriculum for Information Literacy (ANCIL)- Executive Summary. [http://ccfil.pbworks.com/f/Executive_summary.pdf] [Consulta: 10 Sep 2017].

Jacobson, Trudi; Mackey, Thomas P. (eds.) (2016): Metaliteracy in practice. Chicago: Neal-Schuman, an imprint of the American Library Association.

Lauber-Reymann, Magrit (2017): Informationsressourcen. Ein Handbuch für Bibliothekare und Informationsspezialisten. Berlin: Walter de Gruyter & Co (Bibliotheks- und Informationspraxis, 49).

Sühl-Strohmenger, Wilfried (ed.) (2012a): Handbuch Informationskompetenz. Berlin: De Gruyter (De Gruyter Handbuch).



_Include Exit Image InformationLiteratePerson_