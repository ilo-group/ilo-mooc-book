_**Lesson 4**_

  

_**3.4. Take control of your search**_

  


**Learning outcome:**

At the end of this lesson, you will:
*   know how to relate the information found to the original search strategy
    
*   be able to sort and limit your search results by using different criteria that fit your need to get relevant information sources (source, format, year, citation metrics etc.)
    
*   be aware of the positive effect of serendipity search
    
*   be aware of importance of managing the information found and support tools -> link to module 4
    

  

_**3.4.1** **Relating search strategies to the search result (1/2)**_

  

Different search strategies deliver different results. We have learned that ‘facebook AND marketing’ gives completely different results than ‘facebook OR marketing’. But also much smaller changes to the query can change the result list, such as ‘ “facebook marketing” ‘ (as a phrase) and ‘facebook marketing’. So, in this chapter, we will focus on relating search strategies to the information found. It is very helpful to think about what you want the result list to look like and then create the search query.

  

Look at the following three queries. Think about what documents they would deliver.

1\. economic growth china

2\. econom* china

3\. (economic OR growth) china

  

  

_Solution:_

1\. This query will look for documents that contain all three search terms.

2\. This query will look for any terms that begin with “econom” (like economics, economical etc) and the term “china”.

3\. This query will look for documents that contain the term “china” and one of the other terms. So, the query could also be “(economic china) OR (growth china)”. The result list would be the same.

  

_**3.4.1** **Relating search strategies to the search result (2/2)**_

We can also try to do it the other way around. Here you have five titles of documents. Which search strategy is suitable? Imagine that the search system doesn’t truncate automatically and that it just looks for the terms in the title. (The bold terms refer to the search terms.)

  

1\. Behavioral Determinants of Proclaimed Support for **Environment Protection** Policies.

2\. Assessment of **environment protection** activity impact in Russian regions.

3\. Formation of the **environmental protection** activities mechanism in the field of agricultural **environmental** management.

4\. Financing Of **Nature Protection** Activity At Regional Level: Main Trends And Improvement Prospects.

5\. Tourism, **Nature Protection** and Responsibility.

  

  

The term “protection” is present in all five titles. Consequently, it is one of the search terms. Then we have “environment” and “environmental”, so we can assume that the other term is “environment*”. The third term we find is “nature”. In the titles, it’s either “nature” or “environment*” and for that reason, we can assume that the two terms are combined with an OR operator. So, our final query that is suitable for the list of titles, is: “(environment* OR nature) AND protection”.

  

**3.4.2**  **Adapting the search**

Sometimes you don’t get sufficient results or they are not good enough. It is important to not get frustrated then! Mostly, the easiest way to get better results is to adapt the search strategy. In the following, I will show you what to do if you have too few or too many results.

  

**1\. You have too few results.**

First of all, you should check if you made any formal mistakes in your search. Is the spelling correct? Did you use the right search fields and Boolean operators? Did you use relevant search terms? If all this applies, here is some advice:

You should broaden your search. If you have used filter options, check if you can loosen those. Try to use broader terms or use truncation at the end of terms. Use alternative terms (synonyms) and combine them with an OR operator. Search multilingually. If this advice hasn’t helped, look at the references of relevant literature you have already found.

  

**2\. You have too many results.**

Did you use the right connection between the terms? The AND operator between terms helps to find less results. Also use the NOT operator. You can use the Advanced Search to use filters or you can use filters on the result list afterwards. Try to use more specific terms (narrower terms) and combine them with your other search terms. Furthermore, make use of the phrase search.

  

_**3.4.3** **Limiting and sorting the search results**_

  

It can happen that you get too many search results, although your search strategy before was sufficient. In this case, many systems give the opportunity to limit and sort the search results.

  

You can see below the sorting options of EconBiz.

_Include Image 3.4.-1_

  

_**Google Scholar**_

  

_**In the following screencast, I will show you how to reduce the search results by adapting the query and limiting the results afterwards.**_

  

_Video on reducing the search results_

  

**3.4.4 Serendipity Search**

  

“_Serendipity is the gift of finding things we did not know we were looking for.” – Glauco Ortolano_

  

Serendipity search describes a search where you find interesting information but finding that information wasn’t intended in the original search strategy. It can also often happen when you look for something entirely unrelated. You could say that someone “stumbles” upon helpful information. Because serendipity search is a bit abstract to explain, watch this video which shows a fortunate finding of new information:

_Video on Serendipity Search (Video Ende)_

  
**3.4.5 Exercise**

**Relating search strategies to the search result**

A. Different search strategies deliver different results. We have learned that ‘facebook AND marketing’ gives completely different results than ‘facebook OR marketing’. But also much smaller changes to the query can change the result list, such as ‘ “facebook marketing” ‘ (as a phrase) and ‘facebook marketing’. So, in this chapter, we will focus on relating search strategies to the information found. It is very helpful to think about what you want the result list to look like and then create the search query. 

Look at the following three queries. Think about what documents they would deliver.
1. economic growth china
2. econom* china
3. (economic OR growth) china

B. We can also try to do it the other way around. Here you have five titles of documents. Which search strategy is suitable? Imagine that the search system doesn’t truncate automatically and that it just looks for the terms in the title. (The bold terms refer to the search terms.)

1. Behavioral Determinants of Proclaimed Support for Environment Protection Policies.
2. Assessment of environment protection activity impact in Russian regions.
3. Formation of the environmental protection activities mechanism in the field of agricultural environmental management.
4. Financing Of Nature Protection Activity At Regional Level: Main Trends And Improvement Prospects.
5. Tourism, Nature Protection and Responsibility.

_Solution_

_A._
_1. This query will look for documents that contain all three search terms._
_2. This query will look for any terms that begin with “econom” (like economics, economical etc) and the term “china”._
_3. This query will look for documents that contain the term “china” and one of the other terms. So, the query could also be “(economic china) OR (growth china)”. The result list would be the same._


_B._
_The term “protection” is present in all five titles. Consequently, it is one of the search terms. Then we have “environment” and “environmental”, so we can assume that the other term is “environment*”. The third term we find is “nature”. In the titles, it’s either “nature” or “environment*” and for that reason, we can assume that the two terms are combined with an OR operator. So, our final query that is suitable for the list of titles, is: “(environment* OR nature) AND protection”._

**References**

 Suggested References for further learning 

  
  

Association of College and Research Libraries ACRL (2015): Framework for Information Literacy for Higher Education. Online available at: http://www.ala.org/acrl/sites/ala.org.acrl/files/content/issues/infolit/Framework_ILHE.pdf, zuletzt geprüft am 06.09.2017.

Bent, Moira; Stubbings, Ruth (2011): The SCONUL Seven Pillars of Information Literacy. Core model for higher education. Hg. v. SCONUL. London. Online available at: http://www.sconul.ac.uk/sites/default/files/documents/coremodel.pdf, zuletzt geprüft am 10.09.2017.

Coonan, Emma; Secker, Jane (2011): A New Curriculum for Information Literacy (ANCIL)- Executive Summary. Online available at: http://ccfil.pbworks.com/f/Executive_summary.pdf, zuletzt geprüft am 10.09.2017.

Jacobson, Trudi; Mackey, Thomas P. (eds.) (2016): Metaliteracy in practice. Chicago: Neal-Schuman, an imprint of the American Library Association.

Lauber-Reymann, Magrit (2017): Informationsressourcen. Ein Handbuch für Bibliothekare und Informationsspezialisten. Berlin: Walter de Gruyter & Co (Bibliotheks- und Informationspraxis, 49).

Sühl-Strohmenger, Wilfried (Hg.) (2012a): Handbuch Informationskompetenz. Berlin: De Gruyter (De Gruyter Handbuch).

  
  

_Include Exit Image InformationLiteratePerson_