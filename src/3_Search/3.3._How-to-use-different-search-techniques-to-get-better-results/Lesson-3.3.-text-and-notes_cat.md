**Lesson 3.3**

**3.3 Com utilitzar diferents tècniques de cerca per obtenir millors resultats?**

**Introducció**

**Resultats d'aprenentatge:**

Al final d'aquesta lliçó, seràs capaç de:


*  triar el recurs d'informació apropiat pel que fa a la necessitat d'informació
*  definir i determinar l'abast de la teva pregunta de recerca de forma clara i en un llenguatge apropiat
*  identificar i aplicar les tècniques de cerca adequades (operadors de cerca, cerca simple i avançada, navegació, tesaures, camps de cerca...)
*  reconèixer característiques de cerca comunes en diferents bases de dades i llocs web
*  ser conscient dels canvis en les característiques i prestacions dels sistemes de cerca al llarg del temps
  

  

**Diferents formes d'utilitzar les tècniques de cerca**

Depenent dels teus resultats, pots especificar més o fer més genèric teu terme de cerca amb diferents tècniques per trobar resultats que satisfacin millor les seves necessitats.

Aprèn més sobre l'ús alternatiu de les tècniques de recerca fent clic a les següents paraules clau:

1. ampliar els termes de cerca
2. restringir els termes de cerca
3. navegació
4. cerca per camps
    

  
  

**3.3.1 Ampliar els termes de cerca**

Ampliar els termes de cerca és una bona opció si no estàs segur del que estàs buscant. Aquesta tècnica de cerca t'ajudarà a trobar més resultats i a obtenir una visió general d'un tema.  
  

**Sigues menys precís amb els teus termes:**

• Utilitza l'operador booleà OR per connectar termes

_Include Image 3.3.1_

Un o tots dos termes apareixeran en els resultats quan s'utilitzi l'operador OR


• Utilitza sinònims

Una paraula té el mateix o gairebé el mateix significat que una altra.

Por ejemplo: Terme = paraula, expressió

  
• Utilitza eines com [http://www.thesaurus.com/](http://www.thesaurus.com/)


• Utilitza comodins (? *! )

L'ús de comodins permet buscar ortografies alternatives i variants d'una mateixa paraula.

**3.3.2 Restringir els termes de cerca**

Restringir els termes de cerca és una bona opció si has obtingut massa resultats amb el teu terme de cerca. Les següents estratègies t'ajudaran a acotar els resultats i els termes de cerca.
  
**Utilitza termes de cerca més específics:**

*   Afegeix termes de cerca amb els operadors booleans AND / NOT
    

_Include Image 3.3-2_

Els resultats de la cerca inclouran els dos termes de cerca quan s'utilitza l'operador AND

_Include Image 3.3-3_

Només es trobaran les pàgines amb el primer terme, però no amb el segon, quan s'utilitzi l'operador NOT

  
  

**3.3.3 Navegació**

La navegació és una tècnica de cerca que et donarà una visió general del material existent. És especialment útil si encara no estàs familiaritzat amb una base de dades i desitges tenir una impressió general del contingut. Pots fer un cop d'ull i seleccionar el material rellevant. Algunes bases de dades ofereixen classificacions que faciliten la navegació. Naturalment, aquesta tècnica també funciona al món físic: pots anar a una biblioteca i explorar els llibres sobre un tema específic.


**3.3.4 Cerca per camps: cerca simple i cerca avançada**

_Include Image 3.3-4_

_(Cerca simple EconBiz)_

La cerca simple només ofereix un camp de cerca i no permet combinar diversos camps. La cerca avançada, en canvi, ofereix més d'un camp de cerca i diverses opcions de filtre.

_Include Image 3.3-5_

_(Advanced Search EconBiz)_

Pots triar diferents camps de cerca (per exemple, Títols, Temes, Autor, ...) i pots decidir com s'han de combinar els camps (AND, OR NOT). A més, pots afegir més grups de cerca si creus que un no és suficient. Les opcions de filtre a EconBiz, per exemple, són Idioma, Format, Fonts i Any de Publicació. També pots configurar els resultats perquè només siguin d'accés obert. Si tens problemes amb la cerca avançada, trobaràs el link d'ajuda just al costat.

**3.3.5 Exercici**

Imagina que has d'escriure un article sobre els beneficis del marketing de mitjans socials per a les empreses. Quines són les principals paraules claus? Crea una llista de paraules que continguin sinònims, termes més generals i més específics. Si és possible, escull un company i compareu les vostres llistes de paraules.

Sinònims de "benefici"

= avantatge

or= rendibilitat

or= utilitat

or= guany

Termes més generals per a "benefici"

= efecte

Termes més específics per a "benefici"

= cap

Sinònims per a "marketing de mitjans socials"

= marketing de xarxes socials

Termes més generals per a "marketing de mitjans socials"

= marketing online

or= marketing a internet

Termes més específics per a "marketing de mitjans socials"

= marketing a Facebook

or= marketing a Twitter