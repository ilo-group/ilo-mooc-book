**Lektion 3.3**

**Wie werden verschiedene Suchtechniken eingesetzt, um bessere Ergebnisse zu erzielen?**

_**Einleitung**_

**Lernergebnisse:**

* In der Lage sein, eine geeignete Informationsquelle für den Informationsbedarf auszuwählen
    
* Klares und deutliches Erfassen und Definieren der Suchanfrage in einer geeigneten Sprache  

* Identifizieren und Anwenden geeigneter Suchtechniken (Suchoperatoren, Stemming, einfache und erweiterte Suche, Browsing, Thesaurus, suchbare Felder...) 
    
* Erkennen bekannter Suchfunktionen in verschiedenen Datenbanken und im Web
    
* Sich rechtzeitig über unterschiedliche Änderungen der Suchfeatures in den Suchsystemen zu informieren
  

  

_**Unterschiedliche Einsatzmöglichkeiten von Suchtechniken**_

Abhängig von den Ergebnissen könnt ihr euren Suchbegriff mit verschiedenen Techniken spezifizieren oder verallgemeinern, um Ergebnisse zu finden, die euren Bedürfnissen eher entsprechen.

Erfahrt mehr über die alternative Nutzung von Suchtechniken, indem ihr euch durch die folgenden Schlüsselwörter klickt:
  

1. Erweitern des Suchbegriffes
2. Einschränken des Suchbegriffes
3. Browsing
4. Feldsuche 

    

  
  

**3.3.1 Erweitern des Suchbegriffes**

Die Erweiterung des Suchbegriffes ist eine gute Option, wenn ihr euch nicht sicher seid, was genau ihr sucht. Diese Suchtechnik hilft euch dabei mehr Ergebnisse zu finden und sich einen Überblick über ein Thema zu verschaffen.  

**Seid weniger präzise mit ihren Bedingungen:**

 
* Verwendet den booleschen Operator OR, um Begriffe zu verbinden.  


_Bild einbinden 3.3.1._

Bei der Verwendung des Operators OR treten entweder ein oder beide Begriffe in den Ergebnissen auf.

- Synonyme verwenden

Ein Wort hat die gleiche oder fast die gleiche Bedeutung wie ein anderes Wort

Z.B...: Begriff = Phrase, Wort
  

- Verwendet Tools wie http://www.thesaurus.com/


- Verwendet Wildcards (? *!)

Die Verwendung von Wildcards ermöglicht die Suche nach alternativen Schreibweisen.

**3.3.2 Einschränken des  Suchbegriffes**

Die Einschränkung eurer Suchbegriffe ist eine gute Option, wenn ihr zu viele Ergebnisse mit eurem aktuellen Suchbegriff erhalten habt. Die folgenden Strategien helfen euch bei der Spezifizierung deiner Ergebnisse und Suchbegriffe. 
  

**Verwendet spezifischere Suchbegriffe:** 

* Fügt Suchbegriffe mit dem booleschen Operator AND/ NOT hinzu.
    

_Bild 3.3-2_ einbinden

Die Suchergebnisse enthalten bei Verwendung des Operators AND beide Suchbegriffe.

_Bild 3.3-3_ einbinden

Es werden nur Seiten mit dem ersten Begriff gefunden, aber nicht mit dem zweiten, wenn Sie den Operator NOT verwenden.

**3.3.3. Browsing**

Browsing ist eine Suchtechnik, die euch einen Überblick über vorhandenes Material gibt. Es ist besonders hilfreich, wenn ihr noch nicht mit einer Datenbank vertraut seid und euch einen Eindruck über den Inhalt verschaffen wollt. Ihr könnt durch sie blättern und das entsprechende Material auswählen. Einige Datenbanken bieten Klassifizierungen, die das Durchsuchen erleichtern. Natürlich funktioniert dies auch offline – Ihr könnt in eine Bibliothek gehen und die Bücher zu einem bestimmten Thema erkunden.

**3.3.4. Feldsuche**

Bild 3.3-4_ einbinden

_(Einfache Suche EconBiz)_

Die einfache Suche gibt nur ein Suchfeld an und es ist nicht möglich, mehrere Felder zu kombinieren. Die erweiterte Suche bietet jedoch mehr als ein Suchfeld und mehrere Filteroptionen.

Bild 3.3-5_ einbinden

_(Erweiterte Suche EconBiz)_

Ihr könnt verschiedene Suchfelder für den Suchbegriff auswählen (z.B. Titel, Themen, Autor,…) und entscheiden, wie die Felder kombiniert werden sollen (AND, OR, NOT). Außerdem kannst du weitere Suchgruppen hinzufügen, wenn ihr der Meinung seid, dass die vorherigen nicht ausreichend sind. Die Filteroptionen in EconBiz sind z.B. Sprache, Format, Quellen und Erscheinungsjahr. Ihr könnt die Ergebnisse auch nur auf Open Access beschränken. Wenn ihr Probleme mit der erweiterten Suche haben solltet, findet ihr den Hilfetext direkt daneben.  

**3.3.5. Übung**

1.	Stellt euch vor, dass ihr eine Semesterarbeit über die Vorteile von Social Media Marketing für Unternehmen schreiben sollt. Welche sind die wichtigsten Keywords? Erstellt eine Wortliste, die Synonyme, allgemeinere und spezifischere Begriffe enthält. Wenn möglich, wähle einen Partner und besprecht eure Wortlisten.
2.	Findet relevante Begriffe aus den untenstehenden Abfragen und modifiziert sie bei Bedarf. Verwendet die Strategien, die ihr im Abschnitt zuvor gelernt hast. Bindet dabei sowohl eine erweiterte als auch eine eingeschränkte Suche ein und führt sie in einem geeigneten System durch. Analysiert anschließend die Ergebnisliste. F1: Was sind die Kompetenzen der Information literacy? F2: Worin besteht der Unterschied zwischen Informationskompetenz und Metakompetenz? 
    F1: Was sind die Kompetenzen der Information literacy? 
    F2: Worin besteht der Unterschied zwischen Informationskompetenz und Metakompetenz?
  
