**3.3 ¿Cómo utilizar diferentes técnicas de búsqueda para obtener mejores resultados?**

**Introducción**

**Resultados de aprendizaje:**

Al final de esta lección, serás capaz de:

* elegir el recurso de información apropiado con respecto a la necesidad de información
* definir y determinar el alcance de tu pregunta de búsqueda de forma clara y en un lenguaje apropiado
* identificar y aplicar las técnicas de búsqueda adecuadas (operadores de búsqueda, búsqueda simple y avanzada, navegación, tesauro, campos de búsqueda...)
* reconocer características de búsqueda comunes en diferentes bases de datos y sitios web
* ser consciente de los cambios en las características y prestaciones de los sistemas de búsqueda a lo largo del tiempo

  

**Diferentes formas de utilizar las técnicas de búsqueda**

Dependiendo de tus resultados, puedes especificar más o hacer más genérico tu término de búsqueda con diferentes técnicas para encontrar resultados que satisfagan mejor tus necesidades.

Aprende más sobre el uso alternativo de las técnicas de búsqueda haciendo clic en las siguientes palabras clave:

1. ampliar los términos de búsqueda
2. restringir los términos de búsqueda
3. navegación
4. búsqueda por campos
    

**3.3.1 Ampliar los términos de búsqueda**

Ampliar los términos de búsqueda es una buena opción si no estás seguro de lo que estás buscando. Esta técnica de búsqueda te ayudará a encontrar más resultados y a obtener una visión general de un tema

  

**Sé menos preciso con tus términos:**

• Usa el operador booleano OR para conectar términos

_Include Image 3.3.1_

Uno o ambos términos aparecerán en los resultados cuando se utilice el operador OR

• Usa sinónimos

Una palabra tiene el mismo o casi el mismo significado que otra.

Por ejemplo: Término = palabra, expresión
  

• Usa herramientas como [http://www.thesaurus.com/]


• Usa comodines (? *! )

El uso de comodines permite buscar ortografías alternativas y variantes de una misma palabra.

**3.3.2 Restringir los términos de búsqueda**

Restringir los términos de búsqueda es una buena opción si has obtenido demasiados resultados con tu término de búsqueda. Las siguientes estrategias le ayudarán a acotar tus resultados y términos de búsqueda.

**Usa términos de búsqueda más específicos:**

*   Añade términos de búsqueda con los operadores booleanos AND / NOT
    

_Include Image 3.3-2_

Los resultados de la búsqueda incluirán los dos términos de búsqueda cuando se utilice el operador AND.

_Include Image 3.3-3_

Solo se encontrarán las páginas con el primer término, pero no con el segundo, cuando se utilice el operador NOT

  
  

**3.3.3 Navegación**

La navegación es una técnica de búsqueda que te dará una visión general del material existente. Es especialmente útil si aún no estás familiarizado con una base de datos y deseas tener una impresión general del contenido. Puedes echar un vistazo y seleccionar el material relevante. Algunas bases de datos ofrecen clasificaciones que facilitan la navegación. Naturalmente, esta técnica también funciona en el mundo físico: puedes ir a una biblioteca y explorar los libros sobre un tema específico.


**3.3.4 Búsqueda por campos: búsqueda simple y búsqueda avanzada**

_Include Image 3.3-4_

_(Búsqueda simple EconBiz)_

La búsqueda simple solo ofrece un campo de búsqueda y no permite combinar varios campos. La búsqueda avanzada, en cambio, ofrece más de un campo de búsqueda y varias opciones de filtro.

_Include Image 3.3-5_

_(Advanced Search EconBiz)_

Puedes elegir diferentes campos de búsqueda (por ejemplo, Títulos, Temas, Autor,...) y puedes decidir cómo deben combinarse los campos (AND, OR, NOT). Además, puedes añadir más grupos de búsqueda si crees que uno no es suficiente. Las opciones de filtro en EconBiz, por ejemplo, son Idioma, Formato, Fuentes y Año de Publicación. También puedes configurar los resultados para que solo sean de acceso abierto. Si tienes problemas con la búsqueda avanzada, encontrarás el enlace de ayuda justo al lado.

**3.3.5 Ejercicio**

Imagina que tienes que escribir un artículo sobre los beneficios del marketing de medios sociales para las empresas. ¿Cuáles son las principales palabras clave? Crea una lista de palabras que contenga sinónimos, términos más generales y más específicos. Si es posible, elige un compañero y comparad vuestras listas de palabras.

Sinónimos de "beneficio"

= ventaja

or= rentabilidad

or= utilidad

or= ganancia

Términos más generales para "beneficio"

= efecto

Términos más específicos para "beneficio"

= ninguno

Sinónimos para "marketing de medios sociales"

= marketing de redes sociales

Términos más generales para "marketing de medios sociales"

= marketing online

or= marketing en internet


Términos más específicos para "marketing de medios sociales"

= marketing en Facebook

or= marketing en Twitter

Busca los términos relevantes de las consultas que se muestran a continuación y modélalos si es necesario. Utiliza las estrategias que has aprendido en la sección anterior. Haz ambas cosas, una búsqueda ampliada y restringida y ejecútala a través de un sistema. A continuación, analiza la lista de resultados.

P1: ¿Cuáles son las competencias de la alfabetización informacional?
P2: ¿Cuál es la diferencia entre la alfabetización informacional y la meta-alfabetización?