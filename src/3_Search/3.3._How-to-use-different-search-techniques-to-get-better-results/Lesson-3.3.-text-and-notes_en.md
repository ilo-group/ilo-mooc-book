**Lesson 3.3**

**How to use different search techniques to get better results?**

_**Introduction**_

**Learning outcome:**

* to be able to choose the most appropriate information resource regarding your information need
    
* to scope/define a search question clearly and with appropriate search words
    
* to identify and apply appropriate search techniques (search operators, stemming, simple and advance search, browsing, thesaurus, searchable fields…)
    
* to be able to recognize common search features across different systems, databases and web
    
* to be aware of changes of features in search systems over time
    

  

  

_**Different ways of using search techniques**_

Depending on your results you can specify or generalize your search term with different techniques for finding results which will better satisfy your need.

  
Learn more about the alternative usage of search techniques by clicking through the following keywords

  

1. to broaden your search terms
    
2.  to limit your search terms
3.  browsing
4.  field search
    

  
  

**3.3.1 To broaden your search terms**

Broadening your search terms is a good option if you are not sure about exactly what you are looking for. This search technique helps you with finding more results and getting an overview of a topic.

  
  

**Be less precise with your terms:**

  
  

• use the Boolean operator OR to connect terms

_Include Image 3.3.1_

Either one or both terms will occur in the results when using the operator OR

  
  

• Use synonyms

A word has the same or nearly the same meaning as another.

E.g.: Term = phrase, word

  
  

• Use tools like http://www.thesaurus.com/

  
  

• Use wildcards (? *! )

Using wildcards allows searching for alternative spellings.

**3.3.2** **To limit your search terms**

Limiting your search terms is a good option if you have received too many results with your current search term. The following strategies will help you with specifying your results and search terms.

  
  

Use more specific search terms:

*   Add search terms with the Boolean operator AND/ NOT
    

_Include Image 3.3-2_

 The search results will include both of the search terms when using the operator AND

_Include Image 3.3-3_

 Only pages with the first term will be found but not with the second when using the operator NOT

  
  

**3.3.3 Browsing**

Browsing is a search technique which will give you an overview of existing material. It is especially helpful if you are not familiar with a database yet and want to have an impression of the content. You can skim through it and select the relevant material. Some databases offer classifications that make it easier to browse. Naturally, it works offline as well – you can go into a library and explore the books on a specific topic.

  

**3.3.4 Field Search: Simple and Advanced Search**

_Include Image 3.3-4_

_(Simple Search EconBiz)_

The Simple Search just gives one search field and it doesn’t allow you to combine several fields. The Advanced Search, however, offers more than one search field and several filter options.

_Include Image 3.3-5_

_(Advanced Search EconBiz)_

You can choose different search fields for the search string (e.g. Titles, Subjects, Author, …) and you can decide how the fields should be combined (AND, OR, NOT). Also, you can add more search groups if you feel that one is not enough. The filter options in EconBiz, for example, are Language, Format, Sources and Year of Publication. You can also set the results to open access only. If you have problems with the advanced search, you will find the help text right next to it.

**3.3.5 Exercise**

1.	Imagine your task is to write a term paper about the benefits of social media marketing for companies. Which are the main keywords? Create a word list which contains synonyms, broader and narrower terms. If possible, choose a partner and discuss your word lists.
2.	Find relevant terms of the queries shown below and moderate them if necessary. Use the strategies you have learnt in the section before. Do both, a broadened and limited search and run it through a system. Afterwards analyze the result list. 
   
    Q1: What are the competencies of information literacy? 
    Q2: What is the difference between information literacy and meta literacy?

