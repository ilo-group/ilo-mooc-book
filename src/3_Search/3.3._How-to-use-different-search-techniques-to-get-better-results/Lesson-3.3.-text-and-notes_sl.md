              @page { size: 21cm 29.7cm; margin-left: 2.5cm; margin-right: 2.5cm; margin-top: 2.5cm; margin-bottom: 2cm } p { margin-bottom: 0.25cm; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent } a:link { color: #0563c1; text-decoration: underline } 

**Lekcija 3.3**

**Kako uporabljati različne tehnike iskanja za zagotovitev boljših zadetkov?**

**Uvod**

**Učni cilji:**

Na koncu te lekcije boste razvili sposobnosti za:

*   izbiro primernih informacijskih virov, namenjenih določenim informacijskim potrebam
    
*   jasno in primerno opredelitev raziskovalnega področja in postavljanje raziskovalnega vprašanja
    
*   prepoznavanje in uporabo primernih tehnik iskanja (logični operatorji, krajšanje, enostavno in napredno iskanje, brskanje, tezaver, iskalna polja …)
    
*   prepoznavanje skupnih značilnosti različnih podatkovnih zbirk in spletnih iskalnikov
    
*   prepoznavanje sprememb značilnosti sistemov za poizvedovanje skozi čas
    

  

  

**Uporaba različnih tehnik iskanja**

Glede na rezultate iskanja, lahko z različnimi tehnikami iskanja natančneje določimo, ali posplošimo poizvedbo. S tem bodo nadaljnji zadetki bolj ustrezali naši informacijski potrebi.

  

S klikom na naslednje ključne besede boste izvedeli več o uporabi tehnik iskanja.

  

1.  Razširitev iskalnih izrazov
    
2.  Omejitev iskalnih izrazov
    

1.  Brskanje
    
2.  Iskalna polja
    

  
  

**3.3.1 Razširitev iskalnih izrazov**

Razširitev iskalnih izrazov izberemo, če nam ni popolnoma jasno, kaj iščemo. Ta tehnika iskanja vam pomaga pridobiti veliko informacij in vam omogoča pregled nad vsebino raziskovalnega področja.

**Za manjšo natančnost pri izbiri izrazov:**

• Uporabite Boolov operator ALI, da povežeteiskalne izraze.

_Vključite sliko 3.3.1_

Z logičnim operatorjem ALI se bodo pojavili zadetki, ki vsebujejo vsaj enega izmed izrazov oziroma oba.

• Uporaba sopomenk

Sopomenka je beseda, ki ima enak ali skoraj enak pomen kot druga beseda.

Na primer: Pojem = izraz, beseda

• Uporaba orodij kot je [http://www.thesaurus.com/](http://www.thesaurus.com/) ali Splošni geslovnik COBISS (https://plus.cobiss.si/opac7/sgc)

• Uporaba nadomestnih znakov (? \*! )

Nadomestni znaki vam omogočajo iskanje različnih pravopisnih oblik nekega izraza.

**3.3.2** **Omejitev iskalnih izrazov**

  
  

Omejitev iskalnih izrazov izberemo, če smo dobili preveč zadetkov. Naslednje tehnike iskanja vam bodo pomagale pri določanju iskalnega izraza.

  
  

Uporaba posebnih izrazov raziskovalnega področja:

*   Uporaba Boolovih operatorjev IN/NE
    

_Vključite sliko 3.3-2_

-> Z logičnim operatorjem IN se bodo pojavili le zadetki, ki vsebujejo oba izraza.

_Vključite sliko 3.3-3_

-> Z logičnim operatorjem NE se bodo pojavili zadetki, ki vsebujejo samo prvi izraz, ne pa drugega.

  
  

**3.3.3 Brskanje**

Brskanje je tehnika iskanja, ki vam omogoča pregled nad obstoječim gradivom določenega področja. Ta tehnika iskanja je priporočljiva, če podatkovne zbirke še ne poznate dobro in želite pridobiti pregled o njeni vsebini. Z brskanjem pregledate in izberete ustrezno gradivo. Nekatere podatkovne zbirke so porazdeljene v različne kategorije, ki nam pomagajo pri brskanju. Za to tehniko seveda ni potrebno, da ste na spletu – lahko greste v knjižnico in prebrskate police in knjige, ki obravnavajo določeno temo.

  

**3.3.4\. Iskalna polja: enostavno in napredno iskanje**

_Vključite sliko 3.3-4_

_(Simple Search EconBiz)_

Pri enostavnem iskanju lahko vključimo v postopek iskanja informacij le eno iskalno polje. Kombinacija različnih iskalnih polj ni možna. Napredno iskanje pa omogoča iskanje informacij s kombinacijo različnih iskalnih polj. Prav tako lahko spremenite nastavitve iskanja in določite možnosti iskanja.

_Vključite sliko 3.3-5_

_(Advanced Search EconBiz)_

Izberete lahko različna iskalna polja in postavite s tem dodatne pogoje (na primer naslov, vsebinske oznake, avtor,…). Poleg tega lahko z logičnimi operatorji (IN, ALI, NE) določite, kako naj seiskalna polja povezujejo. Prav tako lahko dodate več iskalnih polj, če se vam zdi, da eno ne zadostuje.

Možnosti iskanja na EconBiz so na primer naslednje: jezik, oblika, viri in leto objave. Nastavitve iskanja lahko spremenite tako, da so vključeni le zadetki, ki so odprto dostopni (torej v celoti brez omejitev). Če imate težave z naprednim iskanjem, si lahko preberete navodila za iskanje (pomoč).


**3.3.5. Vaja**
1.	Predstavljajte si, da morate napisati seminarsko nalogo na temo prednosti trženja na Facebooku za podjetja. Katere so ključne besede? Zapišite sopomenke ter širše in ožje pojme. Po možnosti pogovorite se s svojimi kolegi.
2.	Izberite relevantne izraze iskalnih nizov in jih po potrebi preoblikujete. Strategijo, ki ste jo prej spoznali, lahko uporabljate – upoštevajte tako enostavno kakor tudi napredno iskanje.  Potem preverite zadetke. 

F1: Katere kompetence pripišemo informacijski pismenosti? 

F2: V čem se razlikuje informacijska pismenost od meta pismenosti?
